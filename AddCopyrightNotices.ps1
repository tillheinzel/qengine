param($target = ".")

#[System.Globalization.CultureInfo] $ci = [System.Globalization.CultureInfo]::GetCultureInfo("pt-BR")

[System.Globalization.CultureInfo] $ci = [System.Globalization.CultureInfo]::GetCurrentCulture

# Full date pattern with a given CultureInfo
# Look here for available String date patterns: http://www.csharp-examples.net/string-format-datetime/
$currentYear = (Get-Date).year.ToString();

# Header template
$header = "/* COPYRIGHT
 *
 * file=""{0}"" 
 * Copyright (c) 2017 - {1} Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */"

function Write-Header ($file)
{
    # Get the file content as as Array object that contains the file lines
    $content = Get-Content $file
    
    # Getting the content as a String
    $contentAsString =  $content | Out-String
    
    if($contentAsString.StartsWith("/* COPYRIGHT"))
    {
		$content = $content | Select-Object -skip 1
		while(1)
		{
			if($content.count -eq 0) {break}
			if($content[0] -isnot [System.String] ){break;}
			if(-Not $content[0].StartsWith(" *")){break;}
			
			$content = $content | Select-Object -skip 1
		}
    }

    # Splitting the file path and getting the leaf/last part, that is, the file name
    $filename = Split-Path -Leaf $file

    # $fileheader is assigned the value of $header with dynamic values passed as parameters after -f
    $fileheader = $header -f $filename, $currentYear

    # Writing the header to the file
    Set-Content $file $fileheader -encoding UTF8

    # Append the content to the file
    Add-Content $file $content
}

#Filter files getting only .h ones and exclude specific file extensions
Get-ChildItem $target -Filter *.h -Exclude pch.h -Recurse | % `
{
    <# For each file on the $target directory that matches the filter,
       let's call the Write-Header function defined above passing the file as parameter #>
    Write-Header $_.PSPath.Split(":", 3)[2]
}

Get-ChildItem $target -Filter *.cpp -Exclude pch.cpp -Recurse | % `
{
    <# For each file on the $target directory that matches the filter,
       let's call the Write-Header function defined above passing the file as parameter #>
    Write-Header $_.PSPath.Split(":", 3)[2]
}