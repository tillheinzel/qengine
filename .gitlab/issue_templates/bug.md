#### Summary


#### What did you do?
*What lead to you getting the bug*

#### Can you reproduce it?
*Did it just happen once, or does it happen consistently? What do you do to make it happen?*

#### What did you expect to happen?
*Description and any output*

#### What did happen?


#### Ideas for fixes?
*If you have an idea to fix this, write it here. If not, don't worry about it*

