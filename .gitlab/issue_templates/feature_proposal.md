#### Summary

#### What do you propose specifically?


#### What are you trying to achieve? 
*Your specific use case*

#### Can you think of potential other use cases?

#### Which Workaround(s), if any, are you currently using to achieve this?

#### Can you think of alternative ways of how to achieve the same thing?
