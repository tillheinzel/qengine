cmake_minimum_required (VERSION 3.3)
project (QEngine)

set(QENGINE_VERSION "1.0f")

####################################
########### OUTPUT PATHS ###########
####################################

# load settings-file. Every setting has default values, which may be sufficient in many cases.
include(settings.txt OPTIONAL)

if(${PLACE_BINARIES_IN_BUILD_FOLDER})
	message(STATUS "binaries are placed in build-folder")
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
else()
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

####################################
########## GLOBAL DEFINES ##########
####################################


set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_definitions(-DARMA_DONT_PRINT_CXX11_WARNING)
add_definitions("-DARMA_USE_CXX11")

if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	set(GCC true)
else()
	set(GCC false)
endif()

if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	set(CLANG true)
else()
	set(CLANG false)
endif()

if(MSVC)
	add_definitions("/MP")
	add_definitions("/Za")
	add_definitions("/W4")
	add_definitions("/WX")
	# add_definitions("/bigobj")
	set_property(GLOBAL PROPERTY USE_FOLDERS ON)
	set_property(GLOBAL PROPERTY PREDEFINED_TARGETS_FOLDER cmake)
elseif(GCC)
	add_definitions("-Wall -Wno-long-long -Werror")
elseif(CLANG)
	# add_definitions("-Weverything")
	# add_definitions("-Wno-c++98-compat" "-Wno-c++98-compat-pedantic" "-Wno-documentation-unknown-command" "-W")
endif()


# For making things work with vscode (I think?)
include(CMakeToolsHelpers OPTIONAL)

######### submodules #########

# versioning
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmakeModules/")
include(GetGitRevisionDescription)
get_git_head_revision(GIT_REFSPEC GIT_SHA1)
configure_file("include/src/version.cpp.in" "${CMAKE_CURRENT_BINARY_DIR}/version.cpp" @ONLY)

# precompiled headers
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmakeModules/AddPCH/")
include(cotire)
include(AddPCH)

####################################################
##################### SETTINGS #####################
####################################################


## defaults for boolean settings ##
if(NOT DEFINED CREATE_TESTS)
	set(CREATE_TESTS true)
endif()
if(NOT DEFINED CREATE_EXAMPLE_PROJECTS)
	set(CREATE_EXAMPLE_PROJECTS true)
endif()
if(NOT DEFINED ENABLE_MAT_IO)
	set(ENABLE_MAT_IO false)
endif()
if(NOT DEFINED DISABLE_DEBUG_CHECKS)
    set(DISABLE_DEBUG_CHECKS false)
endif()

# not yet a setting, as MKL is required, but probably at some point we can also choose to use blas/lapack or something.
set(USE_MKL true)

# add macro to c++ to enable or disable debug checks based on settings
if(DISABLE_DEBUG_CHECKS)
	add_definitions(-DARMA_NO_DEBUG)
	add_definitions(-DQENGINE_DISABLE_DEBUG_CHECKS)
else() #debug checks enabled
endif()

## Defaults for directories ##

# Armadillo linear algebra library. Header-only, so no lib-dir needed. Default is having it as a subrepo, ensuring a compatible version
if(NOT DEFINED ARMADILLO_INCLUDE_DIR)
	set(ARMADILLO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/dependencies/armadillo/include")
endif()

# Json library. Header-only, so no lib-dir needed. Default is having it as a subrepo, ensuring a compatible version
if(NOT DEFINED JSON_INCLUDE_DIR)
	set(JSON_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/dependencies/json/include")
endif()

# some string-editing to find standard mkl dir automagically on windows
if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
	set(programFilesDir $ENV{ProgramFiles\(x86\)})
	string(REPLACE "\\" "/" programFilesDir ${programFilesDir})
	if(NOT IS_DIRECTORY ${programFilesDir}) #special case for 32 bit windows
		set(programFilesDir $ENV{ProgramFiles})
		string(REPLACE "\\" "/" programFilesDir ${programFilesDir})
	endif()
endif()

# Intel MKL for basic linear algebra (BLAS & LAPack) and fourier-transforms. has to be installed and is typically found in a platform-specific default position
if(USE_MKL)
	if(NOT DEFINED MKL_INCLUDE_DIR)
		if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
			set(MKL_INCLUDE_DIR ${programFilesDir}/IntelSWTools/compilers_and_libraries/windows/mkl/include) 
		elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
			set(MKL_INCLUDE_DIR /opt/intel/compilers_and_libraries/linux/mkl/include) 
		elseif(APPLE)
			set(MKL_INCLUDE_DIR /opt/intel/compilers_and_libraries/mac/mkl/include) 
		endif()
	endif()
	if(NOT DEFINED MKL_LIB_DIR)
		if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
			set(MKL_LIB_DIR ${programFilesDir}/IntelSWTools/compilers_and_libraries/windows/mkl/lib/intel64_win) 
		elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
			set(MKL_LIB_DIR /opt/intel/compilers_and_libraries/linux/mkl/lib/intel64_lin)  
		elseif(APPLE)
			set(MKL_LIB_DIR /opt/intel/compilers_and_libraries/mac/mkl/lib)  
		endif()
	endif()
	
endif()

## Check validity of include and library directories ##

if(USE_MKL)
	if(NOT IS_DIRECTORY ${MKL_INCLUDE_DIR})
		MESSAGE(WARNING "MKL include directory incorrect or not an absolute path: ${MKL_INCLUDE_DIR}")
	endif()
	
	if(NOT IS_DIRECTORY ${MKL_LIB_DIR})
		MESSAGE(WARNING "MKL library directory incorrect or not an absolute path: ${MKL_LIB_DIR}")
	endif()

	link_directories("${MKL_LIB_DIR}")
endif()

# Library to enable read/write to/from matlab data files (.mat). Requires a lot of extra dependencies that typically have to be built from source. (requires libraries matio, hdf5 and zlib, although zlib comes with hdf5)

if(ENABLE_MAT_IO)
	if(NOT IS_DIRECTORY ${MATIO_INCLUDE_DIR})
		MESSAGE(WARNING "matio include directory incorrect or not an absolute path")
	endif()
	if(NOT IS_DIRECTORY ${MATIO_LIB_DIR})
		MESSAGE(WARNING "matio library directory incorrect or not an absolute path")
	endif()
	
	link_directories("${MATIO_LIB_DIR}")
endif()

###################################################
##################### LIBRARY #####################
###################################################

add_subdirectory(include/src) # the actual qengine library

####################################################
##################### EXAMPLES #####################
####################################################

if(CREATE_EXAMPLE_PROJECTS)
	if(GCC)
		remove_definitions("-Werror")
	endif()

	add_subdirectory(example_projects)
	
	if(GCC)
		add_definitions("-Werror")
	endif()
endif()

#################################################
##################### TESTS #####################
#################################################

if(CREATE_TESTS)
    if(MSVC)
        set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
        remove_definitions("/Za")
    endif()

	add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/dependencies/googletest)
	
	set_target_properties(gtest gtest_main PROPERTIES INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "")
	target_include_directories(gtest      INTERFACE "${gtest_SOURCE_DIR}/include")
	target_include_directories(gtest_main INTERFACE "${gtest_SOURCE_DIR}/include")
	set_target_properties(gtest gtest_main PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
	set_target_properties(gtest gtest_main PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
	
    if(MSVC)
        add_definitions("/Za")
		
		set_target_properties(gtest PROPERTIES FOLDER dependencies)
		set_target_properties(gtest_main PROPERTIES FOLDER dependencies)
		
    endif()

	include_directories(${CMAKE_CURRENT_SOURCE_DIR}/test)
	add_subdirectory(test)
endif()




################################################
##################### MISC #####################
################################################

if(MSVC)

	get_property(PREDEF_TARGET_FOLDER GLOBAL PROPERTY PREDEFINED_TARGETS_FOLDER)
	message(STATUS ${PREDEF_TARGET_FOLDER})

    set_target_properties(clean_cotire PROPERTIES FOLDER cmake)
endif()
