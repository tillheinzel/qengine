%%%%%
%% THIS FILE IS JUST A HANDY WAY OF TEMPORARILY CHECKING OUTOUT
%%%%% 

%%
clc
hold off
clear all
 
load('twoparticle-example.mat')

density = @(i) reshape(conj(psis_unopt(:,i)).*psis_unopt(:,i),length(x),length(x)); 
wave_plt = imagesc(x,x,density(1));

colorbar
set(gca,'YDir','normal')

dx = x(2)-x(1);
sum(sum(density(6)))*dx^2;
%target_plt = imagesc(x/(2*pi)+0.25,x/(2*pi)+0.25,abs(reshape(psi_target,length(x),length(x)))); axis equal


for i=1:length(t)
    set(wave_plt,'cdata',density(i))
    sum(sum(density(i)))*dx^2

    pause(0.001)
end
return

%% Target plot
load('twoparticle-example.mat')

target_plt = imagesc(x,x,abs(reshape(psi_t,length(x),length(x)))); axis equal

set(gca,'YDir','normal')

sum(sum(abs(psi_t).^2))*(x(2)-x(1))^2;


%% Plot no interaction
load('twoparticle-example.mat')
Psi = 1/sqrt(2)*(kron(psiL,psiR) + kron(psiR,psiL));

sum(sum(abs(psiL).^2))*(x(2)-x(1))

imagesc(x,x,reshape(abs(Psi).^2,length(x),length(x))-density(1))
colorbar

set(gca,'YDir','normal')
sum(imag(Psi))
trapz(conj(Psi).*psi(:,1))*(x(2)-x(1))^2

%% Plot of 4 lowest states at t=0
load('twoparticle-example.mat')

subplot(2,2,1)
imagesc(x,x,reshape(real(phis_0(:,1)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,2)
imagesc(x,x,reshape(real(phis_0(:,2)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,3)
imagesc(x,x,reshape(real(phis_0(:,3)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,4)
imagesc(x,x,reshape(real(phis_0(:,4)),length(x),length(x)))
set(gca,'YDir','normal')

%% Plot of 4 lowest states at t=T
load('twoparticle-example.mat')

subplot(2,2,1)
imagesc(x,x,reshape(real(phis_t(:,1)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,2)
imagesc(x,x,reshape(real(phis_t(:,2)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,3)
imagesc(x,x,reshape(real(phis_t(:,3)),length(x),length(x)))
set(gca,'YDir','normal')

subplot(2,2,4)
imagesc(x,x,reshape(real(phis_t(:,4)),length(x),length(x)))
set(gca,'YDir','normal')



%% Potentials

load('twoparticle-example.mat')

figure
V_plt1 = plot(x,V_unopt(:,1));
hold on
%V_plt2 = plot(x,V_grape(:,1));

for i=1:length(t)
    set(V_plt1,'ydata',V_unopt(:,i))
    %set(V_plt2,'ydata',V_grape(:,i))

    pause(0.01)
end


%% optimized
load('twoparticle-example.mat')

hold off
density = @(i) reshape(conj(psis_grape(:,i)).*psis_grape(:,i),length(x),length(x)); 
wave_plt = imagesc(x,x,density(1));

colorbar
set(gca,'YDir','normal')

dx = x(2)-x(1);
sum(sum(density(6)))*dx^2;
%target_plt = imagesc(x/(2*pi)+0.25,x/(2*pi)+0.25,abs(reshape(psi_target,length(x),length(x)))); axis equal


for i=1:length(t)
    set(wave_plt,'cdata',density(i))
    sum(sum(density(i)))*dx^2

    pause(0.001)
end


