%% Guess
clc
hold off
clear all
 
load('twolevel-example.mat')

fidelity
[X,Y,Z] = sphere(20);

bloch = surf(X,Y,Z);
hold on;view(0,40)
set(bloch ,'FaceColor',[1 1 1],'FaceAlpha',0.1);
xlabel('x')
ylabel('y')
zlabel('z')

p1 = plot3(R(1,1),R(2,1),R(3,1),'linewidth',2);
p2 = plot3(R_opt(1,1),R_opt(2,1),R_opt(3,1),'linewidth',2);

ascale = 1.1;
a1 = quiver3(0,0,0,ascale*R(1,1),ascale*R(2,1),ascale*R(3,1),'linewidth',2,'color','b');
a2 = quiver3(0,0,0,ascale*R_opt(1,1),ascale*R_opt(2,1),ascale*R_opt(3,1),'linewidth',2,'color','r');
a3 = quiver3(0,0,0,ascale*R_target(1,1),ascale*R_target(2,1),ascale*R_target(3,1),'linewidth',2,'color','g');

legend('Bloch sphere','initial trajectory','optimized trajectory')

animate = 1;
if animate 
    for n = 1:4:size(psi,2)
    set(p1,'xdata',R(1,1:n),'ydata',R(2,1:n),'zdata',R(3,1:n)) 
    set(a1,'udata',ascale*R(1,n),'vdata',ascale*R(2,n),'wdata',ascale*R(3,n))  
    
    set(p2,'xdata',R_opt(1,1:n),'ydata',R_opt(2,1:n),'zdata',R_opt(3,1:n))
    set(a2,'udata',ascale*R_opt(1,n),'vdata',ascale*R_opt(2,n),'wdata',ascale*R_opt(3,n))
    
    pause(0.001)
    end
else 
    set(p1,'xdata',R(1,:),'ydata',R(2,:),'zdata',R(3,:))
    set(p2,'xdata',R_opt(1,:),'ydata',R_opt(2,:),'zdata',R_opt(3,:))
end


return
%% Controls

load('twolevel-example.mat')
hold off

plot(t,u); hold on
plot(t,u_opt)




