cmake_minimum_required (VERSION 3.3)
project (Example_programs)

set(targetName twolevel-example)

# for some reason, doing this with target_compile_definitions does not work
if(MSVC)
	add_compile_options(/wd4244)
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	add_compile_options(-Wno-sign-compare)
endif()

add_executable(${targetName} twolevel-example.cpp)
target_include_directories(${targetName} PRIVATE "${ARMADILLO_INCLUDE_DIR}")
	
target_link_libraries(${targetName} qengine)


if(USE_MKL)
        target_link_libraries(${targetName} mkl_intel_lp64 mkl_sequential mkl_core)
endif()
if(ENABLE_MAT_IO)
	target_link_libraries(${targetName} matio)
endif()
if(MSVC)
        set_target_properties(${targetName} PROPERTIES FOLDER examples)
endif()
