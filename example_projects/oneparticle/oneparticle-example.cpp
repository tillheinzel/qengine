﻿/* COPYRIGHT
 *
 * file="oneparticle-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine/qengine.h>

using namespace qengine;

int main()
{
    std::cout << "Oneparticle Example Program" << std::endl << "---------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    // Control
    const auto dt = 0.01;
    const auto duration = 3.0;
    const auto n_steps = floor(duration / dt); // this is n_steps, not n_points. n_points = n_steps + 1

    const auto x0_dynamic_initial = -1.0;
    const auto tunnelPoint = 0.3;

    const auto forwardTransportTime = floor(0.1*duration / dt);
    const auto tunnelTime = floor(0.3*duration / dt);

    const auto u = makePieceWiseLinearControl({
        {0, x0_dynamic_initial},
        {forwardTransportTime, tunnelPoint} ,
        {tunnelTime + forwardTransportTime, tunnelPoint},
        {n_steps, x0_dynamic_initial}
        }, dt);

    // Hilbert space
    const auto kinFactor = 0.5;
    const auto s = one_particle::makeHilbertSpace(-5, 5, 256, kinFactor);

    // Tweezer potentials
    const auto x = s.x();
    const auto amplitude = 40.0;
    const auto width = 0.25;

    const auto x0_static = 1.0;
    const auto V_static = -amplitude * exp(-pow(x - x0_static, 2) / (2 * width*width)) + amplitude; // static tweezer

    const auto V_dynamic = makePotentialFunction([&x, amplitude, width](const real x0_dynamic) // dynamic tweezer
    {
		const auto x_disp = x - x0_dynamic;
        return -amplitude * exp(-x_disp*x_disp / (2 * width*width)) + amplitude;
    }, u.getFront().front());

    // Kinetic energy
    const auto T = s.T();

    // Hamiltonian
    const auto H = T + V_static + V_dynamic;

    // States
    const auto H_static = s.T() + V_static;
    const auto psi_0 = makeWavefunction(H_static[0]);    // ground state in static tweezer at t=0

    const auto H_dynamic = s.T() + V_dynamic(u.getBack());
    const auto psi_t = makeWavefunction(H_dynamic[0]); // groundstate in dynamic tweezer at t=T

    // Solver
    auto solver = makeFixedTimeStepper(H, psi_0, dt);
    auto t = 0.0;
	
    // Datacontainer and initial values
    DataContainer dc;
    dc["x"] =  x.vec();
    dc["u"] =  u.mat();
    dc["t"].append( t);
    dc["psi"].append( solver.state().vec());
    dc["V"].append( (V_static+V_dynamic(u.getFront())).vec());

    // Propagation over control
    for(auto i = 0; i < n_steps; i++)
    {

        dc["t"].append(t);
        dc["psi"].append(solver.state().vec());
        dc["V"].append((V_static + V_dynamic(u.get(i))).vec());

        t += dt;
        if(i < n_steps-1) solver.step(u.get(i+1));

    }

    /// Optimal Control
	
    bool optimize = true;
    if(optimize)
    {
//        The commented lines below allows the user to provide an analytic expression for the derivative dHdu to the program

//        auto dVdu = makeAnalyticDiffPotential
//                (
//                    [&x, amplitude,width](const RVec& p)
//        {
//            auto x0_dynamic = p.at(0);
//            return -amplitude/pow(width,2) * exp(-pow(x-x0_dynamic,2)/(2*width*width))*(x-x0_dynamic);
//        }
//        );
//        const auto problem = makeStateTransferProblem(H,dVdu,psi_init,psi_target,u);

        const auto problem = makeStateTransferProblem(H, psi_0, psi_t, u);
        const auto stepSizeFinder = makeInterpolatingStepSizeFinder(100, 10);

        const auto stopper = makeStopper([](const auto& optimizer)
        {
            bool stop = false;
            if (optimizer.problem().fidelity() > 0.999) { std::cout << "Fidelity criterion satisfied" << std::endl; stop = true; }
            if (optimizer.iteration() == 50) { std::cout << "Max iterations exceeded" << std::endl; stop = true; }
            if (optimizer.problem().gradient().normL2() < 1e-4) { std::cout << "Gradient norm below threshold" << std::endl; stop = true; }
            if (optimizer.problem().nPropagationSteps() > 3e5) { std::cout << "Max full path propagations exceeded" << std::endl; stop = true; }
            if (stop) std::cout << "STOPPING" << std::endl;
            return stop;
        });

        const auto collector = makeCollector([&dc,n_steps](const auto& optimizer)
        {
            dc["fidelity"].append( optimizer.problem().fidelity());
            std::cout <<
                 "ITER "       << optimizer.iteration() << " | " <<
                 "fidelity : " << optimizer.problem().fidelity() << "\t " <<
                 "stepSize : " << optimizer.stepSize()   << "\t " <<
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
            std::endl;
        });

        auto optimizer = makeGrape_bfgs_H1(problem, stopper, collector, stepSizeFinder);

        collector(optimizer);
        optimizer.optimize();
        const auto u_opt = optimizer.problem().control();

        /// Propagate over optimized solution
        solver.reset(psi_0, u_opt.getFront());

        dc["u_opt"] =  u_opt.mat();
        dc["psi_target"] =  psi_t.vec();
        dc["V_opt"].append( (V_static + V_dynamic(u_opt.get(0))).vec());
        dc["psi_opt"].append( solver.state().vec());

        for (auto i = 1; i < u_opt.size(); i++)
        {
            solver.step(u_opt.get(i));

            dc["V_opt"].append( (V_static + V_dynamic(u_opt.get(i))).vec());
            dc["psi_opt"].append( solver.state().vec());
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "---------------" << std::endl;
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count() << "s";

    //dc.save("oneparticle-example.mat");
    dc.save("oneparticle-example.json");
	
}
