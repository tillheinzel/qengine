﻿/* COPYRIGHT
 *
 * file="oneparticle-timeevolution-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine.h>

using namespace qengine;
using namespace spatial;

using namespace optimal_control;

int main()
{

    auto kinFactor = 0.5;
    auto s = spatial::oneD::makeHilbertSpace(-5, 5, 512, kinFactor);
	auto x = s.spatialDimension();


    auto amplitude = 10.0;
    auto width     = 0.25;                                                                                           ;

    // static tweezer
    auto x0_static = 1.0;
    auto staticTweezer  = -amplitude*exp(-pow(x-x0_static,2)/(2*width*width)) + amplitude;

    // dynamic tweezer
    auto x0_dynamic_initial = -1.0;
    auto dynamicTweezer = [&x,amplitude,width](const RVec& p)
    {
        auto x0_dynamic = p.at(0);
        return -amplitude*exp(-pow(x-x0_dynamic,2)/(2*width*width)) + amplitude;
    };

    // sum of tweezers
    auto tweezerSum = [&x,&staticTweezer,&dynamicTweezer](const RVec& p)
    {
        return staticTweezer + dynamicTweezer(p);
    };


    // potential
    const  auto V = makePotential(tweezerSum,RVec{x0_dynamic_initial});

    // Hamiltonian
	const auto H = s.T() + V; // full hamiltonian

    // States
    auto psi_init =   makeWavefunction((s.T() + staticTweezer)[0]);
    auto psi_target = makeWavefunction((s.T() + dynamicTweezer(RVec{x0_dynamic_initial}))[0]);

    // Stepper
    auto  t = 0.0;
    const auto dt=0.01;
	const auto duration = 3.0;
	const auto n_steps = static_cast<count_t>(duration / dt);
    auto solver = oneD::makeFixedTimeStepper(H,psi_init,dt);


    // Control
    const auto tunnelPoint    = 0.3;
	const auto tunnelTime     = 0.3*duration/dt;
	const auto transportTime  = 0.9*duration/dt - tunnelTime;



    auto u1 = makeLinearControl(x0_dynamic_initial,tunnelPoint,floor(0.1*duration/dt),dt);
    auto u2 = makeLinearControl(tunnelPoint,tunnelPoint,floor(tunnelTime),dt);
    auto u3 = makeLinearControl(tunnelPoint,x0_dynamic_initial,floor(transportTime),dt);


    auto u = glue(glue(u1,u2),u3);

    DataContainer dc;

    dc.append("x",x.vec());
    dc.append("u",u.vec());

    for(auto i=0; i < u.size(); i++)
    {
        if(i%1 == 0)
        {
            dc.append("V",V(u.get(i)).vec());
            dc.append("psi",solver.state().vec());
            dc.append("t",t);
        }

        solver.step(u.get(i));

        t += dt;
    }

    dc.append("fidelity",fidelity(solver.state(),psi_target));



    /// Optimal Control

//    auto alg  = oneD::splitStep(H,x0_dynamic_initial);
    auto dVdu = optimal_control::constructDiffPotential
    (
        [&x, amplitude,width](const RVec& p)
    {
        auto x0_dynamic = p.at(0);
        return amplitude/pow(width,2) * exp(-pow(x-x0_dynamic,2)/(2*width*width))*(x-x0_dynamic);
    }
    );

    auto problem = makeStateTransferProblem(H,dVdu,psi_init,psi_target,u);
//    auto problem =  makeLinearStateTransferProblem(alg,dVdu,psi_init,psi_target,dt,u); // Adding regularization changes nothing + 1e-1*Regularization(u);
    auto stepSizeFinder = makeGrapeStepsizeFinder(100,10);
//    auto stepSizeFinder = [](auto& dir, auto& problem, const auto& algorithm){return -1;};
    auto stopper = [](const auto& optimizer)
    {
        bool stop = false;

//        if(optimizer.problem().fidelity() > 0.964) {std::cout << "Fidelity criterion satisfied" << std::endl; stop=true;}
        if(optimizer.iteration() == 10) {std::cout << "Max iterations exceeded" << std::endl; stop=true;}
//        if(optimizer.problem().gradient().normL2() < 1e-4) {std::cout << "Gradient norm below threshold" << std::endl; stop=true;}
//        if(optimizer.problem().nPropagationSteps() > 2e5) {std::cout << "Max full path propagations exceeded" << std::endl; stop=true;}

        if(stop) std::cout << "STOPPING" << std::endl;

        return stop;
    };

    auto collector = [&dc,&psi_target,&V,&u,&solver,&psi_init](const auto& optimizer)
    {
        dc.append("fidelity",optimizer.problem().fidelity());
        std::cout << "ITER "<< optimizer.iteration() << " | fidelity : " << optimizer.problem().fidelity() << ", stepsize: " << optimizer.previousStepSize()<< ", fpp: " << optimizer.problem().nPropagationSteps() <<std::endl;

        // When stopping optimizer, save the optimized solution
        if(optimizer.stopper()(optimizer))
        {
            solver.reset(psi_init,optimizer.problem().initialControl().front());

            auto u_opt = optimizer.problem().control();
            dc.append("u_opt",u_opt.vec());
            dc.append("psi_target",psi_target.vec());

            for(auto i=0; i < u_opt.size(); i++)
            {
                if(i%1 == 0)
                {
                    dc.append("V_opt",V(u_opt.get(i)).vec());
                    dc.append("psi_opt",solver.state().vec());
                }

                solver.step(u_opt.get(i));
            }
        }
    };

    auto restart = [](const auto& optimizer){return (optimizer.iteration()%5==0);};
    auto norestart = [](const auto& optimizer){return false;};
//    auto optimizer = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder,norestart);

//    auto optimizer = makeGrape_steepest_H1(problem,stopper,collector,stepSizeFinder);
    auto optimizer = makeGrape_steepest_L2(problem,stopper,collector,stepSizeFinder);

    optimizer.collector();
    optimizer.optimize();


    dc.save("oneparticle_export1.mat");

}
