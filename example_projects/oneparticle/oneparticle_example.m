%% Guess
clc
hold off
clear all
%% 
 
load('oneparticle-example.mat')
%load('oneparticle_export1.mat')
%load('oneparticle_export2.mat')

scale = 10;

density = scale*conj(psi(:,1)).*psi(:,1);
target = scale*conj(psi_target).*psi_target;
V_plt = plot(x,V(:,1)); hold on
wave_plt = plot(x,density);
plot(x,target,'b')
%ylim([0 6])


pause()

for i=1:length(t)
   density = conj(psi(:,i)).*psi(:,i);
   
   set(wave_plt,'ydata',scale*density)
   set(V_plt,'ydata',V(:,i))

   
    pause(0.03)
end

pause()

%% Optmized 
load('oneparticle-example.mat')
%load('oneparticle_export1.mat')
%load('oneparticle_export2.mat')

hold off

density = scale*conj(psi_opt(:,1)).*psi_opt(:,1);
target = scale*conj(psi_target).*psi_target;
V_plt = plot(x,V_opt(:,1)); hold on
wave_plt = plot(x,density);
plot(x,target)
%ylim([0 6])
fidelity
for i=1:length(t)
   density = conj(psi_opt(:,i)).*psi_opt(:,i);
   
   set(wave_plt,'ydata',scale*density)
   set(V_plt,'ydata',V_opt(:,i))

   
    pause(0.03)
end

pause()

%% Controls

load('oneparticle-example.mat')
%load('oneparticle_export1.mat')
%load('oneparticle_export2.mat')
hold off

plot(u); hold on
plot(u_opt)




