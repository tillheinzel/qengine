%% Guess
clc
hold off
clear all


%% Rho 
load('bosehubbard-example.mat')

rs = @(rho) reshape(rho,sqrt(size(rho,1)),sqrt(size(rho,1)))

subplot(3,1,1)
rho_plt = imagesc(rs(rho1(:,1)));
colorbar;
caxis([0 1]);

subplot(3,1,2)
rho_opt_plt = imagesc(rs(rho1_opt(:,1)));
colorbar;
caxis([0 1]);

subplot(3,1,3)
U_plt = plot(t,U,t,U_opt);

for i=1:length(t)
    set(rho_plt,'cdata',rs(rho1(:,i)))
    set(rho_opt_plt,'cdata',rs(rho1_opt(:,i)))
    pause(0.01)
end



return

%% Optmized 
load('bosehubbard-example.mat')
subplot(2,1,1); hold off
for i=1:size(cs,1)
   semilogy(t,cs(i,:))
   hold on
end


%% Controls

load('bosehubbard-example.mat')

U = U(1,:);
U_opt = U_opt(1,:);


subplot(2,1,1); hold off
plot(t,U); hold on
plot(t,U_opt); 
leg = legend('U','U^*')
set(leg,'location','north')


subplot(2,1,2); hold off
semilogy((1-fidelity),'.')
xlabel('iterations')


return

%% Units

a0 = 0.53*10e-10;


lambda = 1000-9; % m

d = lambda/2; % m
k = pi/d;

m_si = 1.443e-25; %kg

Er_si = 3.8*10^(-31); % J 

%t = sqrt(m_si/Er_si)*d
t = sqrt(Er_si/m_si)*1/d




