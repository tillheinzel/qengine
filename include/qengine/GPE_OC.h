﻿/* COPYRIGHT
 *
 * file="GPE_OC.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Specifics/Spatial/1D/Gpe/StateTransferProblemFactory.h"
#include "src/Specifics/GeneralHelpers.h"
#include "src/Specifics/Spatial/OptimalControlHelpers.h"


#include "src/Specifics/Spatial/1D/Gpe/StateTransferProblem.impl.h"
