﻿/* COPYRIGHT
 *
 * file="OneParticle.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"

////////// INCLUDES //////////
#include "src/Utility/Constants.h"
#include "src/Utility/Types.h"

#include "src/Generics/Physics/General/OperatorFunctions.h"
#include "src/Generics/Physics/Spatial/Basics.h"
#include "src/Generics/Physics/Spatial/PotentialFunction.h"

#include "src/Specifics/Spatial/makeTimeStepper.h"

#include "src/Specifics/Spatial/1D/ApiHilbertSpace.h"
#include "src/Specifics/Spatial/1D/Gpe/HamiltonianArithmetic.h"
#include "src/Specifics/Spatial/1D/OneParticle/defaultSteppingAlg.h"

#include "src/Generics/Physics/SecondQuantization/KrylovStepping.impl.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Generics/Physics/Spatial/LinearHamiltonian.impl.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Generics/Physics/Spatial/Spectrum.impl.h"

#define QENGINE_USES_1P

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/OneParticle.h"
#endif
