﻿/* COPYRIGHT
 *
 * file="qengine.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"

#include "qengine/OneParticle.h"
#include "qengine/GPE.h"
#include "qengine/TwoParticle.h"
#include "qengine/NLevel.h"
#include "qengine/BoseHubbard.h"

#include "qengine/OptimalControl.h"


/// optimal control

#include "qengine/OneParticle_OC.h"
#include "qengine/GPE_OC.h"
#include "qengine/TwoParticle_OC.h"
#include "qengine/NLevel_OC.h"
#include "qengine/BoseHubbard_OC.h"

/// MISC
#include "src/version.h"
#include "qengine/DataContainer.h"
