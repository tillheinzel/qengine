﻿/* COPYRIGHT
 *
 * file="GPE.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"

////////// INCLUDES //////////
#include "src/Utility/Constants.h"
#include "src/Utility/Types.h"

#include "src/Generics/Physics/General/OperatorFunctions.h"
#include "src/Generics/Physics/Spatial/Basics.h"
#include "src/Generics/Physics/Spatial/PotentialFunction.h"

#include "src/Specifics/Spatial/makeTimeStepper.h"

#include "src/Specifics/Spatial/1D/ApiHilbertSpace.h"

#include "src/Specifics/Spatial/1D/Gpe/HamiltonianArithmetic.h"
#include "src/Specifics/Spatial/1D/Gpe/defaultSteppingAlg.h"
#include "src/Specifics/Spatial/1D/Gpe/Basics.h"
#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Generics/Physics/Spatial/Spectrum.impl.h"

#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.impl.h"

#define QENGINE_USES_GPE

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/GPE_OC.h"
#endif
