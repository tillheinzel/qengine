﻿/* COPYRIGHT
 *
 * file="OptimalControl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"

/// Problems:
#include "src/Generics/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/Generics/OptimalControl/Problems/LinearStateTransferProblem.impl.h"

#include "src/Generics/OptimalControl/Problems/DynamicState.impl.h"

#include "src/Generics/OptimalControl/Problems/Boundaries.h"
#include "src/Generics/OptimalControl/Problems/Regularization.h"
#include "src/Generics/OptimalControl/Problems/SumProblem.h"
#include "src/Generics/OptimalControl/Problems/SumProblem.impl.h"


#include "src/Specifics/Algorithms/GRAPE_steepest.h"
#include "src/Specifics/Algorithms/GRAPE_bfgs.h"

#include "src/Specifics/Algorithms/GROUP_steepest.h"
#include "src/Specifics/Algorithms/GROUP_bfgs.h"

#include "src/Specifics/Algorithms/BasisFactory.h"
#include "src/Specifics/Algorithms/GroupHelpers.h"

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"
#include "src/Specifics/Algorithms/sortAndAddDefaults.h"

#include "src/Generics/OptimalControl/Algorithms/SteepestDescent.impl.h"
#include "src/Generics/OptimalControl/Algorithms/Bfgs.impl.h"

/// Controls:
#include "src/Generics/OptimalControl/Control.h"


#include "src/Specifics/GeneralHelpers.h"

#define QENGINE_USES_OPTIMAL_CONTROL

#ifdef QENGINE_USES_GPE
	#include "qengine/GPE_OC.h"
#endif
#ifdef QENGINE_USES_1P
	#include "qengine/OneParticle_OC.h"
#endif
#ifdef QENGINE_USES_2P
	#include "qengine/TwoParticle_OC.h"
#endif
#ifdef QENGINE_USES_NLEVEL
	#include "qengine/NLevel_OC.h"
#endif
#ifdef QENGINE_USES_BOSEHUBBARD
	#include "qengine/BoseHubbard_OC.h"
#endif
