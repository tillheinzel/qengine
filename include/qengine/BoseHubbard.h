﻿/* COPYRIGHT
 *
 * file="BoseHubbard.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"

#include "src/Utility/Constants.h"
#include "src/Utility/Types.h"

#include "src/Generics/Physics/General/OperatorFunctions.h"
#include "src/Generics/Physics/SecondQuantization/Functions.h"

#include "src/Specifics/SecondQuantized/BoseHubbard/HilbertSpace.h"
#include "src/Specifics/SecondQuantized/makeState.h"
#include "src/Specifics/SecondQuantized/BoseHubbard/makeOperator.h"
#include "src/Specifics/SecondQuantized/BoseHubbard/TimeSteppers.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Generics/Physics/SecondQuantization/Operator.impl.h"
#include "src/Generics/Physics/SecondQuantization/State.impl.h"
#include "src/Generics/Physics/SecondQuantization/Spectrum.impl.h"
#include "src/Generics/Physics/SecondQuantization/KrylovStepping.impl.h"

#define QENGINE_USES_BOSEHUBBARD

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/BoseHubbard_OC.h"
#endif
