﻿/* COPYRIGHT
 *
 * file="TwoParticle.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

////////// INCLUDES //////////

#include "src/version.h"

#include "src/Utility/Constants.h"
#include "src/Utility/Types.h"

#include "src/Generics/Physics/General/OperatorFunctions.h"
#include "src/Generics/Physics/Spatial/Basics.h"
#include "src/Generics/Physics/Spatial/PotentialFunction.h"

#include "src/Specifics/Spatial/makeTimeStepper.h"

#include "src/Specifics/Spatial/2D/TwoParticle/ApiHilbertSpace2P.h"
#include "src/Specifics/Spatial/2D/TwoParticle/HamiltonianArithmetic.h"
#include "src/Specifics/Spatial/2D/TwoParticle/defaultSteppingAlg.h"
#include "src/Specifics/Spatial/2D/TwoParticle/PotentialWithInteraction.h"

#include "src/Generics/Physics/SecondQuantization/KrylovStepping.impl.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Generics/Physics/Spatial/Spectrum.impl.h"

#include "src/Generics/Physics/Spatial/LinearHamiltonian.impl.h"

#define QENGINE_USES_2P

#ifdef QENGINE_USES_OPTIMAL_CONTROL
	#include "qengine/TwoParticle_OC.h"
#endif
