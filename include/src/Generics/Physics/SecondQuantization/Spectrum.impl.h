﻿/* COPYRIGHT
 *
 * file="Spectrum.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/SecondQuantization/Spectrum.h"

#include "src/Generics/Physics/SecondQuantization/State.h"

namespace qengine
{
	namespace second_quantized
	{
		template <class HilbertSpace>
		Spectrum<HilbertSpace>::Spectrum(internal::Spectrum<complex>&& t1, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace) :
			internal::Spectrum<complex>(t1),
			hilbertSpace_(hilbertSpace)
		{
		}

		template <class HilbertSpace>
		Spectrum<HilbertSpace>::Spectrum(const internal::Spectrum<complex>& t1,
			const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace) :
			internal::Spectrum<complex>(t1),
			hilbertSpace_(hilbertSpace)
		{
		}

		template<class HilbertSpace>
		State<HilbertSpace> Spectrum<HilbertSpace>::eigenState(const count_t i) const
		{
			return State<HilbertSpace>(internal::Spectrum<complex>::eigenvector(i), hilbertSpace_);
		}

		template<class HilbertSpace>
		State<HilbertSpace> Spectrum<HilbertSpace>::makeLinearCombination(const CVec& factors, const qengine::NORMALIZATION shouldNormalize) const
		{
			auto state = State<HilbertSpace>(internal::Spectrum<complex>::makeLinearCombination(factors), hilbertSpace_);

			if (shouldNormalize == NORMALIZE)
			{
				return normalize(state);
			}
			return state;
		}

		template <class HilbertSpace>
		internal::SerializedHilbertSpace<HilbertSpace> Spectrum<HilbertSpace>::_hilbertSpace() const
		{
			return hilbertSpace_;
		}
	}
}
