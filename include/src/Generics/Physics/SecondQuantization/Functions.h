﻿/* COPYRIGHT
 *
 * file="Functions.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.h"

#include "src/Generics/Physics/SecondQuantization/State.h"
#include "src/Generics/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	template<template<class> class MatType, typename Number, class HilbertSpace, class... Ts, typename = std::enable_if_t<internal::has_spectrum_v<Operator<MatType, Number, HilbertSpace>, any_return, count_t, Ts...>>>
	State<HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, Number, HilbertSpace>>& l, const qengine::NORMALIZATION shouldNormalize, Ts&&... additionalParams)
	{
		if (shouldNormalize == NORMALIZE)
		{
			return normalize(l.evaluate(std::forward<Ts>(additionalParams)...));
		}
		return l.evaluate(std::forward<Ts>(additionalParams)...);
	}

	template<template<class> class MatType, typename Number, class HilbertSpace, class... Ts, typename = std::enable_if_t<internal::has_spectrum_v<Operator<MatType, Number, HilbertSpace>, any_return, count_t, Ts...>>>
	State<HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, Number, HilbertSpace>>& l, Ts&&... additionalParams)
	{
		return l.evaluate(std::forward<Ts>(additionalParams)...);
	}
}
