﻿/* COPYRIGHT
 *
 * file="OpaqueOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	namespace internal 
	{
		template<class HilbertSpace>
		class OperatorHider_Base
		{
		public:
			virtual ~OperatorHider_Base() {}

			virtual void applyInPlace(State<HilbertSpace>& state) const = 0;
		};

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		class OperatorHider :public OperatorHider_Base<HilbertSpace>
		{
		public:
			OperatorHider(Operator<MatType, ScalarType, HilbertSpace>&& op):operator_(op){}

			virtual void applyInPlace(State<HilbertSpace>& state) const override
			{
				operator_.applyInPlace(state);
			}

		private:
			const Operator<MatType, ScalarType, HilbertSpace> operator_;
		};

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		auto makeOperatorHiderPointer(Operator<MatType, ScalarType, HilbertSpace>&& op)
		{
			return std::make_unique<OperatorHider<MatType, ScalarType, HilbertSpace>>(std::move(op));
		}
	}

	template<class HilbertSpace>
	class OpaqueOperator
	{
	public:
		OpaqueOperator(std::unique_ptr<internal::OperatorHider_Base<HilbertSpace>>&& op):
		operator_(std::move(op))
		{}

		void applyInPlace(State<HilbertSpace>& state) const
		{
			operator_->applyInPlace(state);
		}

	private:
		std::unique_ptr<internal::OperatorHider_Base<HilbertSpace>> operator_;
	};

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	OpaqueOperator<HilbertSpace> makeOpaqueOperator(Operator<MatType, ScalarType, HilbertSpace>&& op)
	{
		return OpaqueOperator<HilbertSpace>(internal::makeOperatorHiderPointer(std::move(op)));
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	OpaqueOperator<HilbertSpace> makeOpaqueOperator(const Operator<MatType, ScalarType, HilbertSpace>& op)
	{
		auto op2 = op;
		return OpaqueOperator<HilbertSpace>(internal::makeOperatorHiderPointer(std::move(op2)));
	}


	template<class HilbertSpace>
	State<HilbertSpace> operator*(const OpaqueOperator<HilbertSpace>& op, State<HilbertSpace> s)
	{
		op.applyInPlace(s);
		return s;
	}

}
