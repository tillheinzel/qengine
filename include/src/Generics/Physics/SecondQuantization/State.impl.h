﻿/* COPYRIGHT
 *
 * file="State.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/SecondQuantization/State.h"

/*
 * Basically all functions forward to Vector<complex> in LinearAlgebra-module
 */

#include <assert.h>

#include "src/Utility/ExtraDebugChecks.h"


namespace qengine
{
	namespace internal
	{
		inline count_t collapseIndex(const std::vector<count_t>& indexVec, std::vector<count_t> dimensions)
		{
			assert(indexVec.size() == dimensions.size());

			for (auto dIt = dimensions.rbegin() + 1; dIt != dimensions.rend(); ++dIt)
			{
				(*dIt) = *dIt * *(dIt - 1);
			}

			auto index = indexVec.back();


			for (auto i = dimensions.size() - 1; i > 0u; --i)
			{
				index += indexVec.at(i - 1) * dimensions.at(i);
			}

			return index;
		}
	}
}

namespace qengine
{
	template <class HilbertSpace>
	State<HilbertSpace>::State(const Vector<complex>& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace): vec_(vec),
	                                                                                                                            hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template <class HilbertSpace>
	State<HilbertSpace>::State(const Vector<real>& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace): vec_(vec),
	                                                                                                                         hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template <class HilbertSpace>
	State<HilbertSpace>::State(Vector<complex>&& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace): vec_(std::move(vec)),
	                                                                                                                       hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template<class HilbertSpace>
	State<HilbertSpace>& State<HilbertSpace>::operator=(const State<HilbertSpace>& other)
	{
		if (&other != this)
		{
			qengine_assert(other._hilbertSpace() == this->_hilbertSpace(), "trying to assign State<HilbertSpace> with different dimensions!");
			vec_ = other.vec_;
		}
		return *this;
	}

	template<class HilbertSpace>
	State<HilbertSpace>& State<HilbertSpace>::operator=(State<HilbertSpace>&& other) noexcept
	{
		if (&other != this)
		{
            assert(other._hilbertSpace() == this->_hilbertSpace());
			vec_ = std::move(other.vec_);
		}
		return *this;
	}

	template<class HilbertSpace>
	State<HilbertSpace> State<HilbertSpace>::operator-() const
	{
		return State<HilbertSpace>(-vec_, _hilbertSpace());
	}

	template<class HilbertSpace>
	void State<HilbertSpace>::normalize()
	{
		vec_ /= vec_.norm();
	}

	template<class HilbertSpace>
	real State<HilbertSpace>::norm() const
	{
		return vec_.norm();
	}

	template<class HilbertSpace>
	RVec State<HilbertSpace>::absSquare() const
	{
		return vec_.absSquare();
	}

	template<class HilbertSpace>
	RVec State<HilbertSpace>::re() const
	{
		return vec_.re();
	}

	template<class HilbertSpace>
	RVec State<HilbertSpace>::im() const
	{
		return vec_.re();
	}

	template<class HilbertSpace>
	complex State<HilbertSpace>::sum() const
	{
		return qengine::sum(vec_);
	}

	template<class HilbertSpace>
	State<HilbertSpace> State<HilbertSpace>::conj() const
	{
		return State<HilbertSpace>(qengine::conj(vec_), _hilbertSpace());
	}

	template<class HilbertSpace>
	complex& State<HilbertSpace>::at(const count_t index)
	{
		return vec_.at(index);
	}

	template<class HilbertSpace>
	const complex& State<HilbertSpace>::at(const count_t index) const
	{
		return vec_.at(index);
	}

	template<class HilbertSpace>
	internal::SerializedHilbertSpace<HilbertSpace> State<HilbertSpace>::_hilbertSpace() const
	{
		return hilbertSpace_;
	}

	template<class HilbertSpace>
	count_t State<HilbertSpace>::size() const
	{
		return vec_.size();
	}

}

namespace qengine
{
	template<class HilbertSpace>
	State<HilbertSpace> normalize(State<HilbertSpace> f)
	{
		f.normalize();
		return f;
	}

	template<class HilbertSpace>
	real norm(const State<HilbertSpace>& f)
	{
		return f.norm();
	}

	template<class HilbertSpace>
	RVec absSquare(const State<HilbertSpace>& f)
	{
		return f.absSquare();
	}

	template<class HilbertSpace>
	RVec re(const State<HilbertSpace>& f)
	{
		return f.re();
	}

	template<class HilbertSpace>
	RVec im(const State<HilbertSpace>& f)
	{
		return f.im();
	}

	template<class HilbertSpace>
	complex sum(const State<HilbertSpace>& f)
	{
		return f.sum();
	}

	template<class HilbertSpace>
	State<HilbertSpace> conj(const State<HilbertSpace>& f)
	{
		return f.conj();
	}
}

namespace qengine
{
	template<class HilbertSpace>
	std::ostream& operator<<(std::ostream& s, const State<HilbertSpace>& f)
	{
		s << f.vec();
		return s;
	}

	template <class HilbertSpace>
	bool operator==(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		return left._hilbertSpace() == right._hilbertSpace() && left.vec() == right.vec();
	}

	template <class HilbertSpace>
	bool operator!=(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		return !(left == right);
	}

	template<class HilbertSpace>
	State<HilbertSpace> operator+(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dimensions!");
		return State<HilbertSpace>(left.vec() + right.vec(), left._hilbertSpace());
	}

	template<class HilbertSpace>
	State<HilbertSpace> operator-(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dimensions!");
		return State<HilbertSpace>(left.vec() - right.vec(), left._hilbertSpace());
	}

	template<class HilbertSpace>
	State<HilbertSpace> operator*(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dimensions!");
		return State<HilbertSpace>(left.vec() * right.vec(), left._hilbertSpace());
	}

	template<class HilbertSpace>
	State<HilbertSpace> operator/(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dimensions!");
		return State<HilbertSpace>(left.vec() / right.vec(), left._hilbertSpace());
	}
}
