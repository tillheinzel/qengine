﻿/* COPYRIGHT
 *
 * file="Operator.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <assert.h>

#include "src/Generics/Physics/SecondQuantization/Operator.h"

#include "src/Generics/Physics/SecondQuantization/Spectrum.h"

namespace qengine
{
	template <template <class> class MatType, typename ScalarType, class HilbertSpace>
	Operator<MatType, ScalarType, HilbertSpace>::Operator(const MatType<ScalarType>& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace) : mat_(mat),
		hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!mat_.hasNan(), "operator has NaN");
	}

	template <template <class> class MatType, typename ScalarType, class HilbertSpace>
	Operator<MatType, ScalarType, HilbertSpace>::Operator(MatType<ScalarType>&& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace) : mat_(mat),
		hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!mat_.hasNan(), "operator has NaN");
	}

	template <template <class> class MatType, typename ScalarType, class HilbertSpace>
	template <typename N, typename T>
	Operator<MatType, ScalarType, HilbertSpace>::Operator(const Operator<MatType, T, HilbertSpace>& other) :
		mat_(other._mat()),
		hilbertSpace_(other._hilbertSpace())
	{
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	Operator<MatType, ScalarType, HilbertSpace>& Operator<MatType, ScalarType, HilbertSpace>::operator=(const Operator& other)
	{
		if (&other != this)
		{
			qengine_assert(other._hilbertSpace() == this->_hilbertSpace(), "trying to assign Operator with different dimensions!");
			mat_ = other.mat_;
		}
		return *this;
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	Operator<MatType, ScalarType, HilbertSpace>& Operator<MatType, ScalarType, HilbertSpace>::operator=(Operator&& other) noexcept
	{
		if (&other != this)
		{
            assert(other._hilbertSpace() == this->_hilbertSpace());
			mat_ = other.mat_;
		}
		return *this;
	}

	//template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	//template <typename T2, typename T>
	//Operator<MatType, ScalarType, HilbertSpace>& Operator<MatType, ScalarType, HilbertSpace>::operator=(const Operator<T>& other)
	//{
	//	qengine_assert(other._hilbertSpace() == dimensions(), "");
	//	mat_ = other._mat();
	//	return *this;
	//}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	void Operator<MatType, ScalarType, HilbertSpace>::applyInPlace(State<HilbertSpace>& state) const
	{
		//qengine_assert(state._hilbertSpace() == this->_hilbertSpace(), "dimensions don't fit!");
		mat_.multiplyInPlace(state.vec());
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	second_quantized::Spectrum<HilbertSpace> Operator<MatType, ScalarType, HilbertSpace>::makeSpectrum(const count_t largestEigenstate) const
	{
		return second_quantized::Spectrum<HilbertSpace>(internal::getSpectrum(mat_, largestEigenstate, 1), _hilbertSpace());
	}

	template <template <class> class MatType, typename ScalarType, class HilbertSpace>
	template <class T, class>
	second_quantized::Spectrum<HilbertSpace> Operator<MatType, ScalarType, HilbertSpace>::makeSpectrum(count_t largestEigenstate, count_t nExtraStates, real tolerance) const
	{
		return second_quantized::Spectrum<HilbertSpace>(internal::getSpectrum(mat_, largestEigenstate, 1, nExtraStates, tolerance), _hilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	ScalarType Operator<MatType, ScalarType, HilbertSpace>::at(const count_t col, const count_t row) const
	{
		return mat_.at(col, row);
	}

}

namespace qengine
{
	template <template <class> class MatType, typename ScalarType, class HilbertSpace>
	std::ostream& operator<<(std::ostream& stream, const Operator<MatType, ScalarType, HilbertSpace>& op)
	{
		stream << op._mat();
		return stream;
	}

	template <template <class> class M1, template <class> class M2, typename N1, typename N2, class HilbertSpace>
	bool operator==(const Operator<M1, N1, HilbertSpace>& left, const Operator<M2, N2, HilbertSpace>& right)
	{
		return left._mat().mat() == right._mat().mat();
	}

	template <template <class> class M1, template <class> class M2, typename N1, typename N2, class HilbertSpace>
	bool operator!=(const Operator<M1, N1, HilbertSpace>& left, const Operator<M2, N2, HilbertSpace>& right)
	{
		return left._mat().mat() == right._mat().mat();
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	State<HilbertSpace> operator*(const Operator<MatType, ScalarType, HilbertSpace>& op, State<HilbertSpace> s)
	{
		op.applyInPlace(s);
		return s;
	}

	template <template <class> class MatType, typename ScalarType1, typename ScalarType2, class HilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const Operator<MatType, ScalarType1, HilbertSpace>& op, const Vector<ScalarType2>& s)
	{
		return Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace>(op._mat() + internal::SqDiagMat<ScalarType2>(s), op._hilbertSpace());
	}

	template <template <class> class MatType, typename ScalarType1, typename ScalarType2, class HilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const Vector<ScalarType2>& s, const Operator<MatType, ScalarType1, HilbertSpace>& op)
	{
		return op + s;
	}
}
