﻿/* COPYRIGHT
 *
 * file="State.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/SecondQuantization/State.fwd.h"


#include "src/Utility/Types.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Generics/Physics/General/SerializedHilbertSpace.h"

#include "src/Generics/Physics/SecondQuantization/isHilbertSpace.h"

namespace qengine
{
	namespace internal
	{
		// calculates a one-dimensional index from a multidimensional one.
		count_t collapseIndex(const std::vector<count_t>& indexVec, std::vector<count_t> dimensions);
	}
}

namespace qengine
{
	template<class HilbertSpace>
	class State
	{
		static_assert(second_quantized::isHilbertSpace_v<HilbertSpace>, "Hilbertspace must be a second quantized hilbertspace");
		/// CTOR DTOR
	public:
		State(const Vector<complex>& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);
		State(const Vector<real>& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);
		State(Vector<complex>&& vec, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);

		State(const State& other) = default;
		State(State&& other) = default;

		~State() = default;

		/// ASSIGNMENT
	public:
		State & operator=(const State& other);
		State& operator=(State&& other) noexcept;

		/// MISC
	public:
		State operator- () const;

		void normalize();
		real norm() const;

		RVec absSquare() const; /// return |this|^2 
		RVec re() const;
		RVec im() const;

		complex sum() const;
		State conj() const;

		complex& at(count_t index);
		const complex& at(count_t index) const;

		count_t size() const;

		CVec & vec() & { return vec_; }
		const CVec& vec() const& { return vec_; }
		CVec&& vec() && {return std::move(vec_); }

		/// INTERNAL USE
	public:
		internal::SerializedHilbertSpace<HilbertSpace>  _hilbertSpace() const;


	private:
		CVec vec_;
		internal::SerializedHilbertSpace<HilbertSpace> hilbertSpace_;
	};
}

namespace qengine
{

	template<class HilbertSpace>
	complex overlap(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		return cdot(left.vec(), right.vec());
	}

	template<class HilbertSpace>
	real fidelity(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		return std::pow(std::abs(overlap(left, right)), 2);
	}

	template<class HilbertSpace>
	real infidelity(const State<HilbertSpace>& left, const State<HilbertSpace>& right)
	{
		return 1 - fidelity(left, right);
	}

	template<class HilbertSpace>
	State<HilbertSpace> normalize(State<HilbertSpace> f);

	template<class HilbertSpace>
	real norm(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	RVec absSquare(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	RVec re(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	RVec im(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	complex sum(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	State<HilbertSpace> conj(const State<HilbertSpace>& f);

	template<class HilbertSpace>
	State<HilbertSpace> toComplex(const State<HilbertSpace>& f) { return f; }
}

namespace qengine
{
	template<class HilbertSpace>
	std::ostream& operator<<(std::ostream& s, const State<HilbertSpace>& f);

	template<class HilbertSpace>
	bool operator== (const State<HilbertSpace>& left, const State<HilbertSpace>& right);
	template<class HilbertSpace>
	bool operator!= (const State<HilbertSpace>& left, const State<HilbertSpace>& right);

	template<class HilbertSpace>
	State<HilbertSpace> operator+(const State<HilbertSpace>& left, const State<HilbertSpace>& right);
	template<class HilbertSpace>
	State<HilbertSpace> operator- (const State<HilbertSpace>& left, const State<HilbertSpace>& right);
	template<class HilbertSpace>
	State<HilbertSpace> operator* (const State<HilbertSpace>& left, const State<HilbertSpace>& right);
	template<class HilbertSpace>
	State<HilbertSpace> operator/ (const State<HilbertSpace>& left, const State<HilbertSpace>& right);

}

namespace qengine
{
	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (const State<HilbertSpace>& f, number2 a)
	{
		return State<HilbertSpace>(f.vec() + a, f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (number2 a, const State<HilbertSpace>& f)
	{
		return State<HilbertSpace>(a + f.vec(), f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (const State<HilbertSpace>& f, number2 a)
	{
		return State<HilbertSpace>(f.vec() - a, f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (number2 a, const State<HilbertSpace>& f)
	{
		return State<HilbertSpace>(a - f.vec(), f._hilbertSpace());
	}
	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (const State<HilbertSpace>& f, number2 a)
	{
		return State<HilbertSpace>(f.vec() * a, f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (number2 a, const State<HilbertSpace>& f)
	{
		return State<HilbertSpace>(a * f.vec(), f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (const State<HilbertSpace>& f, number2 a)
	{
		return State<HilbertSpace>(f.vec() / a, f._hilbertSpace());
	}

	template<class HilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (number2 a, const State<HilbertSpace>& f)
	{
		return State<HilbertSpace>(a / f.vec(), f._hilbertSpace());
	}
}
