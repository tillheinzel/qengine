﻿/* COPYRIGHT
 *
 * file="Operator.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/isOperator.h"

namespace qengine
{
	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	class Operator;
}


namespace qengine
{
	namespace internal
	{
		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		struct hasOperatorLabel<Operator<MatType, ScalarType, HilbertSpace >> : std::true_type{};

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		struct hasLinearLabel<Operator<MatType, ScalarType, HilbertSpace >> : std::true_type{};

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		struct extract_hilbertspace<Operator<MatType, ScalarType, HilbertSpace>>
		{
			using type = HilbertSpace;
		};
	}
}
