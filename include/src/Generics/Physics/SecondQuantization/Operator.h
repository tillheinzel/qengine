﻿/* COPYRIGHT
 *
 * file="Operator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"

#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqDenseMat.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"


#include "src/Generics/Physics/General/SerializedHilbertSpace.h"
#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.h"
#include "src/Generics/Physics/General/extract_hilbertspace.h"

#include "src/Generics/Physics/SecondQuantization/State.h"
#include "src/Generics/Physics/SecondQuantization/isHilbertSpace.h"
#include "src/Generics/Physics/SecondQuantization/Spectrum.fwd.h"
#include "src/Generics/Physics/SecondQuantization/Operator.fwd.h"

#include "src/Utility/ExtraDebugChecks.h"

namespace qengine
{
	// basic static operator with different representations and arithmetic
	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	class Operator : public internal::EigenstateSyntax_CRTP<Operator<MatType, ScalarType, HilbertSpace>>
	{
		static_assert(second_quantized::isHilbertSpace_v<HilbertSpace>, "Hilbertspace must be a second quantized hilbertspace");
		static_assert(internal::is_number_type_v<ScalarType>, "ScalarType for BasicOperator must be a numbertype (real or complex)!");

		/// CTOR DTOR
	public:
		Operator(const MatType<ScalarType>& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);
		Operator(MatType<ScalarType>&& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);

		Operator(const Operator& other) = default;
		Operator(Operator&& other) = default;

		template<typename N = ScalarType, typename T = std::enable_if_t<std::is_same<N, complex>::value, real>>
		Operator(const Operator<MatType, T, HilbertSpace>& other);

		~Operator() = default;

		/// ASSIGNMENT
	public:
		Operator & operator=(const Operator& other);
		Operator& operator=(Operator&& other) noexcept;
		/*
		template<typename T2 = ScalarType, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		Operator& operator=(const Operator<T>& other);*/

		/// MISC
	public:
		void applyInPlace(State<HilbertSpace>& state) const;
		second_quantized::Spectrum<HilbertSpace> makeSpectrum(count_t largestEigenstate) const;

		template<class T = MatType<ScalarType>, class = std::enable_if_t<std::is_same<T, internal::SqSparseMat<ScalarType>>::value>>
		second_quantized::Spectrum<HilbertSpace> makeSpectrum(count_t largestEigenstate, count_t nExtraStates, real tolerance = 0.0) const;

		ScalarType at(count_t col, count_t row) const;

		/// INTERNAL USE
	public:
		const MatType<ScalarType>& _mat() const { return mat_; }
		MatType<ScalarType>& _mat() { return mat_; }

		const internal::SerializedHilbertSpace<HilbertSpace>& _hilbertSpace() const { return hilbertSpace_; }

	protected:
		MatType<ScalarType> mat_;
		internal::SerializedHilbertSpace<HilbertSpace> hilbertSpace_;
	};

	namespace internal
	{

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		auto makeOperator(MatType<ScalarType>&& mat, const HilbertSpace& hilbertSpace)
		{
			return Operator<MatType, ScalarType, HilbertSpace>(std::move(mat), internal::serialize(hilbertSpace));
		}

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		auto makeOperator(MatType<ScalarType>&& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace)
		{
			return Operator<MatType, ScalarType, HilbertSpace>(std::move(mat), hilbertSpace);
		}

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		auto makeOperator(const MatType<ScalarType>& mat, const HilbertSpace& hilbertSpace)
		{
			return Operator<MatType, ScalarType, HilbertSpace>(mat, internal::serialize(hilbertSpace));
		}

		template<template<class> class MatType, typename ScalarType, class HilbertSpace>
		auto makeOperator(const MatType<ScalarType>& mat, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace)
		{
			return Operator<MatType, ScalarType, HilbertSpace>(mat, hilbertSpace);
		}
	}
}


namespace qengine
{
	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	std::ostream& operator<<(std::ostream& stream, const Operator<MatType, ScalarType, HilbertSpace>& op);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class HilbertSpace>
	bool operator==(const Operator<M1, N1, HilbertSpace>& left, const Operator<M2, N2, HilbertSpace>& right);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class HilbertSpace>
	bool operator!=(const Operator<M1, N1, HilbertSpace>& left, const Operator<M2, N2, HilbertSpace>& right);

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	State<HilbertSpace> operator*(const  Operator<MatType, ScalarType, HilbertSpace>& op, State<HilbertSpace> s);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class HilbertSpace>
	auto operator+(const  Operator<M1, N1, HilbertSpace>& left, const  Operator<M2, N2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "");
		return internal::makeOperator(left._mat() + right._mat(), left._hilbertSpace());
	}

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class HilbertSpace>
	auto operator-(const  Operator<M1, N1, HilbertSpace>& left, const  Operator<M2, N2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "");
		return internal::makeOperator(left._mat() - right._mat(), left._hilbertSpace());
	}

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class HilbertSpace>
	auto operator*(const  Operator<M1, N1, HilbertSpace>& left, const  Operator<M2, N2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "");
		return internal::makeOperator(left._mat()*right._mat(), left._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator*(const N1 left, const  Operator<M, N2, HilbertSpace>& right)
	{
		return internal::makeOperator(left*right._mat(), right._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator/(const  Operator<M, N1, HilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() / right, left._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator+(const  Operator<M, N1, HilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() + right, left._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator+(const N1 left, const  Operator<M, N2, HilbertSpace>& right)
	{
		return internal::makeOperator(left + right._mat(), right._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator-(const  Operator<M, N1, HilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() - right, left._hilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class HilbertSpace>
	auto operator-(const N1 left, const  Operator<M, N2, HilbertSpace>& right)
	{
		return internal::makeOperator(left - right._mat(), right._hilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType, class HilbertSpace>
	auto exp(const Operator<MatType, ScalarType, HilbertSpace>& exponent)
	{
		return internal::makeOperator(exp(exponent._mat()), exponent._hilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType1, typename ScalarType2, class HilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const  Operator<MatType, ScalarType1, HilbertSpace>& op, const Vector<ScalarType2>& s);

	template<template<class> class MatType, typename ScalarType1, typename ScalarType2, class HilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const Vector<ScalarType2>& s, const  Operator<MatType, ScalarType1, HilbertSpace>& op);
}
