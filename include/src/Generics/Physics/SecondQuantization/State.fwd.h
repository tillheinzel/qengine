﻿/* COPYRIGHT
 *
 * file="State.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/extract_hilbertspace.h"
#include "src/Generics/Physics/General/isState.h"

namespace qengine
{
	template<class HilbertSpace>
	class State;
}

namespace qengine
{
	namespace internal
	{
		template<class HilbertSpace>
		struct hasStateLabel<State<HilbertSpace>, HilbertSpace>: std::true_type{};
	}
}

namespace qengine
{
	namespace internal
	{
		// this is the general case for Functions of x, but it may be further specialized by e.g. functions of x that live on helper-hilbertspaces
		template<class HilbertSpace>
		struct extract_hilbertspace<State<HilbertSpace>>
		{
			using type = HilbertSpace;
		};
	}
}
