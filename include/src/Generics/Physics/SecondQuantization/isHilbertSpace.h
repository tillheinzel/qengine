﻿/* COPYRIGHT
 *
 * file="isHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Utility/has_function.h"

namespace qengine
{
	namespace second_quantized
	{
		template<class T, class SFINAE = void> struct hasHilbertSpaceLabel: std::false_type{};

		// check if type can be considered Hilbertspace by internal functionality.
		template<class T>
		struct isHilbertSpace: hasHilbertSpaceLabel<T>{};

		template<class T> constexpr bool isHilbertSpace_v = isHilbertSpace<T>::value;
	}
}
