﻿/* COPYRIGHT
 *
 * file="Spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Utility/LinearAlgebra/Spectrum.h"

#include "src/Generics/Physics/General/SerializedHilbertSpace.h"

#include "src/Generics/Physics/SecondQuantization/Spectrum.fwd.h"
#include "src/Generics/Physics/SecondQuantization/State.fwd.h"

namespace qengine
{
	namespace second_quantized
	{
		template<class HilbertSpace>
		class Spectrum : public internal::Spectrum<complex>
		{
		public:
			template<class T1>
			Spectrum(T1&& t1, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace) :
				internal::Spectrum<complex>(std::forward<T1>(t1), 1),
				hilbertSpace_(hilbertSpace)
			{
			}

			Spectrum(internal::Spectrum<complex>&& t1, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);

			Spectrum(const internal::Spectrum<complex>& t1, const internal::SerializedHilbertSpace<HilbertSpace>& hilbertSpace);

			State<HilbertSpace> eigenState(count_t i) const;

			State<HilbertSpace> makeLinearCombination(const CVec& factors, const qengine::NORMALIZATION shouldNormalize = NORMALIZE_NOT) const;

			/// INTERNAL USE ONLY
		public:
			internal::SerializedHilbertSpace<HilbertSpace> _hilbertSpace() const;

		private:
			internal::SerializedHilbertSpace<HilbertSpace> hilbertSpace_;
		};
	}
}
