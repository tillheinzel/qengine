﻿/* COPYRIGHT
 *
 * file="isState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/Utility/Types.h"

namespace qengine
{
	namespace internal
	{
		template<class Psi, class HilbertSpace, class SFINAE = void> struct hasStateLabel : std::false_type {};
		template<class Psi, class HilbertSpace> constexpr bool hasStateLabel_v = hasStateLabel<Psi, HilbertSpace>::value;

		template<class Psi, class HilbertSpace, class SFINAE = void>
		struct isState : std::false_type {};

		template<class Psi, class HilbertSpace> constexpr bool isState_v = isState<Psi, HilbertSpace>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		// A state has to be labelled as a state AND has to have an inner product defined, in the form of the 'overlap' function
		template<class Psi, class HilbertSpace>
		struct isState<Psi, HilbertSpace, std::enable_if_t<
			hasStateLabel_v<Psi, HilbertSpace> &&
			std::is_same<complex, decltype(overlap(std::declval<Psi>(), std::declval<Psi>()))>::value
		,void>> : std::true_type{};
	}
}
