﻿/* COPYRIGHT
 *
 * file="SerializedHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	namespace internal
	{
		template<class HilbertSpace>
		struct SerializedHilbertSpace;

		template<class HilbertSpace>
		SerializedHilbertSpace<HilbertSpace> serialize(const HilbertSpace& hs)
		{
			return SerializedHilbertSpace<HilbertSpace>(hs);
		}

		template<class HilbertSpace>
		HilbertSpace deserialize(const SerializedHilbertSpace<HilbertSpace>& hs)
		{
			return HilbertSpace(hs);
		}
	}
}
