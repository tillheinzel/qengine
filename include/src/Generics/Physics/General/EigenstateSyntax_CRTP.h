﻿/* COPYRIGHT
 *
 * file="EigenstateSyntax_CRTP.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.fwd.h"
#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.fwd.h"

#include "src/Utility/Types.h"

namespace qengine
{
	namespace internal
	{
		template<class Impl>
		class EigenstateSyntax_CRTP
		{
		public:
			LinearCombinationOfEigenstates<Impl> operator[](count_t index) const;

		protected:
			EigenstateSyntax_CRTP() noexcept {}
		};

	}
}
