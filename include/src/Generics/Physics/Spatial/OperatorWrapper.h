﻿/* COPYRIGHT
 *
 * file="OperatorWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/isSqMat.h"

#include "src/Generics/Physics/Spatial/isHilbertSpace.h"
#include "src/Generics/Physics/Spatial/Spectrum.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace>
		class IOperatorWrapper
		{
			static_assert(internal::isHilbertSpace_v<HilbertSpace>, "");
		public:
			virtual ~IOperatorWrapper() = default;

			virtual void applyInPlace(FunctionOfX<complex, HilbertSpace>& wf) const = 0;
		};

		template<class HilbertSpace>
		FunctionOfX<complex, HilbertSpace> operator*(const IOperatorWrapper<HilbertSpace>& op, FunctionOfX<complex, HilbertSpace> psi)
		{
			op.applyInPlace(psi);
			return psi;
		}
	}

	namespace internal
	{
		template<class HilbertSpace>
		struct hasOperatorLabel<spatial::IOperatorWrapper<HilbertSpace>> : std::true_type{};

		template<class HilbertSpace>
		struct hasLinearLabel<spatial::IOperatorWrapper<HilbertSpace>> : std::true_type{};
	}
}


namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace, template<class> class MatType, class ScalarType>
		class OperatorWrapper : public IOperatorWrapper<HilbertSpace>
		{
			static_assert(internal::isHilbertSpace_v<HilbertSpace>, "");
			static_assert(internal::is_SqMat_v<MatType>, "");

		public:
			OperatorWrapper(HilbertSpace hilbertSpace, MatType<ScalarType> mat) :
				IOperatorWrapper<HilbertSpace>(),
				hilbertSpace_(std::move(hilbertSpace)),
				mat_(std::move(mat))
			{}

			void applyInPlace(FunctionOfX<complex, HilbertSpace>& wf) const override
			{
				mat_.multiplyInPlace(wf.vec());
			}

			template<class... Ts>
			Spectrum<complex, HilbertSpace> makeSpectrum(count_t largestEigenstate, Ts... ts) const
			{
				return { internal::getSpectrum(mat_, largestEigenstate, hilbertSpace_.normalization, std::move(ts)...), hilbertSpace_ };
			}

		private:
			HilbertSpace hilbertSpace_;
			MatType<ScalarType> mat_;
		};
		template<class HilbertSpace, template<class> class MatType, class ScalarType>
		FunctionOfX<complex, HilbertSpace> operator*(const OperatorWrapper<HilbertSpace, MatType, ScalarType>& op, FunctionOfX<complex, HilbertSpace> psi)
		{
			op.applyInPlace(psi);
			return psi;
		}
	}


	namespace internal
	{
		template<class HilbertSpace, template<class> class MatType, class ScalarType>
		struct hasOperatorLabel<spatial::OperatorWrapper<HilbertSpace, MatType, ScalarType>> : std::true_type {};

		template<class HilbertSpace, template<class> class MatType, class ScalarType>
		struct hasLinearLabel<spatial::OperatorWrapper<HilbertSpace, MatType, ScalarType>> : std::true_type {};
	}
}

namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace, class ScalarType>
		auto makeOperatorWrapper(FunctionOfX<ScalarType, HilbertSpace> diag)
		{
			return OperatorWrapper<HilbertSpace, internal::SqDiagMat, ScalarType>(diag._hilbertSpace(), internal::SqDiagMat<ScalarType>(diag.vec()));
		}

		template<class HilbertSpace, template<class> class MatType, class ScalarType, class = std::enable_if_t<internal::isHilbertSpace_v<HilbertSpace>>>
		auto makeOperatorWrapper(HilbertSpace hs, MatType<ScalarType> mat)
		{
			return OperatorWrapper<HilbertSpace, MatType, ScalarType>(std::move(hs), std::move(mat));
		}

		template<class HilbertSpace, template<class> class MatType, class ScalarType, class = std::enable_if_t<!internal::isHilbertSpace_v<HilbertSpace>>>
		auto makeOperatorWrapper(const HilbertSpace& hs, MatType<ScalarType> mat)
		{
			return makeOperatorWrapper(hs._hilbertSpace(), std::move(mat));
		}
	}
}
