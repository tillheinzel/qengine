﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/extract_hilbertspace.h"
#include "src/Generics/Physics/Spatial/isHamiltonian.h"

namespace qengine
{
	template<class HilbertSpace, template<class> class Rep>
	class LinearHamiltonian;
}

namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace, template<class> class Rep>
		struct hasHamiltonianLabel<LinearHamiltonian<HilbertSpace, Rep>> : std::true_type {};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class HilbertSpace, template<class> class Rep>
		struct hasLinearLabel<LinearHamiltonian<HilbertSpace, Rep>> : std::true_type {};

		template<class HilbertSpace, template<class> class Rep>
		struct hasOperatorLabel<LinearHamiltonian<HilbertSpace, Rep>> : std::true_type{};
		
		template<class HilbertSpace, template<class> class Rep>
		struct hasGuaranteedHermitianLabel<LinearHamiltonian<HilbertSpace, Rep>> : std::true_type{};

		template<class HilbertSpace, template<class> class Rep>
		struct extract_hilbertspace<LinearHamiltonian<HilbertSpace, Rep>>
		{
			using type = HilbertSpace;
		};
	}
}
