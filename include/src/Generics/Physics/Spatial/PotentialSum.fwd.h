﻿/* COPYRIGHT
 *
 * file="PotentialSum.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/Generics/Physics/General/isOperator.h"

#include "src/Generics/Physics/Spatial/isPotential.h"

namespace qengine
{
	template<class... Vs>
	class PotentialSum;
}

namespace qengine
{
	namespace spatial
	{
		template<class... Vs>
		struct hasPotentialLabel<PotentialSum<Vs...>> : std::true_type{};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class... Vs>
		struct hasOperatorLabel<PotentialSum<Vs...>> : std::true_type{};

		//template<class... Vs> // is guaranteed hermitian if all potentials are guaranteed hermitian
		//struct hasGuaranteedHermitianLabel<PotentialSum<Vs...>, 
		//std::enable_if_t<internal::and_all(hasGuaranteedHermitianLabel_v<Vs>...)>> : std::true_type {};

		//template<class... Vs> // is linear if all potentials are linear
		//struct hasLinearLabel<PotentialSum<Vs...>, std::enable_if_t<internal::and_all(hasLinearLabel_v<Vs>...)>> : std::true_type {};
	}
}
