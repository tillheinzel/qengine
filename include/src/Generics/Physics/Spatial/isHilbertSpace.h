﻿/* COPYRIGHT
 *
 * file="isHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Utility/has_function.h"

namespace qengine
{
	namespace internal
	{
		MAKE_HAS_VAR(hasVar_dim, dim);
		MAKE_HAS_VAR(hasVar_normalization, normalization);
		MAKE_HAS_VAR(hasVar_integrationConstant, integrationConstant);
		MAKE_HAS_VAR(hasVar_dimensions, dimensions);

		MAKE_HAS_STATIC_VAR(hasStatVar_nDimensions, nDimensions);

		// check if type can be considered Hilbertspace by internal functionality.
		template<class T>
		struct isHilbertSpace
		{
			constexpr static bool value = hasVar_dim_v<T, const count_t> &&
				hasVar_normalization_v<T, const real> &&
				hasVar_integrationConstant_v<T, const real> &&
				hasVar_dimensions_v<T, const std::vector<count_t>> &&
				hasStatVar_nDimensions_v<T, const count_t>;
		};

		template<class T> constexpr bool isHilbertSpace_v = isHilbertSpace<T>::value;
	}
}
