﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/Spatial/LinearHamiltonian.h"

#include "src/Generics/Physics/Spatial/Spectrum.h"
#include "src/Utility/ExtraDebugChecks.h"

namespace qengine
{
	template<class HilbertSpace, template<class> class Rep>
	template <class Potential, class Representation, typename >
	LinearHamiltonian<HilbertSpace, Rep>::LinearHamiltonian(const HilbertSpace& hilbertSpace, Representation&& representation, Potential&& potential) :
		potential_(std::forward<Potential>(potential)),
		hamiltonianMatrix_(representation),
		hilbertSpace_(hilbertSpace)
	{
		qengine_assert(!hamiltonianMatrix_.hasNan(), "Hamiltonian has NaN");
	}

	template<class HilbertSpace, template<class> class Rep>
	template <typename ScalarType>
	void LinearHamiltonian<HilbertSpace, Rep>::applyInPlace(FunctionOfX<ScalarType, HilbertSpace>& f) const
	{
		qengine_assert(f._hilbertSpace() == hilbertSpace_, "wrong hilbertspace!");
		hamiltonianMatrix_.multiplyInPlace(f.vec());
	}

	template<class HilbertSpace, template<class> class Rep>
	spatial::Spectrum<real, HilbertSpace> LinearHamiltonian<HilbertSpace, Rep>::makeSpectrum(const count_t largestEigenvalue) const
	{
		return spatial::Spectrum<real, HilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, hilbertSpace_.normalization), hilbertSpace_);
	}

	template<class HilbertSpace, template<class> class Rep>
	std::ostream& operator<<(std::ostream& s, const LinearHamiltonian<HilbertSpace, Rep>& H)
	{
		s << H._hamiltonianMatrix();
		return s;
	}

}
