﻿/* COPYRIGHT
 *
 * file="FunctionOfX.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/Spatial/FunctionOfX.h"

#include "src/Utility/cpp17Replacements/constexpr_if.h"

#include <cassert>

#include "src/Utility/ExtraDebugChecks.h"

namespace qengine
{
	template <class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace>::FunctionOfX(const Vector<ScalarType>& vec,
		const HilbertSpace&
		hilbertSpace) : vec_(vec), hilbertSpace_(hilbertSpace)
	{
		qengine_assert(vec_.size() == hilbertSpace_.dim, "vector must have dimension of HilbertSpace");
		qengine_assert(!vec_.hasNan(), "functionOfX has NaN");
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace>::FunctionOfX(Vector<ScalarType>&& vec,
		const HilbertSpace&
		hilbertSpace) : vec_(std::move(vec)), hilbertSpace_(hilbertSpace)
	{
		qengine_assert(vec_.size() == hilbertSpace_.dim, "vector must have dimension of HilbertSpace");
		qengine_assert(!vec_.hasNan(), "functionOfX has NaN");
	}

	template<class ScalarType, class HilbertSpace>
	template <typename T2, typename T, typename>
	FunctionOfX<ScalarType, HilbertSpace>::FunctionOfX(const FunctionOfX<T, HilbertSpace>& other) : FunctionOfX(Vector<complex>(other.vec()), other._hilbertSpace())
	{
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace>& FunctionOfX<ScalarType, HilbertSpace>::operator=(const FunctionOfX& other)
	{
		if (&other != this)
		{
			qengine_assert(other._hilbertSpace() == this->_hilbertSpace(), "trying to assign FunctionOfX with different hilbertSpaces!");
			vec_ = other.vec_;
		}
		return *this;
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace>& FunctionOfX<ScalarType, HilbertSpace>::operator=(FunctionOfX&& other) noexcept
	{
		if (&other != this)
		{
            assert(other._hilbertSpace() == this->_hilbertSpace());
			vec_ = std::move(other.vec_);
		}
		return *this;
	}

	template<class ScalarType, class HilbertSpace>
	template <typename T2, typename T>
	FunctionOfX<ScalarType, HilbertSpace>& FunctionOfX<ScalarType, HilbertSpace>::operator=(const FunctionOfX<T, HilbertSpace>& other)
	{
		qengine_assert(other._hilbertSpace() == this->_hilbertSpace(), "trying to assign FunctionOfX with different dx!");

		vec_ = other.vec();
		return *this;
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> FunctionOfX<ScalarType, HilbertSpace>::operator-() const
	{
		return FunctionOfX<ScalarType, HilbertSpace>(-vec_, _hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	void FunctionOfX<ScalarType, HilbertSpace>::normalize()
	{
		vec_ *= hilbertSpace_.normalization / vec_.norm();
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> FunctionOfX<ScalarType, HilbertSpace>::absSquare() const
	{
		return FunctionOfX<real, HilbertSpace>(vec_.absSquare(), hilbertSpace_);
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> FunctionOfX<ScalarType, HilbertSpace>::re() const
	{
		const auto& it = *this;
		return internal::constexpr_if(std::is_same<ScalarType, real>{},
			[&it](auto _) {return _(it); },
			[&it](auto _) {return FunctionOfX<real, HilbertSpace>(_(it.vec().re()), it._hilbertSpace()); }
		);
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> FunctionOfX<ScalarType, HilbertSpace>::im() const
	{
		const auto& it = *this;

		return internal::constexpr_if(std::is_same<ScalarType, real>{},
			[&it](auto _) {return _(0.0*it); },
			[&it](auto _) {return FunctionOfX<real, HilbertSpace>(it.vec().im(), _(it._hilbertSpace())); }
		);
	}

	template <class HilbertSpace>
	FunctionOfX<complex, HilbertSpace> makeFunctionOfX(const FunctionOfX<real, HilbertSpace>& re,
		const FunctionOfX<real, HilbertSpace>& im)
	{
		qengine_assert(re._hilbertSpace() == im._hilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<complex, HilbertSpace>{Vector<complex>(re.vec(), im.vec()), re._hilbertSpace()};
	}


	template<class ScalarType, class HilbertSpace>
	real FunctionOfX<ScalarType, HilbertSpace>::norm() const
	{
		return vec_.norm() / hilbertSpace_.normalization;
	}


}

namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> normalize(FunctionOfX<ScalarType, HilbertSpace> f)
	{
		f.normalize();
		return f;
	}

	template<class ScalarType, class HilbertSpace>
	real norm(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return f.norm();
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> absSquare(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return f.absSquare();
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> re(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return f.re();
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> im(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return f.im();
	}

	template<class HilbertSpace>
	FunctionOfX<complex, HilbertSpace> toComplex(const FunctionOfX<real, HilbertSpace>& f)
	{
		return f;
	}
	template<class HilbertSpace>
	FunctionOfX<complex, HilbertSpace> toComplex(const FunctionOfX<complex, HilbertSpace>& f)
	{
		return f;
	}


	template <typename number1, typename number2, class HilbertSpace>
	complex overlap(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2)
	{
		return integrate(conj(f1)*f2);
	}

	template <typename number1, typename number2, class HilbertSpace>
	real fidelity(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2)
	{
		return std::norm(overlap(f1, f2));
	}

	template <typename number1, typename number2, class HilbertSpace>
	real infidelity(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2)
	{
		return 1.0 - fidelity(f1, f2);
	}

}
namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	std::ostream& operator<<(std::ostream& s, const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		s << f.vec();
		return s;
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace>(left.vec() + right.vec(), left._hilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator-(const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace>(left.vec() - right.vec(), left._hilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator*(const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace>(left.vec() * right.vec(), left._hilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator/(const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right)
	{
		qengine_assert(left._hilbertSpace() == right._hilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace>(left.vec() / right.vec(), left._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return sum(f.vec())*f._hilbertSpace().integrationConstant;
	}

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> conj(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return internal::constexpr_if(std::is_same<ScalarType, real>{},
			[&f](auto _) {return _(f); },
			[&f](auto _) {return _(FunctionOfX<complex, HilbertSpace>(conj(f.vec()), f._hilbertSpace())); }
		);
	}

	template<class ScalarType, class HilbertSpace>
	ScalarType min(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return min(f.vec());
	}

	template<class ScalarType, class HilbertSpace>
	ScalarType max(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return max(f.vec());
	}

	template<class ScalarType, class HilbertSpace>
	ScalarType mean(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return mean(f.vec());
	}
}


namespace qengine
{

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(exp(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> pow(const FunctionOfX<ScalarType, HilbertSpace>& f, count_t exponent)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(pow(f.vec(), exponent), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp2(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(exp2(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp10(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(exp10(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(log(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log2(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(log2(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log10(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(log10(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sqrt(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(sqrt(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> abs(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<real, HilbertSpace>(abs(f.vec()), f._hilbertSpace());
	}
}


namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sin(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(sin(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> cos(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(cos(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> tan(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(tan(f.vec()), f._hilbertSpace());
	}

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> asin(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(asin(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> acos(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(acos(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> atan(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(atan(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sinh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(sinh(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> cosh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(cosh(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> tanh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(tanh(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> asinh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(asinh(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> acosh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(acosh(f.vec()), f._hilbertSpace());
	}
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> atanh(const FunctionOfX<ScalarType, HilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(atanh(f.vec()), f._hilbertSpace());
	}

}

namespace qengine
{
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> linspace(const real lower, const real upper, const count_t numberOfPoints, const HilbertSpace& hilbertSpace)
	{
		return FunctionOfX<real, HilbertSpace>(qengine::linspace(lower, upper, numberOfPoints), hilbertSpace);
	}

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> step(const FunctionOfX<real, HilbertSpace>& x, const real stepPos)
	{
		return  FunctionOfX<real, HilbertSpace>(step(x.vec(), stepPos), x._hilbertSpace());
	}

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> box(const FunctionOfX<real, HilbertSpace>& x, const real left, const real right)
	{
		return  FunctionOfX<real, HilbertSpace>(box(x.vec(), left, right), x._hilbertSpace());
	}

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> well(const FunctionOfX<real, HilbertSpace>& x, const real left, const real right)
	{
		return  FunctionOfX<real, HilbertSpace>(well(x.vec(), left, right), x._hilbertSpace());
	}

	// s for "sigmoid" or "soft", whatever you prefer
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> sstep(const FunctionOfX<real, HilbertSpace>& x, const real center, const real hardness)
	{
		return  FunctionOfX<real, HilbertSpace>(sstep(x.vec(), center, hardness), x._hilbertSpace());
	}

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> sbox(const FunctionOfX<real, HilbertSpace>& x, const real left, const real right, const real hardness)
	{
		return  FunctionOfX<real, HilbertSpace>(sbox(x.vec(), left, right, hardness), x._hilbertSpace());
	}

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> swell(const FunctionOfX<real, HilbertSpace>& x, const real left, const real right, const real hardness)
	{
		return  FunctionOfX<real, HilbertSpace>(swell(x.vec(), left, right, hardness), x._hilbertSpace());
	}
}
