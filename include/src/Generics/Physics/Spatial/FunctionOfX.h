﻿/* COPYRIGHT
 *
 * file="FunctionOfX.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Generics/Physics/Spatial/isHilbertSpace.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.fwd.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.traits.h"

namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	class FunctionOfX
	{
		static_assert(internal::isHilbertSpace_v<HilbertSpace>, "");

		/// CTOR DTOR
	public:
		FunctionOfX(const Vector<ScalarType>& vec, const HilbertSpace& hilbertSpace);
		FunctionOfX(Vector<ScalarType>&& vec, const HilbertSpace& hilbertSpace);

		FunctionOfX(const FunctionOfX& other) = default;
        FunctionOfX(FunctionOfX&& other) = default;

		template<typename T2 = ScalarType, typename T, typename SFINAE = std::enable_if_t<std::is_same<T2, complex>::value && std::is_same<T, real>::value>>
		FunctionOfX(const FunctionOfX<T, HilbertSpace>& other);

		~FunctionOfX() = default;
		/// ASSIGNMENT
	public:
		FunctionOfX & operator=(const FunctionOfX& other);
		FunctionOfX& operator=(FunctionOfX&& other) noexcept;

		template<class T2 = ScalarType, class T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		FunctionOfX& operator=(const FunctionOfX<T, HilbertSpace>& other);

		/// MISC
	public:
		FunctionOfX operator- () const;

		void normalize();
		real norm() const;

		FunctionOfX<real, HilbertSpace> absSquare() const; /// return |this|^2 
		FunctionOfX<real, HilbertSpace> re() const;
		FunctionOfX<real, HilbertSpace> im() const;

		Vector<ScalarType>& vec() & noexcept { return vec_; }
		const Vector<ScalarType>& vec() const& noexcept { return vec_; }
		Vector<ScalarType>&& vec() && noexcept {return std::move(vec_); }

		/// INTERNAL USE
	public:

		const auto& _rep() const { return vec_; }

		const HilbertSpace& _hilbertSpace() const noexcept { return hilbertSpace_; }
	private:
		Vector<ScalarType> vec_;
		HilbertSpace hilbertSpace_;
	};

	template<class ScalarType, class HilbertSpace, typename = std::enable_if_t<internal::isHilbertSpace_v<HilbertSpace>>>
	auto makeFunctionOfX(const HilbertSpace& hs, Vector<ScalarType>&& vec)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(std::move(vec), hs);
	}

	template<class ScalarType, class HilbertSpace, typename = std::enable_if_t<internal::isHilbertSpace_v<HilbertSpace>>>
	auto makeFunctionOfX(const HilbertSpace& hs, const Vector<ScalarType>& vec)
	{
		return FunctionOfX<ScalarType, HilbertSpace>(vec, hs);
	}

	template<template<class> class Vector, class HilbertSpace, typename = std::enable_if_t<internal::isHilbertSpace_v<HilbertSpace>>>
	FunctionOfX<complex, HilbertSpace> makeFunctionOfX(const FunctionOfX<real, HilbertSpace>& re, const FunctionOfX<real, HilbertSpace>& im);
}

namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> normalize(FunctionOfX<ScalarType, HilbertSpace> f);

	template<class ScalarType, class HilbertSpace>
	real norm(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> absSquare(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> re(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template <class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> im(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class HilbertSpace>
	FunctionOfX<complex, HilbertSpace> toComplex(const FunctionOfX<real, HilbertSpace>& f);
	template<class HilbertSpace>
	FunctionOfX<complex, HilbertSpace> toComplex(const FunctionOfX<complex, HilbertSpace>& f);


	template <typename number1, typename number2, class HilbertSpace>
	complex overlap(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2);

	template <typename number1, typename number2, class HilbertSpace>
	real fidelity(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2);

	template <typename number1, typename number2, class HilbertSpace>
	real infidelity(const FunctionOfX<number1, HilbertSpace>& f1, const FunctionOfX<number2, HilbertSpace>& f2);

}


namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	std::ostream& operator<< (std::ostream& s, const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+(const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator- (const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator* (const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class HilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator/ (const FunctionOfX<ScalarType1, HilbertSpace>& left, const FunctionOfX<ScalarType2, HilbertSpace>& right);

	template<class ScalarType, class HilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, HilbertSpace>& f, count_t i_from, count_t i_to);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> diff(const FunctionOfX<ScalarType, HilbertSpace>& f, count_t order = 1);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> conj(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	ScalarType min(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	ScalarType max(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	ScalarType mean(const FunctionOfX<ScalarType, HilbertSpace>& f);
}


namespace qengine
{
	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+ (const FunctionOfX<ScalarType1, HilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._hilbertSpace(),f.vec() + a);
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator+ (ScalarType2 a, const FunctionOfX<ScalarType1, HilbertSpace>& f)
	{
		return makeFunctionOfX( f._hilbertSpace(),a + f.vec());
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator- (const FunctionOfX<ScalarType1, HilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._hilbertSpace(),f.vec() - a);
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator- (ScalarType2 a, const FunctionOfX<ScalarType1, HilbertSpace>& f)
	{
		return makeFunctionOfX( f._hilbertSpace(),a - f.vec());
	}
	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator* (const FunctionOfX<ScalarType1, HilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._hilbertSpace(),f.vec() * a);
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator* (ScalarType2 a, const FunctionOfX<ScalarType1, HilbertSpace>& f)
	{
		return makeFunctionOfX( f._hilbertSpace(),a * f.vec());
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator/ (const FunctionOfX<ScalarType1, HilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._hilbertSpace(),f.vec() / a);
	}

	template<class ScalarType1, class ScalarType2, class HilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, HilbertSpace> operator/ (ScalarType2 a, const FunctionOfX<ScalarType1, HilbertSpace>& f)
	{
		return makeFunctionOfX( f._hilbertSpace(),a / f.vec());
	}
}



namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> pow(const FunctionOfX<ScalarType, HilbertSpace>& f, count_t exponent);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp2(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> exp10(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log2(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> log10(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sqrt(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> abs(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> re(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<real, HilbertSpace> im(const FunctionOfX<ScalarType, HilbertSpace>& f);

}
namespace qengine
{
	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sin(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> cos(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> tan(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> asin(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> acos(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> atan(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> sinh(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> cosh(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> tanh(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> asinh(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> acosh(const FunctionOfX<ScalarType, HilbertSpace>& f);

	template<class ScalarType, class HilbertSpace>
	FunctionOfX<ScalarType, HilbertSpace> atanh(const FunctionOfX<ScalarType, HilbertSpace>& f);
}

//special construction functions
namespace qengine
{
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> linspace(real lower, real upper, count_t numberOfPoints, const HilbertSpace& hilbertSpace);

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> step(const FunctionOfX<real, HilbertSpace>& x, real stepPos);

	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> box(const FunctionOfX<real, HilbertSpace>& x, real left, real right);
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> well(const FunctionOfX<real, HilbertSpace>& x, real left, real right);

	// s for "sigmoid" or "soft", whatever you prefer
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> sstep(const FunctionOfX<real, HilbertSpace>& x, real center, real hardness);
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> sbox(const FunctionOfX<real, HilbertSpace>& x, real left, real right, real hardness);
	template<class HilbertSpace>
	FunctionOfX<real, HilbertSpace> swell(const FunctionOfX<real, HilbertSpace>& x, real left, real right, real hardness);
}
