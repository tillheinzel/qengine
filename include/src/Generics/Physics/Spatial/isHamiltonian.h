﻿/* COPYRIGHT
 *
 * file="isHamiltonian.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Generics/Physics/General/isOperator.h"

namespace qengine
{
	namespace spatial
	{
		template<class H, class SFINAE = void> struct hasHamiltonianLabel : public std::false_type {};
		template<class H> constexpr bool hasHamiltonianLabel_v = hasHamiltonianLabel<H>::value;
	}
}

namespace qengine
{
	namespace spatial
	{	
		template<class H, class SFINAE = void> struct isHamiltonian: public std::false_type {};
		template<class H> constexpr bool isHamiltonian_v = isHamiltonian<H>::value;

		template<class, class = void> struct isLinearHamiltonian: public std::false_type{};
		template<class H> struct isLinearHamiltonian<H, std::enable_if_t<isHamiltonian_v<H> && internal::isLinear_v<H>>>: public std::true_type{};
		template<class T> constexpr bool isLinearHamiltonian_v = isLinearHamiltonian<T>::value;
	}
}

namespace qengine
{
	namespace spatial
	{	
		template<class H>
		struct isHamiltonian<H, std::enable_if_t<	
			hasHamiltonianLabel_v<H>
		>>: std::true_type{};
	}
}
