﻿/* COPYRIGHT
 *
 * file="Spectrum.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/Spatial/Spectrum.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace spatial {
		template <class ScalarType, class HilbertSpace>
		template <class T1>
		Spectrum<ScalarType, HilbertSpace>::Spectrum(T1&& t1, const HilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(std::forward<T1>(t1), hilbertSpace.normalization),
			hilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class HilbertSpace>
		Spectrum<ScalarType, HilbertSpace>::Spectrum(internal::Spectrum<ScalarType>&& t1, const HilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(std::move(t1)),
			hilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class HilbertSpace>
		Spectrum<ScalarType, HilbertSpace>::Spectrum(const internal::Spectrum<ScalarType>& t1, const HilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(t1),
			hilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class HilbertSpace>
		FunctionOfX<complex, HilbertSpace> Spectrum<ScalarType, HilbertSpace>::eigenFunction(count_t i) const
		{
			return FunctionOfX<complex, HilbertSpace>(internal::Spectrum<ScalarType>::eigenvector(i), hilbertSpace_);
		}

		template <class ScalarType, class HilbertSpace>
		FunctionOfX<complex, HilbertSpace> Spectrum<ScalarType, HilbertSpace>::makeLinearCombination(const CVec& factors) const
		{
			return FunctionOfX<complex, HilbertSpace>(internal::Spectrum<ScalarType>::makeLinearCombination(factors), hilbertSpace_);
		}
	}
}
