﻿/* COPYRIGHT
 *
 * file="FunctionOfX.traits.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/extract_hilbertspace.h"
#include "src/Generics/Physics/General/isState.h"

#include "src/Generics/Physics/Spatial/isPotential.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.fwd.h"


namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace>
		struct hasPotentialLabel<FunctionOfX<real, HilbertSpace>> : std::true_type {};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class ScalarType, class HilbertSpace>
		struct hasStateLabel<FunctionOfX<ScalarType, HilbertSpace>, HilbertSpace> : std::true_type{};

		template<class ScalarType, class HilbertSpace>
		struct hasOperatorLabel<FunctionOfX<ScalarType, HilbertSpace>> : std::true_type{};

		template<class ScalarType, class HilbertSpace>
		struct hasLinearLabel<FunctionOfX<ScalarType, HilbertSpace>> : std::true_type{};

		template<class HilbertSpace>
		struct hasGuaranteedHermitianLabel<FunctionOfX<real, HilbertSpace>> : std::true_type{};
	}
}
