﻿/* COPYRIGHT
 *
 * file="Basics.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Utility/has_function.h"

#include "src/Generics/Physics/General/LinearCombinationOfEigenstates.h"
#include "src/Generics/Physics/General/extract_hilbertspace.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"

#include "src/DataContainer/SerializedObject.h"

namespace qengine
{
	namespace internal
	{
		MAKE_HAS_FUNCTION_RETURN_QUALIFIED(has_evaluate, evaluate);
	}

	template<class... Ts, class Operator, typename = std::enable_if_t<internal::has_evaluate_v<internal::LinearCombinationOfEigenstates<Operator>, any_return, Ts...>>>
	FunctionOfX<complex, internal::extract_hilbertspace_t<Operator>> makeWavefunction(const internal::LinearCombinationOfEigenstates<Operator>& l, const qengine::NORMALIZATION shouldNormalize, Ts&&... ts)
	{
		auto psi = l.evaluate(std::forward<Ts>(ts)...);
		if (shouldNormalize == NORMALIZE) return normalize(psi);
		return psi;
	}

	template<class... Ts, class Operator, typename = std::enable_if_t<internal::has_evaluate_v<internal::LinearCombinationOfEigenstates<Operator>, any_return, Ts...>>>
	FunctionOfX<complex, internal::extract_hilbertspace_t<Operator>> makeWavefunction(const internal::LinearCombinationOfEigenstates<Operator>& l, Ts&&... ts)
	{
		return l.evaluate(std::forward<Ts>(ts)...);
	}

	template<class HilbertSpace>
	FunctionOfX<complex, internal::extract_hilbertspace_t<HilbertSpace>> makeComplexFunctionOfX(const HilbertSpace& hilbertspace, const CVec& vec)
	{
		return {vec, hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<complex, internal::extract_hilbertspace_t<HilbertSpace>> makeComplexFunctionOfX(const HilbertSpace& hilbertspace, CVec&& vec)
	{
		return {vec, hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<complex, internal::extract_hilbertspace_t<HilbertSpace>> makeComplexFunctionOfX(const HilbertSpace& hilbertspace, const RVec& vec)
	{
		return {CVec(vec), hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<complex, internal::extract_hilbertspace_t<HilbertSpace>> makeComplexFunctionOfX(const HilbertSpace& hilbertspace, RVec&& vec)
	{
		return {CVec(std::move(vec)), hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<complex, internal::extract_hilbertspace_t<HilbertSpace>> makeComplexFunctionOfX(const HilbertSpace& hilbertspace, const SerializedObject& obj)
	{
		return {obj.get<CVec>(), hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<real, internal::extract_hilbertspace_t<HilbertSpace>> makeRealFunctionOfX(const HilbertSpace& hilbertspace, const RVec& vec)
	{
		return {vec, hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<real, internal::extract_hilbertspace_t<HilbertSpace>> makeRealFunctionOfX(const HilbertSpace& hilbertspace, RVec&& vec)
	{
		return {vec, hilbertspace._hilbertSpace()};
	}

	template<class HilbertSpace>
	FunctionOfX<real, internal::extract_hilbertspace_t<HilbertSpace>> makeRealFunctionOfX(const HilbertSpace& hilbertspace, const SerializedObject& obj)
	{
		return { obj.get<RVec>(), hilbertspace._hilbertSpace() };
	}

}
