﻿/* COPYRIGHT
 *
 * file="Spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/Spatial/Spectrum.fwd.h"

#include "src/Utility/LinearAlgebra/Spectrum.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.fwd.h"

namespace qengine
{
	namespace spatial
	{
		template<class ScalarType, class HilbertSpace>
		class Spectrum : public internal::Spectrum<ScalarType>
		{
			static_assert(internal::isHilbertSpace_v<HilbertSpace>, "");
		public:
			template<class T1>
			Spectrum(T1&& t1, const HilbertSpace hilbertSpace);

			Spectrum(internal::Spectrum<ScalarType>&& t1, const HilbertSpace hilbertSpace);

			Spectrum(const internal::Spectrum<ScalarType>& t1, const HilbertSpace hilbertSpace);


			FunctionOfX<complex, HilbertSpace> eigenFunction(count_t i) const;

			FunctionOfX<complex, HilbertSpace> makeLinearCombination(const CVec& factors) const;

		private:
			const HilbertSpace hilbertSpace_;
		};
	}
}
