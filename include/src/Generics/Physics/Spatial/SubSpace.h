﻿/* COPYRIGHT
 *
 * file="SubSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace internal
	{
		class OneDSpace_BaseClass
		{
		public:
			const real xLower;
			const real xUpper;
			const count_t dim;
			const real kinematicFactor;
			const real dx;
			const real integrationConstant;
			const real normalization;
			const std::vector<count_t> dimensions;

			OneDSpace_BaseClass& operator=(const OneDSpace_BaseClass&) = delete;
			OneDSpace_BaseClass& operator=(OneDSpace_BaseClass&&) = delete;

		protected:
			OneDSpace_BaseClass(real xLower, real xUpper, count_t dimension, real kinematicFactor);
			OneDSpace_BaseClass(const OneDSpace_BaseClass& other) = default;
			OneDSpace_BaseClass(OneDSpace_BaseClass&& other) = default;

			~OneDSpace_BaseClass() = default;

		};


		bool isSame(const OneDSpace_BaseClass& left, const OneDSpace_BaseClass& right);

		template<class HilbertSpace, typename = std::enable_if_t<std::is_base_of<OneDSpace_BaseClass, HilbertSpace>::value>>
		FunctionOfX<real, HilbertSpace> makeSpatialDimension(const HilbertSpace& s)
		{
			return FunctionOfX<real, HilbertSpace>(qengine::linspace(s.xLower, s.xUpper, s.dim), s);
		}

	}
}

namespace qengine
{
	namespace internal
	{
		template<count_t FullDim, count_t Index>
		class SubSpace : public OneDSpace_BaseClass
		{
		public:
			SubSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
				OneDSpace_BaseClass(xLower, xUpper, dimension, kinematicFactor)
			{}

			explicit SubSpace(const OneDSpace_BaseClass& other) :
				OneDSpace_BaseClass(other)
			{}

			static constexpr count_t nDimensions = 1;
		};

		template<count_t FullDim, count_t Index>
		bool operator==(const SubSpace<FullDim, Index>& left, const SubSpace<FullDim, Index>& right) { return isSame(left, right); }

		template<count_t FullDim, count_t Index>
		bool operator!=(const SubSpace<FullDim, Index>& left, const SubSpace<FullDim, Index>& right) { return !isSame(left, right); }
/*
		static_assert(hasVar_dim_v<SubSpace<1,1>, const real>, "");
		static_assert(hasVar_integrationConstant_v<SubSpace<1,1>, const real>, "");
		static_assert(isHilbertSpace_v<SubSpace<1,1>>, "");*/
	}
}
