﻿/* COPYRIGHT
 *
 * file="SubSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Generics/Physics/Spatial/SubSpace.h"

#include "src/Utility/ExtraDebugChecks.h"

qengine::internal::OneDSpace_BaseClass::OneDSpace_BaseClass(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
	xLower(xLower),
	xUpper(xUpper),
	dim(dimension),
	kinematicFactor(kinematicFactor),
	dx((xUpper - xLower) / (dimension - 1)),
	integrationConstant(dx), // same as dx. In one dimension we multiply with dx
	normalization(1.0 / std::sqrt(dx)), // 1/sqrt(dx), as integrationconstant in 1d is dx
	dimensions({ dimension })
{
	qengine_assert(xUpper > xLower, "xUpper must be larger than xLower when constructing spatial Hilbert space!");
	qengine_assert(dimension >= 2, "dimension must be larger than or equal 2!");
	qengine_assert(kinematicFactor > 0.0, "kinematicFactor must be positive!");
}

bool qengine::internal::isSame(const OneDSpace_BaseClass& left, const OneDSpace_BaseClass& right)
{
	if (&left == &right) return true;

	return left.dim == right.dim && left.kinematicFactor == right.kinematicFactor && left.xLower == right.xLower && left.xUpper == right.xUpper;
}
