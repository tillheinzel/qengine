﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/Spatial/LinearHamiltonian.fwd.h"

#include <ostream>

#include "src/Utility/Types.h"
#include "src/Utility/nostd/remove_cvref.h"

#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"
#include "src/Generics/Physics/Spatial/Spectrum.fwd.h"

namespace qengine
{
	template<class HilbertSpace, template<class> class Rep>
	class LinearHamiltonian : public internal::EigenstateSyntax_CRTP<LinearHamiltonian<HilbertSpace, Rep>>
	{
		static_assert(internal::isHilbertSpace_v<HilbertSpace>, "");
	public:
		template<class Potential, class Representation, typename = std::enable_if_t<std::is_same<nostd::remove_cvref_t<Potential>, FunctionOfX<real, HilbertSpace>>::value && std::is_same<nostd::remove_cvref_t<Representation>, Rep<real>>::value, void>>
		LinearHamiltonian(const HilbertSpace& hilbertSpace, Representation&& kineticTerm, Potential&& potential);

		template<typename ScalarType>
		void applyInPlace(FunctionOfX<ScalarType, HilbertSpace>& f) const;

		spatial::Spectrum<real, HilbertSpace> makeSpectrum(count_t largestEigenvalue) const;

		/// INTERNAL USE
	public:
		const auto& _potential() const noexcept { return potential_; }
		const Rep<real>& _hamiltonianMatrix() const noexcept { return hamiltonianMatrix_; }
		const auto& _hilbertSpace() const noexcept { return hilbertSpace_; }


	private:
		FunctionOfX<real, HilbertSpace> potential_;
		Rep<real> hamiltonianMatrix_;
		const HilbertSpace hilbertSpace_;
	};
	template<class HilbertSpace, template<class> class Rep>
	std::ostream& operator<<(std::ostream& s, const LinearHamiltonian<HilbertSpace, Rep>& H);
}

namespace qengine
{
	template<class ScalarType, class HilbertSpace, template<class> class Rep>
	FunctionOfX<ScalarType, HilbertSpace> operator*(const LinearHamiltonian<HilbertSpace, Rep>& H, FunctionOfX<ScalarType, HilbertSpace> psi)
	{
		H.applyInPlace(psi);
		return psi;
	}
}
