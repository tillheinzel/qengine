﻿/* COPYRIGHT
 *
 * file="MultiDimensionalSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>
#include <algorithm>

#include "src/Utility/Types.h"
#include "src/Utility/TypeList.h"
#include "src/Utility/ExtraDebugChecks.h"

#include "src/Utility/cpp17Replacements/constexpr_if.h"
#include "src/Utility/cpp17Replacements/and_all.h"
#include "src/Utility/nostd/bool_constant.h"

#include "src/Generics/Physics/Spatial/SubSpace.h"

namespace qengine
{
	namespace internal
	{
		template<count_t Dim, count_t... Indices>
		class MultiDimensionalSpace
		{
		public:
			MultiDimensionalSpace(std::tuple<SubSpace<Dim, Indices>...>&& subspaces) :
				dim(dimProduct(subspaces)),
				integrationConstant(integrationConstantsProduct(subspaces)),
				normalization(normalizationProduct(subspaces)),
				dimensions({ std::get<findTupleIndex<Indices>()>(subspaces).dim... }),
				subspaces_(std::move(subspaces))
			{
			}

			const count_t dim;
			const real integrationConstant;
			const real normalization;
			const std::vector<count_t> dimensions;

			static constexpr count_t nDimensions = sizeof...(Indices);

			template<count_t Index>
			const SubSpace<Dim, Index>& get() const
			{
				return std::get<findTupleIndex<Index>()>(subspaces_);
			}

			count_t linearIndex(const std::vector<count_t>& indices) const
			{
				qengine_assert(indices.size() == sizeof...(Indices), "wrong number of indices!");

				auto it = indices.rbegin();
				auto dimIt = dimensions.rbegin();

				count_t index = *it;
				++it;
				count_t dimProd = 1;

				for (; it != indices.rend(); ++it, ++dimIt)
				{
					dimProd *= *dimIt;
					index += dimProd * (*it);
				}
				return index;
			}

		private:
			std::tuple<SubSpace<Dim, Indices>...> subspaces_;

			template<count_t Index> constexpr static std::size_t findTupleIndex()
			{
				return qengine::internal::findFirstIndex<SubSpace<Dim, Index>>(qengine::internal::TypeList<SubSpace<Dim, Indices>...>());
			}

			//template<class NumberType, class Getter>
			//constexpr NumberType multiProduct(const std::tuple<internal::SubSpace<Dim, Indices>...>& subspaces, Getter getter)
			//{
			//	NumberType product = 1;
			//	(void)std::initializer_list<int>{ (product *= getter(std::get<findTupleIndex<Indices>()>(subspaces)), 0)... };
			//	return product;
			//}

			constexpr static count_t dimProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				count_t product = 1;
				(void)std::initializer_list<int>{ (product *= std::get<findTupleIndex<Indices>()>(subspaces).dim, 0)... };
				return product;
			}
			constexpr static real normalizationProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				real product = 1;
				(void)std::initializer_list<int>{ (product *= std::get<findTupleIndex<Indices>()>(subspaces).normalization, 0)... };
				return product;
			}
			constexpr static real integrationConstantsProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				real product = 1;
				(void)std::initializer_list<int>{ (product *= std::get<findTupleIndex<Indices>()>(subspaces).integrationConstant, 0)... };
				return product;
			}
		};

		template<count_t Dim, count_t... Indices>
		bool operator==(const MultiDimensionalSpace<Dim, Indices...>& left, const MultiDimensionalSpace<Dim, Indices...>& right)
		{
			return internal::and_all((left.template get<Indices>() == right.template get<Indices>())...);
		}

		template<count_t Dim, count_t... Indices>
		bool operator!=(const MultiDimensionalSpace<Dim, Indices...>& left, const MultiDimensionalSpace<Dim, Indices...>& right)
		{
			return !(left == right);
		}

		template<count_t Dim, count_t Index1, count_t Index2>
		auto tensor(const internal::SubSpace<Dim, Index1>& left, const internal::SubSpace<Dim, Index2>& right)
		{
			return internal::constexpr_if(nostd::bool_constant<(Index1 < Index2)>{},
				[&](auto _)
			{
				return MultiDimensionalSpace<Dim, Index1, Index2>(std::make_tuple(_(left), _(right)));
			},
				[&](auto _)
			{
				return MultiDimensionalSpace<Dim, Index2, Index1>(std::make_tuple(_(right), _(left)));
			});
		}
	}
}

