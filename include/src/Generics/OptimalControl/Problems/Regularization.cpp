﻿/* COPYRIGHT
 *
 * file="Regularization.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Generics/OptimalControl/Problems/Regularization.h"

namespace qengine
{
	Regularization::Regularization(const internal::Maybe_Owner<Control>& controlRef, const real factor) :
		controlRef_(controlRef),
		factor_(factor)
	{
	}

	void Regularization::update(const Control& control)
	{
		if (!controlRef_.is_referencing()) controlRef_() = control;
	}

	void Regularization::_update(const count_t index, const RVec& x)
	{
		if (!controlRef_.is_referencing()) controlRef_().set(index, x);
	}

	real Regularization::cost() const
	{
		return 0.5*factor_*dotH1(controlRef_.get(), controlRef_.get());
	}

	real Regularization::_cost(const count_t i) const
	{
		auto& control = controlRef_();
		auto theNorm = 0.0;
		if (i == 0u)
		{
			/// forward/backward differences for first and last point in control (best possible approximation)
			theNorm = (control.get(1) - control.get(0)).norm();
		}
		else if (i == control.size() - 1)
		{
			theNorm = (control.get(control.size() - 1) - control.get(control.size() - 2)).norm();
		}
		else
		{
			/// central difference for all other points (more accurate)
			theNorm = (0.5*(control.get(i + 1) - control.get(i - 1))).norm();
		}

		return factor_ * theNorm*theNorm;
	}

	Control Regularization::gradient() const
	{
		auto& control = controlRef_();
		auto retval = Control::zeros(control.size(), control.paramCount(), control.dt());
		/// currently excludes endpoints. commented out below is what should be done to include endpoints
		const auto a = factor_ / controlRef_.get().dt();
		for (auto i = 1ull; i < control.size() - 1; ++i)
		{
			retval.set(i, a*(2 * control.get(i) - control.get(i - 1) - control.get(i + 1)));
		}
		//for (auto i = 0u; i < control.size(); ++i)
		//{
		//	if (i < control.size()-1) 
		//	{
		//		retval.at(i) -= 2 * factor_*(control.at(i + 1) - control.at(i));
		//	}
		//	if (i > 0u) 
		//	{
		//		retval.at(i) -= 2 * factor_*(control.at(i - 1) - control.at(i));
		//	}
		//}

		return retval;
	}

	RVec Regularization::_gradient(count_t i) const
	{
		auto& control = controlRef_();
		auto retval = RVec(control.paramCount(), 0.0);
		if (i == 0u)
		{
			/// return 0-gradient for i = start, for consistency.
		}
		else if (i == control.size() - 1)
		{
			/// return 0-gradient for i = start, for consistency.
		}
		else
		{
			/// central difference for all other points 
			retval -= (factor_)*(control.get(i + 1) - control.get(i));
			retval -= (factor_)*(control.get(i - 1) - control.get(i));
		}

		return retval;
	}

	void Regularization::_retargetControl(const internal::Maybe_Owner<Control>& controlRef)
	{
		controlRef_ = controlRef;
	}

	real& Regularization::factor()
	{
		return factor_;
	}

	const real& Regularization::factor() const
	{
		return factor_;
	}

	Regularization& Regularization::operator*=(const real factor)
	{
		factor_ *= factor;
		return *this;
	}

	Regularization operator*(const real factor, Regularization reg)
	{
		return reg *= factor;
	}
}
