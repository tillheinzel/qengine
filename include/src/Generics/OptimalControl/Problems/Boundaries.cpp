﻿/* COPYRIGHT
 *
 * file="Boundaries.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Generics/OptimalControl/Problems/Boundaries.h"

#include <algorithm>
#include <iostream>

namespace
{
	qengine::RVec maxVec(qengine::RVec v)
	{
		for (auto i = 0u; i < v.size(); ++i)
		{
			v.at(i) = std::max(v.at(i), 0.0);
		}
		return v;
	}
}

namespace qengine
{
	Boundaries::Boundaries(const internal::Maybe_Owner<Control>& controlRef, const RVec lowerleft, const RVec upperright, const real factor) :
		controlRef_(controlRef),
		lowerleft_(lowerleft),
		upperright_(upperright),
		factor_(factor)
	{
	}

	void Boundaries::update(const Control& control)
	{
		if (!controlRef_.is_referencing()) controlRef_() = control;
	}

	void Boundaries::_update(const count_t index, const RVec& x)
	{
		if (!controlRef_.is_referencing()) controlRef_().set(index, x);
	}

	real Boundaries::cost() const
	{
		auto& control = controlRef_();

		auto retval = 0.0;
		for (auto i = 0u; i < control.size(); ++i)
		{
			retval += _cost(i);
		}
		return retval;
	}

	real Boundaries::_cost(const count_t i) const
	{
		auto& control = controlRef_();
		const auto no = (maxVec(control.at(i) - upperright_) + maxVec(lowerleft_ - control.at(i))).norm();

		return 0.5*factor_ * no*no;
	}

	Control Boundaries::gradient() const
	{
		auto& control = controlRef_();

		auto retval = Control::zeros(control.size(), control.paramCount(), control.dt());

		for (auto i = 1u; i < control.size() - 1; ++i)
		{
			retval.set(i, _gradient(i));
		}
		return retval;
	}

	RVec Boundaries::_gradient(const count_t i) const
	{
		auto& control = controlRef_();
		return factor_* (-maxVec(lowerleft_ - control.at(i)) + maxVec(control.at(i) - upperright_));
	}

	void Boundaries::_retargetControl(const internal::Maybe_Owner<Control>& controlRef)
	{
		controlRef_ = controlRef;
	}

	real& Boundaries::factor()
	{
		return factor_;
	}

	const real& Boundaries::factor() const
	{
		return factor_;
	}

	Boundaries& Boundaries::operator*=(const real factor)
	{
		factor_ *= factor;
		return *this;
	}

	Boundaries operator*(const real factor, Boundaries reg)
	{
		return reg *= factor;
	}
}
