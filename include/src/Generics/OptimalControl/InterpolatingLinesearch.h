﻿/* COPYRIGHT
 *
 * file="InterpolatingLinesearch.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <cmath>
#include <stdexcept>
#include <algorithm>
#include <iostream>
namespace qengine
{
	class InterpolatingLinesearch
	{
	public:
		explicit InterpolatingLinesearch(const double alphamin, const double xtol, const unsigned int maxfev, const double c1, const double c2) :
			alphamin(alphamin),
			c1(c1),
			c2(c2),
			xtol(xtol),
			maxfev(maxfev)
		{
			if (c1 < 0.0 || c2 < 0.0 || xtol < 0.0 || alphamin < 0.0 || maxfev <= 0) { throw std::runtime_error("illegal parameters in InterpolatingLinesearch Constructor"); }
		}

		template<class Function>
		double operator() (const Function& costAndGradient, const double alphaMax,const  double initialAlpha, const double initialGradientValue, const double initialFunctionValue) const
		{
			//static_assert(is_callable_v<Function(double), double>, "InterpolatingLinesearch: function must take double and return double!");

			auto f = initialFunctionValue;
			auto quiet = true;
			//
			//   This is a slightly adapted version of cvsrch.m as found at :
			//   http ://www.cs.umd.edu/users/oleary/software/
			//
			//   Translation of minpack subroutine cvsrch
			//   Dianne O'Leary   July 1991
			//     **********
			//
			//     Subroutine cvsrch
			//
			//     The purpose of cvsrch is to find a step which satisfies
			//     a sufficient decrease condition and a curvature condition.
			//     The user must provide a subroutine which calculates the
			//     function and the gradient.
			//
			//     At each stage the subroutine updates an interval of
			//     uncertainty with endpoints stx and sty.The interval of
			//     uncertainty is initially chosen so that it contains a
			//     minimizer of the modified function
			//
			//          f(x + alpha*p) - f(x) - c1*alpha*(gradf(x)'p).
			//
			//     If a step is obtained for which the modified function
			//     has a nonpositive function value and nonnegative derivative,
			//     then the interval of uncertainty is chosen so that it
			//     contains a minimizer of f(x + alpha*p).
			//
			//     The algorithm is designed to find a step which satisfies
			//     the sufficient decrease condition
			//
			//           f(x + alpha*p) <= f(x) + c1*alpha*(gradf(x)'p),
			//
			//     and the curvature condition
			//
			//           abs(gradf(x + alpha*p)'p)) <= c2*abs(gradf(x)'p).
			//
			//     If c1 is less than c2 and if, for example, the function
			//     is bounded below, then there is always a step which satisfies
			//     both conditions.If no step can be found which satisfies both
			//     conditions, then the algorithm usually stops when rounding
			//     errors prevent further progress.In this case alpha only
			//     satisfies the sufficient decrease condition.
			//
			//     The subroutine statement is
			//
			//        subroutine cvsrch(phi, n, x, f, g, p, alpha, c1, c2, xtol,
			//                          alphamin, alphaMax, maxfev, info, nfev, wa)
			// where
			//
			//	phi is the name of the user - supplied subroutine which
			//         calculates the function and the gradient.phi must
			//      	  be declared in an external statement in the user
			//         calling program, and should be written as follows.
			//
			//         function[f, g] = phi(n, x) (Matlab)     (10 / 2010 change in documentation)
			// (derived from Fortran subroutine phi(n, x, f, g))
			// integer n
			//         f
			//         x(n), g(n)
			// ----------
			//         Calculate the function at x and
			//         return this value in the variable f.
			//         Calculate the gradient at x and
			//         return this vector in g.
			//	  ----------
			//	  return
			//	  end
			//
			//       n is a positive integer input variable set to the number
			//	  of variables.
			//
			//	x is an array of length n.On input it must contain the
			//	  base point for the line search.On output it contains
			//         x + alpha*s.
			//
			//	f is a variable.On input it must contain the value of f
			//         at x.On output it contains the value of f at x + alpha*s.
			//
			//	g is an array of length n.On input it must contain the
			//         gradient of f at x.On output it contains the gradient
			//         of f at x + alpha*s.
			//
			//	p is an input array of length n which specifies the
			//         search direction.
			//
			//	alpha is a nonnegative variable.On input alpha contains an
			//         initial estimate of a satisfactory step.On output
			//         alpha contains the final estimate.
			//
			//       c1 and c2 are nonnegative input variables.Termination
			//         occurs when the sufficient decrease condition and the
			//         directional derivative condition are satisfied.
			//
			//	xtol is a nonnegative input variable.Termination occurs
			//         when the relative width of the interval of uncertainty
			//	  is at most xtol.
			//
			//	alphamin and alphaMax are nonnegative input variables which
			//	  specify lower and upper bounds for the step.
			//
			//	maxfev is a positive integer input variable.Termination
			//         occurs when the number of calls to phi is at least
			//         maxfev by the end of an iteration.
			//
			//	info is an integer output variable set as follows :
			//
			//	  info = 0  Improper input parameters.
			//
			//	  info = 1  The sufficient decrease condition and the
			//                   directional derivative condition hold.
			//
			//	  info = 2  Relative width of the interval of uncertainty
			//		    is at most xtol.
			//
			//	  info = 3  Number of calls to phi has reached maxfev.
			//
			//	  info = 4  The step is at the lower bound alphamin.
			//
			//	  info = 5  The step is at the upper bound alphaMax.
			//
			//	  info = 6  Rounding errors prevent further progress.
			//                   There may not be a step which satisfies the
			//                   sufficient decrease and curvature conditions.
			//                   Tolerances may be too small.
			//
			//       nfev is an integer output variable set to the number of
			//         calls to phi.
			//
			//	wa is a work array of length n.
			//
			//     Subprograms called
			//
			//	user - supplied......phi
			//
			//	MINPACK - supplied...cstep
			//
			//	FORTRAN - supplied...abs, max, min
			//
			//     Argonne National Laboratory.MINPACK Project.June 1983
			// Jorge J.More', David J. Thuente
			//
			//     **********

			auto alpha = initialAlpha;
			unsigned int info = 0;
			unsigned int infoc = 1;
			unsigned int nfev = 0;
			//
			//     Check the input parameters for errors.
			//
			if (alpha <= 0.0 || alphaMax < alphamin) { throw std::runtime_error("illegal parameters in interpolating linesearch. alphaInit: " + std::to_string(alpha) + "; alphaMax: " + std::to_string(alphaMax)); }

			//
			//     Compute the initial gradient in the search direction
			//     and check that s is a descent direction.
			//
			auto gtd = initialGradientValue;
			if (gtd >= 0.0) {
				std::cout << "s is not a descent direction" << std::endl;
				return 0;
			}
			//
			//     Initialize local variables.
			//
			auto brackt = false;
			auto stage1 = true;

			auto finit = f;
			auto dgtest = c1 * gtd;
			auto width = alphaMax - alphamin;
			auto width1 = 2 * width;
			//      x0 = x;
			//
			//     The variables stx, fx, dgx contain the values of the step,
			//     function, and directional derivative at the best step.
			//     The variables sty, fy, dgy contain the value of the step,
			//     function, and derivative at the other endpoint of
			//     the interval of uncertainty.
			//     The variables alpha, f, dg contain the values of the step,
			//     function, and derivative at the current step.
			//
			auto stx = 0.0;
			auto fx = finit;
			auto dgx = gtd;
			auto sty = 0.0;
			auto fy = finit;
			auto dgy = gtd;
			//
			//     Start of iteration.
			//

			// predeclare values used inside loop

			double stmin, stmax, dg, ftest1;
			while (true) {
				//
				//        Set the minimum and maximum steps to correspond
				//        to the present interval of uncertainty.
				//
				if (brackt) {
					stmin = std::min(stx, sty);
					stmax = std::max(stx, sty);
				}
				else {
					stmin = stx;
					stmax = alpha + 4 * (alpha - stx);
				}
				//
				//        Force the step to be within the bounds alphaMax and alphamin.
				//
				alpha = std::max(alpha, alphamin);
				alpha = std::min(alpha, alphaMax);
				//
				//        If an unusual termination is to occur then let
				//        alpha be the lowest point obtained so far.
				//
				if ((brackt && (alpha <= stmin || alpha >= stmax)) || nfev >= maxfev - 1 || infoc == 0 || (brackt && stmax - stmin <= xtol * stmax)) { alpha = stx; }
				//
				//        Evaluate the function and gradient at alpha
				//        and compute the directional derivative.
				//
				//         x = x0 + alpha * p;
				std::tie(f, dg) = costAndGradient(alpha);
				//f = phi(alpha);
				//         g = grad(1);
				nfev = nfev + 1;
				if (!quiet)	std::cout << "CVSRCH" << nfev << "obj = " << f << " step = " << alpha << std::endl;

				//dg = gradphi(alpha);
				ftest1 = finit + alpha * dgtest;
				//
					//        Test for convergence.
					//
				if ((brackt && (alpha <= stmin || alpha >= stmax)) || infoc == 0) { info = 6; }
				if (alpha == alphaMax && f <= ftest1 && dg <= dgtest) { info = 5; }
				if (alpha == alphamin && (f > ftest1 || dg >= dgtest)) { info = 4; }
				if (nfev >= maxfev) { info = 3; }
				if (brackt && stmax - stmin <= xtol * stmax) { info = 2; }
				if (f <= ftest1 && std::abs(dg) <= c2 * (-gtd)) { info = 1; }
				//
				//        Check for termination.
				//
				if (info != 0) {
					return alpha;
				}
				//
				//        In the first stage we seek a step for which the modified
				//        function has a nonpositive value and nonnegative derivative.
				//
				if (stage1 && f <= ftest1 && dg >= std::min(c1, c2)*gtd) { stage1 = 0; }
				//
				//        A modified function is used to predict the step only if
				//        we have not obtained a step for which the modified
				//        function has a nonpositive function value and nonnegative
				//        derivative, and if a lower function value has been
				//        obtained but the decrease is not sufficient.
				//
				if (stage1 && f <= fx && f > ftest1) {
					//
					//           Define the modified function and derivative values.
					//
					auto fm = f - alpha * dgtest;
					auto fxm = fx - stx * dgtest;
					auto fym = fy - sty * dgtest;
					auto dgm = dg - dgtest;
					auto dgxm = dgx - dgtest;
					auto dgym = dgy - dgtest;
					//
					//           Call cstep to update the interval of uncertainty
					//           and to compute the new step.
					//
					infoc = minpack_cstep(stx, fxm, dgxm, sty, fym, dgym, alpha, fm, dgm, brackt, stmin, stmax);
					//
					//           Reset the function and gradient values for f.
					//
					fx = fxm + stx * dgtest;
					fy = fym + sty * dgtest;
					dgx = dgxm + dgtest;
					dgy = dgym + dgtest;
				}
				else {
					//
					//           Call cstep to update the interval of uncertainty
					//           and to compute the new step.
					//
					infoc = minpack_cstep(stx, fx, dgx, sty, fy, dgy, alpha, f, dg, brackt, stmin, stmax);
				}
				//
				//        Force a sufficient decrease in the size of the
				//        interval of uncertainty.
				//
				if (brackt) {
					if (std::abs(sty - stx) >= 0.66*width1) {
						alpha = stx + 0.5*(sty - stx);
					}
					width1 = width;
					width = std::abs(sty - stx);
				}
				//
				//        End of iteration.
				//

					//
					//     Last card of subroutine cvsrch.
			}
		}


	private:
		const double alphamin;
		const double c1;
		const double c2;
		const double xtol;
		const double maxfev;

		unsigned int minpack_cstep(double& stx, double& fx, double& dx, double& sty, double& fy, double& dy, double& stp, double& fp, double& dp, bool& brackt, const double stpmin, const double stpmax) const
		{
			//
			//   This is a slightly adapted version of cstep.m as found at :
		//   http ://www.cs.umd.edu/users/oleary/software/
			//
			//   Translation of minpack subroutine cstep
			//   Dianne O'Leary   July 1991
			//     **********
			//
			//     Subroutine cstep
			//
			//     The purpose of cstep is to compute a safeguarded step for
			//     a linesearch and to update an interval of uncertainty for
			//     a minimizer of the function.
			//
			//     The parameter stx contains the step with the least function
			//     value.The parameter stp contains the current step.It is
			//     assumed that the derivative at stx is negative in the
			//     direction of the step.If brackt is set true then a
			//     minimizer has been bracketed in an interval of uncertainty
			//     with endpoints stx and sty.
			//
			//     The subroutine statement is
			//
			//       subroutine cstep(stx, fx, dx, sty, fy, dy, stp, fp, dp, brackt,
				//                        stpmin, stpmax, info)
			//
			//     where
			//
			//       stx, fx, and dx are variables which specify the step,
			//         the function, and the derivative at the best step obtained
			//         so far.The derivative must be negative in the direction
			//         of the step, that is, dx and stp - stx must have opposite
			//         signs.On output these parameters are updated appropriately.
			//
			//       sty, fy, and dy are variables which specify the step,
			//         the function, and the derivative at the other endpoint of
			//         the interval of uncertainty.On output these parameters are
			//         updated appropriately.
			//
			//       stp, fp, and dp are variables which specify the step,
			//         the function, and the derivative at the current step.
			//         If brackt is set true then on input stp must be
			//         between stx and sty.On output stp is set to the new step.
			//
			//       brackt is a logical variable which specifies if a minimizer
			//         has been bracketed.If the minimizer has not been bracketed
			//         then on input brackt must be set false.If the minimizer
			//         is bracketed then on output brackt is set true.
			//
			//       stpmin and stpmax are input variables which specify lower
			//         and upper bounds for the step.
			//
			//       info is an integer output variable set as follows :
		//         If info = 1, 2, 3, 4, 5, then the step has been computed
			//         according to one of the five cases below.Otherwise
			//         info = 0, and this indicates improper input parameters.
			//
			//     Subprograms called
			//
			//       FORTRAN - supplied ... abs, max, min, sqrt
			//                        ... dble
			//
			//     Argonne National Laboratory.MINPACK Project.June 1983
			// Jorge J.More', David J. Thuente
			//
			//     **********
			const auto p66 = 0.66;
			auto info = 0;
			//
			//     Check the input parameters for errors.
			//
			if ((brackt && (stp <= std::min(stx, sty) || stp >= std::max(stx, sty))) || dx * (stp - stx) >= 0.0 || stpmax < stpmin) return info;
			//
			//     Determine if the derivatives have opposite sign.
			//
			const auto sgnd = dp * (dx / std::abs(dx));

			/// CASES: basically a switch

			// values used inside the cases
			bool bound;
			double theta, s, gamma, p, q, r, stpc, stpq, stpf;

			//
			//     First case.A higher function value.
			//     The minimum is bracketed.If the cubic step is closer
			//     to stx than the quadratic step, the cubic step is taken,
			//     else the average of the cubic and quadratic steps is taken.
			//


			if (fp > fx) {
				info = 1;
				bound = 1;
				theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
				s = max(theta, dx, dp);
				gamma = s * std::sqrt(std::pow(theta / s, 2) - (dx / s)*(dp / s));
				if (stp < stx) {
					gamma = -gamma;
				}
				p = (gamma - dx) + theta;
				q = ((gamma - dx) + gamma) + dp;
				r = p / q;
				stpc = stx + r * (stp - stx);
				stpq = stx + ((dx / ((fx - fp) / (stp - stx) + dx)) / 2)*(stp - stx);
				if (std::abs(stpc - stx) < std::abs(stpq - stx)) {
					stpf = stpc;
				}
				else {
					stpf = stpc + (stpq - stpc) / 2;
				}
				brackt = 1;
				//
					//     Second case.A lower function value and derivatives of
					//     opposite sign.The minimum is bracketed.If the cubic
					//     step is closer to stx than the quadratic(secant) step,
					//     the cubic step is taken, else the quadratic step is taken.
					//
			}
			else if (sgnd < 0.0) {
				info = 2;
				bound = 0;
				theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
				s = max(theta, dx, dp);
				gamma = s * std::sqrt(std::pow(theta / s, 2) - (dx / s)*(dp / s));
				if (stp > stx) {
					gamma = -gamma;
				}
				p = (gamma - dp) + theta;
				q = ((gamma - dp) + gamma) + dx;
				r = p / q;
				stpc = stp + r * (stx - stp);
				stpq = stp + (dp / (dp - dx))*(stx - stp);
				if (std::abs(stpc - stp) > std::abs(stpq - stp)) {
					stpf = stpc;
				}
				else {
					stpf = stpq;
				}
				brackt = 1;
				//
					//     Third case.A lower function value, derivatives of the
					//     same sign, and the magnitude of the derivative decreases.
					//     The cubic step is only used if the cubic tends to infinity
					//     in the direction of the step or if the minimum of the cubic
					//     is beyond stp.Otherwise the cubic step is defined to be
					//     either stpmin or stpmax.The quadratic(secant) step is also
					//     computed and if the minimum is bracketed then the step
					//     closest to stx is taken, else the step farthest away is taken.
					//
			}
			else if (std::abs(dp) < std::abs(dx)) {
				info = 3;
				bound = 1;
				theta = 3 * (fx - fp) / (stp - stx) + dx + dp;
				s = max(theta, dx, dp);
				//
					//        The case gamma = 0 only arises if the cubic does not tend
					//        to infinity in the direction of the step.
					//
				gamma = s * std::sqrt(std::max(0.0, std::pow(theta / s, 2) - (dx / s)*(dp / s)));
				if (stp > stx) {
					gamma = -gamma;
				}
				p = (gamma - dp) + theta;
				q = (gamma + (dx - dp)) + gamma;
				r = p / q;
				if (r < 0.0 && gamma != 0.0) {
					stpc = stp + r * (stx - stp);
				}
				else if (stp > stx) {
					stpc = stpmax;
				}
				else {
					stpc = stpmin;
				}
				stpq = stp + (dp / (dp - dx))*(stx - stp);
				if (brackt) {
					if (std::abs(stp - stpc) < std::abs(stp - stpq)) {
						stpf = stpc;
					}
					else {
						stpf = stpq;
					}
				}
				else {
					if (std::abs(stp - stpc) > std::abs(stp - stpq)) {
						stpf = stpc;
					}
					else {
						stpf = stpq;
					}
				}
				//
				//     Fourth case.A lower function value, derivatives of the
				//     same sign, and the magnitude of the derivative does
				// not decrease.If the minimum is not bracketed, the step
				//     is either stpmin or stpmax, else the cubic step is taken.
				//
			}
			else {
				info = 4;
				bound = 0;
				if (brackt) {
					theta = 3 * (fp - fy) / (sty - stp) + dy + dp;
					s = max(theta, dx, dp);
					gamma = s * std::sqrt(std::pow(theta / s, 2) - (dy / s)*(dp / s));
					if (stp > sty) {
						gamma = -gamma;
					}
					p = (gamma - dp) + theta;
					q = ((gamma - dp) + gamma) + dy;
					r = p / q;
					stpc = stp + r * (sty - stp);
					stpf = stpc;
				}
				else if (stp > stx) {
					stpf = stpmax;
				}
				else {
					stpf = stpmin;
				}
			}
			//
			//     Update the interval of uncertainty.This update does not
			//     depend on the new step or the case analysis above.
			//
			if (fp > fx) {
				sty = stp;
				fy = fp;
				dy = dp;
			}
			else {
				if (sgnd < 0.0) {
					sty = stx;
					fy = fx;
					dy = dx;
				}
				stx = stp;
				fx = fp;
				dx = dp;
			}
			//
			//     Compute the new step and safeguard it.
			//
			stpf = std::min(stpmax, stpf);
			stpf = std::max(stpmin, stpf);
			stp = stpf;
			if (brackt && bound) {
				if (sty > stx) {
					stp = std::min(stx + p66 * (sty - stx), stp);
				}
				else {
					stp = std::max(stx + p66 * (sty - stx), stp);
				}
			}
			return info;
			//
			//     Last card of subroutine cstep.
			//

		}

		static constexpr double max(double a, double b, double c) noexcept
		{
			return std::max(a, std::max(b, c));
		}
	};


}
