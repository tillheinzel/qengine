﻿/* COPYRIGHT
 *
 * file="DressedAlgorithm.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	template<class Algorithm, class Stopper, class ResetPolicy>
	class DressedAlgorithm : public Algorithm
	{
	public:
		const auto& stopper() const { return stopper_; }
		const auto& resetter() const { return resetter_; }
		const auto& dressedRestarter() const { return Algorithm::stopper(); }
		count_t numberOfResets() const { return resetCount_; }

		auto optimize()
		{
			auto& shouldStop = stopper_;
			auto& resetAlgorithm = resetter_;

			std::unique_ptr<decltype(Algorithm::optimize())> retval;
			while (true)
			{
				retval = std::make_unique<decltype(Algorithm::optimize())>(Algorithm::optimize());

				if (shouldStop(*this))
				{
					break;
				}
				else 
				{
					resetAlgorithm(*this);
					++resetCount_;
				}
			}

			return *retval;
		}

		DressedAlgorithm(Algorithm&& alg, const Stopper& stopper, const ResetPolicy& resetPolicy) :
			Algorithm(std::move(alg)),
			stopper_(stopper),
			resetter_(resetPolicy)
		{}

	private:
		Stopper stopper_;
		ResetPolicy resetter_;
		count_t resetCount_ = 0;
	};
}
