﻿/* COPYRIGHT
 *
 * file="Bfgs.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/Types.h"

#include "src/Utility/is_callable.h"

#include "src/Generics/OptimalControl/Algorithms/Bfgs.h"


namespace qengine
{
	template<class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	count_t Bfgs<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>::stepsSinceLastRestart() const
	{
		return _previousIterations.size();
	}

	template<class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	std::tuple<real, Params, Collector>
		Bfgs<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>::optimize()
	{
		auto& problem = _problem;
		// declare aliases for hooks so they look like function-calls in the code below
		auto& shouldStop = _stopper;
		auto& collect = _collector;
		auto& calculateStepSize = _stepSizeFinder;
		auto& shouldRestart = _restartPolicy;
		auto& calculateGradient = _gradientPolicy;
		auto& dot = _innerProductPolicy;

		auto grad = calculateGradient(problem);
		auto x = problem._params();

		auto g0 = grad;
		auto x0 = x;

		while (true)
		{
			// update
			_stepDirection = -grad;
			for (const auto& h : _previousIterations)
			{
				_stepDirection -= h.c * (dot(h.step, grad) * h.r - dot(h.z, grad) * h.step);
			}

			_stepSize = calculateStepSize(*this);
			x += _stepSize * _stepDirection;

			problem.update(x);

			// prepare save of iteration=
			grad = calculateGradient(problem);

			auto y = grad - g0;
			auto step = x - x0;

			_q = dot(step, y);
			saveIteration(step, y, _q); // pushes onto 'previousIterations'

			++_iteration;
			collect(*this);
			if (shouldStop(*this)) {
				break;
			}

			_restart = shouldRestart(*this);
			if (_restart)
			{
				resetBfgs();
			}

			g0 = grad;
			x0 = problem._params();
		}

		return std::make_tuple(problem.cost(), problem._params(), _collector);
	}

	template <class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	void Bfgs<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>::resetBfgs()
	{
		_previousIterations.clear();
	}

	template <class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	Bfgs<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>::Bfgs(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy, const RestartPolicy& restartPolicy) : _problem(problem),
		_stopper(stopper),
		_collector(collector),
		_stepSizeFinder(stepSizeFinder),
		_innerProductPolicy(innerProductPolicy),
		_gradientPolicy(gradientPolicy),
		_restartPolicy(restartPolicy)
	{
		// todo: make these work with gcc
		//static_assert(is_callable_v<Stopper, bool(const Bfgs&)>, "Stopper must be callable with (const algorithm&) -> bool");
		//static_assert(is_callable_v<Collector, void(const Bfgs&)>, "Collector must be callable with (const algorithm&) -> void");
		//static_assert(is_callable_v<StepSizeFinder, real(Params, Problem&, const Bfgs&)>, "StepSizeFinder must be callable with (const Control&, Problem&, const algorithm&) -> real");
		//static_assert(is_callable_v<InnerProductPolicy, real(const Params&, const Params&)>, "InnerProductPolicy must be callable with (const Control&, const Control&) -> real");
		//static_assert(is_callable_v<GradientPolicy, Params(Problem&)>, "GradientPolicy must be callable with (const Problem&) -> Control");
		//static_assert(is_callable_v<RestartPolicy, bool(const Bfgs&)>, "RestartPolicy must be callable with (const algorithm&) -> bool");
		//static_assert(internal::is_OptimizationProblem_v<Problem, Params>, "The Problem does not fulfill the interface for an optimization-problem!");

	}

	template<class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	void Bfgs<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>::saveIteration(const Params& step, const Params& y, double q)
	{
		auto& dot = _innerProductPolicy;

		PreviousIteration thisIteration;
		thisIteration.step = step;

		thisIteration.z = y;
		for (const auto& h : _previousIterations)
		{
			thisIteration.z += h.c * (dot(h.step, y) * h.r - dot(h.z, y) * h.step);
		}

		thisIteration.c = 1.0 / q;

		auto d = 1 + thisIteration.c*dot(y, thisIteration.z);
		thisIteration.r = d * step - thisIteration.z;

		_previousIterations.push_back(thisIteration);
	}
}
