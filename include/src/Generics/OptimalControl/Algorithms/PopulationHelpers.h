﻿/* COPYRIGHT
 *
 * file="PopulationHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>
#include <iostream>
#include <vector>

#include "src/Utility/Types.h"

#include "src/Utility/is_callable.h"

#include "src/Utility/LinearAlgebra/Vector.h"

namespace qengine{

class PopulationMember{
private:
    std::tuple<double,RVec> memberInformation_; // cost at index 0, path at index 1

public:

    // GETTERS // note: should these be const?
    real& cost = std::get<0>(memberInformation_);
    RVec& x    = std::get<1>(memberInformation_);

    // CTORS
    PopulationMember(){};

    template<class Function> // , std::enable_if_t<is_callable_v<Function, real(RVec)>, int> = 0>
    PopulationMember(const RVec& x, const Function& f) : memberInformation_(f(x), x) {}

    //		template<class Function, std::enable_if_t<is_callable_v<Function, std::tuple<double, RVec>(RVec)>, int> = 0>
    //		PopMember(const RVec& x, const Function& f) : memberInformation_(f(x), x) {}

    PopulationMember(const PopulationMember& other)
        : memberInformation_{ other.memberInformation_}
    {
    }

    PopulationMember(PopulationMember&& other) noexcept
        : memberInformation_{ std::move(other.memberInformation_)}
    {
    }

    // ASSIGNMENT
    PopulationMember& operator=(const PopulationMember& other)
    {
        if (this == &other)
            return *this;
        memberInformation_ = other.memberInformation_;
        return *this;
    }

    PopulationMember& operator=(PopulationMember&& other) noexcept
    {
        if (this == &other)
            return *this;
        memberInformation_ = std::move(other.memberInformation_);
        return *this;
    }

    // OPERATORS
    bool operator<(const PopulationMember& other) const
    {
        return cost < other.cost;
    }

    PopulationMember& operator+(const PopulationMember& other){
        x += other.x;
        return *this;
    }

    PopulationMember& operator-(){
        x.operator-();
        return *this;
    }

    // todo: implement iterators for vector
    //		friend std::ostream& operator<<(std::ostream& stream, const PopMember& p)
    //		{
    //			stream << p.cost << " : { ";
    //			for (const auto& x : p.x)
    //			{
    //				stream << x << " , ";
    //			}
    //			stream << " }" << std::endl;
    //			return stream;
    //		}
};





///
    using PopulationVector = std::vector<PopulationMember>;

    template<class Function, class PointMaker>
    PopulationVector createPopulation(const Function& f, const PointMaker& makeX, count_t popSize){
        //		static_assert(is_callable_v<Function, decltype(makeX)>, "Function must be callable with return value of makeX!");
        PopulationVector population;
        population.reserve(popSize);

        for(auto i=0u; i<popSize;++i){
            auto x = makeX(i);
            auto popMember = PopulationMember(x,f);

            population.push_back(popMember);
        }

        return population;
    }
}
