cmake_minimum_required (VERSION 3.3)
project (QEngine)

# OPTIMAL CONTROL LIBRARY
file(GLOB_RECURSE SOURCES 
	"*.h" "*.cpp"
)

add_library(optimal_control OBJECT ${SOURCES})
target_include_directories(optimal_control PRIVATE "${ARMADILLO_INCLUDE_DIR}")
	
	
if(USE_MKL)
	target_compile_definitions(optimal_control PRIVATE -DUSE_MKL=1)
	
	target_include_directories(optimal_control PRIVATE "${MKL_INCLUDE_DIR}")
endif()

if(MSVC)
	source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})
endif()
