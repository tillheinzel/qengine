﻿/* COPYRIGHT
 *
 * file="applyElementWise.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


namespace qengine
{
	namespace internal
	{
		template<class... Ts, class F, std::size_t... I>
		constexpr auto applyElementWise_impl(const std::tuple<Ts...>& t, const F& f, std::index_sequence<I...>)
		{
			return std::make_tuple(f(std::get<I>(t))...);
		}

		template<class... Ts, class F>
		constexpr std::tuple<Ts...> applyElementWise(const std::tuple<Ts...>& t, const F& f)
		{
			return applyElementWise_impl(t, f, std::make_index_sequence<sizeof...(Ts)>{});
		}

		template<class... Ts, class F, std::size_t... I>
		constexpr std::tuple<Ts...> applyElementWise_impl(const std::tuple<Ts...>& left, const std::tuple<Ts...>& right, const F& f, std::index_sequence<I...>)
		{
			return std::make_tuple(f(std::get<I>(left), std::get<I>(right))...);
		}

		template<class... Ts, class F>
		constexpr std::tuple<Ts...> applyElementWise(const std::tuple<Ts...>& left, const std::tuple<Ts...>& right, const F& f)
		{
			return applyElementWise_impl(left, right, f, std::make_index_sequence<sizeof...(Ts)>{});
		}
	}
}
