﻿/* COPYRIGHT
 *
 * file="CachingCallable.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

namespace qengine
{
	namespace internal
	{
		template<class F, class Ret, class... Params>
		class CachingCallable
		{
		public:
			const Ret& operator() (const Params&... params)
			{
				if(std::tie(params...) != currentParams_)
				{
					currentParams_ = 
					updateCache(params...);
				}
				return cache_;
			}

		private:
			void updateCache(const Params&... params)
			{
				f_(cache_, params...);
			}

			Ret cache_;
			F f_;
			std::tuple<Params...> currentParams_;
		};
	}
}
