﻿/* COPYRIGHT
 *
 * file="stdTupleFromVec.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/apply_vec.h"

namespace qengine
{
	namespace internal
	{
		template<class... Params, class Vec >
		auto stdTupleFromVec(Vec&& v)
		{
			auto constructTuple = [](auto... ts) {return std::tuple <Params...>(ts...); };
			return apply_vec<sizeof...(Params)>(constructTuple, v);
		}
	}
}
