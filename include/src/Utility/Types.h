﻿/* COPYRIGHT
 *
 * file="Types.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <complex>

/// 
/// \file Types.h
/// @brief Definitions of basic types and type-operations used within the QEngine
/// 

namespace qengine
{
	using real = double;
	using complex = std::complex<double>;

	using count_t = unsigned long long;

	inline real absSq(const complex& c)
	{
		return c.real()*c.real() + c.imag()*c.imag();
	}
}


namespace qengine
{
	inline std::ostream& operator<<(std::ostream& stream, complex c)
	{
		stream << c.real() << " + " << c.imag() << "i";
		return stream;
	}

	inline std::ostream& writeTo(std::ostream& stream, complex c)
	{
		auto a = c.real();
		auto b = c.imag();
		if (b == 0.0)
		{
			stream << a;
		}
		else if (a == 0)
		{
			stream << b << "i";
		}
		else
		{
			stream << a << "+" << b << "i";
		}
		return stream;
	}
}

namespace qengine
{
	namespace internal
	{
		template<typename> struct is_number_type { constexpr static bool value = false; };
		template<typename T> constexpr static bool is_number_type_v = is_number_type<T>::value;

		template<> struct is_number_type<real> { constexpr static bool value = true; };
		template<> struct is_number_type<complex> { constexpr static bool value = true; };
	}
}

namespace qengine
{
	namespace internal
	{
		template<typename Left, typename Right>
		struct NumberMin;

		template<typename Left, typename Right>
		using NumberMin_t = typename NumberMin<Left, Right>::type;

		template<> struct NumberMin<real, real> { using type = real; };
		template<> struct NumberMin<real, complex> { using type = real; };
		template<> struct NumberMin<complex, real> { using type = real; };
		template<> struct NumberMin<complex, complex> { using type = complex; };

		template<template<class> class T, class Left, class Right>
		struct NumberMin<T<Left>, T<Right>> { using type = T<NumberMin_t<Left, Right>>; };
	}

}

namespace qengine
{
	namespace internal
	{
		template<class>
		struct getNumType;

		template<class T>
		using getNumType_t = typename getNumType<T>::type;

		template<template<typename> class X, typename number>
		struct getNumType<X<number>> { using type = number; };
	}
}

namespace qengine
{
	namespace internal
	{
		inline real toNumber(const real& t)
		{
			return t;
		}

		inline complex toNumber(const complex& t)
		{
			return t;
		}

		template<class T>
		std::enable_if_t<std::is_convertible<T, real>::value && !(std::is_same<T, real>::value), real> toNumber(const T& t)
		{
			return static_cast<real>(t);
		}

		template<class T>
		std::enable_if_t<std::is_convertible<T, complex>::value && !std::is_same<T, complex>::value && !std::is_convertible<T, real>::value, complex> toNumber(const T& t)
		{
			return static_cast<complex>(t);
		}

		template<typename T>
		struct is_convertible_to_number_type
		{
			constexpr static bool value = std::is_convertible<T, real>::value || std::is_convertible<T, complex>::value;
		};

		template<typename T>
		constexpr static bool is_convertible_to_number_type_v = is_convertible_to_number_type<T>::value;
	}
}


namespace qengine
{

	namespace internal
	{
		template<typename Left, typename Right, class SFINAE = void>
		struct NumberMax;

		template<typename Left, typename Right>
		using NumberMax_t = typename NumberMax<Left, Right>::type;

		template<> struct NumberMax<real, real> { using type = real; };
		template<> struct NumberMax<real, complex> { using type = complex; };
		template<> struct NumberMax<complex, real> { using type = complex; };
		template<> struct NumberMax<complex, complex> { using type = complex; };

		template<class Left, class Right>
		struct NumberMax < Left, Right, std::enable_if_t<is_convertible_to_number_type_v<Left>&& is_convertible_to_number_type_v<Right>>>
		{
			using type = NumberMax_t<decltype(toNumber(std::declval<Left>())), decltype(toNumber(std::declval<Right>()))>;
		};

		template<template<class> class T, class Left, class Right>
		struct NumberMax<T<Left>, T<Right>> { using type = T<NumberMax_t<Left, Right>>; };


	}
}

namespace qengine
{
	enum NORMALIZATION { NORMALIZE, NORMALIZE_NOT };
}
