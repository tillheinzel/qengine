﻿/* COPYRIGHT
 *
 * file="Maybe_Owner.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <utility>
#include <memory>

namespace qengine
{
	namespace internal
	{
		template<class T>
		class Maybe_Owner
		{
		public:
			/*implicit*/ Maybe_Owner(const T& t) :
				is_referencing_(false),
				ptr_(std::make_unique<T>(t)),
				ref_(*ptr_)
			{
			}

			/*implicit*/ Maybe_Owner(std::reference_wrapper<T> t) :
				is_referencing_(true),
				ref_(t)
			{
			}

			Maybe_Owner(Maybe_Owner&& other) noexcept:
				is_referencing_(other.is_referencing_),
				ptr_(std::move(other.ptr_)),
			ref_(is_referencing_? other.ref_: std::ref(*ptr_))
			{
			}

			Maybe_Owner(const Maybe_Owner& other):
				is_referencing_(other.is_referencing_),
				ptr_(is_referencing_ ? nullptr		: std::make_unique<T>(*other.ptr_)),
				ref_(is_referencing_ ? other.ref_	: std::ref(*ptr_))
			{
			}

			Maybe_Owner& operator=(const Maybe_Owner& other)
			{
				if (this == &other)
					return *this;
				is_referencing_ = other.is_referencing_;
				ptr_ = is_referencing_ ? nullptr : std::make_unique<T>(*other.ptr_);
				ref_ = is_referencing_ ? other.ref_ : std::ref(*ptr_);
				return *this;
			}

			Maybe_Owner& operator=(Maybe_Owner&& other) noexcept
			{
				if (this == &other)
					return *this;
				is_referencing_ = other.is_referencing_;
				ptr_ = std::move(other.ptr_);
				ref_ = is_referencing_ ? other.ref_ : std::ref(*ptr_);
				return *this;
			}

			T& get() { return ref_.get(); }
			const T& get() const { return ref_.get(); }

			T& operator()() { return get(); }
			const T& operator()() const { return get(); }

			bool is_referencing() const { return is_referencing_; };
		private:
			bool is_referencing_;

			std::unique_ptr<T> ptr_;
			std::reference_wrapper<T> ref_;

		};
	}
}
