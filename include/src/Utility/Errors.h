﻿/* COPYRIGHT
 *
 * file="Errors.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <stdexcept>


namespace qengine
{

	class DC_invalidNameError : public std::runtime_error
	{
	public:
		explicit DC_invalidNameError(const std::string& message)  : std::runtime_error(message) {}
	};
	class DC_invalidDimensionsError : public std::runtime_error
	{
	public:
		explicit DC_invalidDimensionsError(const std::string& message) : std::runtime_error(message) {}
	};
	class DC_FileIOError : public std::runtime_error
	{
	public:
		explicit DC_FileIOError(const std::string& message)  : std::runtime_error(message) {}
	};
	class DC_wrongTypeError : public std::runtime_error
	{
	public:
		explicit DC_wrongTypeError(const std::string& message)  : std::runtime_error(message) {}
	};
}

