﻿/* COPYRIGHT
 *
 * file="ParallelDriver.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <thread>

#include <future>
#include <queue>
#include <iostream>

namespace qengine
{
	namespace internal
	{
		class ParallelDriver
		{
			class MyThread
			{
			public:
				template<class Functor>
				explicit MyThread(Functor f)
					: t(makeCatch(f))
				{}

				void join()
				{
					t.join();
				}
			private:
				std::thread t;

				template<class Functor>
				auto makeCatch(Functor f)
				{
					return [f]()
					{
						try { f(); }
						catch (std::exception& e)
						{
							std::cout << e.what() << std::endl;
						}
					};
				}

			};

		public:
			explicit ParallelDriver(unsigned int maxThreads = std::thread::hardware_concurrency()) :maxThreads(maxThreads)
			{
				//std::cout << "driver has " << maxThreads << " threads." << std::endl;
			}

			template<class Functor>
			void addTask(Functor newTask)
			{
				tasks.emplace(newTask);
			}

			void run()
			{
				std::vector<MyThread> threads;
				while (!tasks.empty())
				{
					while (!tasks.empty() && threads.size() < maxThreads) // fill up to maxThreads or until no more tasks
					{
						threads.push_back(MyThread(std::move(tasks.front())));
						tasks.pop();
					}

					for (auto& thread : threads)
					{
						thread.join();
					}

					threads.clear();
				}
			}
		private:
			unsigned int maxThreads;

			std::queue<std::function<void(void)>> tasks;
		};

	}
}
