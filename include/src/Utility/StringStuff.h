﻿/* COPYRIGHT
 *
 * file="StringStuff.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <string>

namespace qengine
{
	namespace internal
	{
		namespace stringStuff
		{
			inline std::string tail(std::string const& source, size_t const length) {
				if (length >= source.size()) { return source; }
				return source.substr(source.size() - length);
			} // tail

			inline std::string removePath(std::string path)
			{
				const auto slashpos = path.rfind('/');
				// if we find a slash, remove it and everything before it
				if (slashpos != std::string::npos)
				{
					path = tail(path, path.size() - slashpos - 1);
				}

				const auto backslashpos = path.rfind("\\");
				if (backslashpos != std::string::npos)
				{
					path = tail(path, path.size() - backslashpos - 1);
				}

				return path;
			}

			inline std::string getExtension(const std::string& path)
			{
				const auto filename = removePath(path);
				const auto dotpos = filename.rfind('.');
				if (dotpos == std::string::npos)
				{
					return std::string("");
				}

				const auto extension = tail(filename, filename.size() - dotpos - 1);
				return extension;
			}
		}
	}
}
