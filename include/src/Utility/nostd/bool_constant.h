﻿/* COPYRIGHT
 *
 * file="bool_constant.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once



namespace nostd
{
	template<bool V>
	using bool_constant = std::integral_constant<bool, V>;
}
