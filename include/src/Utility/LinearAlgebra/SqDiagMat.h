﻿/* COPYRIGHT
 *
 * file="SqDiagMat.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/Spectrum.h"

namespace qengine 
{
	namespace internal
	{
		template<class number> class SqSparseMat;

		template<typename number>
		class SqDiagMat
		{
			// CTOR/DTOR
		public:
			explicit SqDiagMat(count_t dim);
			explicit SqDiagMat(const Vector<number>& diag);
			explicit SqDiagMat(Vector<number>&& diag);
			
			template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
			explicit SqDiagMat(const SqDiagMat<T>& other);

			// ASSIGNMENT
		public:

			SqDiagMat& operator=(const Vector<number>& diag);
			SqDiagMat& operator=(Vector<number>&& diag);

			// DATA ACCESS
		public:
			Vector<number>& diag();
			const Vector<number>& diag() const;


			// MATH OPERATIONS
		public:
			void multiplyInPlace(CVec& vector) const;

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator/=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator*=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator+=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator-=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator+=(const SqDiagMat<number1>& other);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDiagMat& operator-=(const SqDiagMat<number1>& other);

			SqDiagMat operator-() const;

			//MISC
		public:
			explicit operator SqSparseMat<number>() const;

			count_t dim() const;
			bool isHermitian() const;

			number at(count_t nRow, count_t nCol) const;

			bool hasNan() const;

		private:
			qengine::Vector<number> diag_;
		};
	}
}

namespace qengine {
	namespace internal
	{
		template<typename number>
		std::ostream& operator<<(std::ostream& stream,const SqDiagMat<number>& mat);

		template<typename number1, typename number2>
		SqDiagMat<NumberMax_t<number1, number2>> operator* (const SqDiagMat<number1>& left, const SqDiagMat<number2>& right);

		template<typename number1, typename number2>
		SqDiagMat<NumberMax_t<number1, number2>> kron (const SqDiagMat<number1>& left, const SqDiagMat<number2>& right);

		template<typename number>
		SqDiagMat<number> exp(const SqDiagMat<number>& d);

		template<typename number>
		SqDiagMat<number> pow(const SqDiagMat<number>& d, count_t exponent);


		template <typename MatNumber>
		Spectrum<complex> getSpectrum(const SqDiagMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization = 1.0);

		template <typename MatNumber>
		Spectrum<real> getSpectrum_herm(const SqDiagMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization = 1.0);
	}
}
