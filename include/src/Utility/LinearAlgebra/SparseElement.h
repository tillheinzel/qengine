﻿/* COPYRIGHT
 *
 * file="SparseElement.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <memory>

namespace arma
{
    template<class> class SpMat_MapMat_val;
}

namespace qengine
{
	namespace internal
	{
		template<class T>
		class SparseElement
		{
		public:
			explicit SparseElement(arma::SpMat_MapMat_val<T>&& data);

			SparseElement(SparseElement&& other) noexcept;
			SparseElement(const SparseElement& other);

			~SparseElement();

			arma::SpMat_MapMat_val<T>& data();
			const arma::SpMat_MapMat_val<T>& data() const;

			operator T() const;

			SparseElement& operator=(const SparseElement& other);
			SparseElement& operator=(SparseElement&& other) noexcept;

			SparseElement& operator= (T val);
			SparseElement& operator+= (T val);
			SparseElement& operator-= (T val);
			SparseElement& operator*= (T val);
			SparseElement& operator/= (T val);

		private:
			std::unique_ptr<arma::SpMat_MapMat_val<T>> data_;
		};

	}
}
