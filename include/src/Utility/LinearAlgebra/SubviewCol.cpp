﻿/* COPYRIGHT
 *
 * file="SubviewCol.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#include "src/Utility/LinearAlgebra/SubviewCol.h"

#include <armadillo>

#include "src/Utility/LinearAlgebra/Matrix.h"


namespace qengine {
	namespace internal{

		/// CTOR
		template<typename number>
		SubviewCol<number>::SubviewCol(Matrix<number>& Mat, count_t j)
			: Mat_(Mat),
			// col_(std::make_unique<arma::subview_col<number>>(Mat,j),
			j_(j)
		{
		}

		template <typename number>
		SubviewCol<number>::SubviewCol(const SubviewCol& other) :
			Mat_(other.Mat_),
			j_(other.j_)
		{
		}

		template <typename number>
		SubviewCol<number>::SubviewCol(SubviewCol&& other) noexcept:
		Mat_(other.Mat_),
		j_(other.j_)
		{
		}

		/// ASSIGNMENT
		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator=(const Vector<number>& v)
		{
			Mat_._mat().col(j_) = v._vec();
			return *this;
		}

		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator=(const SubviewCol<number>& svc)
		{
			Mat_._mat().col(j_) = svc.Mat_._mat().col(svc.j_);
			return *this;
		}

		template <typename number>
		SubviewCol<number>& SubviewCol<number>::operator=(SubviewCol<number>&& svc) noexcept
		{
			Mat_._mat().col(j_) = svc.Mat_._mat().col(svc.j_);
			return *this;
		}

		template<typename number>
		SubviewCol<number>::operator Vector<number>() const
		{
			return Vector<number>(Mat_._mat().col(j_));
		}

		/// DATA ACCESS
		template<typename number>
		number& SubviewCol<number>::at(count_t n) {
			return Mat_.at(n, j_);
		}

		template<typename number>
		const number& SubviewCol<number>::at(count_t n) const {
			return Mat_.at(n, j_);
		}


		template<typename number>
		Vector<real> SubviewCol<number>::re() const {
			return Vector<number>(*this).re();
		}

		template<typename number>
		Vector<real> SubviewCol<number>::im() const {
			return Vector<number>(*this).im();
		}

		/// MATH OPERATORS

		template<typename number>
		template<typename number1, typename>
		SubviewCol<number>& SubviewCol<number>::operator/=(number1 t) {
			Mat_._mat().col(j_) /= t;
			return *this;
		}

		template<typename number>
		template<typename number1, typename>
		SubviewCol<number>& SubviewCol<number>::operator*=(number1 t) {
			Mat_._mat().col(j_) *= t;
			return *this;
		}

		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator+=(const SubviewCol& other) {
			Mat_._mat().col(j_) += other.Mat_._mat().col(other.j_);
			return *this;
		};

		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator-=(const SubviewCol& other) {
			Mat_._mat().col(j_) -= other.Mat_._mat().col(other.j_);
			return *this;
		};

		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator+=(const Vector<number>& other) {
			Mat_._mat().col(j_) += other._vec();
			return *this;
		};

		template<typename number>
		SubviewCol<number>& SubviewCol<number>::operator-=(const Vector<number>& other) {
			Mat_._mat().col(j_) -= other._vec();
			return *this;
		};

		template<typename number>
		template<typename number1, typename>
		SubviewCol<number>& SubviewCol<number>::operator+=(const number1 t) {
			Mat_._mat().col(j_) += t;
			return *this;
		}

		template<typename number>
		template<typename number1, typename>
		SubviewCol<number>& SubviewCol<number>::operator-=(const number1 t) {
			Mat_._mat().col(j_) -= t;
			return *this;
		}

		template<typename number>
		Vector<number> SubviewCol<number>::operator-() {
			return Vector<number>(-Mat_._mat().col(j_));
		}

		/// MISC
		template<typename number>
		count_t SubviewCol<number>::size() const {
			return Mat_.n_rows();
		}

		template<typename number>
		real SubviewCol<number>::norm() const {
			return Vector<number>(*this).norm(); // todo: is this ok?
		}

		template<typename number>
		Vector<real> SubviewCol<number>::absSquare() const {
			return Vector<number>(*this).absSquare();
		}

		template <typename number>
		bool SubviewCol<number>::hasNan() const
		{
			return Mat_._mat().col(j_).has_nan();
		}
	}
}

namespace qengine {
	namespace internal{

		template <typename number1, typename number2>
		NumberMax_t<number1, number2> dot(const SubviewCol<number1>& left, const SubviewCol<number2>& right)
		{
			return dot(Vector<number1>(left), Vector<number2>(right));
		}

		template <typename number1, typename number2>
		NumberMax_t<number1, number2> cdot(const SubviewCol<number1>& left, const SubviewCol<number2>& right)
		{
			return cdot(Vector<number1>(left), Vector<number2>(right));
		}

		template<typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator+ (const SubviewCol<number1>& left, const SubviewCol<number2>& right) {
			return Vector<NumberMax_t<number1, number2>>(Vector<number1>(left) + Vector<number2>(right));
		}

		template <typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator-(const SubviewCol<number1>& left, const Vector<number2>& right)
		{
			return static_cast<Vector<number1>>(left) - right;
		}

		template <typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator-(const Vector<number1>& left, const SubviewCol<number2>& right)
		{
			return  left-static_cast<Vector<number2>>(right);
		}

		template <typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator-(const SubviewCol<number1>& left, const SubviewCol<number2>& right)
		{
			return  static_cast<Vector<number1>>(left) - static_cast<Vector<number2>>(right);
		}
	}
}


namespace qengine {
	namespace internal{
		template class SubviewCol<real>;
		template class SubviewCol<complex>;


		template SubviewCol<real>& SubviewCol<real>::operator/= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator/= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator/= (complex);

		template SubviewCol<real>& SubviewCol<real>::operator*= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator*= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator*= (complex);

		template SubviewCol<real>& SubviewCol<real>::operator+= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator+= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator+= (complex);

		template SubviewCol<real>& SubviewCol<real>::operator-= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator-= (real);
		template SubviewCol<complex>& SubviewCol<complex>::operator-= (complex);

		template Vector<real> operator+ (const SubviewCol<real>& left, const SubviewCol<real>& right);
		template Vector<complex> operator+ (const SubviewCol<complex>& left, const SubviewCol<real>& right);
		template Vector<complex> operator+ (const SubviewCol<real>& left, const SubviewCol<complex>& right);
		template Vector<complex> operator+ (const SubviewCol<complex>& left, const SubviewCol<complex>& right);

		template real       dot(const SubviewCol<real>&     left, const SubviewCol<real>& right);
		template complex    dot(const SubviewCol<complex>&  left, const SubviewCol<real>& right);
		template complex    dot(const SubviewCol<real>&     left, const SubviewCol<complex>& right);
		template complex    dot(const SubviewCol<complex>&  left, const SubviewCol<complex>& right);

		template complex    cdot(const SubviewCol<complex>& left, const SubviewCol<real>& right);
		template complex    cdot(const SubviewCol<complex>& left, const SubviewCol<complex>& right);

		template Vector<real> operator- (const Vector<real>& left, const SubviewCol<real>& right);
		template Vector<complex> operator- (const Vector<complex>& left, const SubviewCol<real>& right);
		template Vector<complex> operator- (const Vector<real>& left, const SubviewCol<complex>& right);
		template Vector<complex> operator- (const Vector<complex>& left, const SubviewCol<complex>& right);

		template Vector<real> operator- (const SubviewCol<real>& left, const Vector<real>& right);
		template Vector<complex> operator- (const SubviewCol<complex>& left, const Vector<real>& right);
		template Vector<complex> operator- (const SubviewCol<real>& left, const Vector<complex>& right);
		template Vector<complex> operator- (const SubviewCol<complex>& left, const Vector<complex>& right);

		template Vector<real> operator- (const SubviewCol<real>& left, const SubviewCol<real>& right);
		template Vector<complex> operator- (const SubviewCol<complex>& left, const SubviewCol<real>& right);
		template Vector<complex> operator- (const SubviewCol<real>& left, const SubviewCol<complex>& right);
		template Vector<complex> operator- (const SubviewCol<complex>& left, const SubviewCol<complex>& right);
	}
}



