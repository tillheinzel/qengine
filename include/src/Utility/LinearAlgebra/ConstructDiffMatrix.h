﻿/* COPYRIGHT
 *
 * file="ConstructDiffMatrix.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

namespace qengine
{
	namespace internal
	{
		//SqHermitianBandMat<real> ConstructDiffMatrix(count_t dim, count_t order, count_t width, real dx);
	}
}
