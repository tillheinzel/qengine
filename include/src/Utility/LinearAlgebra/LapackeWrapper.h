﻿/* COPYRIGHT
 *
 * file="LapackeWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

/*
 * Our own wrapper for lapacke. Not particularly impressive currently.
 */

#if USE_MKL != 0
	#include <mkl_lapacke.h>
	#include <mkl_cblas.h>
#else
	#include <lapacke.h>
	#include <cblas.h>
#endif
