﻿/* COPYRIGHT
 *
 * file="SqSparseMat.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/SparseMatrix.h"
#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/Spectrum.h"

namespace qengine
{
	namespace internal
	{
		template<class> class SqDiagMat;
		template<class> class SqDenseMat;
		template<class> class SqSparseMat;
		template<class> class SqHermitianBandMat;

		template<typename number>
		class SqSparseMat
		{
			// CTOR/DTOR
		public:
			explicit SqSparseMat(count_t dim);
			explicit SqSparseMat(const SparseMatrix<number>& mat);
			explicit SqSparseMat(SparseMatrix<number>&& mat);

			explicit SqSparseMat(Vector<number>&& v);
			explicit SqSparseMat(const Vector<number>& v);

			// todo: implement these ctors

			template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
			explicit SqSparseMat(const SqSparseMat<T>& other);
			
			// DATA ACCESS
		public:
			SparseMatrix<number>& mat();
			const SparseMatrix<number>& mat() const;


			// MATH OPERATIONS
		public:
			void multiplyInPlace(Vector<complex>& vector) const;

			template<typename T = number, typename = std::enable_if_t<std::is_same<T, real>::value>>
			void multiplyInPlace(Vector<real>& vector) const;


			SqSparseMat& operator+=(const SqDiagMat<number>& other);


			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator/=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator*=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator+=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator-=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator+=(const SqSparseMat<number1>& t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqSparseMat& operator-=(const SqSparseMat<number1>& t);

			SqSparseMat operator-() const;


			number at(count_t nRow, count_t nCol) const;

			//MISC
		public:
			count_t dim() const;
			bool isHermitian() const;

			bool hasNan() const;

		private:
			SparseMatrix<number> mat_;
		};
	}
}

namespace qengine {
	namespace internal {
		std::ostream& operator<<(std::ostream& stream, const SqSparseMat<real>& matrix);
		std::ostream& operator<<(std::ostream& stream, const SqSparseMat<complex>& matrix);
	}
}

namespace qengine
{
	namespace internal
	{

		template<typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> operator+ (const SqSparseMat<number1>& left, const SqDiagMat<number2>& right);

		template<typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> operator* (const SqSparseMat<number1>& left, const SqSparseMat<number2>& right);

		template<typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> kron(const SqSparseMat<number1>& left, const SqSparseMat<number2>& right);

		template<typename number>
		SqSparseMat<number> exp(const SqSparseMat<number>& d);

		template<>
		SqSparseMat<real> identityMatrix<SqSparseMat>(const count_t dim);

		template <typename Number>
		Spectrum<complex> getSpectrum(const SqSparseMat<Number>& matrix, count_t largestEigenstate, real normalization, count_t extraStates, real tol = 0.0);

		template<typename Number>
		Spectrum<real> getSpectrum_herm(const SqSparseMat<Number>& matrix, count_t largestEigenstate, real normalization, count_t extraStates, real tol = 0.0);

		template <typename Number>
		Spectrum<complex> getSpectrum(const SqSparseMat<Number>& matrix, const count_t largestEigenstate, const real normalization = 1.0, const real tol = 0.0) { return internal::getSpectrum(matrix, largestEigenstate, normalization, 0ull, tol); }

		template<typename Number>
		Spectrum<real> getSpectrum_herm(const SqSparseMat<Number>& matrix, const count_t largestEigenstate, const real normalization = 1.0, const real tol = 0.0) { return internal::getSpectrum_herm(matrix, largestEigenstate, normalization, 0ull, tol); }

	}
}
