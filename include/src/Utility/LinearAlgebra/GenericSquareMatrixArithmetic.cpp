﻿/* COPYRIGHT
 *
 * file="GenericSquareMatrixArithmetic.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"

#include "src/Utility/LinearAlgebra/SqDenseMat.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

namespace 
{
	template<typename number>
	const auto& rep(const qengine::internal::SqDiagMat<number>& mat) { return mat.diag(); }

	template<typename number>
	const auto& rep(const qengine::internal::SqDenseMat<number>& mat) { return mat.mat(); }
	template<typename number>
	const auto& rep(const qengine::internal::SqSparseMat<number>& mat) { return mat.mat(); }
	template<typename number>
	const auto& rep(const qengine::internal::SqHermitianBandMat<number>& mat) { return mat.bands(); }
	
	template<class number, template<class> class SqMat, class numberMat>
	SqMat<number> convToNumber(const SqMat<numberMat>& m, number)
	{
        return SqMat<number>(m);
	}

	template<class number, template<class> class SqMat>
	const SqMat<number>& convToNumber(const SqMat<number>& m, number)
	{
		return m;
	}
}

namespace qengine
{
	namespace internal
	{

		template<typename number, template<typename> class SqMat, typename>
		CVec operator* (const SqMat<number>& mat, CVec vec)
		{
			mat.multiplyInPlace(vec);
			return vec;
		}

		template<typename number1, typename number2, template<typename> class SqMat, typename>
		SqMat<NumberMax_t<number1, number2>> operator+(const SqMat<number1>& left, const SqMat<number2>& right)
		{
			auto retval = convToNumber<NumberMax_t<number1, number2>>(left);
			retval += right;

			return retval;
		}

		template<typename number1, typename number2, template<typename> class SqMat, typename>
		SqMat<NumberMax_t<number1, number2>> operator-(const SqMat<number1>& left, const SqMat<number2>& right)
		{
			auto retval = convToNumber<NumberMax_t<number1, number2>>(left);
			retval -= right;

			return retval;
		}
		
		template<typename number1, typename number2, template<typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator*(number1 left, const SqMat<number2>& right)
		{
			SqMat<NumberMax_t<number1, number2>> retval = convToNumber<NumberMax_t<number1, number2>>(right);
			retval *= left;

			return retval;
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator*(const SqMat<number2>& left, number1 right)
		{
			return right * left;
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator/(const SqMat<number2>& left, number1 right)
		{
			SqMat<NumberMax_t<number1, number2>> retval = convToNumber<NumberMax_t<number1, number2>>(left);
			retval /= right;

			return retval;
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator+(number1 left, const SqMat<number2>& right)
		{
			SqMat<NumberMax_t<number1, number2>> retval = convToNumber<NumberMax_t<number1, number2>>(right);
			retval += left;

			return retval;
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator+(const SqMat<number2>& left, number1 right)
		{
			return right + left;
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator-(number1 left, const SqMat<number2>& right)
		{
			return right + (-left);
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		SqMat<NumberMax_t<number1, number2>> operator-(const SqMat<number2>& left, number1 right)
		{
			return  left + (-right);
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		bool operator==(const SqMat<number1>& left, const SqMat<number2>& right)
		{
			return rep(left) == rep(right);
		}

		template <typename number1, typename number2, template <typename> class SqMat, typename, typename>
		bool operator!=(const SqMat<number1>& left, const SqMat<number2>& right)
		{
			return !(left == right);
		}
	}
}


namespace qengine
{
	namespace internal
	{
		// Diagmat
		template CVec operator* (const SqDiagMat<real>&, CVec);
		template CVec operator* (const SqDiagMat<complex>&, CVec);

        template SqDiagMat<real>    operator+ (const SqDiagMat<real>&, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator+ (const SqDiagMat<complex>&, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator+ (const SqDiagMat<real>&, const SqDiagMat<complex>&);
		template SqDiagMat<complex> operator+ (const SqDiagMat<complex>&, const SqDiagMat<complex>&);

        template SqDiagMat<real>    operator- (const SqDiagMat<real>&, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator- (const SqDiagMat<complex>&, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator- (const SqDiagMat<real>&, const SqDiagMat<complex>&);
		template SqDiagMat<complex> operator- (const SqDiagMat<complex>&, const SqDiagMat<complex>&);
		
        template SqDiagMat<real>    operator* (real, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator* (complex, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator* (real, const SqDiagMat<complex>&);
		template SqDiagMat<complex> operator* (complex, const SqDiagMat<complex>&);

		template SqDiagMat<real>    operator* (const SqDiagMat<real>&, real);
		template SqDiagMat<complex> operator* (const SqDiagMat<real>&, complex);
		template SqDiagMat<complex> operator* (const SqDiagMat<complex>&, real);
		template SqDiagMat<complex> operator* (const SqDiagMat<complex>&, complex);

        template SqDiagMat<real>    operator/ (const SqDiagMat<real>&, real);
		template SqDiagMat<complex> operator/ (const SqDiagMat<real>&, complex);
		template SqDiagMat<complex> operator/ (const SqDiagMat<complex>&, real);
		template SqDiagMat<complex> operator/ (const SqDiagMat<complex>&, complex);

        template SqDiagMat<real>    operator+ (real, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator+ (complex, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator+ (real, const SqDiagMat<complex>&);
		template SqDiagMat<complex> operator+ (complex, const SqDiagMat<complex>&);

        template SqDiagMat<real>    operator+ (const SqDiagMat<real>&, real);
		template SqDiagMat<complex> operator+ (const SqDiagMat<real>&, complex);
		template SqDiagMat<complex> operator+ (const SqDiagMat<complex>&, real);
		template SqDiagMat<complex> operator+ (const SqDiagMat<complex>&, complex);

        template SqDiagMat<real>    operator- (real, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator- (complex, const SqDiagMat<real>&);
		template SqDiagMat<complex> operator- (real, const SqDiagMat<complex>&);
		template SqDiagMat<complex> operator- (complex, const SqDiagMat<complex>&);

        template SqDiagMat<real>    operator- (const SqDiagMat<real>&, real);
		template SqDiagMat<complex> operator- (const SqDiagMat<real>&, complex);
		template SqDiagMat<complex> operator- (const SqDiagMat<complex>&, real);
		template SqDiagMat<complex> operator- (const SqDiagMat<complex>&, complex);

		template bool operator== (const SqDiagMat<real>&, const SqDiagMat<real>&);
		template bool operator== (const SqDiagMat<complex>&, const SqDiagMat<real>&);
		template bool operator== (const SqDiagMat<real>&, const SqDiagMat<complex>&);
		template bool operator== (const SqDiagMat<complex>&, const SqDiagMat<complex>&);

		template bool operator!= (const SqDiagMat<real>&, const SqDiagMat<real>&);
		template bool operator!= (const SqDiagMat<complex>&, const SqDiagMat<real>&);
		template bool operator!= (const SqDiagMat<real>&, const SqDiagMat<complex>&);
		template bool operator!= (const SqDiagMat<complex>&, const SqDiagMat<complex>&);

		// SqDenseMat
		template CVec operator* (const SqDenseMat<real>&, CVec);
		template CVec operator* (const SqDenseMat<complex>&, CVec);

        template SqDenseMat<real>    operator+ (const SqDenseMat<real>&, const SqDenseMat<real>&);
        template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator+ (const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, const SqDenseMat<complex>&);

        template SqDenseMat<real>    operator- (const SqDenseMat<real>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator- (const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator- (const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator- (const SqDenseMat<complex>&, const SqDenseMat<complex>&);

        template SqDenseMat<real>    operator* (real, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator* (complex, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator* (real, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator* (complex, const SqDenseMat<complex>&);

        template SqDenseMat<real>    operator* ( const SqDenseMat<real>&, real);
        template SqDenseMat<complex> operator* ( const SqDenseMat<real>&, complex);
        template SqDenseMat<complex> operator* ( const SqDenseMat<complex>&, real);
        template SqDenseMat<complex> operator* ( const SqDenseMat<complex>&, complex);

        template SqDenseMat<real>    operator/ (const SqDenseMat<real>&, real);
		template SqDenseMat<complex> operator/ (const SqDenseMat<real>&, complex);
		template SqDenseMat<complex> operator/ (const SqDenseMat<complex>&, real);
		template SqDenseMat<complex> operator/ (const SqDenseMat<complex>&, complex);

        template SqDenseMat<real>    operator+ (real, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator+ (complex, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator+ (real, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator+ (complex, const SqDenseMat<complex>&);

        template SqDenseMat<real>    operator+ (const SqDenseMat<real>&, real);
		template SqDenseMat<complex> operator+ (const SqDenseMat<real>&, complex);
		template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, real);
		template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, complex);

        template SqDenseMat<real>    operator- (real, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator- (complex, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator- (real, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator- (complex, const SqDenseMat<complex>&);

        template SqDenseMat<real>    operator- (const SqDenseMat<real>&, real);
		template SqDenseMat<complex> operator- (const SqDenseMat<real>&, complex);
		template SqDenseMat<complex> operator- (const SqDenseMat<complex>&, real);
		template SqDenseMat<complex> operator- (const SqDenseMat<complex>&, complex);

		template bool operator== (const SqDenseMat<real>&, const SqDenseMat<real>&);
		template bool operator== (const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template bool operator== (const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template bool operator== (const SqDenseMat<complex>&, const SqDenseMat<complex>&);

		template bool operator!= (const SqDenseMat<real>&, const SqDenseMat<real>&);
		template bool operator!= (const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template bool operator!= (const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template bool operator!= (const SqDenseMat<complex>&, const SqDenseMat<complex>&);

		// SqHermitianBandMat
		template CVec operator* (const SqHermitianBandMat<real>&, CVec);

		template SqHermitianBandMat<real> operator+ (const SqHermitianBandMat<real>&, const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real> operator- (const SqHermitianBandMat<real>&, const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real> operator* (real, const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real> operator* (const SqHermitianBandMat<real>&, real);

		template SqHermitianBandMat<real> operator/ (const SqHermitianBandMat<real>&, real);

		template SqHermitianBandMat<real> operator+ (real, const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real> operator+ (const SqHermitianBandMat<real>&, real);

		template SqHermitianBandMat<real> operator- (real, const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real> operator- (const SqHermitianBandMat<real>&, real);

		template bool operator== (const SqHermitianBandMat<real>&, const SqHermitianBandMat<real>&);

		template bool operator!= (const SqHermitianBandMat<real>&, const SqHermitianBandMat<real>&);

        // SqSparseMat
        template CVec operator* (const SqSparseMat<real>&, CVec);
        template CVec operator* (const SqSparseMat<complex>&, CVec);

        template SqSparseMat<real>    operator+ (const SqSparseMat<real>&, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator+ (const SqSparseMat<complex>&, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator+ (const SqSparseMat<real>&, const SqSparseMat<complex>&);
        template SqSparseMat<complex> operator+ (const SqSparseMat<complex>&, const SqSparseMat<complex>&);

        template SqSparseMat<real>    operator- (const SqSparseMat<real>&, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator- (const SqSparseMat<complex>&, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator- (const SqSparseMat<real>&, const SqSparseMat<complex>&);
        template SqSparseMat<complex> operator- (const SqSparseMat<complex>&, const SqSparseMat<complex>&);

        template SqSparseMat<real>    operator* (real, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator* (complex, const SqSparseMat<real>&);
        template SqSparseMat<complex> operator* (real, const SqSparseMat<complex>&);
        template SqSparseMat<complex> operator* (complex, const SqSparseMat<complex>&);

		template SqSparseMat<real>    operator* (const SqSparseMat<real>&, real);
		template SqSparseMat<complex> operator* (const SqSparseMat<real>&, complex);
		template SqSparseMat<complex> operator* (const SqSparseMat<complex>&, real);
		template SqSparseMat<complex> operator* (const SqSparseMat<complex>&, complex);

        template SqSparseMat<real>    operator/ (const SqSparseMat<real>&, real);
        template SqSparseMat<complex> operator/ (const SqSparseMat<real>&, complex);
        template SqSparseMat<complex> operator/ (const SqSparseMat<complex>&, real);
        template SqSparseMat<complex> operator/ (const SqSparseMat<complex>&, complex);

		template bool operator== (const SqSparseMat<real>&, const SqSparseMat<real>&);
		template bool operator== (const SqSparseMat<complex>&, const SqSparseMat<real>&);
		template bool operator== (const SqSparseMat<real>&, const SqSparseMat<complex>&);
		template bool operator== (const SqSparseMat<complex>&, const SqSparseMat<complex>&);

		template bool operator!= (const SqSparseMat<real>&, const SqSparseMat<real>&);
		template bool operator!= (const SqSparseMat<complex>&, const SqSparseMat<real>&);
		template bool operator!= (const SqSparseMat<real>&, const SqSparseMat<complex>&);
		template bool operator!= (const SqSparseMat<complex>&, const SqSparseMat<complex>&);

    }
}
