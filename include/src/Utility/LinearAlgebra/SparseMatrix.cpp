﻿/* COPYRIGHT
 *
 * file="SparseMatrix.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#include "src/Utility/LinearAlgebra/SparseMatrix.h"

#include <armadillo>

namespace
{
	template<class A, class B, class T = std::enable_if_t<std::is_same<A, B>::value>>
	const arma::SpMat<B>& maybe_conv(const arma::SpMat<B>& b) { return b; }

	template<class A, class B, class T = std::enable_if_t<!std::is_same<A, B>::value && std::is_same<A, qengine::complex>::value && std::is_same<B, qengine::real>::value >>
	arma::SpMat<qengine::complex> maybe_conv(const  arma::SpMat<B>& b) { return arma::sp_cx_mat(b, arma::sp_mat(b.n_rows, b.n_cols)); }

	//template<>
	//void maybe_conv<qengine::real>(const arma::sp_cx_mat& b) { static_assert(); }

}

namespace qengine{
    namespace internal{
        /// CTORS


        template<typename number>
        SparseMatrix<number>::SparseMatrix(count_t n_rows,count_t n_cols): mat_(std::make_unique<arma::SpMat<number>>(n_rows,n_cols))
        {
        }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(const arma::Mat<number>& data): mat_(std::make_unique<arma::SpMat<number>>(data))
        {
        }


        template<typename number>
        SparseMatrix<number>::SparseMatrix(arma::Mat<number>&& data): mat_(std::make_unique<arma::SpMat<number>>(std::move(data)))
        {
        }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(const arma::SpMat<number>& data): mat_(std::make_unique<arma::SpMat<number>>(data))
        {
        }


        template<typename number>
        SparseMatrix<number>::SparseMatrix(arma::SpMat<number>&& data): mat_(std::make_unique<arma::SpMat<number>>(std::move(data)))
        {
        }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(const Matrix<number>& data): mat_(std::make_unique<arma::SpMat<number>>(data._mat()))
        {
        }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(Matrix<number>&& data): mat_(std::make_unique<arma::SpMat<number>>(std::move(data._mat())))
        {
        }

        template<typename number>
        template<typename T2, typename T>
        SparseMatrix<number>::SparseMatrix(const SparseMatrix<T>& realMat, const SparseMatrix<T>& imagMat): mat_(std::make_unique<arma::SpMat<number>>(realMat.mat(),imagMat.mat()))
        {
        }

	    template <typename number>
	    template <typename T2, typename T>
	    SparseMatrix<number>::SparseMatrix(const SparseMatrix<T>& realMat): mat_(std::make_unique<arma::SpMat<number>>(maybe_conv<number>(realMat.mat())))
	    {
	    }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(const SparseMatrix &other): mat_(std::make_unique<arma::SpMat<number>>(*other.mat_))
        {
        }

        template<typename number>
        SparseMatrix<number>::SparseMatrix(SparseMatrix&& other) noexcept : mat_(std::move(other.mat_)){}
		
        template<typename number>
        SparseMatrix<number>::~SparseMatrix() = default;

        /// ASSIGNMENT

        template<typename number>
        SparseMatrix<number>& SparseMatrix<number>::operator=(const SparseMatrix &other) {
            if(this == &other){
                return *this;
            }
            *mat_ = *other.mat_;
            return *this;
        }

	    template <typename number>
	    SparseMatrix<number>& SparseMatrix<number>::operator=(SparseMatrix&& other) noexcept
	    {
			if (this == &other) {
				return *this;
			}
			mat_ = std::move(other.mat_);
			return *this;
	    }


	    /// DATA ACCESS

        template<typename number>
		SparseElement<number> SparseMatrix<number>::at(count_t row_index,count_t col_index){
            return SparseElement<number>(mat_->at(row_index,col_index));
        }

        template<typename number>
        const SparseElement<number> SparseMatrix<number>::at(count_t row_index,count_t col_index) const{
            return SparseElement<number>(mat_->at(row_index,col_index));
        }

	    template <typename number>

		SparseElement<number> SparseMatrix<number>::at(count_t linearIndex)
	    {
			return SparseElement<number>(mat_->at(linearIndex));
	    }

	    template <typename number>
	    const SparseElement<number> SparseMatrix<number>::at(count_t linearIndex) const
	    {
			return SparseElement<number>(mat_->at(linearIndex));
	    }


	    template<typename number>
        arma::SpMat<number>& SparseMatrix<number>::mat()
        {
            return *mat_;
        }

        template<typename number>
        const arma::SpMat<number>& SparseMatrix<number>::mat() const
        {
            return *mat_;
        }

        template<typename number>
        SparseMatrix<real> SparseMatrix<number>::re() const
        {
            return SparseMatrix<real>(arma::real(mat()));
        }

        template<typename number>
        SparseMatrix<real> SparseMatrix<number>::im() const
        {
            return SparseMatrix<real>(arma::real(mat()));
        }


        /// MATH OPERATIONS

        template <typename number>
        template <typename number1, typename>
        SparseMatrix<number>& SparseMatrix<number>::operator/=(number1 t)
        {
            *mat_ /= t;
            return *this;
        }

        template <typename number>
        template <typename number1, typename>
        SparseMatrix<number>& SparseMatrix<number>::operator*=(number1 t)
        {
            *mat_ *= t;
            return *this;
        }


        template<typename number>
        template<typename number1, typename >
        SparseMatrix<number>& SparseMatrix<number>::operator+=(const SparseMatrix<number1>& other)
        {
            *mat_ += maybe_conv<number>(other.mat());
            return *this;
        }

        template<typename number>
        template<typename number1, typename >
        SparseMatrix<number>& SparseMatrix<number>::operator-=(const SparseMatrix<number1>& other)
        {
            *mat_ -= maybe_conv<number>(other.mat());
            return *this;
        }

        template<typename number>
        SparseMatrix<number> SparseMatrix<number>::operator-() const{
            return SparseMatrix<number>(-mat());
        }

        /// MISC
        template<typename number>
        count_t SparseMatrix<number>::n_rows() const{
            return mat_->n_rows;
        }

        template<typename number>
        count_t SparseMatrix<number>::n_cols() const{
            return mat_->n_cols;
        }


        template<typename number>
        count_t SparseMatrix<number>::n_elem() const{
            return mat_->n_elem;
        }

	    template <typename number>
	    bool SparseMatrix<number>::hasNan() const
	    {
			return mat_->has_nan();
	    }
    }
}

namespace qengine
{
    namespace internal
    {
        std::ostream& operator<<(std::ostream& stream,const SparseMatrix<real>& matrix){
            stream << matrix.mat() << std::endl;
            return stream;
        }

        std::ostream& operator<<(std::ostream& stream,const SparseMatrix<complex>& matrix){
            stream << matrix.mat() << std::endl;
            return stream;
        }

		namespace
		{
			template <typename number>
			bool equalsImplementation(const SparseMatrix<number>& left, const SparseMatrix<number>& right)
			{
				if (left.n_rows() != right.n_rows()) return false;
				if (left.n_cols() != right.n_cols()) return false;
				if (left.mat().n_nonzero != right.mat().n_nonzero) return false;

				for (auto i_row = 0u; i_row<left.n_rows(); ++i_row)
				{
					for (auto i_col = 0u; i_col < left.n_cols(); ++i_col)
					{
						if (complex(left.at(i_row, i_col)) != complex(left.at(i_row, i_col))) return false;
					}
				}
				return true;
			}

			bool equalsImplementation(const Sp_RMat& left, const Sp_CMat& right)
			{
				Sp_CMat complexLeft{ left };
				return equalsImplementation(complexLeft, right);
			}

			bool equalsImplementation(const Sp_CMat& left, const Sp_RMat& right)
			{
				return equalsImplementation(right, left);
			}
		}

	    template <typename number1, typename number2>
	    bool operator==(const SparseMatrix<number1>& left, const SparseMatrix<number2>& right)
	    {
			return equalsImplementation(left, right);
	    }

	    template <typename number1, typename number2>
	    bool operator!=(const SparseMatrix<number1>& left, const SparseMatrix<number2>& right)
	    {
			return !(left == right);
	    }

	    template<typename number1, typename number2>
        SparseMatrix<NumberMax_t<number1, number2>> operator+ (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right){
            return SparseMatrix<NumberMax_t<number1,number2>>(maybe_conv<NumberMax_t<number1, number2>>(left.mat())+ maybe_conv<NumberMax_t<number1, number2>>(right.mat()));
        }


        template<typename number1, typename number2>
        SparseMatrix<NumberMax_t<number1, number2>> operator- (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right){
            return SparseMatrix<NumberMax_t<number1,number2>>(maybe_conv<NumberMax_t<number1, number2>>(left.mat())- maybe_conv<NumberMax_t<number1, number2>>(right.mat()));
        }



        template<typename number1, typename number2, typename>
        SparseMatrix<NumberMax_t<number1, number2>> operator* (number1 left, const SparseMatrix<number2>& right){
            return SparseMatrix<NumberMax_t<number1,number2>>(left*maybe_conv<NumberMax_t<number1, number2>>(right.mat()));
        };

        template<typename number1, typename number2, typename>
        SparseMatrix<NumberMax_t<number1, number2>> operator* (const SparseMatrix<number1>& left,number2 right){
            return SparseMatrix<NumberMax_t<number1,number2>>(right*maybe_conv<NumberMax_t<number1, number2>>(left.mat()));
        };

        template<typename number1,typename number2>
        SparseMatrix<NumberMax_t<number1,number2>> operator* (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right){
            return SparseMatrix<NumberMax_t<number1,number2>>(maybe_conv<NumberMax_t<number1, number2>>(left.mat())*maybe_conv<NumberMax_t<number1, number2>>(right.mat()));
        }

        template<typename number1, typename number2, typename>
        SparseMatrix<NumberMax_t<number1, number2>> operator/ (const SparseMatrix<number1>& left,number2 right){
            return SparseMatrix<NumberMax_t<number1,number2>>(maybe_conv<NumberMax_t<number1, number2>>(left.mat())/right);
        };

        template<typename number1,typename number2>
        SparseMatrix<NumberMax_t<number1,number2>> operator% (const SparseMatrix<number1>& left,const SparseMatrix<number2>& right){
            return SparseMatrix<NumberMax_t<number1,number2>>(maybe_conv<NumberMax_t<number1, number2>>(left.mat())% maybe_conv<NumberMax_t<number1, number2>>(right.mat()));
        }

	    template <typename number1, typename number2>
	    SparseMatrix<NumberMax_t<number1, number2>> kron(const SparseMatrix<number1>& left, const SparseMatrix<number2>& right)
	    {
			using number3 = NumberMax_t<number1, number2>;

			arma::SpMat<number1> A = left.mat();
			arma::SpMat<number2> B = right.mat();

		    auto a_data = arma::Col<number1>(A.values, A.n_nonzero);
		    auto b_data = arma::Col<number2>(B.values, B.n_nonzero);

		    auto c_data = arma::Col<number3>(A.n_nonzero * B.n_nonzero, arma::fill::zeros);


		    arma::uvec rowind(A.n_nonzero * B.n_nonzero, arma::fill::zeros);
		    arma::uvec colptr(A.n_cols * B.n_cols + 1);

		    auto rowind_a = arma::uvec(A.row_indices, A.n_nonzero);
		    auto rowind_b = arma::uvec(B.row_indices, B.n_nonzero);

		    auto colptr_a = arma::uvec(A.col_ptrs, A.n_cols + 1);
		    auto colptr_b = arma::uvec(B.col_ptrs, B.n_cols + 1);

		    auto i_c = 0u;
		    //std::cout << colptr_a << std::endl;
		    //std::cout << colptr_b << std::endl;
		    //std::cout << rowind_a << std::endl;
		    //std::cout << rowind_b << std::endl;

		    auto it_colptr = colptr.begin();
		    for (auto col_a = 0ull; col_a < A.n_cols; ++col_a)
		    {
			    for (auto col_b = 0ull; col_b < B.n_cols; ++col_b)
			    {
				    for (auto i_a = colptr_a(col_a); i_a < colptr_a(col_a + 1); ++i_a)
				    {
					    for (auto i_b = colptr_b(col_b); i_b < colptr_b(col_b + 1); ++i_b)
					    {
						    c_data(i_c) = a_data(i_a) * b_data(i_b);
						    rowind(i_c) = rowind_a(i_a) * A.n_rows + rowind_b(i_b);
						    ++i_c;
					    }
				    }
					++it_colptr;
				    *it_colptr = i_c;
			    }
		    }

		    return SparseMatrix<NumberMax_t<number1, number2>>(arma::SpMat<number3>(rowind, colptr, c_data, A.n_rows * B.n_rows, A.n_cols * B.n_cols));
	    }
    }
}


namespace qengine{
    namespace internal{
        // EXPLICIT INSTANTIATION OF TEMPLATES

        template class SparseMatrix<real>;
        template class SparseMatrix<complex>;

        template SparseMatrix<complex>::SparseMatrix(const SparseMatrix<real>&,const SparseMatrix<real>&);
        template SparseMatrix<complex>::SparseMatrix(const SparseMatrix<real>&);

        template SparseMatrix<real>&    SparseMatrix<real>::operator*= (real);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator*= (real);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator*= (complex);

        template SparseMatrix<real>&    SparseMatrix<real>::operator/= (real);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator/= (real);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator/= (complex);

        template SparseMatrix<real>&    SparseMatrix<real>::operator+=(const SparseMatrix<real>&);
		template SparseMatrix<complex>& SparseMatrix<complex>::operator+=(const SparseMatrix<real>& other);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator+=(const SparseMatrix<complex>&);

        template SparseMatrix<real>&    SparseMatrix<real>::operator-=(const SparseMatrix<real>&);
		template SparseMatrix<complex>& SparseMatrix<complex>::operator-=(const SparseMatrix<real>& other);
        template SparseMatrix<complex>& SparseMatrix<complex>::operator-=(const SparseMatrix<complex>&);

		template bool operator== (const SparseMatrix<real>&    left, const SparseMatrix<real>&    right);
		template bool operator== (const SparseMatrix<real>&    left, const SparseMatrix<complex>& right);
		template bool operator== (const SparseMatrix<complex>& left, const SparseMatrix<real>&    right);
		template bool operator== (const SparseMatrix<complex>& left, const SparseMatrix<complex>& right);

		template bool operator!= (const SparseMatrix<real>&    left, const SparseMatrix<real>&    right);
		template bool operator!= (const SparseMatrix<real>&    left, const SparseMatrix<complex>& right);
		template bool operator!= (const SparseMatrix<complex>& left, const SparseMatrix<real>&    right);
		template bool operator!= (const SparseMatrix<complex>& left, const SparseMatrix<complex>& right);

        template  SparseMatrix<real>    operator+ (const SparseMatrix<real>& left,   const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator+ (const SparseMatrix<real>& left, const SparseMatrix<complex>& right);
		template  SparseMatrix<complex> operator+ (const SparseMatrix<complex>& left, const SparseMatrix<real>& right);
        template  SparseMatrix<complex> operator+ (const SparseMatrix<complex>& left,const SparseMatrix<complex>& right);

        template  SparseMatrix<real>    operator- (const SparseMatrix<real>& left,   const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator- (const SparseMatrix<real>& left, const SparseMatrix<complex>& right);
		template  SparseMatrix<complex> operator- (const SparseMatrix<complex>& left, const SparseMatrix<real>& right);
        template  SparseMatrix<complex> operator- (const SparseMatrix<complex>& left,const SparseMatrix<complex>& right);

        template  SparseMatrix<real>    operator* (real    left,   const SparseMatrix<real>&       right);
        template  SparseMatrix<complex> operator* (real    left,   const SparseMatrix<complex>&    right);
		template  SparseMatrix<complex> operator* (complex left, const SparseMatrix<real>&       right);
        template  SparseMatrix<complex> operator* (complex left,   const SparseMatrix<complex>&    right);

        template  SparseMatrix<real>    operator* (const SparseMatrix<real>&    left, real    right);
        template  SparseMatrix<complex> operator* (const SparseMatrix<complex>& left, real    right);
		template  SparseMatrix<complex> operator* (const SparseMatrix<real>&    left, complex right);
        template  SparseMatrix<complex> operator* (const SparseMatrix<complex>& left, complex right);

        template  SparseMatrix<real>    operator* (const SparseMatrix<real>&     left, const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator* (const SparseMatrix<complex>&  left, const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator* (const SparseMatrix<real>&     left, const SparseMatrix<complex>& right);
        template  SparseMatrix<complex> operator* (const SparseMatrix<complex>&  left, const SparseMatrix<complex>& right);

        template  SparseMatrix<real>    operator/ (const SparseMatrix<real>&    left, real    right);
        template  SparseMatrix<complex> operator/ (const SparseMatrix<complex>& left, real    right);
        template  SparseMatrix<complex> operator/ (const SparseMatrix<real>&    left, complex right);
        template  SparseMatrix<complex> operator/ (const SparseMatrix<complex>& left, complex right);

        template  SparseMatrix<real>    operator% (const SparseMatrix<real>& left,   const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator% (const SparseMatrix<complex>& left, const SparseMatrix<real>& right);
		template  SparseMatrix<complex> operator% (const SparseMatrix<real>& left, const SparseMatrix<complex>& right);
        template  SparseMatrix<complex> operator% (const SparseMatrix<complex>& left,   const SparseMatrix<complex>& right);

        template  SparseMatrix<real>    kron (const SparseMatrix<real>& left,   const SparseMatrix<real>& right);
		template  SparseMatrix<complex> kron(const SparseMatrix<complex>& left, const SparseMatrix<real>& right);
		template  SparseMatrix<complex> kron(const SparseMatrix<real>& left, const SparseMatrix<complex>& right);
        template  SparseMatrix<complex> kron(const SparseMatrix<complex>& left,   const SparseMatrix<complex>& right);
    }
}
