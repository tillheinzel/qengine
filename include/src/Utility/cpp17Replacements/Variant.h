﻿/* COPYRIGHT
 *
 * file="Variant.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>
#include <memory>

#include "src/Utility/TypeList.h"
#include "src/Utility/DeepCopy.h"

namespace qengine
{
	enum class TYPE: int;

	template<class T, class... Ts>
	constexpr TYPE classToType()
	{
		return static_cast<TYPE>(internal::findFirstIndex<T>(internal::TypeList<Ts...>{}));
	}

	template<class... Ts>
	class Variant
	{
	public:
		Variant() :type_(TYPE(sizeof...(Ts))){}

		template<class T, class = std::enable_if_t<internal::isInList<std::decay_t<T>, Ts...>(), void>>
		Variant(const T& t)
		:
		type_(classToType<T, Ts...>())
		{
			constexpr auto index = internal::findFirstIndex<std::decay_t<T>>(internal::TypeList<Ts...>{});
			//t.cheese();
			//static_assert(index == sizeof...(Ts),"");
			std::get<index>(vals_) = std::make_unique<std::decay_t<T>>(T(t));
		}

		Variant(const Variant& other) :
			type_(other.type_)
		{
			vals_ = internal::deep_copy(other.vals_, std::make_index_sequence<sizeof...(Ts)>{});
		}

		Variant(Variant&& other) noexcept:
			type_(other.type_),
			vals_(std::move(other.vals_))
		{
		}

		Variant& operator=(const Variant& other)
		{
			if (&other != this)
			{
				type_ = other.type_;
				vals_ = internal::deep_copy(other.vals_, std::make_index_sequence<sizeof...(Ts)>{});
			}

			return *this;
		}

		Variant& operator=(Variant&& other)
		{
			if (&other != this)
			{
				type_ = other.type_;
				vals_ = std::move(other.vals_);
			}

			return *this;
		}


		template<class T> 
		const T& get() const
		{
			constexpr auto index = internal::findFirstIndex<T>(internal::TypeList<Ts...>{});
			static_assert(index < sizeof...(Ts), "Error: Requested Type is not a valid type!");

			if (std::get<index>(vals_)) return *std::get<index>(vals_);
			throw std::runtime_error("this is not the correct type");
		}
		
		template<class T> 
		T& get()
		{
			constexpr auto index = internal::findFirstIndex<T>(internal::TypeList<Ts...>{});
			static_assert(index < sizeof...(Ts), "Error: Requested Type is not a valid type!");

			if (std::get<index>(vals_)) return *std::get<index>(vals_);
			throw std::runtime_error("this is not the correct type");
		}

		template<class T> void set(T&& expr)
		{
			constexpr auto index = internal::findFirstIndex<std::decay_t<T>>(internal::TypeList<Ts...>{});
			static_assert(index < sizeof...(Ts), "Error: Requested Type is not a valid type!");

			std::get<index>(vals_) = std::make_unique<std::decay_t<T>>(std::forward<T>(expr));
		}

		TYPE type() const { return type_; }

		template<class T>
		constexpr static TYPE toType() { return classToType<T, Ts...>(); }

		template<class T>
		constexpr static bool isInList() { return internal::isInList<T, Ts...>(); }

	private:
		TYPE type_;
		std::tuple<std::unique_ptr<Ts>...> vals_;
	};
}
