﻿/* COPYRIGHT
 *
 * file="and_fold.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/nostd/bool_constant.h"

namespace qengine
{
	namespace internal
	{
		constexpr bool and_fold_impl()
		{
			return true;
		}

		template<bool B, bool... Bs>
		constexpr bool and_fold_impl(nostd::bool_constant<B>, nostd::bool_constant<Bs>... bs)
		{
			return B ? and_fold_impl(bs...) : false; // this is to make certain the evaluation ends at the first false
		}

		template<bool... Bs>
		constexpr bool and_fold()
		{
			return and_fold_impl(nostd::bool_constant<Bs>()...);
		}
	}
}
