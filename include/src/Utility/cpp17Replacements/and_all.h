﻿/* COPYRIGHT
 *
 * file="and_all.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

/// "&&" functionality for parameter packs. Better solution exists in c++17.
namespace qengine
{
	namespace internal
	{
		template<typename ... V>
		constexpr bool and_all(const V &... v) {
			bool result = true;
			(void)std::initializer_list<int>{ (result = result && v, 0)... };
			return result;
		}

	}
}
