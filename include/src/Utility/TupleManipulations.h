﻿/* COPYRIGHT
 *
 * file="TupleManipulations.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

namespace qengine
{
	namespace internal
	{
		template<typename T, typename... Ts>
		auto head(std::tuple<T,Ts...> t)
		{
			return std::get<0>(t);
		}

		template < std::size_t... Ns, typename... Ts >
		auto tail_impl(std::index_sequence<Ns...>, std::tuple<Ts...> t)
		{
			return  std::make_tuple(std::get<Ns + 1u>(t)...);
		}

		template<std::size_t I = 1u, typename... Ts>
		auto tail(std::tuple<Ts...> t)
		{
			return  tail_impl(std::make_index_sequence<sizeof...(Ts)-I>(), t);
		}

		template<class... Ts>
		constexpr std::size_t size(std::tuple<Ts...>)
		{
			return sizeof...(Ts);
		}
	}
}
