﻿/* COPYRIGHT
 *
 * file="DataStruct.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <string>
#include <map>


namespace qengine
{
	class SerializedObject;

	class DataStruct
	{
	public:
		DataStruct();

		std::string typeId = "";
		std::map<std::string, SerializedObject> members;

	};
}
