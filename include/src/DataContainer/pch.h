#pragma once
#pragma warning(push, 0)

#include <memory>
#include <string>
#include <complex>

#include <map>
#include <vector>
#include <mutex>

#include <regex>

#include <fstream>

#include <nlohmann/json.hpp>

#include <armadillo>

#pragma warning(pop)