cmake_minimum_required (VERSION 3.3)
project (QEngine)

set(SOURCES
	DataContainer.cpp 
	DataContainer.h 

	JsonWrapper.h 
	JsonWrapper.cpp 
	
	pch.h
	pch.cpp

	isSerializable.h

	DataStruct.h
	DataStruct.cpp
	SerializedObject.h
	SerializedObject.cpp

	VectorSerialization.h
)

if(${ENABLE_MAT_IO})
	list(APPEND SOURCES
		MatioWrapper.h 
		MatioWrapper.cpp 
	)
endif()


add_library(DataContainer OBJECT ${SOURCES})
target_include_directories(DataContainer PRIVATE ${ARMADILLO_INCLUDE_DIR})	
target_include_directories(DataContainer PRIVATE ${JSON_INCLUDE_DIR})

# if(MSVC)
	# target_compile_options(DataContainer PRIVATE "/Zm")
# endif()

if(${ENABLE_MAT_IO})
	target_compile_definitions(DataContainer PRIVATE "-DBUILD_WITH_MATIO")
	target_include_directories(DataContainer PRIVATE ${MATIO_INCLUDE_DIR})
endif()

if(MSVC)
	source_group("" FILES ${SOURCES})
endif()

set (pchFile "${CMAKE_CURRENT_SOURCE_DIR}/pch.h")
add_pch(DataContainer ${pchFile})
