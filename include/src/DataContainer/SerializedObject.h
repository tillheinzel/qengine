﻿/* COPYRIGHT
 *
 * file="SerializedObject.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <vector>

#include "src/Utility/Types.h"
#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"

#include "src/DataContainer/isSerializable.h"
#include "src/DataContainer/Exceptions.h"


namespace qengine
{
	class DataStruct;

	class SerializedObject
	{
	public:
		enum TYPE
		{
			EMPTY, STRING, REAL, COMPLEX, STRUCT, LIST
		};
		enum FORMAT 
		{
			NA, SCALAR, VECTOR, MATRIX
		};

		SerializedObject();
		~SerializedObject();

		template<class T, typename = std::enable_if_t<internal::isSerializable_v<T>>>
		SerializedObject(const T& t) : SerializedObject(toSerializedObject(t)) {}


		SerializedObject(real t);
		SerializedObject(const complex& t);
		SerializedObject(const std::string& t);

		template<class T, class = std::enable_if_t<std::is_arithmetic<T>::value && !nostd::is_same_v<T, real>, void>>
		SerializedObject(T t) : SerializedObject(static_cast<real>(t)) {}


		SerializedObject(const SerializedObject& other);
		SerializedObject(SerializedObject&& other) noexcept;

		SerializedObject& operator=(const SerializedObject& other);
		SerializedObject& operator=(SerializedObject&& other) noexcept;

		template<class T, std::enable_if_t<internal::isSerializable_v<T> || nostd::is_same_v<T, real> || nostd::is_same_v<T, complex> || nostd::is_same_v<T, std::string>, int> = 0>
		T get() const
		{
			return internal::constexpr_ternary(internal::isSerializable<T>(),
				[this](auto _) {return fromSerializedObject(_(*this), internal::Type<T>()); },
				[this](auto _) 
			{
				return _(this)->operator T();
			}
				);

			
		}

		template<class T, std::enable_if_t<std::is_arithmetic<T>::value && !nostd::is_same_v<T, real>, int> = 0>
		T get() const
		{
			return static_cast<T>(real(*this));
		}
		
		template<class T, std::enable_if_t<std::is_arithmetic<T>::value && !nostd::is_same_v<T, real>, int> = 0>
		operator T() const
		{
			return static_cast<T>(real(*this));
		}

		template<class T, std::enable_if_t<internal::isSerializable_v<T>, int> = 0>
		operator T() const
		{
			return fromSerializedObject(*this, internal::Type<T>());
		}

		operator real() const;
		operator complex() const;
		operator std::string() const;

		SerializedObject& append(const SerializedObject& other);

		/// INTERNAL USE
	public:

		TYPE _type() const { return type_; }
		FORMAT _format() const { return format_; }
		
		void _set(const std::string& string);
		void _set(const RMat& mat);
		void _set(const CMat& mat);
		void _set(const DataStruct& dataStruct);
		void _set(const std::vector<SerializedObject>& list);


		const std::unique_ptr<std::string>& _string() const { return string_; }
		const std::unique_ptr<RMat>& _rmat() const { return rmat_; }
		const std::unique_ptr<CMat>& _cmat() const { return cmat_; }
		const std::unique_ptr<DataStruct>& _dataStruct() const { return dataStruct_; }
		const std::vector<SerializedObject>& _list() const { return list_; }

		void clear();

	private:


		TYPE type_ = EMPTY;
		FORMAT format_ = NA;
		
		std::unique_ptr<std::string> string_;
		std::unique_ptr<RMat> rmat_;
		std::unique_ptr<CMat> cmat_;
		std::unique_ptr<DataStruct> dataStruct_;
		std::vector<SerializedObject> list_;

		template<class T>
		void appendFormatSwitch(Matrix<T>& thisMat, const Matrix<T>& otherMat, FORMAT thisFormat, FORMAT otherFormat, const SerializedObject& other);
	};

}

