﻿/* COPYRIGHT
 *
 * file="DataContainer.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <memory>
#include <string>
#include <complex>

#include "src/Utility/LinearAlgebra/SparseMatrix.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Utility/has_function.h"

#include "src/DataContainer/SerializedObject.h"
#include "src/DataContainer/DataStruct.h"

#include "src/DataContainer/VectorSerialization.h"

namespace qengine 
{
	class DataContainer
	{
	public:
		SerializedObject& operator[] (const std::string& key)
		{
			return map_[key];
		}

		void save(std::string filename) const;
		void save(std::string filename, std::string format) const;
		void load(std::string filename);
		void load(std::string filename, std::string format);
		void clear();

		std::vector<std::string> varNames() const;
		bool hasVar(std::string name) const;
		count_t nVars() const;
		
		real getReal(std::string varname) const;
		RVec getRVec(std::string varname) const;
		RMat getRMat(std::string varname) const;

		complex getComplex(std::string varname) const;
		CVec getCVec(std::string varname) const;
		CMat getCMat(std::string varname) const;

		std::map<std::string, SerializedObject>& _map() { return map_; };
	private:
		std::map<std::string, SerializedObject> map_;
	};

}
