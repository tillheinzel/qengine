﻿/* COPYRIGHT
 *
 * file="StepsizeFinders.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/Types.h"
#include "src/Utility/NumericGradients.h"

#include "src/Utility/nostd/remove_cvref.h"

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"
#include "src/Generics/OptimalControl/InterpolatingLinesearch.h"


namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct StepSizeFinder {};
		}
	}
}

namespace qengine
{
	template<class F>
	internal::PolicyWrapper<internal::policy::StepSizeFinder, F> makeStepSizeFinder(F f)
	{
		return internal::PolicyWrapper<internal::policy::StepSizeFinder, F>{f};
	}

	inline auto makeInterpolatingStepSizeFinder(const real maxStepSize, const real maxInitialStepSizeGuess, const real alphaMin = 0, const real xtol = 1e-10, const count_t maxfev = 100, const real c1 = 1e-4, const real c2 = 0.9)
	{
		return makeStepSizeFinder([il{ InterpolatingLinesearch(alphaMin, xtol, static_cast<unsigned>(maxfev), c1, c2) }, maxStepSize, maxInitialStepSizeGuess](const auto& algorithm)
		{
			const auto& constProblem = algorithm.problem();
			using ProblemType = std::decay_t<decltype(constProblem)>;

			auto& problem = const_cast<ProblemType&>(constProblem);


			auto& dir = const_cast<nostd::remove_cvref_t<decltype(algorithm.stepDirection())>&>(algorithm.stepDirection());

			const auto& initialParam = problem._params();
			const auto initialCost = problem.cost();
			const auto epsilon = 1e-6;

			auto f_cost = [&problem, &dir, &initialParam](real pmEps)
			{
				problem.update(initialParam + pmEps * dir);
				return problem.cost();
			};

			// protect againt analytic gradient not being a descent direction
			auto initialGradient = (f_cost(epsilon)-initialCost)/epsilon;
			if (initialGradient > 0)
			{
				std::cout << "changed direction" << std::endl;
				dir = -dir;
				initialGradient *= -1;
			}

			auto func = [&f_cost, epsilon](real stepSize)
			{
				auto cost = f_cost(stepSize);
				auto gradInDirection = (f_cost(stepSize+epsilon)-cost)/epsilon;

				return std::make_tuple(cost, gradInDirection);
			};

			//auto initialStepSizeGuess =  maxInitialStepSizeGuess ;
			auto initialStepSizeGuess = algorithm.stepSize() == 0.0 ? maxInitialStepSizeGuess : algorithm.stepSize();
			real stepSize = il(func, maxStepSize, initialStepSizeGuess, initialGradient, initialCost);
			return stepSize;
		});
	}
}
