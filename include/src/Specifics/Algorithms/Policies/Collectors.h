﻿/* COPYRIGHT
 *
 * file="Collectors.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"

namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct Collector {};
		}
	}
}

namespace qengine
{
	template<class F>
	internal::PolicyWrapper<internal::policy::Collector, F> makeCollector(F f)
	{
		return internal::PolicyWrapper<internal::policy::Collector, F>{f};
	}	

	namespace internal
	{
		template<class F1, class F2>
		auto operator+ (const PolicyWrapper<policy::Collector, F1>& left, const PolicyWrapper<policy::Collector, F2>& right)
		{
			return makeStopper([f1{ left.f }, f2{ right.f }](const auto& alg)
			{
				f1(alg);
				f2(alg);
			});
		}
	}

	inline auto makeEmptyCollector()
	{
		return makeCollector([](const auto&) {});
	}

}
