﻿/* COPYRIGHT
 *
 * file="PolicyWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	namespace internal 
	{
		template<class PolicyIdentifier, class F>
		struct PolicyWrapper
		{
		public:
			PolicyWrapper(F f): f(f){}

			template<class... Ts>
			auto operator() (Ts&&... ts) -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return f(std::forward<Ts>(ts)...);
			}

			template<class... Ts>
			auto operator() (Ts&&... ts) const -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return f(std::forward<Ts>(ts)...);
			}

			F f;
		};
	}
}
