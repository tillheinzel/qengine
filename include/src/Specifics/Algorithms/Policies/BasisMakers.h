﻿/* COPYRIGHT
 *
 * file="BasisMakers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/Types.h"

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"
#include "src/Specifics/Algorithms/BasisFactory.h"

namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct BasisMaker {};
		}
	}
}

namespace qengine
{
	template<class F>
	internal::PolicyWrapper<internal::policy::BasisMaker, F> makeBasisMaker(F f)
	{
		return internal::PolicyWrapper<internal::policy::BasisMaker, F>{f};
	}	

	inline auto makeRandSineBasisMaker(const count_t basisSize, const Control& shapeFunction, const real maxRand)
	{
		return makeBasisMaker(
			[basisSize, maxRand, shapeFunction]()
		{
			return shapeFunction * makeSineBasis(basisSize, shapeFunction.metaData(), maxRand);
		});
	}

}
