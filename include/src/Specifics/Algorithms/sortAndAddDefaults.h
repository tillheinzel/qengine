﻿/* COPYRIGHT
 *
 * file="sortAndAddDefaults.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/Utility/Types.h"
#include "src/Utility/TypeList.h"

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"
#include "src/Utility/nostd/bool_constant.h"
#include "src/Utility/nostd/apply.h"

#include "src/Utility/TypeMap.h"

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"

namespace qengine
{
	namespace internal
	{
		template<class CheckPolicy, class DefaultsMap, class InputMap>
		auto sortAndAddDefaults_impl(const DefaultsMap defaults, const InputMap& inputs)
		{
			return internal::constexpr_ternary(nostd::bool_constant<InputMap::template isInList<CheckPolicy>()>(),
				[&](auto _)
			{
				return 
					std::make_tuple(_(inputs).template get<CheckPolicy>())
					;
			}, 
				[&](auto _)
			{
				return 
					std::make_tuple(_(defaults).template get<CheckPolicy>())
				;
			});
		}

		template<class CheckPolicy, class NextCheck, class... RemainingPolicies, class DefaultsMap, class InputMap>
		auto sortAndAddDefaults_impl(const DefaultsMap defaults, const InputMap& inputs)
		{
			auto currentPolicy = internal::constexpr_ternary(nostd::bool_constant<InputMap::template isInList<CheckPolicy>()>(),
				[&](auto _)
			{
				return 
					_(inputs).template get<CheckPolicy>()
				;
			},
				[&](auto _)
			{
				return 
					_(defaults).template get<CheckPolicy>()
				;
			});

			return std::tuple_cat(std::make_tuple(currentPolicy), sortAndAddDefaults_impl<NextCheck, RemainingPolicies...>(defaults, inputs));
		}

		template<class... DefaultIDs, class... DefaultPolicies, class... InputIDs, class... InputPolicies >
		auto sortAndAddDefaults(const TypeMap<std::tuple<DefaultIDs...>, std::tuple<DefaultPolicies...>>& map, TypeMap<std::tuple<InputIDs...>, std::tuple<InputPolicies...>> inputs)
		{
			return sortAndAddDefaults_impl<DefaultIDs...>(map, inputs);
		}
	}
}

namespace qengine
{
	namespace internal
	{
		template<class... InputIDs, class... InputFunctions>
		TypeMap<std::tuple<InputIDs...>, std::tuple<InputFunctions...>> toTypeMap(PolicyWrapper<InputIDs, InputFunctions>... Ps)
		{
			return makeTypeMap(TypeList<InputIDs...>(), std::make_tuple(Ps.f...));
		}

		template<class... DefaultIDs>
		constexpr bool areInputIDsValid(TypeList<DefaultIDs...>, TypeList<>)
		{
			return true;
		}

		template<class... DefaultIDs, class InputID, class... InputIDs >
		constexpr bool areInputIDsValid(TypeList<DefaultIDs...> d, TypeList<InputID, InputIDs...>)
		{
			return isInList<InputID, DefaultIDs...>() ? internal::areInputIDsValid(d, TypeList<InputIDs...>{}) : false;
		}

		/// This is the generic way to create an optimal control algorithm with a bunch of policies. 
		/// The orderedDefaultMap contains the defaults for the policies in the order they should be passed to the creation-function f
		/// Ps contains the wrapped policies that the user has passed in, in any order. 
		template<class F, class M, class... InputIDs, class... InputPolicies>
		auto makeAlgorithm(F f, M orderedDefaultMap, PolicyWrapper<InputIDs, InputPolicies>... Ps)
		{
			auto inputMap = toTypeMap(Ps...);
			static_assert(internal::areInputIDsValid(internal::getKeys(internal::Type<M>()), internal::getKeys(internal::Type<decltype(inputMap)>())), "One of the passed-in policies is not allowed for this algorithm");
			return nostd::apply(f, internal::sortAndAddDefaults(orderedDefaultMap, inputMap));
		}
	}
}
