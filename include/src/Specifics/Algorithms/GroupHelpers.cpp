﻿/* COPYRIGHT
 *
 * file="GroupHelpers.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Specifics/Algorithms/GroupHelpers.h"

namespace qengine
{
	Control makeSigmoidShapeFunction(const Control& t, const real sigma)
	{
        const auto b = 1000;
        auto t0 = 5 * t.dt() * t.endTime();
		auto a = t.endTime() / t0 * std::log(b * sigma / (1 - sigma));
		auto shapeFunction = 1.0 / (1 + b * exp(-a * t));
		shapeFunction.at(0) = 0.0;
		return shapeFunction * invert(shapeFunction);
	}
}
