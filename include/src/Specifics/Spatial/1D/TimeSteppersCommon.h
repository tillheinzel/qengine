﻿/* COPYRIGHT
 *
 * file="TimeSteppersCommon.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Specifics/Spatial/TimeEvolutionHelpers.h"

namespace qengine
{
	template<class HilbertSpace, class = std::enable_if_t<internal::isHilbertSpace_v<HilbertSpace>>>
	FunctionOfX<real, HilbertSpace> makeDefaultImagPot(const HilbertSpace& hilbertSpace, real barrierIntensity = 20 /*pretty arbitrary number. should probably depend on energy-units in system*/)
	{
		const auto N = hilbertSpace.dim;
		const auto dx = hilbertSpace.dx;

		return makeFunctionOfX(hilbertSpace, internal::makeDefaultImagPotVector(N, dx, barrierIntensity));
	}

	template<class AlgMaker, class HilbertSpace>
	auto addImagBounds(AlgMaker&& algMaker, const FunctionOfX<real, HilbertSpace> imagPot)
	{
		return[algMaker{ std::forward<AlgMaker>(algMaker) }, imagPot](auto&&... ps)
		{
			auto alg = algMaker(std::forward<decltype(ps)>(ps)...);
			return [alg =std::move(alg), imagPot](FunctionOfX<complex, HilbertSpace>& psi, real dt, auto&&... ts) mutable -> void
			{
				static_assert(nostd::is_same_v<void, decltype(alg(psi, dt, std::forward<decltype(ts)>(ts)...))>, "");
				alg(psi, dt, std::forward<decltype(ts)>(ts)...);
				auto intermediate = exp(std::abs(dt)*imagPot)*psi; // assumes imagpot to be positive!
				psi = intermediate;
			}; 
		};
	}

	template<class AlgMaker, class HilbertSpace>
	auto addImagBounds(AlgMaker&& algMaker, const FunctionOfX<real, HilbertSpace> imagPot, const real dt)
	{
		auto expPot = exp(std::abs(dt)*imagPot);
		return[algMaker{ std::forward<AlgMaker>(algMaker) }, expPot](auto&&... ps)
		{
			auto alg = algMaker(std::forward<decltype(ps)>(ps)...);
			return [alg = std::move(alg), expPot](FunctionOfX<complex, HilbertSpace>& psi, auto&&... ts) mutable -> void
			{
				static_assert(nostd::is_same_v<void, decltype(alg(psi, std::forward<decltype(ts)>(ts)...))>, "");
				alg(psi, std::forward<decltype(ts)>(ts)...);
				auto intermediate = expPot * psi; // assumes imagpot to be positive!
				psi = intermediate;
			}; 
		};
	}

	namespace oneD
	{
		template<class T>
		auto makeApplier(const T& t)
		{
			return [&t](auto& psi)
			{
				auto intermediate = t * psi;
				psi = intermediate;//avoid move because of fft.
			};
		}

		template<class HilbertSpace>
		auto makePSpaceKinetic(HilbertSpace s)
		{
			return makeFunctionOfX(s, internal::makePSpaceKineticVec(s.dx, s.dim, s.kinematicFactor));
		}

	}
}
