﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/Spatial/SubSpace.h"
#include "src/Specifics/Spatial/1D/LinearHamiltonian1D.h"


namespace qengine
{
	namespace oneD
	{
		// user-friendly front for HilbertSpace that contains some helper-functionality. Does not fulfill isHilbertSpace!
		class ApiHilbertSpace
		{
			using HilbertSpace = internal::SubSpace<1, 1>;
		public:
			ApiHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor);

			const FunctionOfX<real, HilbertSpace>& spatialDimension() const { return spatialDimension_; }
			const FunctionOfX<real, HilbertSpace>& x() const { return spatialDimension_; }

			LinearHamiltonian1D<internal::SubSpace<1, 1>> T() const;
			LinearHamiltonian1D<internal::SubSpace<1, 1>> makeKinetic() const;

			real xLower() const { return hilbertSpace_.xLower; }
			real xUpper() const { return hilbertSpace_.xUpper; }
			count_t dim() const { return hilbertSpace_.dim; }
			real kinematicFactor() const { return hilbertSpace_.kinematicFactor; }
			real dx() const { return hilbertSpace_.dx; }
			real integrationConstant() const { return hilbertSpace_.integrationConstant; }
			real normalization() const { return hilbertSpace_.normalization; }

			/// INTERNAL USE ONLY
		public:
			const internal::SubSpace<1, 1>& _hilbertSpace() const { return hilbertSpace_; }

		private:
			const HilbertSpace hilbertSpace_;
			FunctionOfX<real, internal::SubSpace<1, 1>> spatialDimension_;

		};
	}

	namespace one_particle 
	{
		oneD::ApiHilbertSpace makeHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor = 0.5);
	}
	namespace gpe 
	{
		oneD::ApiHilbertSpace makeHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor = 0.5);
	}
}

namespace qengine
{
	namespace internal
	{
		template<>
		struct extract_hilbertspace<oneD::ApiHilbertSpace>
		{
			using type = internal::SubSpace<1, 1>;
		};
	}
}
