﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian1D.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.h"

#include "src/Generics/Physics/Spatial/SubSpace.h"
#include "src/Generics/Physics/Spatial/LinearHamiltonian.h"
#include "src/Generics/Physics/Spatial/MakeKinetic.h"

namespace qengine
{
	template<class HilbertSpace>
	using LinearHamiltonian1D = LinearHamiltonian<HilbertSpace, internal::SqHermitianBandMat>;
}

namespace qengine
{
	namespace internal
	{
		template<count_t FullDim, count_t Index>
		LinearHamiltonian1D<SubSpace<FullDim, Index>> makeKinetic(const SubSpace<FullDim, Index>& s)
		{
			auto dx = s.dx;
			auto dim = s.dim;
			auto k = s.kinematicFactor;

			return LinearHamiltonian1D<internal::SubSpace<FullDim, Index>>(s, makeKineticMatrix(dim, dx, k), FunctionOfX<real, internal::SubSpace<FullDim, Index>>(RVec(s.dim, 0.0), s));
		}
	}
}
