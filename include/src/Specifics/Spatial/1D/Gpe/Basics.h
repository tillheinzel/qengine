﻿/* COPYRIGHT
 *
 * file="Basics.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"
#include "src/Generics/Physics/Spatial/Basics.h"

#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.h"


namespace qengine
{
	template<class HilbertSpace, class ScalarType>
	complex cVariance(const HamiltonianGpe<HilbertSpace>& op, const FunctionOfX<ScalarType, HilbertSpace>& psi)
	{
		auto simpleOp = op._H0() + op._beta()*psi.absSquare();
		return cVariance(simpleOp, psi);
	}

	template<class ScalarType, class HilbertSpace>
	real variance(const HamiltonianGpe<HilbertSpace>& op, const FunctionOfX<ScalarType, HilbertSpace>& psi)
	{
		auto c = cVariance(op, psi);
		qengine_assert(std::abs(c.imag()) < 1e-10, "variance has nonzero imaginary part!");
		return c.real();
	}
}
