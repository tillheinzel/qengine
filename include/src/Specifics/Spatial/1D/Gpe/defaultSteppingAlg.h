﻿/* COPYRIGHT
 *
 * file="defaultSteppingAlg.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Utility/Constants.h"

#include "src/Generics/Physics/General/TimeStepper.h"

#include "src/Generics/Physics/Spatial/SplitStep.h"
#include "src/Generics/Physics/Spatial/HamiltonianFunction.h"

#include "src/Specifics/Spatial/1D/TimeSteppersCommon.h"
#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.h"

namespace qengine
{
	template<class HilbertSpace>
	auto makeSplitStepAlgorithm(const HamiltonianGpe<HilbertSpace>& H)
	{
		return[V{ H._potential() }, T{ oneD::makePSpaceKinetic(H._hilbertSpace()) }, beta{ H._beta() }, splitStepAlg{ SplitStepAlgorithm{} }](auto& psi, real dt)
		{
			using namespace std::complex_literals;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = oneD::makeApplier(expT);

			auto applyExpV = [beta, V, dt](auto& psi)
			{
				auto intermediate = exp(-1i * 0.5*dt*(V + beta * psi.absSquare()))*psi;
				psi = intermediate;
			};

			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}


	template<class HilbertSpace>
	auto makeSplitStepAlgorithm(const HamiltonianGpe<HilbertSpace>& H, const real dt)
	{
		using namespace std::complex_literals;
		const auto T = oneD::makePSpaceKinetic(H._hilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;
		};

		auto minushalfIdt = -1i * 0.5*dt;
		return[V{ H._potential() }, minushalfIdt, applyExpT, beta{ H._beta() }, splitStepAlg{ SplitStepAlgorithm{} }](auto& psi)
		{
			auto applyExpV = [beta, V, minushalfIdt](auto& psi)
			{
				auto intermediate = exp(minushalfIdt*(V + beta * psi.absSquare()))*psi;
				psi = intermediate;
			};

			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}

	template<class HilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<HilbertSpace>, PotentialFunction>& H, const Tuple<Params...>& initialParams)
	{

		using namespace std::complex_literals;
		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[splitStepAlg{ SplitStepAlgorithm{} }, V, T{ oneD::makePSpaceKinetic(H._H0()._hilbertSpace()) }, beta{ H._H0()._beta() }, V_cache = V(initialParams)](auto& psi, const real dt, const auto& p2) mutable
		{
			using namespace std::complex_literals;
			auto intermediate = exp(-1i * 0.5*(V_cache + beta * psi.absSquare())*dt)*psi;
			psi = intermediate;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = oneD::makeApplier(expT);
			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);

			intermediate = exp(-1i * 0.5*(V_cache + beta * psi.absSquare())*dt)*psi;
			psi = intermediate;
		};
	}

	template<class HilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<HilbertSpace>, PotentialFunction>& H, const real dt, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		const auto T = oneD::makePSpaceKinetic(H._H0()._hilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;
		};

		auto minushalfIdt = -1i * 0.5*dt;

		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[splitStepAlg{ SplitStepAlgorithm{} }, V, applyExpT, beta{ H._H0()._beta() }, minushalfIdt, V_cache = V(initialParams)](auto& psi, const auto& p2) mutable
		{

			auto intermediate = exp(minushalfIdt*(V_cache + beta * psi.absSquare()))*psi;
			psi = intermediate;


			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);
			intermediate = exp(minushalfIdt*(V_cache + beta * psi.absSquare()))*psi;
			psi = intermediate;

		};
	}

}

namespace qengine
{
	template<class HilbertSpace>
	auto makeDefaultStepAlgorithm(const HamiltonianGpe<HilbertSpace>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._hilbertSpace()));
	}

	template<class HilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<HilbertSpace>, PotentialFunction>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._H0()._hilbertSpace()));
	}

	template<class HilbertSpace>
	auto makeDefaultStepAlgorithm(const HamiltonianGpe<HilbertSpace>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._hilbertSpace()), dt);
	}

	template<class HilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<HilbertSpace>, PotentialFunction>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._hilbertSpace()), dt);
	}
}
