﻿/* COPYRIGHT
 *
 * file="GpeNonlinearity.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	struct GpeTerm { real beta; };
	inline GpeTerm makeGpeTerm(const real beta) { return GpeTerm{ beta }; }
}

namespace qengine
{
	namespace spatial
	{
		template<> struct isPotential<GpeTerm> : std::true_type {};
	}
}

namespace qengine
{
	namespace internal
	{
		template<>
		struct isLinear<GpeTerm>: std::false_type {};
	}
}
