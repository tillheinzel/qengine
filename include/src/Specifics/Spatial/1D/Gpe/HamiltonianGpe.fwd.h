﻿/* COPYRIGHT
 *
 * file="HamiltonianGpe.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/General/extract_hilbertspace.h"
#include "src/Generics/Physics/Spatial/isHamiltonian.h"

namespace qengine 
{
	template<class HilbertSpace>
	class HamiltonianGpe;
}

namespace qengine
{
	namespace spatial
	{
		template<class HilbertSpace>
		struct hasHamiltonianLabel<HamiltonianGpe<HilbertSpace>>:std::true_type{};
	}
}
namespace qengine
{
	namespace internal
	{
		template<class HilbertSpace>
		struct hasOperatorLabel<HamiltonianGpe<HilbertSpace>> : std::true_type {};

		template<class HilbertSpace>
		struct hasLinearLabel<HamiltonianGpe<HilbertSpace>> : std::true_type {};

		template<class HilbertSpace>
		struct hasGuaranteedHermitianLabel<HamiltonianGpe<HilbertSpace>> : std::true_type {};

		template<class HilbertSpace>
		struct extract_hilbertspace<HamiltonianGpe<HilbertSpace>>
		{
			using type = HilbertSpace;
		};
	}
}
