﻿/* COPYRIGHT
 *
 * file="GpeDiagonalizer.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	enum class GROUNDSTATE_ALGORITHM { OPTIMAL_DAMPING, MIXING };
	
	template<class HilbertSpace>
	class GpeDiagonalizer
	{
	public:
		spatial::Spectrum<real, HilbertSpace> operator()(const HamiltonianGpe<HilbertSpace>& H, const LinearHamiltonian1D<HilbertSpace>& H0, GpeTerm gpeTerm, count_t largestEigenvalue, GROUNDSTATE_ALGORITHM groundstateAlgorithm, real convergenceCriterion , count_t maxIterationsPerEigenstate, const std::vector<real>& mixingValues) const;

		static FunctionOfX<complex, HilbertSpace> groundStateOptimalDamping(const LinearHamiltonian1D<HilbertSpace>& H0, real beta, real convergenceCriterion, count_t maxIterations);
		static FunctionOfX<complex, HilbertSpace> groundStateMixing(const LinearHamiltonian1D<HilbertSpace>& H0, real beta, real convergenceCriterion, count_t maxIterations, real mixingValue);
	};
}
