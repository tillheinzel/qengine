﻿/* COPYRIGHT
 *
 * file="HamiltonianArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"

#include "src/Specifics/Spatial/1D/LinearHamiltonian1D.h"
#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.h"

namespace qengine
{
	template<class HilbertSpace>
	LinearHamiltonian1D<HilbertSpace> operator+(const LinearHamiltonian1D<HilbertSpace>& H, const FunctionOfX<real, HilbertSpace>& V)
	{
		return LinearHamiltonian1D<HilbertSpace>(H._hilbertSpace(), H._hamiltonianMatrix() + internal::SqDiagMat<real>(V.vec()), H._potential() + V);
	}

	template<class HilbertSpace>
	LinearHamiltonian1D<HilbertSpace> operator-(LinearHamiltonian1D<HilbertSpace> H, const FunctionOfX<real, HilbertSpace>& V)
	{
		return H + (-V);
	}

	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace> operator+(const LinearHamiltonian1D<HilbertSpace>& H0, const GpeTerm gpeTerm)
	{
		return HamiltonianGpe<HilbertSpace>(H0, gpeTerm.beta);
	}

	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace> operator+(const HamiltonianGpe<HilbertSpace>& H, const FunctionOfX<real, HilbertSpace>& V)
	{
		return HamiltonianGpe<HilbertSpace>(H._H0() + V, H._beta());
	}

	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace> operator-(const HamiltonianGpe<HilbertSpace>& H, const FunctionOfX<real, HilbertSpace>& V)
	{
		return H + (-V);
	}

	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace> operator+(const HamiltonianGpe<HilbertSpace>& H, const GpeTerm gpeTerm)
	{
		return HamiltonianGpe<HilbertSpace>(H._H0(), H._beta() + gpeTerm.beta);
	}
}
