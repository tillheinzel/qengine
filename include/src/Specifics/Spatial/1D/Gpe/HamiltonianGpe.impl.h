﻿/* COPYRIGHT
 *
 * file="HamiltonianGpe.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once
#include <iostream>

#include "src/Specifics/Spatial/1D/Gpe/HamiltonianGpe.h"
#include "src/Specifics/Spatial/1D/Gpe/GpeDiagonalizer.impl.h"

#include "src/Generics/Physics/Spatial/Basics.h"
#include "src/Generics/Physics/Spatial/LinearHamiltonian.impl.h"

namespace qengine
{
	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace>::HamiltonianGpe(const LinearHamiltonian1D<HilbertSpace>& H0, const real beta) :
		H0_(H0),
		nonlinearity_(GpeTerm{ beta })
	{
		qengine_assert(!std::isnan(beta), "beta cannot be NaN!");
	}

	template<class HilbertSpace>
	HamiltonianGpe<HilbertSpace>::HamiltonianGpe(LinearHamiltonian1D<HilbertSpace>&& H0, const real beta) :
		H0_(std::move(H0)),
		nonlinearity_(GpeTerm{ beta })
	{
		qengine_assert(!std::isnan(beta), "beta cannot be NaN!");
	}

	template<class HilbertSpace>
	template <typename ScalarType>
	void HamiltonianGpe<HilbertSpace>::applyInPlace(FunctionOfX<ScalarType, HilbertSpace>& f) const
	{
		qengine_assert(f._hilbertSpace() == _hilbertSpace(), "wrong x-space!");

		auto nonlinearPart = nonlinearity_.beta * f.absSquare()*f;
		H0_.applyInPlace(f);

		f.vec() += nonlinearPart.vec();
	}
	
	template<class HilbertSpace>
	spatial::Spectrum<real, HilbertSpace> HamiltonianGpe<HilbertSpace>::makeSpectrum(const count_t largestEigenvalue, const GROUNDSTATE_ALGORITHM alg, const real convergenceCriterion, const count_t maxIterationsPerEigenstate, const std::vector<real>& mixingValues) const
	{
		return GpeDiagonalizer<HilbertSpace>::operator()(*this, H0_, nonlinearity_, largestEigenvalue, alg, convergenceCriterion, maxIterationsPerEigenstate, mixingValues);
	}


	template<class HilbertSpace>
	std::ostream& operator<<(std::ostream& s, const HamiltonianGpe<HilbertSpace>& H)
	{
		s << "beta: " << H._beta() << std::endl;
		s << H._H0();
		return s;
	}
}
