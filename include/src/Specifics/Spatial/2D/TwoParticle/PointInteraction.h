﻿/* COPYRIGHT
 *
 * file="PointInteraction.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"

#include "src/Specifics/Spatial/2D/TwoParticle/CommonDimensionSpace.h"

namespace qengine
{
	namespace two_particle
	{
		namespace detail
		{
			class PointInteractionSpace : public internal::OneDSpace_BaseClass
			{
			public:
				PointInteractionSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
					OneDSpace_BaseClass(xLower, xUpper, dimension, kinematicFactor)
				{}

				explicit PointInteractionSpace(const OneDSpace_BaseClass& other) :
					OneDSpace_BaseClass(other)
				{}

				static constexpr count_t nDimensions = 1;

				explicit operator internal::MultiDimensionalSpace<2, 1, 2>() const
				{
					return internal::tensor(internal::SubSpace<2, 1>(*this), internal::SubSpace<2, 2>(*this));
				}
			};

			inline bool operator==(const PointInteractionSpace& left, const PointInteractionSpace& right) { return isSame(left, right); }

			inline bool operator!=(const PointInteractionSpace& left, const PointInteractionSpace& right) { return !isSame(left, right); }


		}
	}

	namespace internal
	{
		template<class ScalarType>
		struct extract_hilbertspace<FunctionOfX<ScalarType, two_particle::detail::PointInteractionSpace>>
		{
			using type = internal::MultiDimensionalSpace<2, 1, 2>;
		};
	}

	namespace two_particle
	{
		template<class N>
		using PointInteraction = FunctionOfX<N, detail::PointInteractionSpace>;


		/// pointInteractions is interpreted as interactionStrength(x)*delta(x1-x2), and the delta-interaction carries a factor of 1/dx
		template<class N>
		PointInteraction<N> makePointInteraction(const Potential<N>& interactionStrength)
		{
			return PointInteraction<N>(interactionStrength.vec() / interactionStrength._hilbertSpace().dx, detail::PointInteractionSpace(interactionStrength._hilbertSpace()));
		}

		/// pointInteractions is interpreted as interactionStrength(x)*delta(x1-x2), and the delta-interaction carries a factor of 1/dx
		template<class N>
		PointInteraction<N> makePointInteraction(Potential<N>&& interactionStrength)
		{
			return PointInteraction<N>(std::move(interactionStrength).vec() / interactionStrength._hilbertSpace().dx, detail::PointInteractionSpace(interactionStrength._hilbertSpace()));
		}

	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>> operator*(const two_particle::PointInteraction<N1>& left, FunctionOfX<N2, internal::MultiDimensionalSpace<2, 1, 2>> f)
	{
		qengine_assert(f._hilbertSpace().template get<1>().dim == f._hilbertSpace().template get<2>().dim, "");
		auto dim = f._hilbertSpace().template get<1>().dim;
		for (auto i = 0u; i < dim; ++i)
		{
			f.vec().at(i * dim + i) *= left.vec().at(i);
		}
		return f;
	}
}

namespace qengine
{
	namespace internal
	{
		template<class N>
		FunctionOfX<N, internal::MultiDimensionalSpace<2, 1, 2>> uplift(const two_particle::PointInteraction<N>& pointInteraction)
		{
			const auto xDim = pointInteraction._hilbertSpace().dim;

			auto vec = Vector<N>(xDim * xDim, 0.0);

			for (auto i = 0u; i < xDim; ++i)
			{
				vec.at(i * xDim + i) = pointInteraction.vec().at(i);
			}

			return makeFunctionOfX(internal::MultiDimensionalSpace<2, 1, 2>(
				std::make_tuple(
					internal::SubSpace<2, 1>(pointInteraction._hilbertSpace()),
					internal::SubSpace<2, 2>(pointInteraction._hilbertSpace())
				)), std::move(vec));

		}
	}
}
