﻿/* COPYRIGHT
 *
 * file="Hamiltonian2P.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Specifics/Spatial/2D/TwoParticle/Hamiltonian2P.h"

#include "src/Utility/ExtraDebugChecks.h"

#include "src/Generics/Physics/Spatial/Spectrum.impl.h"

namespace qengine
{
	template <typename ScalarType>
	void Hamiltonian2P::applyInPlace(FunctionOfX<ScalarType, HilbertSpace>& f) const
	{
		qengine_assert(f._hilbertSpace() == hilbertSpace_, "wrong hilbertspace!");
		hamiltonianMatrix_.multiplyInPlace(f.vec());
	}

	template void Hamiltonian2P::applyInPlace(FunctionOfX<real, Hamiltonian2P::HilbertSpace>&) const;
	template void Hamiltonian2P::applyInPlace(FunctionOfX<complex, Hamiltonian2P::HilbertSpace>&) const;

	spatial::Spectrum<real, Hamiltonian2P::HilbertSpace> Hamiltonian2P::makeSpectrum(const count_t largestEigenvalue) const
	{
		return spatial::Spectrum<real, HilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, hilbertSpace_.normalization), hilbertSpace_);
	}

	spatial::Spectrum<real, Hamiltonian2P::HilbertSpace> Hamiltonian2P::makeSpectrum(const count_t largestEigenvalue, const count_t nExtraStates, const real tolerance) const
	{
		return spatial::Spectrum<real, HilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, hilbertSpace_.normalization, nExtraStates, tolerance), hilbertSpace_);
	}
}
