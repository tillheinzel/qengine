﻿/* COPYRIGHT
 *
 * file="Hamiltonian2P.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/Types.h"
#include "src/Utility/nostd/remove_cvref.h"


#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Generics/Physics/General/EigenstateSyntax_CRTP.h"
#include "src/Generics/Physics/General/extract_hilbertspace.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"
#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"
#include "src/Generics/Physics/Spatial/Spectrum.h"
#include "src/Generics/Physics/Spatial/isHamiltonian.h"

#include "src/Specifics/Spatial/2D/TwoParticle/CommonDimensionSpace.h"
#include "src/Specifics/Spatial/2D/TwoParticle/PointInteraction.h"

#include "src/Specifics/Spatial/2D/TwoParticle/PotentialWithInteraction.h"

namespace qengine
{
	class Hamiltonian2P : public internal::EigenstateSyntax_CRTP<Hamiltonian2P>
	{
	public:
		using HilbertSpace = internal::MultiDimensionalSpace<2, 1, 2>;

		template<class Representation, class Potentials,
			typename = std::enable_if_t<
			std::is_same<nostd::remove_cvref_t<Potentials>, PotentialWithInteraction>::value &&
			std::is_same<nostd::remove_cvref_t<Representation>, internal::SqSparseMat<real>>::value>>
			Hamiltonian2P(const HilbertSpace& hilbertSpace, Representation&& representation, Potentials&& potentials);

		template<typename ScalarType>
		void applyInPlace(FunctionOfX<ScalarType, HilbertSpace>& f) const;

		spatial::Spectrum<real, HilbertSpace> makeSpectrum(count_t largestEigenvalue) const;
		spatial::Spectrum<real, HilbertSpace> makeSpectrum(count_t largestEigenvalue, count_t nExtraStates, real tolerance = 0.0) const;

		/// INTERNAL USE
	public:
		const auto& _potential() const { return get<0>(potentials_); }
		const internal::SqSparseMat<real>& _hamiltonianMatrix() const { return hamiltonianMatrix_; }
		const auto& _interactionPotential() const { return get<1>(potentials_); }
		const HilbertSpace& _hilbertSpace() const { return hilbertSpace_; }
		const auto& _potentials() const { return potentials_; }

	private:
		const HilbertSpace hilbertSpace_;
		PotentialWithInteraction potentials_;
		internal::SqSparseMat<real> hamiltonianMatrix_;
	};

	template <class Representation, class Potentials, typename>
	Hamiltonian2P::Hamiltonian2P(const HilbertSpace& hilbertSpace, Representation&& representation, Potentials&& potentials) :
		hilbertSpace_(hilbertSpace),
		potentials_(potentials),
		hamiltonianMatrix_(std::forward<Representation>(representation))
	{
		qengine_assert(!hamiltonianMatrix_.hasNan(), "Hamiltonian2P constructor: NaN found!");
	}

	namespace internal
	{
		template <class Representation, class Potentials, typename = std::enable_if_t<std::is_constructible<Hamiltonian2P, Hamiltonian2P::HilbertSpace, Representation, Potentials>::value>>
		Hamiltonian2P makeHamiltonian(const Hamiltonian2P::HilbertSpace& s, Representation&& rep, Potentials&& pot)
		{
			return Hamiltonian2P(s, std::forward<Representation>(rep), std::forward<Potentials>(pot));
		}

		template<class Representation, class Potential, class Interaction,
			typename = std::enable_if_t<
			std::is_same<nostd::remove_cvref_t<Potential>, FunctionOfX<real, two_particle::CommonDimensionSpace>>::value &&
			std::is_same<nostd::remove_cvref_t<Representation>, internal::SqSparseMat<real>>::value &&
			std::is_same<nostd::remove_cvref_t<Interaction>, two_particle::PointInteraction<real>>::value>>
			Hamiltonian2P makeHamiltonian(const Hamiltonian2P::HilbertSpace& s, Representation&& rep, Potential&& pot, Interaction&& interaction)
		{
			return Hamiltonian2P(s, std::forward<Representation>(rep), std::forward<Potential>(pot) + std::forward<Interaction>(interaction));
		}
	}


	namespace spatial
	{
		template<>
		struct hasHamiltonianLabel<Hamiltonian2P> : std::true_type {};

	}

	namespace internal
	{
		template<>
		struct hasOperatorLabel<Hamiltonian2P> : std::true_type {};

		template<>
		struct hasLinearLabel<Hamiltonian2P> : std::true_type {};

		template<>
		struct hasGuaranteedHermitianLabel<Hamiltonian2P> : std::true_type {};

		template<>
		struct extract_hilbertspace<Hamiltonian2P>
		{
			using type = Hamiltonian2P::HilbertSpace;
		};
	}
}

namespace qengine
{
		template<class ScalarType>
		FunctionOfX<ScalarType, Hamiltonian2P::HilbertSpace> operator*(const Hamiltonian2P& H, FunctionOfX<ScalarType, Hamiltonian2P::HilbertSpace> psi)
		{
			H.applyInPlace(psi);
			return psi;
		}

}
