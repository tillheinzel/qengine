﻿/* COPYRIGHT
 *
 * file="CrossArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Generics/Physics/Spatial/FunctionOfX.h"
#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"

#include "src/Specifics/Spatial/1D/LinearHamiltonian1D.h"
#include "src/Specifics/Spatial/2D/LinearHamiltonian2D.h"

#include "src/Specifics/Spatial/2D/TwoParticle/HamiltonianArithmetic.h"

namespace qengine
{
	namespace internal
	{
		template<class ScalarType>
		FunctionOfX<ScalarType, MultiDimensionalSpace<2, 1, 2>> uplift(const FunctionOfX<ScalarType, SubSpace<2, 1>>& f, const MultiDimensionalSpace<2, 1, 2>& targetSpace)
		{
			const auto onesRight = RVec(targetSpace.get<2>().dim, 1.0);
			return FunctionOfX<ScalarType, MultiDimensionalSpace<2, 1, 2>>(kron(f.vec(), onesRight), targetSpace);
		}
		template<class ScalarType>
		FunctionOfX<ScalarType, MultiDimensionalSpace<2, 1, 2>> uplift(const FunctionOfX<ScalarType, SubSpace<2, 2>>& f, const MultiDimensionalSpace<2, 1, 2>& targetSpace)
		{
			const auto onesLeft = RVec(targetSpace.get<1>().dim, 1.0);
			return FunctionOfX<ScalarType, MultiDimensionalSpace<2, 1, 2>>(kron(onesLeft, f.vec()), targetSpace);
		}
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>> operator+(const FunctionOfX<N1, internal::SubSpace<2, 1>>& left, const FunctionOfX<N2, internal::SubSpace<2, 2>>& right)
	{
		auto space = internal::tensor(left._hilbertSpace(), right._hilbertSpace());

		return internal::uplift(left, space) + internal::uplift(right, space);
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator+(const FunctionOfX<N1, internal::SubSpace<2, 2>>& left, const FunctionOfX<N2, internal::SubSpace<2, 1>>& right)
	{
		return right + left;
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator-(const FunctionOfX<N1, internal::SubSpace<2, 1>>& left, const FunctionOfX<N2, internal::SubSpace<2, 2>>& right)
	{
		return left + (-right);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator-(const FunctionOfX<N1, internal::SubSpace<2, 2>>& left, const FunctionOfX<N2, internal::SubSpace<2, 1>>& right)
	{
		return (-right) + left;
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator*(const FunctionOfX<N1, internal::SubSpace<2, 1>>& left, const FunctionOfX<N2, internal::SubSpace<2, 2>>& right)
	{
		return FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
			(
				kron(left.vec(), right.vec()),
				internal::tensor(left._hilbertSpace(), right._hilbertSpace())
				);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator*(const FunctionOfX<N1, internal::SubSpace<2, 2>>& left, const FunctionOfX<N2, internal::SubSpace<2, 1>>& right)
	{
		return right * left;
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator/(const FunctionOfX<N1, internal::SubSpace<2, 1>>& left, const FunctionOfX<N2, internal::SubSpace<2, 2>>& right)
	{
		auto space = internal::tensor(left._hilbertSpace(), right._hilbertSpace());

		return internal::uplift(left, space) / internal::uplift(right, space);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator/(const FunctionOfX<N1, internal::SubSpace<2, 2>>& left, const FunctionOfX<N2, internal::SubSpace<2, 1>>& right)
	{
		auto space = internal::tensor(left._hilbertSpace(), right._hilbertSpace());

		return internal::uplift(left, space) / internal::uplift(right, space);
	}
}

namespace qengine
{
	inline LinearHamiltonian2D operator+(const LinearHamiltonian1D<internal::SubSpace<2, 1>>& left, const LinearHamiltonian1D<internal::SubSpace<2, 2>>& right)
	{
		auto idLeft = internal::identityMatrix<internal::SqSparseMat>(left._hilbertSpace().dim);
		auto idRight = internal::identityMatrix<internal::SqSparseMat>(right._hilbertSpace().dim);

		return LinearHamiltonian2D(internal::tensor(left._hilbertSpace(), right._hilbertSpace()),
			kron(static_cast<internal::SqSparseMat<real>>(left._hamiltonianMatrix()), idRight) + kron(idLeft, static_cast<internal::SqSparseMat<real>>(right._hamiltonianMatrix())),
			left._potential() + right._potential()
		);
	}

	inline LinearHamiltonian2D operator+(const LinearHamiltonian1D<internal::SubSpace<2, 2>>& left, const LinearHamiltonian1D<internal::SubSpace<2, 1>>& right)
	{
		return right + left;
	}

	template<count_t dim>
	LinearHamiltonian2D operator+ (const LinearHamiltonian2D& left, const FunctionOfX<real, internal::SubSpace<2, dim>>& pot)
	{
		return left + internal::uplift(pot, left._hilbertSpace());
	}
	template<count_t dim>
	LinearHamiltonian2D operator- (const LinearHamiltonian2D& left, const FunctionOfX<real, internal::SubSpace<2, dim>>& pot)
	{
		return left - internal::uplift(pot, left._hilbertSpace());
	}
}


namespace qengine
{
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator*(const FunctionOfX<N1, two_particle::CommonDimensionSpace>& left, const FunctionOfX<N2, internal::MultiDimensionalSpace<2, 1, 2>>& right)
	{
		return internal::uplift(left, right._hilbertSpace())*right;
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, internal::MultiDimensionalSpace<2, 1, 2>>
		operator*(const FunctionOfX<N2, internal::MultiDimensionalSpace<2, 1, 2>>& left, const FunctionOfX<N1, two_particle::CommonDimensionSpace>& right)
	{
		return right*left;
	}
}
