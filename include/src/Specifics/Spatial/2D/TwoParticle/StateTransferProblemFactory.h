﻿/* COPYRIGHT
 *
 * file="StateTransferProblemFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/Spatial/HamiltonianFunction.h"
#include "src/Generics/Physics/Spatial/Spectrum.impl.h"

#include "src/Generics/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/Specifics/Spatial/OptimalControlHelpers.h"

#include "src/Specifics/Spatial/2D/TwoParticle/Hamiltonian2P.h"
#include "src/Specifics/Spatial/2D/TwoParticle/defaultSteppingAlg.h"

namespace qengine
{
	template<class PotentialFunction, class dHdu>
	auto makeStateTransferProblem(
		const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H,
		const dHdu dV,
		const FunctionOfX<complex, Hamiltonian2P::HilbertSpace>& initialState,
		const FunctionOfX<complex, Hamiltonian2P::HilbertSpace>& targetState,
		const Control& x0
	)
	{
		using namespace spatial;
		const auto& dt = x0.dt();

		auto alg = addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._potential()._hilbertSpace()), dt);
		auto algb = addImagBounds(splitStep(H, -dt), makeDefaultImagPot(H._H0()._potential()._hilbertSpace()), dt);

		return makeLinearStateTransferProblem(alg, algb, dV, initialState, targetState, x0);
	}

	template<class HilbertSpace, class PotentialFunction>
	auto makeStateTransferProblem(
		const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H,
		const FunctionOfX<complex, HilbertSpace>& initialState,
		const FunctionOfX<complex, HilbertSpace>& targetState,
		const Control& x0
	)
	{
		auto dV = makeNumericDiffPotential(H._potentialFunction());
		return makeStateTransferProblem(H, dV, initialState, targetState, x0);
	}
}
