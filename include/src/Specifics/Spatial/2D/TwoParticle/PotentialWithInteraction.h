﻿/* COPYRIGHT
 *
 * file="PotentialWithInteraction.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/Generics/Physics/General/extract_hilbertspace.h"

#include "src/Generics/Physics/Spatial/isPotential.h"
#include "src/Generics/Physics/Spatial/FunctionOfX.h"
#include "src/Generics/Physics/Spatial/PotentialSum.h"

#include "src/Specifics/Spatial/2D/TwoParticle/CommonDimensionSpace.h"
#include "src/Specifics/Spatial/2D/TwoParticle/PointInteraction.h"

namespace qengine
{
	using PotentialWithInteraction = PotentialSum<two_particle::Potential<real>, two_particle::PointInteraction<real>>;

	template<class N1, class N2>
	PotentialWithInteraction
		operator+ (const two_particle::Potential<N1>& left, const two_particle::PointInteraction<N2> right)
	{
		return makePotentialSum(left, right);
	}

	template<class N1, class N2>
	PotentialWithInteraction
		operator+ (const two_particle::PointInteraction<N1>& left, const two_particle::Potential<N2> right)
	{
		return makePotentialSum(right, left);
	}

	inline PotentialWithInteraction operator-(const PotentialWithInteraction& left, const PotentialWithInteraction& right)
	{
		return  makePotentialSum(get<0>(left) - get<0>(right), get<1>(left) - get<1>(right));
	}

	inline PotentialWithInteraction operator* (const real left, const PotentialWithInteraction& right)
	{
		return  makePotentialSum(left*get<0>(right), left*get<1>(right));
	}

	inline PotentialWithInteraction operator* (const PotentialWithInteraction& left, const real right)
	{
		return right * left;
	}

	template<class ScalarType>
	FunctionOfX<ScalarType, internal::MultiDimensionalSpace<2, 1, 2>> operator*(const PotentialWithInteraction& left, const FunctionOfX<ScalarType, internal::MultiDimensionalSpace<2, 1, 2>>& right)
	{
		return get<0>(left)*right +
			get<1>(left)*right;
	}
}

namespace qengine
{
	namespace internal
	{
		template<> struct extract_hilbertspace<PotentialWithInteraction>
		{
			using type = MultiDimensionalSpace<2, 1, 2>;
		};
	}
}
