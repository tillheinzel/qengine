﻿/* COPYRIGHT
 *
 * file="CommonDimensionSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Generics/Physics/Spatial/SubSpace.h"
#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"

namespace qengine
{
	namespace two_particle
	{
		class CommonDimensionSpace : public internal::OneDSpace_BaseClass
		{
		public:
			CommonDimensionSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
				OneDSpace_BaseClass(xLower, xUpper, dimension, kinematicFactor)
			{}

			explicit CommonDimensionSpace(const OneDSpace_BaseClass& other) :
				OneDSpace_BaseClass(other)
			{}

			static constexpr count_t nDimensions = 1;

			explicit operator internal::MultiDimensionalSpace<2, 1, 2>() const
			{
				return internal::tensor(internal::SubSpace<2, 1>(*this), internal::SubSpace<2, 2>(*this));
			}
		};

		inline bool operator==(const CommonDimensionSpace& left, const CommonDimensionSpace& right) { return isSame(left, right); }

		inline bool operator!=(const CommonDimensionSpace& left, const CommonDimensionSpace& right) { return !isSame(left, right); }


		template<class N>
		using Potential = FunctionOfX<N, CommonDimensionSpace>;
	}



	namespace internal
	{
		template<class ScalarType>
		struct extract_hilbertspace<FunctionOfX<ScalarType, two_particle::CommonDimensionSpace>>
		{
			using type = internal::MultiDimensionalSpace<2, 1, 2>;
		};
	}
}


namespace qengine
{
	namespace internal
	{
		template<class N>
		FunctionOfX<N, internal::MultiDimensionalSpace<2, 1, 2>> uplift(const two_particle::Potential<N>& V)
		{
			const auto& s = V._hilbertSpace();

			const auto foo = makeFunctionOfX(internal::SubSpace<2, 1>(s), V._vec());
			return foo + makeFunctionOfX(internal::SubSpace<2, 2>(s), V._vec());
		}
	}
}
