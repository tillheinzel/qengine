﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian2D.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Generics/Physics/Spatial/Spectrum.h"
#include "src/Generics/Physics/Spatial/LinearHamiltonian.h"
#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"

namespace qengine
{
	using LinearHamiltonian2D = LinearHamiltonian<internal::MultiDimensionalSpace<2, 1, 2>, internal::SqSparseMat>;
}
