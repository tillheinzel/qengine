﻿/* COPYRIGHT
 *
 * file="makeState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/toString.h"
#include "src/Generics/Physics/SecondQuantization/FockOperators.h"

#include "src/Specifics/SecondQuantized/NLevel/HilbertSpace.h"
#include "src/Specifics/SecondQuantized/BoseHubbard/HilbertSpace.h"

namespace qengine
{

	State<n_level::HilbertSpace> makeState(const n_level::HilbertSpace& s, const LinearCombinationOfFocks& focks, NORMALIZATION shouldNormalize = NORMALIZE_NOT);
	State<bosehubbard::HilbertSpace> makeState(const bosehubbard::HilbertSpace& s, const LinearCombinationOfFocks& def, NORMALIZATION shouldNormalize = NORMALIZE_NOT);

	template<class HilbertSpace, class = std::enable_if_t<second_quantized::isHilbertSpace_v<HilbertSpace>>>
	State<HilbertSpace> makeState(const HilbertSpace& s, const CVec& vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		if (shouldNormalize == NORMALIZE_NOT) return State<HilbertSpace>(vec, s);
		return normalize(State<HilbertSpace>(vec, s));
	}

	template<class HilbertSpace, class = std::enable_if_t<second_quantized::isHilbertSpace_v<HilbertSpace>>>
	State<HilbertSpace> makeState(const HilbertSpace& s, const RVec& vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, CVec(vec), shouldNormalize);
	}

	template<class HilbertSpace, class = std::enable_if_t<second_quantized::isHilbertSpace_v<HilbertSpace>>>
	State<HilbertSpace> makeState(const HilbertSpace& s, std::initializer_list<real> vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, RVec(vec), shouldNormalize);
	}

	template<class HilbertSpace, class = std::enable_if_t<second_quantized::isHilbertSpace_v<HilbertSpace>>>
	State<HilbertSpace> makeState(const HilbertSpace& s, std::initializer_list<complex> vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, CVec(vec), shouldNormalize);
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<bosehubbard::HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, bosehubbard::HilbertSpace>>& l, Ts&&... ts)
	{
		//// todo: figure out why these do not work when making Hilbertspace a template
		return l.evaluate(std::forward<Ts>(ts)...);
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<bosehubbard::HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, bosehubbard::HilbertSpace>>& l, const NORMALIZATION shouldNormalize, Ts&&... ts)
	{
		auto state = l.evaluate(std::forward<Ts>(ts)...);
		if (shouldNormalize == NORMALIZE) return normalize(state);
		return state;
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<n_level::HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, n_level::HilbertSpace>>& l, Ts&&... ts)
	{
		return l.evaluate(std::forward<Ts>(ts)...);
	}
	template<template<class> class MatType, class NumberType, class... Ts>
	State<n_level::HilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, n_level::HilbertSpace>>& l, const NORMALIZATION shouldNormalize, Ts&&... ts)
	{
		auto state = l.evaluate(std::forward<Ts>(ts)...);
		if (shouldNormalize == NORMALIZE) return normalize(state);
		return state;
	}
}
