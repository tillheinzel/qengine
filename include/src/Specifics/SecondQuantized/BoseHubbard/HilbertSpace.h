﻿/* COPYRIGHT
 *
 * file="HilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <unordered_map>

#include "src/Utility/LinearAlgebra/Functions.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Generics/Physics/SecondQuantization/isHilbertSpace.h"
#include "src/Generics/Physics/General/SerializedHilbertSpace.h"
#include "src/Generics/Physics/SecondQuantization/Operator.h"
#include "src/Generics/Physics/SecondQuantization/FockState.h"

#include "src/Specifics/SecondQuantized/BoseHubbard/hashing.h"

namespace qengine
{
	namespace bosehubbard
	{

		class HilbertSpace
		{
		public:
			using map_t = std::unordered_map<internal::FockState, count_t, detail::Hash>;

		public:
			HilbertSpace(count_t nParticles, count_t nSites);

			HilbertSpace(const HilbertSpace& other) = default;

			HilbertSpace& operator=(const HilbertSpace& other) = default;

			/// GETTERS
			count_t nParticles() const;
			count_t nSites() const;
			count_t nBasisElements() const;
			count_t dim() const;

			/// SPECIAL OBJECT CREATION FUNCTIONS
		public:
			Operator<internal::SqSparseMat, real, HilbertSpace> makeHoppingOperator(bool usePeriodicBounds = true) const;
			Operator<internal::SqDenseMat, real, HilbertSpace> makeDenseHoppingOperator(bool usePeriodicBounds = true) const;


			Operator<internal::SqDiagMat, real, HilbertSpace> makeOnSiteOperator() const;
			Operator<internal::SqDiagMat, real, HilbertSpace> transformPotential(const RVec& vec) const;


			State<bosehubbard::HilbertSpace> makeMott() const;
			State<bosehubbard::HilbertSpace> makeSuperFluid() const;


			CMat singleParticleDensityMatrix(const State<HilbertSpace>& state) const;

			/// INTERNAL USE
		public:
			count_t _get(const internal::FockState& f) const;
			bool _isInBasis(const internal::FockState& f) const;
			std::vector<internal::FockState> _basis() const;
			map_t _stateMap() const;
			const RMat& _basisMat() const { return basisMat_; }

		private:
			/// DATAMEMBERS
			count_t nParticles_;
			count_t nSites_;
			//count_t dim_;
			std::vector<internal::FockState> basis_;
			map_t stateMap_;
			RMat basisMat_;
		};


		inline HilbertSpace makeHilbertSpace(const count_t nParticles, const count_t nSites) { return HilbertSpace(nParticles, nSites); }


	}

	namespace second_quantized
	{
		template<>
		struct hasHilbertSpaceLabel<bosehubbard::HilbertSpace> : std::true_type{};
	}
}


namespace qengine
{
	namespace internal
	{
		template<>
		struct SerializedHilbertSpace<bosehubbard::HilbertSpace>
		{
			const count_t nParticles;
			const count_t nSites;
			const count_t dim;

			SerializedHilbertSpace(const bosehubbard::HilbertSpace& s) :
				nParticles(s.nParticles()),
				nSites(s.nSites()),
				dim(s.dim())
			{}

			SerializedHilbertSpace(const SerializedHilbertSpace& s) = default;
			SerializedHilbertSpace(SerializedHilbertSpace&& s) noexcept = default;
		};

		inline bool operator==(const SerializedHilbertSpace<bosehubbard::HilbertSpace>& left, const SerializedHilbertSpace<bosehubbard::HilbertSpace>& right)
		{
			return left.nParticles == right.nParticles && left.nSites == right.nSites;
		}

	}
}
