﻿/* COPYRIGHT
 *
 * file="StateTransferProblemFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>
#include "src/Utility/is_callable.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Generics/Physics/Spatial/HamiltonianFunction.h"
#include "src/Generics/OptimalControl/Control.h"
#include "src/Generics/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/Generics/Physics/SecondQuantization/KrylovStepping.h"

#include "src/Specifics/SecondQuantized/OptimalControlHelpers.h"
#include "src/Specifics/SecondQuantized/BoseHubbard/HilbertSpace.h"


namespace qengine
{
	template<
		class dHdu,
		class HamiltonianFunctor,
		typename = std::enable_if_t<
		is_callable_v<HamiltonianFunctor, Operator<internal::SqSparseMat, real, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqSparseMat, complex, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqDenseMat, real, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqDenseMat, complex, bosehubbard::HilbertSpace>(RVec)>
		>
	>
		auto makeStateTransferProblem(
			const HamiltonianFunctor& H,
			const dHdu dH,
			const State<bosehubbard::HilbertSpace>& initialState,
			const State<bosehubbard::HilbertSpace>& targetState,
			const Control& x0,
			const count_t krylovOrder
		)
	{
		const auto dt = x0.dt();

		auto alg = krylov(H, krylovOrder, dt);
		auto algb = krylov(H, krylovOrder, -dt);

		return makeLinearStateTransferProblem(alg, algb, dH, initialState, targetState, x0);
	}

	template<
		class HamiltonianFunctor,
		typename = std::enable_if_t<
		is_callable_v<HamiltonianFunctor, Operator<internal::SqSparseMat, real, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqSparseMat, complex, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqDenseMat, real, bosehubbard::HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunctor, Operator<internal::SqDenseMat, complex, bosehubbard::HilbertSpace>(RVec)>
		>
	>
		auto makeStateTransferProblem(
			const HamiltonianFunctor& H,
			const State<bosehubbard::HilbertSpace>& initialState,
			const State<bosehubbard::HilbertSpace>& targetState,
			const Control& x0,
			const count_t krylovOrder
		)
	{
		auto dHdu = makeNumericDiffHamiltonian(H, x0.paramCount());
		return makeStateTransferProblem(H, dHdu, initialState, targetState, x0, krylovOrder);
	}

}
