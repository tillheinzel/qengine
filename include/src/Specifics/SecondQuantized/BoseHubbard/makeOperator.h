﻿/* COPYRIGHT
 *
 * file="makeOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Generics/Physics/SecondQuantization/FockOperators.h"

#include "src/Specifics/SecondQuantized/BoseHubbard/HilbertSpace.h"

namespace qengine
{
	Operator<internal::SqDenseMat, real, bosehubbard::HilbertSpace> makeDenseOperator(const bosehubbard::HilbertSpace& s, std::initializer_list<std::initializer_list<real>> mat);
	Operator<internal::SqDenseMat, complex, bosehubbard::HilbertSpace> makeDenseOperator(const bosehubbard::HilbertSpace& s, std::initializer_list<std::initializer_list<complex>> mat);

	Operator<internal::SqDenseMat, complex, bosehubbard::HilbertSpace> makeDenseOperator(const bosehubbard::HilbertSpace& s, const FockOperator& op);

	Operator<internal::SqSparseMat, complex, bosehubbard::HilbertSpace> makeOperator(const bosehubbard::HilbertSpace& s, const FockOperator& op);
	Operator<internal::SqSparseMat, complex, bosehubbard::HilbertSpace> makeOperator(const bosehubbard::HilbertSpace& s, const FockOperator& op);
}
