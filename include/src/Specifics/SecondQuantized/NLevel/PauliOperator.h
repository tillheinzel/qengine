﻿/* COPYRIGHT
 *
 * file="PauliOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"
#include "src/Generics/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	namespace internal
	{
		template<class> class SqDenseMat;
	}

	namespace n_level
	{
		class HilbertSpace;

		Operator<internal::SqDenseMat, real, HilbertSpace> makePauliI();
		Operator<internal::SqDenseMat, real, HilbertSpace> makePauliX();
		Operator<internal::SqDenseMat, complex, HilbertSpace> makePauliY();
		Operator<internal::SqDenseMat, real, HilbertSpace> makePauliZ();
	}
}
