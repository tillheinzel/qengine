﻿/* COPYRIGHT
 *
 * file="HilbertSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Specifics/SecondQuantized/NLevel/HilbertSpace.h"
#include "src/Utility/ExtraDebugChecks.h"

namespace qengine
{
	namespace n_level
	{
		HilbertSpace::HilbertSpace(const count_t nLevels) : nLevels_(nLevels)
		{
		}

		HilbertSpace::HilbertSpace(const internal::SerializedHilbertSpace<HilbertSpace>& serializedHilbertSpace) :
			nLevels_(serializedHilbertSpace.nLevels)
		{
		}

		count_t HilbertSpace::nLevels() const
		{
			return nLevels_;
		}

		count_t HilbertSpace::_get(const internal::FockState& basisState) const
		{
			qengine_assert(basisState._quanta().size() == 1, "only single-mode basisStates are possible for nlevel");
			qengine_assert(basisState.at(0) < nLevels(), "index out of bounds");

			return basisState.at(0);
		}

		HilbertSpace makeHilbertSpace(const count_t nLevels)
		{
			return HilbertSpace(nLevels);
		}
	}

	namespace internal
	{
		SerializedHilbertSpace<n_level::HilbertSpace>::SerializedHilbertSpace(
			const n_level::HilbertSpace& hs) : nLevels(hs.nLevels())
		{
		}

		SerializedHilbertSpace<n_level::HilbertSpace>::
			SerializedHilbertSpace(const count_t nLevels) : nLevels(nLevels)
		{
		}

		bool operator==(const SerializedHilbertSpace<n_level::HilbertSpace>& left,
			const SerializedHilbertSpace<n_level::HilbertSpace>& right)
		{
			return left.nLevels == right.nLevels;
		}
	}
}
