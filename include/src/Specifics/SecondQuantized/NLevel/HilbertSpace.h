﻿/* COPYRIGHT
 *
 * file="HilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Generics/Physics/SecondQuantization/isHilbertSpace.h"
#include "src/Generics/Physics/General/SerializedHilbertSpace.h"
#include "src/Generics/Physics/SecondQuantization/FockState.h"

namespace qengine
{
	namespace n_level
	{
		class HilbertSpace
		{
		public:
			explicit HilbertSpace(const count_t nLevels);
			/*implicit*/ HilbertSpace(const internal::SerializedHilbertSpace<HilbertSpace>& serializedHilbertSpace);

			count_t nLevels() const;

			/// INTERNAL USE ONLY
		public:
			count_t _get(const internal::FockState& basisState) const;
		private:
			count_t nLevels_;
		};

		HilbertSpace makeHilbertSpace(const count_t nLevels);
	}

	namespace internal
	{
		template<>
		struct SerializedHilbertSpace<n_level::HilbertSpace>
		{
			SerializedHilbertSpace(const n_level::HilbertSpace& hs);
			SerializedHilbertSpace(const count_t nLevels);
			count_t nLevels;
		};

		bool operator==(const SerializedHilbertSpace<n_level::HilbertSpace>& left,
			const SerializedHilbertSpace<n_level::HilbertSpace>& right);
	}


	namespace second_quantized
	{
		template<>
		struct hasHilbertSpaceLabel<n_level::HilbertSpace> : std::true_type {};
	}
}
