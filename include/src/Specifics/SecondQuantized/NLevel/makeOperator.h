﻿/* COPYRIGHT
 *
 * file="makeOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Types.h"

#include "src/Utility/LinearAlgebra/SqDenseMat.h"


#include "src/Generics/Physics/SecondQuantization/Operator.h"
#include "src/Generics/Physics/SecondQuantization/FockOperators.h"

#include "src/Specifics/SecondQuantized/NLevel/HilbertSpace.h"


namespace qengine
{
	Operator<internal::SqDenseMat, complex, n_level::HilbertSpace> makeOperator(const n_level::HilbertSpace& s, const FockOperator& op);

	Operator<internal::SqDenseMat, real, n_level::HilbertSpace> makeOperator(const n_level::HilbertSpace& s, RMat rep);
	Operator<internal::SqDenseMat, complex, n_level::HilbertSpace> makeOperator(const n_level::HilbertSpace& s, CMat rep);

	Operator<internal::SqDenseMat, real, n_level::HilbertSpace> makeOperator(const n_level::HilbertSpace& s, std::initializer_list<std::initializer_list<real>> rep);
	Operator<internal::SqDenseMat, complex, n_level::HilbertSpace> makeOperator(const n_level::HilbertSpace& s, std::initializer_list<std::initializer_list<complex>> rep);
}
