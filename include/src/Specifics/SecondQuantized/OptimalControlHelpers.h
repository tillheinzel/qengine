﻿/* COPYRIGHT
 *
 * file="OptimalControlHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <functional>

#include "src/Utility/is_callable.h"

#include "src/Generics/Physics/SecondQuantization/Operator.h"
#include "src/Generics/Physics/SecondQuantization/OpaqueOperator.h"

namespace qengine
{
	template<
		class... Ts,
		class HilbertSpace = internal::extract_hilbertspace_t<decltype(std::declval<std::tuple_element_t<0, std::tuple<Ts...>>>()(std::declval<RVec>()))>
	>
		auto makeAnalyticDiffHamiltonian(const Ts&... ts)
	{
		//static_assert(internal::and_all((is_callable_v<Ts, Operator<internal::SqDenseMat, real, HilbertSpace>(RVec)> || is_callable_v<Ts, Operator<internal::SqDenseMat, complex, HilbertSpace>(RVec)>)...), 
		//	"parameters must be callable with RVec and return operators!");

		auto wrap = [](auto opFunc)
		{
			return [opFunc](const RVec& params)
			{
				return makeOpaqueOperator(opFunc(params));
			};
		};

		using dHdui = std::function<OpaqueOperator<HilbertSpace>(const Vector<real>&)>;

		return std::vector<dHdui>{wrap(ts)...};
	}

	template<class HamiltonianFunction,
		class HilbertSpace = internal::extract_hilbertspace_t<decltype(std::declval<HamiltonianFunction>()(std::declval<RVec>()))>,
		typename = std::enable_if_t<
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, complex, HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, real, HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunction, Operator<internal::SqSparseMat, complex, HilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunction, Operator<internal::SqSparseMat, real, HilbertSpace>(RVec)>
		>
	>
		auto makeNumericDiffHamiltonian(HamiltonianFunction H, const count_t paramCount, const real epsilon = 1e-6)
	{
		using dHdui = std::function<decltype(H(RVec{}))(const Vector<real>&) > ;
		std::vector<dHdui> retval;

		auto dir = RVec(paramCount, 0.0);
		auto divisor = 1.0 / (2 * epsilon); // precalculate
		for (auto i = 0u; i < paramCount; ++i)
		{
			dir.at(i) = 1;

			auto edir = epsilon * dir;

			retval.push_back(dHdui(
				[edir, H, divisor](const RVec& x)
			{
				return divisor * (H(x + edir) - H(x - edir));
			}));

			dir.at(i) = 0;
		}

		return retval;
	}
}
