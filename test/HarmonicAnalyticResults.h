﻿/* COPYRIGHT
 *
 * file="HarmonicAnalyticResults.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Vector.h"


using namespace qengine;

inline RVec harmonicGroundstate(const RVec& xArray, double x, double omega)
{
    using namespace qengine;
    using namespace internal;

    auto normalization = static_cast<double>(std::pow(omega / PI, 1 / 4.0));
    return normalization*exp(-omega / 2 * pow(xArray - x, 2));
}

inline RVec harmonicFirstExcitedstate(const RVec& xArray, double x, double omega)
{
    using namespace qengine;

    auto normalization = static_cast<double>(std::pow(omega / PI, 1 / 4.0)*std::sqrt(2*omega));
    return normalization*(xArray - x) * exp(-omega / 2 * pow(xArray - x, 2));
}

inline RVec harmonicSecondExcitedstate(const RVec& xArray, double x, double omega)
{
    using namespace qengine;

    auto normalization = static_cast<double>(std::pow(omega / PI, 1 / 4.0)*1/sqrt(2.0));
    return normalization*( 2 * omega*pow(xArray - x, 2)-1.0) * exp(-omega / 2 * pow(xArray - x, 2));
}

// create lambda return harmonic potential based on param (x0, omega)
 inline auto makeHarmonicPotentialFunction(const RVec& xArray)
{
     return [xArray](std::vector<double> param)->RVec
     {
         auto _x0 = param.at(0);
         auto _omega = param.at(1);

         return (0.5*_omega*_omega*(xArray - _x0) * (xArray - _x0));
     };
}

//template<unsigned int n>
//arma::vec harmonicEigenstate(const arma::vec& xArray, double x, double omega) { static_assert(n < 3, "only n= 0,1,2 are allowed"); }
//
//
// template<> inline arma::vec harmonicEigenstate<0>(const arma::vec& xArray, double x, double omega) { return harmonicGroundstate(xArray, x, omega); }
// template<> inline arma::vec harmonicEigenstate<1>(const arma::vec& xArray, double x, double omega) { return harmonicFirstExcitedstate(xArray, x, omega); }
// template<> inline arma::vec harmonicEigenstate<2>(const arma::vec& xArray, double x, double omega) { return harmonicSecondExcitedstate(xArray, x, omega); }
//

// template<unsigned int n, class f_x0, class f_xc, class f_xcDiff, class f_Intx0xc>
// auto makeDrivenHarmonicEigenstate(const arma::vec& xArray, double omega, f_x0 x0, f_xc xc, f_xcDiff xcDiff, f_Intx0xc Intx0xc)
//{
//    return [&xArray, omega, x0, xc, xcDiff, Intx0xc](double t) ->arma::cx_vec
//    {
//        double a1 = 0.0;
//        double a2 = 0.0;
//        arma::vec a3 = 0.0*xArray;

//        a1 = -(n + 0.5)*omega*t;
//        a2 = xcDiff(t)*(xArray - 0.5*xc(t));
//        a3 = 0.5*omega*omega/**Intx0xc(t)*/;

//        return harmonicEigenstate<n>(xArray, xc(t), omega) % arma::exp(QMath::1i*(a1+a2+a3));
//    };
//}



// inline auto makeLinearDrivenAnalyticWavefunction(const arma::vec& xArray, double omega, double x0Base, double drivingFactor)
// {
//     auto a = drivingFactor;
//     auto x0_f = [=](double t) {return x0Base + a*t; };
//     auto xc = [=](double t) {return x0Base + a*(t - std::sin(omega*t) / omega); };
//     auto xcDiff = [=](double t) {return a*(1.0 - std::cos(omega*t)); };
//     auto Intx0xc = [=](double t) {return a*a*(t*t*t / 3 - std::sin(omega*t) / (omega*omega*omega) - t*std::cos(omega*t) / (omega*omega)); };

//     auto analyticState = makeDrivenHarmonicEigenstate<0>(xArray, omega, x0_f, xc, xcDiff, Intx0xc);
//     return [analyticState](double t) {return QPhysics::Wavefunction(analyticState(t)); };
// }


// inline auto makeShakeupDrivenAnalyticWavefunction(const arma::vec& xArray, double omega, double x0Base, double drivingFactor)
// {
//     auto a = drivingFactor;
//     auto x0_f = [=](double t) {return x0Base + a*std::sin(omega*t); };
//     auto xc = [=](double t) {return x0Base + a*0.5*(std::sin(omega*t) - omega*t*std::cos(omega*t)); };
//     auto xcDiff = [=](double t) {return a*0.5*(omega*omega*t*std::sin(omega*t)); };
//     auto Intx0xc = [=](double t) {return a*a*0.25/omega*(omega*t*(1 + 0.5*std::cos(2 * omega*t)) /*- 0.25 * 3 * std::sin(2 * omega*t)*/); };

//     auto analyticState = makeDrivenHarmonicEigenstate<0>(xArray, omega, x0_f, xc, xcDiff, Intx0xc);
//     return [analyticState](double t) {return QPhysics::Wavefunction(analyticState(t)); };
// }


// inline auto makeConstantlyDrivenAnalyticWavefunction(const arma::vec& xArray, double omega, double x0Base, double drivingFactor)
// {
//     auto a = drivingFactor;
//     auto x0_f = [=](double) {return x0Base + a; };
//     auto xc = [=](double t) {return x0Base + a*(1-std::cos(omega*t)); };
//     auto xcDiff = [=](double t) {return a*omega*std::sin(omega*t); };
//     auto Intx0xc = [=](double t) {return a*a*(t-std::sin(omega*t)/omega); };

//     auto analyticState = makeDrivenHarmonicEigenstate<0>(xArray, omega, x0_f, xc, xcDiff, Intx0xc);
//     return [analyticState](double t) {return QPhysics::Wavefunction(analyticState(t)); };
// }

