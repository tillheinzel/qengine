﻿/* COPYRIGHT
 *
 * file="T_DataContainer_json.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#include <gtest/gtest.h>
#include <fstream>
#include <regex>

#include "qengine/DataContainer.h"
#include "src/DataContainer/JsonWrapper.h"

#include "additionalAsserts.h"
#include "src/Utility/Constants.h"

using namespace qengine;
using namespace std::complex_literals;

class DataContainer_Json_Fixture : public ::testing::Test
{
public:

};

TEST_F(DataContainer_Json_Fixture, play)
{
	const SerializedObject obj;

	nlohmann::json j;

	//j["cheese"] = obj;


	std::map<std::string, SerializedObject> mymap;
	mymap["obj"] = obj;

	//for (const auto& a : mymap)
	//{
	//	j[a.first] = a.second;
	//}

	//std::cout << j << std::endl;
}

TEST_F(DataContainer_Json_Fixture, saveLoadEmpty)
{
	auto dc = DataContainer{};

	dc.save("jsonTest.json");
	dc.load("jsonTest.json");

	ASSERT_EQ(0u, dc.varNames().size());
}

TEST_F(DataContainer_Json_Fixture, saveLoadString)
{
	static_assert(!internal::isSerializable_v<nlohmann::json>, "");
	static_assert(!internal::isSerializable_v<std::string>, "");

	auto saver = DataContainer{};
	const auto myString = std::string("CheeseCake");
	saver["myString"] = myString;


	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myStringLoad = std::string(loader["myString"]);
	ASSERT_EQ(myString, myStringLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadReal)
{
	auto saver = DataContainer{};
	const auto myReal = 1.0;
	saver["myReal"] = myReal;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRealLoad = loader.getReal("myReal");
	ASSERT_EQ(myReal, myRealLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadRealWithExp)
{
	auto saver = DataContainer{};
	const auto myReal = 1.12345678910111e-16;
	saver["myReal"] = myReal;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRealLoad = loader.getReal("myReal");
	ASSERT_EQ(myReal, myRealLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadRVec)
{
	auto saver = DataContainer{};
	const auto myRVec = RVec{ 1,2,3 };
	saver["myRVec"] = myRVec;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRVecLoad = loader.getRVec("myRVec");
	ASSERT_EQ(myRVec, myRVecLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadRVecWithExp)
{
	auto saver = DataContainer{};
	const auto myRVec = 1e-16*RVec{ 1,2,3 };
	saver["myRVec"] = myRVec;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");
	
	const auto myRVecLoad = loader.getRVec("myRVec");
	ASSERT_EQ(myRVec, myRVecLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadRMat)
{
	auto saver = DataContainer{};
	const auto myRMat = RMat{ {1,0}, {0,1}};
	saver["myRMat"] = myRMat;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRMatLoad = loader.getRMat("myRMat");
	ASSERT_EQ(myRMat, myRMatLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadAppendOfRMats)
{
	using namespace std::literals::complex_literals;
	auto saver = DataContainer{};
	const auto myRMat = RMat{ { 1,0 },{ 0,1 } };
	saver["myRMat"] = myRMat;
	saver["myRMat"].append(myRMat);
	saver["myRMat"].append(myRMat);
	saver["myRMat"].append(myRMat);
	saver["myRMat"].append(myRMat);


	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const std::vector<RMat> myRMatLoad = loader["myRMat"];
	ASSERT_EQ(myRMat, myRMatLoad.at(0));
	ASSERT_EQ(myRMat, myRMatLoad.at(1));
	ASSERT_EQ(myRMat, myRMatLoad.at(2));
	ASSERT_EQ(myRMat, myRMatLoad.at(3));
	ASSERT_EQ(myRMat, myRMatLoad.at(4));
}
TEST_F(DataContainer_Json_Fixture, saveLoadListOfRMats)
{
	using namespace std::literals::complex_literals;
	auto saver = DataContainer{};
	const auto myRMat = RMat{ { 1,0 },{ 0,1 } };
	const auto list = std::vector<RMat>{ myRMat, myRMat , myRMat , myRMat , myRMat };
	saver["myRMat"] = list;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const std::vector<RMat> myRMatLoad = loader["myRMat"];
	ASSERT_EQ(myRMat, myRMatLoad.at(0));
	ASSERT_EQ(myRMat, myRMatLoad.at(1));
	ASSERT_EQ(myRMat, myRMatLoad.at(2));
	ASSERT_EQ(myRMat, myRMatLoad.at(3));
	ASSERT_EQ(myRMat, myRMatLoad.at(4));
}

TEST_F(DataContainer_Json_Fixture, saveLoadRMatWithExp)
{
	auto saver = DataContainer{};
	const auto myRMat = 1e-16*RMat{ {1,0}, {0,1}};
	saver["myRMat"] = myRMat;

	saver.save("jsonTest.json");
	saver.save("verySmallRMat.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRMatLoad = loader.getRMat("myRMat");
	ASSERT_EQ(myRMat, myRMatLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadMultipleRealObjects)
{
	auto saver = DataContainer{};
	const auto myReal = 1.0;
	saver["myReal"] = myReal;
	const auto myRVec = RVec{ 1,2,3 };
	saver["myRVec"] = myRVec;
	const auto myRMat = RMat{ {1,0}, {0,1}};
	saver["myRMat"] = myRMat;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myRealLoad = loader.getReal("myReal");
	ASSERT_EQ(myReal, myRealLoad);
	const auto myRVecLoad = loader.getRVec("myRVec");
	ASSERT_EQ(myRVec, myRVecLoad);
	const auto myRMatLoad = loader.getRMat("myRMat");
	ASSERT_EQ(myRMat, myRMatLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadComplex)
{
	auto saver = DataContainer{};
	const auto myComplex = 1.0+1i;
	saver["myComplex"] = myComplex;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myComplexLoad = loader.getComplex("myComplex");
	ASSERT_EQ(myComplex, myComplexLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadCVec)
{
	using namespace std::literals::complex_literals;

	auto saver = DataContainer{};
	const auto myCVec = CVec{ 1.0+1i,2.0i,3.0 };
	saver["myCVec"] = myCVec;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myCVecLoad = loader.getCVec("myCVec");
	ASSERT_EQ(myCVec, myCVecLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadCMat)
{
	using namespace std::literals::complex_literals;
	auto saver = DataContainer{};
	const auto myCMat = CMat{ { 1,1i },{ -1i,1 } };
	saver["myCMat"] = myCMat;

	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const auto myCMatLoad = loader.getCMat("myCMat");
	ASSERT_EQ(myCMat, myCMatLoad);
}

TEST_F(DataContainer_Json_Fixture, saveLoadListOfCMats)
{
	using namespace std::literals::complex_literals;
	auto saver = DataContainer{};
	const auto myCMat = CMat{ { 1,1i },{ -1i,1 } };
	saver["myCMat"] = myCMat;
	saver["myCMat"].append(myCMat);


	saver.save("jsonTest.json");
	auto loader = DataContainer{};
	loader.load("jsonTest.json");

	const std::vector<CMat> myCMatLoad = loader["myCMat"];
	ASSERT_EQ(myCMat, myCMatLoad.at(0));
	ASSERT_EQ(myCMat, myCMatLoad.at(1));
}

//TEST_F(DataContainer_Json_Fixture, saveLoadSp_RMat)
//{
//	using namespace std::literals::complex_literals;
//	auto saver = DataContainer{};
//	const auto mySp_RMat = internal::Sp_RMat(RMat{ { 1,0 },{ 0,1 } });
//	saver.set("mySp_RMat", mySp_RMat);
//
//	saver.save("jsonTest.json");
//	auto loader = DataContainer{};
//	loader.load("jsonTest.json");
//
//	const auto mySp_RMatLoad = loader.getSp_RMat("mySp_RMat");
//	ASSERT_EQ(mySp_RMat, mySp_RMatLoad);
//}

//TEST_F(DataContainer_Json_Fixture, saveLoadSp_CMat)
//{
//	using namespace std::literals::complex_literals;
//	auto saver = DataContainer{};
//	const auto mySp_CMat = internal::Sp_CMat(CMat{ { 1.0+1i,0 },{ 0,-1i } });
//	saver.set("mySp_CMat", mySp_CMat);
//
//	saver.save("jsonTest.json");
//	auto loader = DataContainer{};
//	loader.load("jsonTest.json");
//
//	const auto mySp_CMatLoad = loader.getSp_CMat("mySp_CMat");
//	ASSERT_EQ(mySp_CMat, mySp_CMatLoad);
//}


TEST_F(DataContainer_Json_Fixture, saveLoadMultipleRealObjectsWithDefaultFormat)
{
	auto saver = DataContainer{};
	const auto myReal = 1.0;
	saver["myReal"] = myReal;
	const auto myRVec = RVec{ 1,2,3 };
	saver["myRVec"] = myRVec;
	const auto myRMat = RMat{ { 1,0 },{ 0,1 } };
	saver["myRMat"] = myRMat;

	saver.save("jsonTest");
	auto loader = DataContainer{};
	loader.load("jsonTest","json");

	const auto myRealLoad = loader.getReal("myReal");
	ASSERT_EQ(myReal, myRealLoad);
	const auto myRVecLoad = loader.getRVec("myRVec");
	ASSERT_EQ(myRVec, myRVecLoad);
	const auto myRMatLoad = loader.getRMat("myRMat");
	ASSERT_EQ(myRMat, myRMatLoad);
}


TEST_F(DataContainer_Json_Fixture, saveLoadMultipleRealObjectsWithSecondaryParameterFormat)
{
	auto saver = DataContainer{};
	const auto myReal = 1.0;
	saver["myReal"] = myReal;
	const auto myRVec = RVec{ 1,2,3 };
	saver["myRVec"] = myRVec;
	const auto myRMat = RMat{ { 1,0 },{ 0,1 } };
	saver["myRMat"] = myRMat;

	saver.save("jsonTest","json");
	auto loader = DataContainer{};
	loader.load("jsonTest");

	const auto myRealLoad = loader.getReal("myReal");
	ASSERT_EQ(myReal, myRealLoad);
	const auto myRVecLoad = loader.getRVec("myRVec");
	ASSERT_EQ(myRVec, myRVecLoad);
	const auto myRMatLoad = loader.getRMat("myRMat");
	ASSERT_EQ(myRMat, myRMatLoad);
}
