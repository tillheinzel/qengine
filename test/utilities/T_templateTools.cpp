﻿/* COPYRIGHT
 *
 * file="T_templateTools.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/AutoMember.h"
#include "src/Utility/Maybe_Owner.h"
#include "src/Utility/has_function.h"
#include "src/Utility/Types.h"
#include "src/Utility/TypeList.h"
#include "src/Utility/cpp17Replacements/Tuple.h"
#include "src/Utility/cpp17Replacements/and_fold.h"

using namespace qengine;
using namespace internal;

class TemplateTools_Fixture : public ::testing::Test
{
public:
};

TEST_F(TemplateTools_Fixture, Maybe_Owner)
{
	// EXPLICIT CONSTRUCTION AND BASIC FUNCTIONALITY
	auto s = 1u;

	auto owning = Maybe_Owner<unsigned>{ s };
	auto referencing = Maybe_Owner<unsigned>{ std::ref(s) };

	s += 1;

	ASSERT_EQ(s, owning.get() + 1); // the owning Maybe_Owner has a copy of s, which is still 1
	ASSERT_EQ(s, referencing.get()); // the referencing Maybe_Owner has a reference to s, which is updated to 2

	ASSERT_NE(&s, &owning.get());
	ASSERT_EQ(&s, &referencing.get());

	// IMPLICIT CONSTRUCTION AND MOVE CONSTRUCTION
	auto implicit_ctor_testing = [](Maybe_Owner<unsigned> m) {return m; };

	auto s2 = 1u;
	auto owning2 = implicit_ctor_testing(s2);
	auto referencing2 = implicit_ctor_testing(std::ref(s2));

	s2 += 1;

	ASSERT_EQ(s2, owning2.get() + 1);
	ASSERT_EQ(s2, referencing2.get());

	ASSERT_NE(&s2, &owning2.get());
	ASSERT_EQ(&s2, &referencing2.get());

	s2 = 1;
	// COPY CONSTRUCTION
	auto owning3 = owning2;
	auto referencing3 = referencing2;

	s2 += 1;

	ASSERT_EQ(s2, owning3.get() + 1);
	ASSERT_EQ(s2, referencing3.get());

	ASSERT_NE(&s2, &owning3.get());
	ASSERT_EQ(&s2, &referencing3.get());

	ASSERT_NE(&owning2.get(), &owning3.get());
	ASSERT_EQ(&referencing2.get(), &referencing3.get());

	// COPY ASSIGNMENT
	owning3 = owning;
	referencing3 = referencing;

	ASSERT_EQ(s, owning3.get() + 1);
	ASSERT_EQ(s, referencing3.get());

	ASSERT_NE(&s, &owning3.get());
	ASSERT_EQ(&s, &referencing3.get());

	ASSERT_NE(&owning2.get(), &owning3.get());
	ASSERT_NE(&owning.get(), &owning3.get());
	ASSERT_EQ(&referencing.get(), &referencing3.get());
	ASSERT_NE(&referencing2.get(), &referencing3.get());

	// MOVE ASSIGNMENT
	owning3 = Maybe_Owner<unsigned>(owning);
	referencing3 = Maybe_Owner<unsigned>(referencing);

	ASSERT_EQ(s, owning3.get() + 1);
	ASSERT_EQ(s, referencing3.get());

	ASSERT_NE(&s, &owning3.get());
	ASSERT_EQ(&s, &referencing3.get());

	ASSERT_NE(&owning2.get(), &owning3.get());
	ASSERT_NE(&owning.get(), &owning3.get());
	ASSERT_EQ(&referencing.get(), &referencing3.get());
	ASSERT_NE(&referencing2.get(), &referencing3.get());
}

TEST_F(TemplateTools_Fixture, and_fold)
{
	static_assert(and_fold(), "");
	static_assert(and_fold<true>(), "");
	static_assert(and_fold<true, true, true, true, true, true, true, true>(), "");


	static_assert(!and_fold<false>(), "");
	static_assert(!and_fold<true, true, true, false, true, true, true, true>(), "");
	static_assert(!and_fold<false, true, true, false>(), "");
}


class TypeListFixture : public ::testing::Test
{
public:
};


TEST_F(TypeListFixture, findFirstIndex)
{
	using Uniques = TypeList<int, bool, real, complex>;
	static_assert(findFirstIndex<int>(Uniques{}) == 0, "");
	static_assert(findFirstIndex<bool>(Uniques{}) == 1, "");
	static_assert(findFirstIndex<real>(Uniques{}) == 2, "");
	static_assert(findFirstIndex<complex>(Uniques{}) == 3, "");

	// If it is not in there, it returns past-the-end index
	static_assert(findFirstIndex<unsigned>(Uniques{}) == 4, "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(findFirstIndex<int>(NotUnique{}) == 0, "");
	static_assert(findFirstIndex<bool>(NotUnique{}) == 1, "");
	static_assert(findFirstIndex<real>(NotUnique{}) == 3, "");
	static_assert(findFirstIndex<complex>(NotUnique{}) == 4, "");
	static_assert(findFirstIndex<unsigned>(NotUnique{}) == 5, "");
}

TEST_F(TypeListFixture, isInList)
{
	using Uniques = TypeList<int, bool, real, complex>;

	static_assert(isInList<int>(Uniques{}), "");
	static_assert(isInList<bool>(Uniques{}), "");
	static_assert(isInList<real>(Uniques{}), "");
	static_assert(isInList<complex>(Uniques{}), "");

	static_assert(!isInList<unsigned>(Uniques{}), "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(isInList<int>(NotUnique{}), "");
	static_assert(isInList<bool>(NotUnique{}), "");
	static_assert(isInList<real>(NotUnique{}), "");
	static_assert(isInList<complex>(NotUnique{}), "");

	static_assert(!isInList<unsigned>(NotUnique{}), "");
}

TEST_F(TypeListFixture, containsNoDuplicates)
{
	using Uniques = TypeList<int, bool, real, complex>;
	static_assert(containsNoDuplicates(Uniques{}), "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(!containsNoDuplicates(NotUnique{}), "");
}

class Tuple_Fixture : public ::testing::Test
{
public:
};

TEST_F(Tuple_Fixture, applyToEach)
{
	auto f = [](const auto left, const auto right) {return left + right; };

	const auto left = makeTuple(1, 2);
	const auto right = makeTuple(3, 4);

	constexpr size_t size = internal::getFirstElementSize<decltype(left), decltype(right)>();
	static_assert(size == 2, "");


	auto result = internal::applyToEach(f, left, right);

	ASSERT_EQ(std::get<0>(result.data), 4);
	ASSERT_EQ(std::get<1>(result.data), 6);
}
