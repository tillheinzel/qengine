﻿/* COPYRIGHT
 *
 * file="T_fft.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Utility/Fft/Fft.h"

#include "additionalAsserts.h"

using namespace qengine;
using namespace internal;

class FFT_fixture : public ::testing::Test
{
public:

};

TEST_F(FFT_fixture, 1d)
{

	auto  x = CVec( 128, {1.0 , 1.0} );
	auto xsave = x;

	FFT fft(std::ref(x));

	fft.doFft();
	fft.doInverseFft();

	ASSERT_NEAR_V(xsave, x, 1e-14);
}


TEST_F(FFT_fixture, 2d)
{

	auto  x = CVec(16*16, { 1.0 , 1.0 });
	auto dims = std::vector<count_t>{ 16,16 };
	auto xsave = x;

	FFT fft(std::ref(x), dims);

	fft.doFft();
	fft.doInverseFft();

	ASSERT_NEAR_V(xsave, x, 1e-14);
}

TEST_F(FFT_fixture, 3d)
{

	auto  x = CVec(16 * 16*16, { 1.0 , 1.0 });
	auto dims = std::vector<count_t>{ 16,16,16 };
	auto xsave = x;

	FFT fft(std::ref(x), dims);

	fft.doFft();
	fft.doInverseFft();

	ASSERT_NEAR_V(xsave, x, 1e-14);
}

TEST_F(FFT_fixture,gaussian){
    
    int N = static_cast<int>(pow(2,7));
    real X = 10; // sampling period
    real dx = X/N;
    auto x = CVec(linspace(-X/2.0, X/2.0-dx, N));
    auto fx = exp(-x*x); // gaussian function to be transformed
    
    auto Fs = 1/dx;
    auto dk = Fs/N; 
    auto k_nyquist = Fs/2.0;
    auto k = CVec(linspace(-k_nyquist,k_nyquist-dk,N));
    
    //step 1: 'fftshift': fx =[fx1 fx2 ... fxN] -> [fxN/2 fxN/2+1... fxN fx1 ... fxN/2-1]
    CVec Fk_temp(fx.dim());
    for(int i=0;i<N/2;i++){
        Fk_temp.at(i)     = fx.at(i+N/2);
        Fk_temp.at(i+N/2) = fx.at(i);
    }
    
    // step 2: do the fft
    FFT fft(std::ref(Fk_temp));
    fft.doFft();
    
    // step 3: fftshift back
    CVec Fk(Fk_temp.dim());
    for(int i=0;i<N/2;i++){
        Fk.at(i)     = Fk_temp.at(i+N/2);
        Fk.at(i+N/2) = Fk_temp.at(i);
    }
    
    // step 4: scale appropriately
    Fk *= dx;
    
    // Analytical result:
    auto Fk_analytical = sqrt(PI)*exp(-PI*PI*k*k);
    
    // Test
    for(auto i=0u;i<Fk.dim();i++){
        ASSERT_NEAR_C(Fk_analytical.at(i), Fk.at(i), 1e-9);
        ASSERT_NEAR(0,Fk.at(0).imag(),1e-10); // Fourier transform of gaussian is purely real
    }
    
    
}
