﻿/* COPYRIGHT
 *
 * file="T_NLevel_3Level_TE.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/NLevel.h"

using namespace qengine;
using namespace second_quantized;
using namespace fock;


class T_NLevel_3Level_TE : public testing::Test {

public:
    const double tolerance = 0.01;
    const double dt = 0.001;
};



TEST_F(T_NLevel_3Level_TE, STIRAP)
{

    auto hilbertSpace = n_level::makeHilbertSpace(3u);

    auto detuning = 100000.0;
    auto rabi_frequency_max = 1000;

    auto T = 10.0;

    auto Rabi_Pulse = [=](double t) {
        return rabi_frequency_max*(t/T);
    };

    auto Rabi_Stokes = [=](double t) {
        return rabi_frequency_max-Rabi_Pulse(t);
    };

    auto HDyn = makeOperatorFunction([=](double t) {

		return makeOperator(hilbertSpace, { {  0,             0.5*Rabi_Pulse(t),  0},
			{0.5*Rabi_Pulse(t), detuning,     0.5*Rabi_Stokes(t)},
			{ 0,             0.5*Rabi_Stokes(t), 0} });
    }, 0.0);

    auto stepper = makeTimeStepper(HDyn, makeState(hilbertSpace, fock::state{ 0 }));

    ASSERT_NEAR(stepper.state().absSquare().at(0), 1, tolerance);

    for(auto i = 1; i < round(T/dt); i++)
    {
        stepper.step(dt, dt*i);
        ASSERT_NEAR(stepper.state().absSquare().at(1), 0, tolerance);
    }

    ASSERT_NEAR(stepper.state().absSquare().at(2), 1, tolerance);
}



