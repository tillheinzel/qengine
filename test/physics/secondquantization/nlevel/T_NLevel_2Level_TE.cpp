﻿/* COPYRIGHT
 *
 * file="T_NLevel_2Level_TE.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "F_NLevel_2Level.h"

using namespace qengine;
using namespace second_quantized;
using namespace fock;
using namespace std::complex_literals;


class T_NLevel_2Level_TE : public testing::Test {

public:
    const double tolerance = 1e-5;
    const double dt = 0.001;
    const double hbar = 1;
    RVec E = RVec{1.1, 2.2};
    const double omega = (E.at(1)-E.at(0))/hbar;
    const double gamma = hbar;


    F_NLevel_2Level tl = F_NLevel_2Level(E.at(0), E.at(1), omega, gamma);

};

TEST_F(T_NLevel_2Level_TE, propagate_with_H0)
{
	auto stepper = makeTimeStepper(tl.H0, makeState(tl.hilbertSpace, state{ 0 }));

    for(auto i = 1; i < 100; i++)
    {
        stepper.step(dt);
        ASSERT_NEAR(fidelity(stepper.state(), makeState(tl.hilbertSpace, state{0})), 1, tolerance);
    }

}


TEST_F(T_NLevel_2Level_TE, propagate_with_H0_dyn)
{

    auto HDyn = makeOperatorFunction([=](double /*t*/) {
            return tl.H0;
    }, dt);

    auto stepper = makeTimeStepper(HDyn, makeState(tl.hilbertSpace, state{0}));

    for(auto i = 1; i < 100; i++)
    {
        stepper.step(dt, dt*i);
        ASSERT_NEAR(fidelity(stepper.state(), makeState(tl.hilbertSpace, state{ 0 })), 1, tolerance);
    }

}



TEST_F(T_NLevel_2Level_TE, propagate_with_HDyn)
{
    auto HDyn = makeOperatorFunction([=](double t) {
		return tl.H0 + makeOperator(tl.hilbertSpace, { {0, tl.gamma*exp(1i*tl.omega*t)},
			{tl.gamma*exp(-1i * tl.omega*t), 0.0} });
    }, 0.0);

    auto stepper = makeTimeStepper(HDyn, makeState(tl.hilbertSpace, state{ 0 }));

    for(auto i = 1; i < 10000; i++)
    {
        stepper.step(dt, dt*i);
        ASSERT_NEAR(stepper.state().absSquare().at(1), tl.analyticProbabilityOfBeingInSecondLevel(dt*i), tolerance);
    }
}


TEST_F(T_NLevel_2Level_TE, propagate_forward_and_backwards_with_HDyn)
{
    auto HDyn = makeOperatorFunction([=](double t) {
		return tl.H0 + makeOperator(tl.hilbertSpace, { {0, tl.gamma*exp(1i*tl.omega*t)},
			{ tl.gamma*exp(-1i * tl.omega*t), 0.0} });
    }, 0.0);

    auto stepper = makeTimeStepper(HDyn, makeState(tl.hilbertSpace, state{0}));


    for(auto i = 1; i < 10000; i++)
    {
        stepper.step(dt, dt*i);
        ASSERT_NEAR(stepper.state().absSquare().at(1), tl.analyticProbabilityOfBeingInSecondLevel(dt*i), tolerance);
    }


    for(auto i = 9999; i > 0; i--)
    {
        stepper.step(-dt, dt*(i-1));
        ASSERT_NEAR(stepper.state().absSquare().at(1), tl.analyticProbabilityOfBeingInSecondLevel(dt*(i-1)), tolerance) << i;
    }

    ASSERT_NEAR(makeState(tl.hilbertSpace, state{0}).absSquare().at(0), stepper.state().absSquare().at(0), tolerance);

}



TEST_F(T_NLevel_2Level_TE, eigenstateRotation)
{
    for (auto j = 0u; j < tl.hilbertSpace.nLevels(); j++)
    {
        auto state = makeState(tl.hilbertSpace, fock::state{ j });

        auto T = 2.0*PI / E.at(j); // Phase period: exp(-i E0 T) = exp(-i 2*PI)
        auto dt_1000 = T*0.001;

        auto propagator = makeTimeStepper(tl.H0, state);

        for (auto i = 1u; i <= 1000; i++) {
            propagator.step(dt_1000);
        }

        ASSERT_NEAR(1.0, fidelity(state, propagator.state()), tolerance) << "Eigenstate:" << j;
        ASSERT_NEAR(0.0, std::abs(exp(-1i*E.at(j)*T) - overlap(state, propagator.state())), 4.2e-5) << "Eigenstate:" << j;
    }
}


