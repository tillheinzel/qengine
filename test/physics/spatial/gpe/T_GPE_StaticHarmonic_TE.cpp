﻿/* COPYRIGHT
 *
 * file="T_GPE_StaticHarmonic_TE.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "F_GPE_StaticHarmonic.h"


using namespace qengine;
using namespace std::complex_literals;


class T_GPE_StaticHarmonic_TE_beta0 : public F_GPE_StaticHarmonic {

public:
    const double tolerance = 1e-5;
	double defaultDt = 0.001;


	const real beta = 0.0;
	AUTO_MEMBER(H, T + V + makeGpeTerm(beta));
};


TEST_F(T_GPE_StaticHarmonic_TE_beta0, fixedTimeStepper_step_from_to)
{
	auto psi0 = makeWavefunction(H[0]);

	auto stepper = makeFixedTimeStepper(H, psi0, defaultDt);

	for (auto i = 1; i < 10; i++)
	{
		stepper.step();
		ASSERT_NEAR(fidelity(stepper.state(), psi0), 1, tolerance);
	}
}

TEST_F(T_GPE_StaticHarmonic_TE_beta0, fixedTimeStepper_cstep)
{
	auto psi0 = makeWavefunction(H[0]);

	auto stepper = makeFixedTimeStepper(H, psi0, defaultDt);

	for (auto i = 1; i < 10; i++)
	{
		stepper.cstep();
		ASSERT_NEAR(fidelity(stepper.state(), psi0), 1, tolerance);
	}
}



TEST_F(T_GPE_StaticHarmonic_TE_beta0, timeStepper_step_from_to)
{
	auto psi0 = makeWavefunction(H[0]);

	auto stepper = makeTimeStepper(H, psi0);

	for (auto i = 1; i < 10; i++)
	{
		stepper.step(defaultDt);
		ASSERT_NEAR(fidelity(stepper.state(), psi0), 1, tolerance);
	}
}

TEST_F(T_GPE_StaticHarmonic_TE_beta0, timeStepper_step_to)
{
	auto psi0 = makeWavefunction(H[0]);

	auto stepper = makeTimeStepper(H, psi0);

	for (auto i = 1; i < 10; i++)
	{
		stepper.step(defaultDt);
		ASSERT_NEAR(fidelity(stepper.state(), psi0), 1, tolerance);
	}
}


TEST_F(T_GPE_StaticHarmonic_TE_beta0, timeStepper_cstep_from_to)
{
	auto psi0 = makeWavefunction(H[0]);

	auto stepper = makeTimeStepper(H, psi0);

	for (auto i = 1; i < 10; i++)
	{
		stepper.cstep(defaultDt);
		ASSERT_NEAR(fidelity(stepper.state(), psi0), 1, tolerance);
	}
}


TEST_F(T_GPE_StaticHarmonic_TE_beta0, groundstateRotation)
{

	auto spectrum = H.makeSpectrum(0);

	auto psi = spectrum.eigenFunction(0);
	auto E = spectrum.eigenvalue(0);

	ASSERT_NEAR(E, omega/ 2, tolerance);
	ASSERT_NEAR(0.0, variance(H, psi), tolerance);
	ASSERT_NEAR(E, expectationValue(H, psi), tolerance);

	const auto duration = 2 * PI / E;

	const auto N = 1000;
	auto dt = duration / (4 * N);
	//std::cout << dt << std::endl;
	auto propagator = makeTimeStepper(H, psi);


	ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << " t: " << 0;
    ASSERT_NEAR(0.0, std::abs(1.0 - overlap(psi, propagator.state())), tolerance) << " t: " << 0;

	//auto maxDiff = 0.0;
	for (auto t = dt; t <= duration / 4; t += dt)
	{
		propagator.step(dt);

		ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance)  << " t: " << t;
        ASSERT_NEAR(0.0, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())), tolerance) << " t: " << t;
		/*
					maxDiff = std::max(maxDiff, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())));
					std::cout << maxDiff << std::endl;*/
	}
}

TEST_F(T_GPE_StaticHarmonic_TE_beta0, firststateRotation)
{
	auto spectrum = H.makeSpectrum(1);

	auto psi = spectrum.eigenFunction(1);
	const auto E = spectrum.eigenvalue(1);

	ASSERT_NEAR(E, 3*omega/ 2, 3*tolerance);
	ASSERT_NEAR(0.0, variance(H, psi), tolerance);
	ASSERT_NEAR(E, expectationValue(H, psi), tolerance);

	const auto duration = 2 * PI / E;

	const auto N = 1000;
	const auto dt = duration / (4 * N);
	//std::cout << dt << std::endl;
	auto propagator = makeTimeStepper(H, psi);


	ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << " t: " << 0;
    ASSERT_NEAR(0.0, std::abs(1.0 - overlap(psi, propagator.state())), tolerance) << " t: " << 0;

	//auto maxDiff = 0.0;
	for (auto t = dt; t <= duration / 4; t += dt)
	{
		propagator.step(dt);

		ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance)  << " t: " << t;
        ASSERT_NEAR(0.0, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())), tolerance) << " t: " << t;
		/*
					maxDiff = std::max(maxDiff, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())));
					std::cout << maxDiff << std::endl;*/
	}
}


class T_GPE_StaticHarmonic_TE_beta10 : public F_GPE_StaticHarmonic {

public:
	const double tolerance = 1e-5;
	double defaultDt = 0.001;


	const real beta = 10.0;
	AUTO_MEMBER(H, T + V + makeGpeTerm(beta));
};

TEST_F(T_GPE_StaticHarmonic_TE_beta10, groundstateRotation)
{
	auto spectrum = H.makeSpectrum(0, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING, 1e-14);

	auto psi = spectrum.eigenFunction(0);
	auto E = spectrum.eigenvalue(0);

    EXPECT_NEAR(0.0, variance(H, psi), tolerance);
    EXPECT_NEAR(E, expectationValue(H, psi), tolerance);

	const auto duration = 2 * PI / E;

	const auto N = 1000;
	const auto dt = duration / (4 * N);
	//std::cout << dt << std::endl;
	auto propagator = makeTimeStepper(H, psi);


	EXPECT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << " t: " << 0;
    EXPECT_NEAR(0.0, std::abs(1.0 - overlap(psi, propagator.state())), tolerance) << " t: " << 0;

	//auto maxDiff = 0.0;
	for (auto t = dt; t <= duration / 4; t += dt)
	{
		propagator.step(dt);

		ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << " t: " << t;
		ASSERT_NEAR(0.0, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())), 7e-7) << " t: " << t;
		/*
		maxDiff = std::max(maxDiff, std::abs(exp(-1i*E*t) - overlap(psi, propagator.state())));
		std::cout << maxDiff << std::endl;*/
	}
}

TEST_F(T_GPE_StaticHarmonic_TE_beta10, firststateRotation)
{
	auto spectrum = H.makeSpectrum(1, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING, 1e-12, 40000, { 1, 3 * 1e-4 });
	auto psi = spectrum.eigenFunction(1);
	auto E = spectrum.eigenvalue(1);

	ASSERT_NEAR(expectationValue(H, psi), spectrum.eigenvalue(1), tolerance);
	ASSERT_NEAR(variance(H, psi), 0, tolerance);
	
	//auto E2 = expectationValue(H, psi);

	const auto duration = 2 * PI / E;

	const auto N = 2000;
	const auto dt = duration / (4 * N);
	//std::cout << dt << std::endl;
	auto propagator = makeTimeStepper(H, psi);


	EXPECT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << " t: " << 0;
    EXPECT_NEAR(0.0, std::abs(1.0 - overlap(psi, propagator.state())), tolerance) << " t: " << 0;

	//auto maxval = 0.0;
	for (auto t = dt; t <= duration; t += dt)
	{
		propagator.step(dt);


		ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), 1.4e-6) << " t/T: " << t/duration;
		
        ASSERT_NEAR(0.0, std::abs(1.0 - overlap(exp(-1i*E*t)*psi, propagator.state())), tolerance) << " t/T: " << t/duration;
		//maxval = std::max(maxval, std::abs(1.0 - overlap(exp(-1i*E*t)*psi, propagator.state())));
		//std::cout << maxval << std::endl;
	}
}
