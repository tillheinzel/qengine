﻿/* COPYRIGHT
 *
 * file="T_PotentialFunction.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/OneParticle.h"

#include "additionalAsserts.h"

using namespace qengine;


class T_Spatial_PotentialFunction : public ::testing::Test{};

TEST_F(T_Spatial_PotentialFunction, singleReal)
{
	auto s = one_particle::makeHilbertSpace(-1, 1, 5);
	auto x = s.x();
	auto V = makePotentialFunction([x](real r) {return r*x * x; }, 0.0);

	for (auto t = 0.0; t < 10; t += 0.5) 
	{
		EXPECT_NEAR_V((t * x*x).vec(), V(t).vec(), 1e-16);
		//EXPECT_NEAR_V((0 * x*x).vec(), V({ 0.0 }).vec(), 1e-16);
		EXPECT_NEAR_V((t * x*x).vec(), V(RVec{ t }).vec(), 1e-16);
		EXPECT_NEAR_V((t * x*x).vec(), V(Tuple<RVec>{ RVec{t} }).vec(), 1e-16);
	}
}

TEST_F(T_Spatial_PotentialFunction, multipleReals)
{
	auto s = one_particle::makeHilbertSpace(-1, 1, 5);
	auto x = s.x();
	auto V = makePotentialFunction([x](real r, real t) {return (r+t)*x * x; }, 0.0, 0.0);

	for (real t = 0.0, r = 0.0; t < 10; t += 0.5, r+=0.5) 
	{
		EXPECT_NEAR_V(((r+t) * x*x).vec(), V(r,t).vec(), 1e-16);
		//EXPECT_NEAR_V(((r + t) * x*x).vec(), V({ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(RVec{ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(Tuple<real,real>{ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(Tuple<RVec>{ RVec{r,t} }).vec(), 1e-16);
	}
}


TEST_F(T_Spatial_PotentialFunction, callWithRVec)
{
	auto s = one_particle::makeHilbertSpace(-1, 1, 5);
	auto x = s.x();
	auto V = makePotentialFunction([x](RVec a) {return (a.at(0) + a.at(1))*x * x; }, RVec{ 0.0, 0.0 });

	for (real t = 0.0, r = 0.0; t < 10; t += 0.5, r+=0.5) 
	{
		//EXPECT_NEAR_V(((r + t) * x*x).vec(), V({ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(RVec{ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(Tuple<RVec>{ RVec{ r, t }}).vec(), 1e-16);

	}
}

TEST_F(T_Spatial_PotentialFunction, callWithComplex)
{
	auto s = one_particle::makeHilbertSpace(-1, 1, 5);
	auto x = s.x();
	auto V = makePotentialFunction([x](complex a) {return (a.real() + a.imag())*x * x; }, complex{ 0.0, 0.0 });

	for (real t = 0.0, r = 0.0; t < 10; t += 0.5, r+=0.5) 
	{
		//EXPECT_NEAR_V(((r + t) * x*x).vec(), V({ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(complex{ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(Tuple<complex>{ complex{ r, t }}).vec(), 1e-16);

	}
}

TEST_F(T_Spatial_PotentialFunction, callWithStdVec)
{
	auto s = one_particle::makeHilbertSpace(-1, 1, 5);
	auto x = s.x();
	auto V = makePotentialFunction([x](std::vector<real> a) {return (a.at(0) + a.at(1))*x * x; }, std::vector<real>{ 0.0, 0.0 });

	for (real t = 0.0, r = 0.0; t < 10; t += 0.5, r+=0.5) 
	{
		//EXPECT_NEAR_V(((r + t) * x*x).vec(), V({ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(std::vector<real>{ r,t }).vec(), 1e-16);
		EXPECT_NEAR_V(((r + t) * x*x).vec(), V(Tuple<std::vector<real>>{ std::vector<real>{ r, t }}).vec(), 1e-16);

	}
}
