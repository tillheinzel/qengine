﻿/* COPYRIGHT
 *
 * file="T_1P_DrivenHarmonic_TE.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "F_1P_DrivenHarmonic.h"

using namespace qengine;


class T_1P_DrivenHarmonic_TE : public testing::Test {
public:
	F_1P_DrivenHarmonic dh = F_1P_DrivenHarmonic();
	const double tolerance = 1e-5;
	const double dt = 0.001;

};


TEST_F(T_1P_DrivenHarmonic_TE, InitialOffset_StaticPotential)
{
	auto offset = 0.3;
	dh.setMotion_InitialOffset_StaticPotential(offset);


	auto H_dynamic = dh.T + makePotentialFunction([=](real) 
	{
		return 0.5*pow(dh.omega, 2)*pow(dh.x - offset, 2);
	}, 0.0);

	for (auto n = 0; n <= 2; n++) {

		auto propagator = makeFixedTimeStepper(H_dynamic, makeWavefunction(dh.H_static[n]), dt);

		for (auto i = 1; i < 1000; i++)
		{

			propagator.step(dt*i);

			ASSERT_NEAR(fidelity(propagator.state(), dh.getDrivenHarmonicEigenstate(n)(dt*i)), 1, tolerance);
		}
	}
}


TEST_F(T_1P_DrivenHarmonic_TE, NoOffset_OscillateWithAmplitude)
{

	const auto amplitude = 0.5;
	dh.setMotion_NoOffset_OscillateWithAmplitude(amplitude);

	auto H_dynamic = dh.T + makePotentialFunction([=](double t) {
		return 0.5*pow(dh.omega, 2)*pow(dh.x - amplitude * t, 2);
	}, 0.0);

	for (auto n = 0; n <= 2; n++) {

		auto propagator = makeFixedTimeStepper(H_dynamic, makeWavefunction(dh.H_static[n]), dt);

		for (auto i = 1; i < 1000; i++)
		{
			propagator.step(dt*i);

			ASSERT_NEAR(fidelity(propagator.state(), dh.getDrivenHarmonicEigenstate(n)(dt*i)), 1, tolerance);
		}
	}
}
