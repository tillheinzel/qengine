﻿/* COPYRIGHT
 *
 * file="T_Spatial_MultiDimensionalSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/Generics/Physics/Spatial/MultiDimensionalSpace.h"


using namespace qengine;

class T_Spatial_MultiDimensionalSpace : public ::testing::Test {};

template<count_t FullDim, count_t DimIndex>
internal::SubSpace<FullDim, DimIndex> subs(count_t n = 2)
{
	return internal::SubSpace<FullDim, DimIndex>(-1, 1, n, 0.5);
}

TEST_F(T_Spatial_MultiDimensionalSpace, index)
{
	auto s = internal::MultiDimensionalSpace<2, 1, 2>(std::make_tuple( subs<2,1>(),subs<2,2>() ));

	EXPECT_EQ(0u, s.linearIndex({ 0, 0 }));
	EXPECT_EQ(1u, s.linearIndex({ 0, 1 }));
	EXPECT_EQ(2u, s.linearIndex({ 1, 0 }));
	EXPECT_EQ(3u, s.linearIndex({ 1, 1 }));

	auto s2 = internal::MultiDimensionalSpace<3, 1, 2, 3>(std::make_tuple( subs<3,1>(4),subs<3,2>(3),subs<3,3>(2) ));


	EXPECT_EQ(0u, s2.linearIndex({ 0, 0, 0 }));
	EXPECT_EQ(1u, s2.linearIndex({ 0, 0, 1 }));
	EXPECT_EQ(2u, s2.linearIndex({ 0, 1, 0 }));
	EXPECT_EQ(3u, s2.linearIndex({ 0, 1, 1 }));
	EXPECT_EQ(4u, s2.linearIndex({ 0, 2, 0 }));
	EXPECT_EQ(5u, s2.linearIndex({ 0, 2, 1 }));
	EXPECT_EQ(6u, s2.linearIndex({ 1, 0, 0 }));
	EXPECT_EQ(7u, s2.linearIndex({ 1, 0, 1 }));
	EXPECT_EQ(8u, s2.linearIndex({ 1, 1, 0 }));
	EXPECT_EQ(9u, s2.linearIndex({ 1, 1, 1 }));
	EXPECT_EQ(10u, s2.linearIndex({ 1, 2, 0 }));
	EXPECT_EQ(11u, s2.linearIndex({ 1, 2, 1 }));
	EXPECT_EQ(12u, s2.linearIndex({ 2, 0, 0 }));
	EXPECT_EQ(13u, s2.linearIndex({ 2, 0, 1 }));
	EXPECT_EQ(14u, s2.linearIndex({ 2, 1, 0 }));
	EXPECT_EQ(15u, s2.linearIndex({ 2, 1, 1 }));
	EXPECT_EQ(16u, s2.linearIndex({ 2, 2, 0 }));
	EXPECT_EQ(17u, s2.linearIndex({ 2, 2, 1 }));
	EXPECT_EQ(18u, s2.linearIndex({ 3, 0, 0 }));
	EXPECT_EQ(19u, s2.linearIndex({ 3, 0, 1 }));
	EXPECT_EQ(20u, s2.linearIndex({ 3, 1, 0 }));
	EXPECT_EQ(21u, s2.linearIndex({ 3, 1, 1 }));
	EXPECT_EQ(22u, s2.linearIndex({ 3, 2, 0 }));
	EXPECT_EQ(23u, s2.linearIndex({ 3, 2, 1 }));
}
