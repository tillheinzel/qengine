﻿/* COPYRIGHT
 *
 * file="T_SqSparseMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"
#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "additionalAsserts.h"
#include "HarmonicAnalyticResults.h"

#include "SqMat_Fixture.h"


using namespace qengine::internal;
using namespace std::complex_literals;

class SqSparseMat_Fixture : public SqMat_Fixture
{
	public:
	arma::sp_mat makeHmat() const
	{
		auto a = 30.0 / 12.0;
		auto b = -16.0 / 12.0;
		auto c = 1.0 / 12.0;

		auto scale = 0.5 / (dx*dx);

		auto kinetic = arma::mat(defaultDim, defaultDim, arma::fill::zeros);
		kinetic.diag(0) += a*scale;
		kinetic.diag(1) += b*scale;
		kinetic.diag(-1) += b*scale;
		kinetic.diag(2) += c*scale;
		kinetic.diag(-2) += c*scale;

		auto potential = 0.5*omega*omega*(xArray - x0) * (xArray - x0);
		auto Hmat = kinetic;
		Hmat.diag() += potential._vec();
		return arma::sp_mat(Hmat);
	}

	SqSparseMat<real> H{ Sp_RMat(makeHmat()) };
	SqSparseMat<complex> iH{ Sp_CMat(arma::sp_cx_mat(arma::sp_mat(defaultDim, defaultDim), makeHmat())) };
	SqSparseMat<complex> cH{ Sp_CMat(arma::sp_cx_mat(makeHmat(), arma::sp_mat(defaultDim, defaultDim))) };

    SqSparseMat<real> sp_mr22{ Sp_RMat{RMat(2, 2, 1) }};
    SqSparseMat<real> sp_mr22original= sp_mr22;

    SqSparseMat<complex> sp_mc22{ Sp_CMat{CMat(2, 2, {1,1}) }};
    SqSparseMat<complex> sp_mc22original = sp_mc22;

    complex one{ 1, 1 };
    complex zero{ 0, 0 };
};


TEST_F(SqSparseMat_Fixture,constructors){

    SparseMatrix<real> sparse(3,3);
    ASSERT_NO_THROW(SqSparseMat<real> t(3));
    ASSERT_NO_THROW(SqSparseMat<real> t(sparse));
    ASSERT_NO_THROW(SqSparseMat<real> t(SparseMatrix<real>(3,3)));
}

TEST_F(SqSparseMat_Fixture, traits)
{
    static_assert(is_SqMat_v<SqSparseMat>, "SqSparseMat does not fulfill is_matrix!");
}

TEST_F(SqSparseMat_Fixture, dimensions)
{
	EXPECT_EQ(defaultDim, H.dim());
	EXPECT_EQ(defaultDim, iH.dim());
	EXPECT_EQ(defaultDim, cH.dim());
}

TEST_F(SqSparseMat_Fixture, isHermitian)
{
	EXPECT_TRUE(H.isHermitian());
	EXPECT_FALSE(iH.isHermitian());

	EXPECT_TRUE(cH.isHermitian());
}

TEST_F(SqSparseMat_Fixture, multiplyWithState)
{
    EXPECT_NEAR(omega*0.5, std::real(cdot(harm0, H*harm0))*dx, 2e-7);
    EXPECT_NEAR(0, std::imag(cdot(harm0, H*harm0))*dx, 1e-7);

    EXPECT_NEAR(0, std::real(cdot(harm0, iH*harm0))*dx, 1e-7);
    EXPECT_NEAR(omega*0.5, std::imag(cdot(harm0, iH*harm0))*dx, 2e-7);

    auto psi = (harm0 + 1i*harm1) / std::sqrt(2);
    EXPECT_NEAR(omega, std::real(cdot(psi, H*psi))*dx, 8e-7) << "psi = harm0+i*harm1";
    EXPECT_NEAR(0, std::imag(cdot(psi, H*psi))*dx, 1e-7) << "psi = harm0+i*harm1";

    EXPECT_NEAR(0, std::real(cdot(psi, iH*psi))*dx, 1e-7) << "psi = harm0+i*harm1";
    EXPECT_NEAR(omega, std::imag(cdot(psi, iH*psi))*dx, 8e-7) << "psi = harm0+i*harm1";
}

TEST_F(SqSparseMat_Fixture, eigHerm)
{
    count_t largestEigenstate = 3;


    //arma::sp_mat A = arma::sprandu<arma::sp_mat>(dim, dim, 0.1);
    //arma::sp_mat B = A.t()*A;

    //arma::vec eigval;
    //arma::mat eigvec;

    ////std::cout << B;
    ////std::cout << arma::mat(B);

    //arma::eigs_sym(
    //	eigval,
    //	eigvec,
    //    B,
    //	largestEigenstate,
    //	"sa");
    //

	auto spectrum = getSpectrum_herm(H, largestEigenstate + 20);

	const auto& vals = spectrum.eigenvalues();
	const auto& vecs = spectrum.eigenvectors();

    //ASSERT_EQ(largestEigenstate + 1, vals.size());
    //ASSERT_EQ(largestEigenstate + 1, vecs.n_cols());

    arma::vec expectedvals = omega*(0.5 + arma::linspace(0, static_cast<double>(largestEigenstate), largestEigenstate + 1));

    for (auto i = 0u; i <= largestEigenstate; ++i)
    {
        // todo: find out why this fails for tolerance = 1e-6
        EXPECT_NEAR(expectedvals.at(i), vals.at(i), 5e-4) << "x val" << i;

        auto state = CVec(vecs.col(i));
        EXPECT_NEAR(vals.at(i), std::real(dot(state, H*state)), 1e-7) << "x vec" << i;
    }
}

TEST_F(SqSparseMat_Fixture, exponentiate)
{
////     note: arma::expmat is not defined explicitly for sparse matrices.. we have to use these annoying eyebleeding workarounds
    arma::Mat<real> temp1_real = arma::Mat<real>(H.mat().mat());
    temp1_real = arma::expmat(temp1_real);
    SqSparseMat<real> temp2_real{Sp_RMat{temp1_real}};
    auto temp_harm0 = harm0;
    temp2_real.multiplyInPlace(temp_harm0);
    EXPECT_NEAR_V(exp(H)*harm0, CVec(temp_harm0), tolerance);


    // Complex
    arma::Mat<complex> temp1_complex = arma::Mat<complex>((1i*H.mat()).mat());
    temp1_complex = arma::expmat(temp1_complex);
    SqSparseMat<complex> temp2_complex{Sp_CMat{Sp_RMat(arma::real(temp1_complex)),Sp_RMat(arma::imag(temp1_complex))}};
    temp_harm0 = harm0;
    temp2_complex.multiplyInPlace(temp_harm0);
    EXPECT_NEAR_V(exp(iH)*harm0, CVec(temp_harm0), tolerance);
}

//TEST_F(SqSparseMat_Fixture,pow){
//    auto pow1 = pow(sp_mr22,1);
//    auto pow2 = pow(sp_mr22,2);
//    auto pow3 = pow(sp_mr22,3);

//    EXPECT_EQ(1,pow1.mat().at(0,0)); EXPECT_EQ(1,pow1.mat().at(0,1));
//    EXPECT_EQ(1,pow1.mat().at(1,0)); EXPECT_EQ(1,pow1.mat().at(1,1));
//    EXPECT_EQ(2,pow2.mat().at(0,0)); EXPECT_EQ(2,pow2.mat().at(0,1));
//    EXPECT_EQ(2,pow2.mat().at(1,0)); EXPECT_EQ(2,pow2.mat().at(1,1));
//    EXPECT_EQ(4,pow3.mat().at(0,0)); EXPECT_EQ(4,pow3.mat().at(0,1));
//    EXPECT_EQ(4,pow3.mat().at(1,0)); EXPECT_EQ(4,pow3.mat().at(1,1));

//    auto cpow1 = pow(sp_mc22,1);
//    auto cpow2 = pow(sp_mc22,2);
//    auto cpow3 = pow(sp_mc22,3);

//    EXPECT_EQ(one,(complex)cpow1.mat().at(0,0)); EXPECT_EQ(one,(complex)cpow1.mat().at(0,1));
//    EXPECT_EQ(one,(complex)cpow1.mat().at(1,0)); EXPECT_EQ(one,(complex)cpow1.mat().at(1,1));
//    EXPECT_EQ(complex(0,4),(complex)cpow2.mat().at(0,0)); EXPECT_EQ(complex(0,4),(complex)cpow2.mat().at(0,1));
//    EXPECT_EQ(complex(0,4),(complex)cpow2.mat().at(1,0)); EXPECT_EQ(complex(0,4),(complex)cpow2.mat().at(1,1));
//    EXPECT_EQ(complex(-8,8),(complex)cpow3.mat().at(0,0)); EXPECT_EQ(complex(-8,8),(complex)cpow3.mat().at(0,1));
//    EXPECT_EQ(complex(-8,8),(complex)cpow3.mat().at(1,0)); EXPECT_EQ(complex(-8,8),(complex)cpow3.mat().at(1,1));
//}


TEST_F(SqSparseMat_Fixture,diagonaladdition){
//    SparseMatrix<real> b(arma::Mat<real>(3,3,arma::fill::eye));

    auto H0 = H;
    H += SqDiagMat<real>(RVec(defaultDim,1.0));

    for(auto i=0u;i<defaultDim;++i){
        for(auto j=0u;j<defaultDim;++j){
            if(i==j)ASSERT_EQ(H0.mat().at(i,j)+1.0,H.mat().at(i,j));
            else    ASSERT_EQ(H0.mat().at(i,j),H.mat().at(i,j));
        }
    }


    auto iH0 = iH;
    iH += SqDiagMat<complex>(CVec(defaultDim,1i));

    for(auto i=0u;i<defaultDim;++i){
        for(auto j=0u;j<defaultDim;++j){
            complex lhs = iH0.mat().mat().at(i,j);
            complex rhs = iH.mat().mat().at(i,j);
            if(i==j) lhs += 1i;
            ASSERT_NEAR_C(lhs,rhs,1e-10);
        }
    }

    auto cH0 = cH;
    cH += SqDiagMat<complex>(CVec(defaultDim,complex{1.0,1.0}));

    for(auto i=0u;i<defaultDim;++i){
        for(auto j=0u;j<defaultDim;++j){
            complex lhs = cH0.mat().mat().at(i,j);
            complex rhs = cH.mat().mat().at(i,j);
            if(i==j) lhs += complex{1.0,1.0};
            ASSERT_NEAR_C(lhs,rhs,1e-10);
        }
    }
}



TEST_F(SqSparseMat_Fixture, inplaceSum)
{

    sp_mr22 += sp_mr22;
    EXPECT_EQ(2, sp_mr22.mat().at(0, 0));
    EXPECT_EQ(2, sp_mr22.mat().at(0, 1));
    EXPECT_EQ(2, sp_mr22.mat().at(1, 0));
    EXPECT_EQ(2, sp_mr22.mat().at(1, 1));
    sp_mr22 = sp_mr22original;

    sp_mc22 += sp_mr22;
    EXPECT_EQ(complex(2,1),(complex) sp_mc22.mat().at(0, 0));
    EXPECT_EQ(complex(2,1),(complex) sp_mc22.mat().at(0, 1));
    EXPECT_EQ(complex(2,1),(complex) sp_mc22.mat().at(1, 0));
    EXPECT_EQ(complex(2,1),(complex) sp_mc22.mat().at(1, 1));
    sp_mc22 = sp_mc22original;

    sp_mc22 += sp_mc22;
    EXPECT_EQ(2.0*one,(complex) sp_mc22.mat().at(0, 0));
    EXPECT_EQ(2.0*one,(complex) sp_mc22.mat().at(1, 0));
    EXPECT_EQ(2.0*one,(complex) sp_mc22.mat().at(0, 1));
    EXPECT_EQ(2.0*one,(complex) sp_mc22.mat().at(1, 1));
    sp_mc22 = sp_mc22original;
}


TEST_F(SqSparseMat_Fixture, inplaceSubtract) {


    sp_mr22 -= sp_mr22;
    EXPECT_EQ(0, sp_mr22.mat().at(0, 0));
    EXPECT_EQ(0, sp_mr22.mat().at(0, 1));
    EXPECT_EQ(0, sp_mr22.mat().at(1, 0));
    EXPECT_EQ(0, sp_mr22.mat().at(1, 1));
    sp_mr22 = sp_mr22original;

    sp_mc22 -= sp_mr22;
    EXPECT_EQ(complex(0,1),(complex) sp_mc22.mat().at(0, 0));
    EXPECT_EQ(complex(0,1),(complex) sp_mc22.mat().at(0, 1));
    EXPECT_EQ(complex(0,1),(complex) sp_mc22.mat().at(1, 0));
    EXPECT_EQ(complex(0,1),(complex) sp_mc22.mat().at(1, 1));
    sp_mc22 = sp_mc22original;

    sp_mc22 -= sp_mc22;
    EXPECT_EQ(zero,(complex) sp_mc22.mat().at(0, 0));
    EXPECT_EQ(zero,(complex) sp_mc22.mat().at(1, 0));
    EXPECT_EQ(zero,(complex) sp_mc22.mat().at(0, 1));
    EXPECT_EQ(zero,(complex) sp_mc22.mat().at(1, 1));
    sp_mc22 = sp_mc22original;
}

TEST_F(SqSparseMat_Fixture,inplaceScalarDivision){

    // scalar division
    sp_mr22 /=2.0;
    EXPECT_EQ(0.5,sp_mr22.mat().at(0,0));
    EXPECT_EQ(0.5,sp_mr22.mat().at(0,1));
    EXPECT_EQ(0.5,sp_mr22.mat().at(1,0));
    EXPECT_EQ(0.5,sp_mr22.mat().at(1,1));
    sp_mr22 = sp_mr22original;

    sp_mc22 /=2.0;
    EXPECT_EQ(0.5*one,(complex)sp_mc22.mat().at(0,0));
    EXPECT_EQ(0.5*one,(complex)sp_mc22.mat().at(1,0));
    EXPECT_EQ(0.5*one,(complex)sp_mc22.mat().at(0,1));
    EXPECT_EQ(0.5*one,(complex)sp_mc22.mat().at(1,1));
    sp_mc22 = sp_mc22original;

    sp_mc22 /=one;
    EXPECT_EQ(complex(1,0),(complex)sp_mc22.mat().at(0,0));
    EXPECT_EQ(complex(1,0),(complex)sp_mc22.mat().at(1,0));
    EXPECT_EQ(complex(1,0),(complex)sp_mc22.mat().at(0,1));
    EXPECT_EQ(complex(1,0),(complex)sp_mc22.mat().at(1,1));
    sp_mc22 = sp_mc22original;
}


TEST_F(SqSparseMat_Fixture, negate) {

    // negation
    EXPECT_EQ(-1, -sp_mr22.mat().at(0, 0));
    EXPECT_EQ(-1, -sp_mr22.mat().at(0, 1));
    EXPECT_EQ(-1, -sp_mr22.mat().at(1, 0));
    EXPECT_EQ(-1, -sp_mr22.mat().at(1, 1));

    EXPECT_EQ(-1.0*one, -(complex)sp_mc22.mat().at(0, 0));
    EXPECT_EQ(-1.0*one, -(complex)sp_mc22.mat().at(0, 1));
    EXPECT_EQ(-1.0*one, -(complex)sp_mc22.mat().at(1, 0));
    EXPECT_EQ(-1.0*one, -(complex)sp_mc22.mat().at(1, 1));
}


TEST_F(SqSparseMat_Fixture, matrixAddition){


    ////// addition
    // real+real
    EXPECT_EQ(2,(sp_mr22+sp_mr22).mat().at(0,0));EXPECT_EQ(2,(sp_mr22+sp_mr22).mat().at(0,1));
    EXPECT_EQ(2,(sp_mr22+sp_mr22).mat().at(1,0));EXPECT_EQ(2,(sp_mr22+sp_mr22).mat().at(1,1));

    // complex+real
    EXPECT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).mat().at(0,0)); EXPECT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).mat().at(0,1));
    EXPECT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).mat().at(1,0)); EXPECT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).mat().at(1,1));

    // real+complex
    EXPECT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).mat().at(0,0)); EXPECT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).mat().at(0,1));
    EXPECT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).mat().at(1,0)); EXPECT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).mat().at(1,1));

    // comple+complex
    EXPECT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).mat().at(0,0)); EXPECT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).mat().at(0,1));
    EXPECT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).mat().at(1,0)); EXPECT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).mat().at(1,1));

}

TEST_F(SqSparseMat_Fixture, matrixSubtraction) {

    ////// subtraction
    // real-real
    EXPECT_EQ(0, (sp_mr22 - sp_mr22).mat().at(0, 0)); EXPECT_EQ(0, (sp_mr22 - sp_mr22).mat().at(0, 1));
    EXPECT_EQ(0, (sp_mr22 - sp_mr22).mat().at(1, 0)); EXPECT_EQ(0, (sp_mr22 - sp_mr22).mat().at(1, 1));

    // complex-real
    EXPECT_EQ(complex(0, 1), (complex)(sp_mc22 - sp_mr22).mat().at(0, 0)); EXPECT_EQ(complex(0, 1), (complex)(sp_mc22 - sp_mr22).mat().at(0, 1));
    EXPECT_EQ(complex(0, 1), (complex)(sp_mc22 - sp_mr22).mat().at(1, 0)); EXPECT_EQ(complex(0, 1), (complex)(sp_mc22 - sp_mr22).mat().at(1, 1));

    // real-complex
    EXPECT_EQ(complex(0, -1), (complex)(sp_mr22 - sp_mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, -1), (complex)(sp_mr22 - sp_mc22).mat().at(0, 1));
    EXPECT_EQ(complex(0, -1), (complex)(sp_mr22 - sp_mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, -1), (complex)(sp_mr22 - sp_mc22).mat().at(1, 1));

    // complex+complex
    EXPECT_EQ(zero, (complex)(sp_mc22 - sp_mc22).mat().at(0, 0)); EXPECT_EQ(zero, (complex)(sp_mc22 - sp_mc22).mat().at(0, 1));
    EXPECT_EQ(zero, (complex)(sp_mc22 - sp_mc22).mat().at(1, 0)); EXPECT_EQ(zero, (complex)(sp_mc22 - sp_mc22).mat().at(1, 1));

}


TEST_F(SqSparseMat_Fixture, matrixMultiplication) {


    ///// multiplication
    // real*real
    EXPECT_EQ(2.0, (sp_mr22*sp_mr22).mat().at(0, 0)); EXPECT_EQ(2.0, (sp_mr22*sp_mr22).mat().at(0, 1));
    EXPECT_EQ(2.0, (sp_mr22*sp_mr22).mat().at(1, 0)); EXPECT_EQ(2.0, (sp_mr22*sp_mr22).mat().at(1, 1));

    // real*complex
    EXPECT_EQ(complex(2, 2), (complex)(sp_mr22*sp_mc22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (complex)(sp_mr22*sp_mc22).mat().at(0, 1));
    EXPECT_EQ(complex(2, 2), (complex)(sp_mr22*sp_mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (complex)(sp_mr22*sp_mc22).mat().at(1, 1));

    // complex*real
    EXPECT_EQ(complex(2, 2), (complex)(sp_mc22*sp_mr22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (complex)(sp_mc22*sp_mr22).mat().at(0, 1));
    EXPECT_EQ(complex(2, 2), (complex)(sp_mc22*sp_mr22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (complex)(sp_mc22*sp_mr22).mat().at(1, 1));

    // complex*complex
    EXPECT_EQ(complex(0, 4), (complex)(sp_mc22*sp_mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, 4), (complex)(sp_mc22*sp_mc22).mat().at(0, 1));
    EXPECT_EQ(complex(0, 4), (complex)(sp_mc22*sp_mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, 4), (complex)(sp_mc22*sp_mc22).mat().at(1, 1));
}


TEST_F(SqSparseMat_Fixture, scalarMultiplication) {

    /// SCALAR MULTIPLICATION

    // real scalar * real mat
    EXPECT_EQ(2, (2.0*sp_mr22).mat().at(0, 0)); EXPECT_EQ(2, (2.0*sp_mr22).mat().at(0, 1));
    EXPECT_EQ(2, (2.0*sp_mr22).mat().at(1, 0)); EXPECT_EQ(2, (2.0*sp_mr22).mat().at(1, 1));

    // real scalar * complex mat
    EXPECT_EQ(complex(2, 2), (complex)(2.0*sp_mc22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (complex)(2.0*sp_mc22).mat().at(0, 1));
    EXPECT_EQ(complex(2, 2), (complex)(2.0*sp_mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (complex)(2.0*sp_mc22).mat().at(1, 1));

    // complex scalar * real mat
    EXPECT_EQ(complex(2, 2), (complex)(complex(2, 2)*sp_mr22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (complex)(complex(2, 2)*sp_mr22).mat().at(0, 1));
    EXPECT_EQ(complex(2, 2), (complex)(complex(2, 2)*sp_mr22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (complex)(complex(2, 2)*sp_mr22).mat().at(1, 1));

    // complex scalar * complex mat
    EXPECT_EQ(complex(0, 4), (complex)(complex(2, 2)*sp_mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, 4),(complex) (complex(2, 2)*sp_mc22).mat().at(0, 1));
    EXPECT_EQ(complex(0, 4), (complex)(complex(2, 2)*sp_mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, 4),(complex) (complex(2, 2)*sp_mc22).mat().at(1, 1));

}

TEST_F(SqSparseMat_Fixture, scalarDivision) {

    /// SCALAR DIVISION
    // real mat / real scalar
    EXPECT_EQ(0.5, (sp_mr22 / 2.0).mat().at(0, 0)); EXPECT_EQ(0.5, (complex)(sp_mr22 / 2.0).mat().at(0, 1));
    EXPECT_EQ(0.5, (sp_mr22 / 2.0).mat().at(1, 0)); EXPECT_EQ(0.5, (complex)(sp_mr22 / 2.0).mat().at(1, 1));

    // real mat / complex scalar
    EXPECT_EQ(complex(0.25, -0.25), (complex)(sp_mr22 / complex(2, 2)).mat().at(0, 0)); EXPECT_EQ(complex(0.25, -0.25), (complex)(sp_mr22 / complex(2, 2)).mat().at(0, 1));
    EXPECT_EQ(complex(0.25, -0.25), (complex)(sp_mr22 / complex(2, 2)).mat().at(1, 0)); EXPECT_EQ(complex(0.25, -0.25), (complex)(sp_mr22 / complex(2, 2)).mat().at(1, 1));

    // complex mat / real scalar
    EXPECT_EQ(complex(0.5, 0.5), (complex)(sp_mc22 / 2.0).mat().at(0, 0)); EXPECT_EQ(complex(0.5, 0.5), (complex)(sp_mc22 / 2.0).mat().at(0, 1));
    EXPECT_EQ(complex(0.5, 0.5), (complex)(sp_mc22 / 2.0).mat().at(1, 0)); EXPECT_EQ(complex(0.5, 0.5), (complex)(sp_mc22 / 2.0).mat().at(1, 1));

    // complex mat/complex scalar
    EXPECT_EQ(complex(0.5, 0), (complex)(sp_mc22 / complex(2, 2)).mat().at(0, 0)); EXPECT_EQ(complex(0.5, 0), (complex)(sp_mc22 / complex(2, 2)).mat().at(0, 1));
    EXPECT_EQ(complex(0.5, 0), (complex)(sp_mc22 / complex(2, 2)).mat().at(1, 0)); EXPECT_EQ(complex(0.5, 0), (complex)(sp_mc22 / complex(2, 2)).mat().at(1, 1));
}


TEST_F(SqSparseMat_Fixture, kron)
{
	const auto dim = 3;
	const auto density = 0.5;

	const auto Ar = SqSparseMat<real>(SparseMatrix<real>(arma::sprandn(dim, dim, density)));
	const auto Br = SqSparseMat<real>(SparseMatrix<real>(arma::sprandn(dim, dim, density)));

	const auto Ac = SqSparseMat<complex>(SparseMatrix<complex>(arma::sp_cx_mat(arma::sprandn(dim, dim, density), arma::sprandn(dim, dim, density))));
	const auto Bc = SqSparseMat<complex>(SparseMatrix<complex>(arma::sp_cx_mat(arma::sprandn(dim, dim, density), arma::sprandn(dim, dim, density))));

	const auto Crr = SqSparseMat<real>(SparseMatrix<real>(arma::sp_mat(arma::mat(arma::kron(arma::mat(Ar.mat().mat()), arma::mat(Br.mat().mat()))))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = SqSparseMat<complex>(SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::mat(Ar.mat().mat()), arma::cx_mat(Bc.mat().mat()))))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = SqSparseMat<complex>(SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat().mat()), arma::mat(Br.mat().mat()))))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = SqSparseMat<complex>(SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat().mat()), arma::cx_mat(Bc.mat().mat()))))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}
