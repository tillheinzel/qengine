﻿/* COPYRIGHT
 *
 * file="T_SparseMatrix.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/SparseMatrix.h"
#include "src/Utility/LinearAlgebra/Matrix.h"

#include "additionalAsserts.h"

using namespace qengine;
using namespace internal;
using namespace std::complex_literals;

class SparseMatrix_Fixture : public ::testing::Test {
public:
    SparseMatrix_Fixture():one(1.0,1.0),zero(0.0,0.0){};

    complex one;
    complex zero;

};

TEST_F(SparseMatrix_Fixture,constructors){
    std::vector<real>  rvec = {1,2,3,4,5,6};
    std::vector<complex> cvec = {{1.0,1.0},{2.0,2.0},{3.0,3.0},{4.0,4.0},{5.0,5.0},{6.0,6.0}};

    SparseMatrix<real> mr32(3, 2); // real 3x2 matrix
    SparseMatrix<complex> mc46(4, 6); // complex 4x6 matrix

    ASSERT_NO_THROW(SparseMatrix<real>(3,3));
    ASSERT_NO_THROW(SparseMatrix<complex>(3,3));


    ASSERT_NO_THROW(SparseMatrix<real>(3,2));
    ASSERT_NO_THROW(SparseMatrix<complex>(1,2));

    // copy constructors from arma mats
    arma::Mat<real> mat_real(3,3,arma::fill::eye);
    ASSERT_NO_THROW(SparseMatrix<real> a(mat_real));

    arma::Mat<complex> mat_complex(3,3,arma::fill::eye);
    ASSERT_NO_THROW(SparseMatrix<complex> a(mat_complex));

    arma::SpMat<real> sp_mat_real(mat_real);
    ASSERT_NO_THROW(SparseMatrix<real> a(sp_mat_real));

    arma::SpMat<complex> sp_mat_complex(mat_complex);
    ASSERT_NO_THROW(SparseMatrix<complex> a(sp_mat_complex));

    // move constructors for arma mats
	ASSERT_NO_THROW(SparseMatrix<real> a{ arma::Mat<real>(3,3,arma::fill::eye) });
	ASSERT_NO_THROW(SparseMatrix<complex> a{ arma::Mat<complex>(3,3,arma::fill::eye) });
	ASSERT_NO_THROW(SparseMatrix<real> a{ arma::SpMat<real>(mat_real) });
	ASSERT_NO_THROW(SparseMatrix<complex> a{ arma::SpMat<complex>(mat_complex) });
}

TEST_F(SparseMatrix_Fixture,mathoperators){



    Matrix<real> mr22(2, 2, 1); // real 2x2 matrix of ones
    SparseMatrix<real> sp_mr22(mr22);
    auto sp_mr22original = sp_mr22;

    SparseMatrix<complex> sp_mc22(sp_mr22,sp_mr22);
    auto sp_mc22original = sp_mc22;

    // addition
    sp_mr22+=sp_mr22;
    ASSERT_EQ(2,sp_mr22.at(0,0));
    ASSERT_EQ(2,sp_mr22.at(0,1));
    ASSERT_EQ(2,sp_mr22.at(1,0));
    ASSERT_EQ(2,sp_mr22.at(1,1));
    sp_mr22=sp_mr22original;


    sp_mc22+=sp_mr22;
    ASSERT_EQ(one+1.0,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(one+1.0,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(one+1.0,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(one+1.0,(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;


    sp_mc22+=sp_mc22;
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(1,1));

    sp_mc22 = sp_mc22original;

    // subtraction
    sp_mr22-=sp_mr22;
    ASSERT_EQ(0,sp_mr22.at(0,0));
    ASSERT_EQ(0,sp_mr22.at(0,1));
    ASSERT_EQ(0,sp_mr22.at(1,0));
    ASSERT_EQ(0,sp_mr22.at(1,1));
    sp_mr22=sp_mr22original;


    sp_mc22-=sp_mr22;
    ASSERT_EQ(one-1.0,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(one-1.0,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(one-1.0,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(one-1.0,(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;


    sp_mc22-=sp_mc22;
    ASSERT_EQ(zero,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(zero,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(zero,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(zero,(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;

    // scalar multiplication
    sp_mr22 *=2.0;
    ASSERT_EQ(2,sp_mr22.at(0,0));
    ASSERT_EQ(2,sp_mr22.at(0,1));
    ASSERT_EQ(2,sp_mr22.at(1,0));
    ASSERT_EQ(2,sp_mr22.at(1,1));
    sp_mr22 = sp_mr22original;

    sp_mc22 *=2.0;
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(2.0*one,(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;

    sp_mc22 *=one;
    ASSERT_EQ(complex(0,2),(complex)sp_mc22.at(0,0));
    ASSERT_EQ(complex(0,2),(complex)sp_mc22.at(1,0));
    ASSERT_EQ(complex(0,2),(complex)sp_mc22.at(0,1));
    ASSERT_EQ(complex(0,2),(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;

    // scalar division
    sp_mr22 /=2.0;
    ASSERT_EQ(0.5,sp_mr22.at(0,0));
    ASSERT_EQ(0.5,sp_mr22.at(0,1));
    ASSERT_EQ(0.5,sp_mr22.at(1,0));
    ASSERT_EQ(0.5,sp_mr22.at(1,1));
    sp_mr22 = sp_mr22original;

    sp_mc22 /=2.0;
    ASSERT_EQ(0.5*one,(complex)sp_mc22.at(0,0));
    ASSERT_EQ(0.5*one,(complex)sp_mc22.at(1,0));
    ASSERT_EQ(0.5*one,(complex)sp_mc22.at(0,1));
    ASSERT_EQ(0.5*one,(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;

    sp_mc22 /=one;
    ASSERT_EQ(complex(1,0),(complex)sp_mc22.at(0,0));
    ASSERT_EQ(complex(1,0),(complex)sp_mc22.at(1,0));
    ASSERT_EQ(complex(1,0),(complex)sp_mc22.at(0,1));
    ASSERT_EQ(complex(1,0),(complex)sp_mc22.at(1,1));
    sp_mc22 = sp_mc22original;


    // negation
    ASSERT_EQ(-1,-sp_mr22.at(0,0));
    ASSERT_EQ(-1,-sp_mr22.at(0,1));
    ASSERT_EQ(-1,-sp_mr22.at(1,0));
    ASSERT_EQ(-1,-sp_mr22.at(1,1));

    ASSERT_EQ(-1.0*one,-(complex)sp_mc22.at(0,0));
    ASSERT_EQ(-1.0*one,-(complex)sp_mc22.at(0,1));
    ASSERT_EQ(-1.0*one,-(complex)sp_mc22.at(1,0));
    ASSERT_EQ(-1.0*one,-(complex)sp_mc22.at(1,1));

}
TEST_F(SparseMatrix_Fixture,dataaccess)
{
    SparseMatrix<real> mr33(arma::Mat<real>(3,3,arma::fill::eye));

    mr33.at(1,2) = 5;

    ASSERT_EQ(1,mr33.at(0,0));
    ASSERT_EQ(1,mr33.at(1,1));
    ASSERT_EQ(1,mr33.at(2,2));
    ASSERT_EQ(0,mr33.at(0,1));
    ASSERT_EQ(5,mr33.at(1,2));

    real b = mr33.at(1,2);
    ASSERT_EQ(5,b);
}

TEST_F(SparseMatrix_Fixture,misc){

    SparseMatrix<real> mr32(3,2); // real 3x2 matrix
    SparseMatrix<complex> mc46(4,6); // complex 4x6 matrix

    ASSERT_EQ(3u,mr32.n_rows());
    ASSERT_EQ(2u,mr32.n_cols());
    ASSERT_EQ(3u*2u,mr32.n_elem());

    ASSERT_EQ(4u,mc46.n_rows());
    ASSERT_EQ(6u,mc46.n_cols());
    ASSERT_EQ(4u*6u,mc46.n_elem());

    arma::mat kinetic = arma::mat(3, 3, arma::fill::ones);
    SparseMatrix<real> spr(kinetic);

    //    1i*spr;

    arma::sp_cx_mat a(spr.mat(),0*spr.mat());
    1i*a;
    SparseMatrix<complex> A(a);
//    1i*A;
    //    arma::conv_to<arma::sp_cx_mat>::from(spr.mat());

    //    std::cout << A;
}

TEST_F(SparseMatrix_Fixture,freeMatrixMatrixOperations){

    RMat mr22(2,2,1);
    CMat mc22(2,2,one);

    Sp_RMat sp_mr22(mr22);
    Sp_CMat sp_mc22(mc22);

    ////// addition
    // real+real
    ASSERT_EQ(2,(sp_mr22+sp_mr22).at(0,0));ASSERT_EQ(2,(sp_mr22+sp_mr22).at(0,1));
    ASSERT_EQ(2,(sp_mr22+sp_mr22).at(1,0));ASSERT_EQ(2,(sp_mr22+sp_mr22).at(1,1));


    // complex+real
    ASSERT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).at(0,0)); ASSERT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).at(0,1));
    ASSERT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).at(1,0)); ASSERT_EQ(complex(2,1),(complex)(sp_mc22+sp_mr22).at(1,1));

    // real+complex
    ASSERT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).at(0,0)); ASSERT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).at(0,1));
    ASSERT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).at(1,0)); ASSERT_EQ(complex(2,1),(complex)(sp_mr22+sp_mc22).at(1,1));

    // comple+complex
    ASSERT_EQ(0,(sp_mr22-sp_mr22).at(0,0));ASSERT_EQ(0,(sp_mr22-sp_mr22).at(0,1));
    ASSERT_EQ(0,(sp_mr22-sp_mr22).at(1,0));ASSERT_EQ(0,(sp_mr22-sp_mr22).at(1,1));

    ASSERT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).at(0,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).at(1,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mc22+sp_mc22).at(1,1));

    ////// subtraction
    // real-real

    ASSERT_EQ(0,(sp_mr22-sp_mr22).at(0,0)); ASSERT_EQ(0,(sp_mr22-sp_mr22).at(0,1));
    ASSERT_EQ(0,(sp_mr22-sp_mr22).at(1,0)); ASSERT_EQ(0,(sp_mr22-sp_mr22).at(1,1));

    // complex-real
    //ASSERT_EQ(complex(0,1),(complex)(sp_mc22-sp_mr22).at(0,0)); ASSERT_EQ(complex(0,1),(complex)(sp_mc22-sp_mr22).at(0,1));
//    ASSERT_EQ(complex(0,1),(complex)(sp_mc22-sp_mr22).at(1,0)); ASSERT_EQ(complex(0,1),(complex)(sp_mc22-sp_mr22).at(1,1));

    // real-complex

//    ASSERT_EQ(complex(0,-1),(complex)(sp_mr22-sp_mc22).at(0,0)); ASSERT_EQ(complex(0,-1),(complex)(sp_mr22-sp_mc22).at(0,1));
//    ASSERT_EQ(complex(0,-1),(complex)(sp_mr22-sp_mc22).at(1,0)); ASSERT_EQ(complex(0,-1),(complex)(sp_mr22-sp_mc22).at(1,1));

    // complex+complex
    ASSERT_EQ(zero,(complex)(sp_mc22-sp_mc22).at(0,0)); ASSERT_EQ(zero,(complex)(sp_mc22-sp_mc22).at(0,1));
    ASSERT_EQ(zero,(complex)(sp_mc22-sp_mc22).at(1,0)); ASSERT_EQ(zero,(complex)(sp_mc22-sp_mc22).at(1,1));


    ///// multiplication
    // real*real
    ASSERT_EQ(2.0,(sp_mr22*sp_mr22).at(0,0)); ASSERT_EQ(2.0,(sp_mr22*sp_mr22).at(0,1));
    ASSERT_EQ(2.0,(sp_mr22*sp_mr22).at(1,0)); ASSERT_EQ(2.0,(sp_mr22*sp_mr22).at(1,1));

    // real*complex
    ASSERT_EQ(complex(2,2),(complex)(sp_mr22*sp_mc22).at(0,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mr22*sp_mc22).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(sp_mr22*sp_mc22).at(1,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mr22*sp_mc22).at(1,1));

    // complex*real
    ASSERT_EQ(complex(2,2),(complex)(sp_mc22*sp_mr22).at(0,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mc22*sp_mr22).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(sp_mc22*sp_mr22).at(1,0)); ASSERT_EQ(complex(2,2),(complex)(sp_mc22*sp_mr22).at(1,1));

    // complex*complex
    ASSERT_EQ(complex(0,4),(complex)(sp_mc22*sp_mc22).at(0,0)); ASSERT_EQ(complex(0,4),(complex)(sp_mc22*sp_mc22).at(0,1));
    ASSERT_EQ(complex(0,4),(complex)(sp_mc22*sp_mc22).at(1,0)); ASSERT_EQ(complex(0,4),(complex)(sp_mc22*sp_mc22).at(1,1));
}

TEST_F(SparseMatrix_Fixture,freeScalarMatrixOperations){

    RMat mr22(2,2,1);
    CMat mc22(2,2,one);

    Sp_RMat sp_mr22(mr22);
    Sp_CMat sp_mc22(mc22);



    /// SCALAR MULTIPLICATION

    // real scalar * real mat
    ASSERT_EQ(2,(2.0*sp_mr22).at(0,0));ASSERT_EQ(2,(2.0*sp_mr22).at(0,1));
    ASSERT_EQ(2,(2.0*sp_mr22).at(1,0));ASSERT_EQ(2,(2.0*sp_mr22).at(1,1));


    // real scalar * complex mat
    ASSERT_EQ(complex(2,2),(complex)(2.0*sp_mc22).at(0,0));ASSERT_EQ(complex(2,2),(complex)(2.0*sp_mc22).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(2.0*sp_mc22).at(1,0));ASSERT_EQ(complex(2,2),(complex)(2.0*sp_mc22).at(1,1));

    // complex scalar * real mat
    ASSERT_EQ(complex(2,2),(complex)(complex(2,2)*sp_mr22).at(0,0));ASSERT_EQ(complex(2,2),(complex)(complex(2,2)*sp_mr22).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(complex(2,2)*sp_mr22).at(1,0));ASSERT_EQ(complex(2,2),(complex)(complex(2,2)*sp_mr22).at(1,1));

    // complex scalar * complex mat
    ASSERT_EQ(complex(0,4),(complex)(complex(2,2)*sp_mc22).at(0,0));ASSERT_EQ(complex(0,4),(complex)(complex(2,2)*sp_mc22).at(0,1));
    ASSERT_EQ(complex(0,4),(complex)(complex(2,2)*sp_mc22).at(1,0));ASSERT_EQ(complex(0,4),(complex)(complex(2,2)*sp_mc22).at(1,1));


    // real mat * real scalar
    ASSERT_EQ(2,(sp_mr22*2.0).at(0,0));ASSERT_EQ(2,(sp_mr22*2.0).at(0,1));
    ASSERT_EQ(2,(sp_mr22*2.0).at(1,0));ASSERT_EQ(2,(sp_mr22*2.0).at(1,1));

    // complex mat * real scalar
    ASSERT_EQ(complex(2,2),(complex)(sp_mc22*2.0).at(0,0));ASSERT_EQ(complex(2,2),(complex)(sp_mc22*2.0).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(sp_mc22*2.0).at(1,0));ASSERT_EQ(complex(2,2),(complex)(sp_mc22*2.0).at(1,1));

    // real mat * complex scalar
    ASSERT_EQ(complex(2,2),(complex)(sp_mr22*complex(2,2)).at(0,0));ASSERT_EQ(complex(2,2),(complex)(sp_mr22*complex(2,2)).at(0,1));
    ASSERT_EQ(complex(2,2),(complex)(sp_mr22*complex(2,2)).at(1,0));ASSERT_EQ(complex(2,2),(complex)(sp_mr22*complex(2,2)).at(1,1));

    // complex scalar * complex mat
    ASSERT_EQ(complex(0,4),(complex)(sp_mc22*complex(2,2)).at(0,0));ASSERT_EQ(complex(0,4),(complex)(sp_mc22*complex(2,2)).at(0,1));
    ASSERT_EQ(complex(0,4),(complex)(sp_mc22*complex(2,2)).at(1,0));ASSERT_EQ(complex(0,4),(complex)(sp_mc22*complex(2,2)).at(1,1));

//    /// SCALAR DIVISION
//    // real mat / real scalar
    ASSERT_EQ(0.5,(sp_mr22/2.0).at(0,0));ASSERT_EQ(0.5,(sp_mr22/2.0).at(0,1));
    ASSERT_EQ(0.5,(sp_mr22/2.0).at(1,0));ASSERT_EQ(0.5,(sp_mr22/2.0).at(1,1));

    // real mat / complex scalar
    ASSERT_EQ(complex(0.25,-0.25),(complex)(sp_mr22/complex(2,2)).at(0,0));ASSERT_EQ(complex(0.25,-0.25),(complex)(sp_mr22/complex(2,2)).at(0,1));
    ASSERT_EQ(complex(0.25,-0.25),(complex)(sp_mr22/complex(2,2)).at(1,0));ASSERT_EQ(complex(0.25,-0.25),(complex)(sp_mr22/complex(2,2)).at(1,1));

    // complx mat / real scalar
    ASSERT_EQ(complex(0.5,0.5),(complex)(sp_mc22/2.0).at(0,0));ASSERT_EQ(complex(0.5,0.5),(complex)(sp_mc22/2.0).at(0,1));
    ASSERT_EQ(complex(0.5,0.5),(complex)(sp_mc22/2.0).at(1,0));ASSERT_EQ(complex(0.5,0.5),(complex)(sp_mc22/2.0).at(1,1));

    // complex mat/complex scalar
    ASSERT_EQ(complex(0.5,0),(complex)(sp_mc22/complex(2,2)).at(0,0));ASSERT_EQ(complex(0.5,0),(complex)(sp_mc22/complex(2,2)).at(0,1));
    ASSERT_EQ(complex(0.5,0),(complex)(sp_mc22/complex(2,2)).at(1,0));ASSERT_EQ(complex(0.5,0),(complex)(sp_mc22/complex(2,2)).at(1,1));

    /// ELEMENTWISE MULTIPLICATION
    sp_mr22 = Sp_RMat(RMat(2,2,2));
    sp_mc22 = Sp_CMat(CMat(2,2,complex(2.0,2.0)));

    // real%real
    ASSERT_EQ(4,(sp_mr22%sp_mr22).at(0,0));ASSERT_EQ(4,(sp_mr22%sp_mr22).at(0,1));
    ASSERT_EQ(4,(sp_mr22%sp_mr22).at(1,0));ASSERT_EQ(4,(sp_mr22%sp_mr22).at(1,1));

    // real%complex
    ASSERT_EQ(complex(4,4),(complex)(sp_mr22%sp_mc22).at(0,0));ASSERT_EQ(complex(4,4),(complex)(sp_mr22%sp_mc22).at(0,1));
    ASSERT_EQ(complex(4,4),(complex)(sp_mr22%sp_mc22).at(1,0));ASSERT_EQ(complex(4,4),(complex)(sp_mr22%sp_mc22).at(1,1));

    // complex%real
    ASSERT_EQ(complex(4,4),(complex)(sp_mc22%sp_mr22).at(0,0));ASSERT_EQ(complex(4,4),(complex)(sp_mc22%sp_mr22).at(0,1));
    ASSERT_EQ(complex(4,4),(complex)(sp_mc22%sp_mr22).at(1,0));ASSERT_EQ(complex(4,4),(complex)(sp_mc22%sp_mr22).at(1,1));

    // complex%real
    ASSERT_EQ(complex(0,8),(complex)(sp_mc22%sp_mc22).at(0,0));ASSERT_EQ(complex(0,8),(complex)(sp_mc22%sp_mc22).at(0,1));
    ASSERT_EQ(complex(0,8),(complex)(sp_mc22%sp_mc22).at(1,0));ASSERT_EQ(complex(0,8),(complex)(sp_mc22%sp_mc22).at(1,1));
}

TEST_F(SparseMatrix_Fixture,kron)
{
	const auto n_rows = 2;
	const auto n_cols = 3;
	const auto density = 0.5;

	const auto Ar = SparseMatrix<real>(arma::sprandn(n_rows, n_cols, density));
	const auto Br = SparseMatrix<real>(arma::sprandn(n_rows, n_cols, density));

	const auto Ac = SparseMatrix<complex>(arma::sp_cx_mat(arma::sprandn(n_rows, n_cols, density), arma::sprandn(n_rows, n_cols, density)));
	const auto Bc = SparseMatrix<complex>(arma::sp_cx_mat(arma::sprandn(n_rows, n_cols, density), arma::sprandn(n_rows, n_cols, density)));


	const auto Crr = SparseMatrix<real>(arma::sp_mat(arma::mat(arma::kron(arma::mat(Ar.mat()), arma::mat(Br.mat())))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::mat(Ar.mat()), arma::cx_mat(Bc.mat())))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat()), arma::mat(Br.mat())))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = SparseMatrix<complex>(arma::sp_cx_mat(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat()), arma::cx_mat(Bc.mat())))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}
