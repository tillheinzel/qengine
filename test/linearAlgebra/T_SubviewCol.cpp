﻿/* COPYRIGHT
 *
 * file="T_SubviewCol.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/SubviewCol.h"

#include "additionalAsserts.h"

using namespace qengine;
using namespace internal;
class SubviewCol_Fixture : public ::testing::Test {
public:
	SubviewCol_Fixture(): 
		data{1,2,3,4,5,6,7,8,9}, 
		rm33(3,3,data), 
		svc3(rm33,0),
		cdata{{1,2},{2,3},{3,4},{4,5},{5,6},{6,7},{7,8},{8,9},{9,10}},
		cm33(3,3,cdata),
		csvc31(cm33,0)
	{};
	
	real data[9];
	RMat rm33;
	SubviewCol<real> svc3;

	complex cdata[9];
	CMat cm33;
	SubviewCol<complex> csvc31;
};


TEST_F(SubviewCol_Fixture,accessCols){
	////// real
	RMat rm({{1,2,3},{4,5,6}});
	RVec rv9(2,9); // fill vector of length 2 with 9's
	
	ASSERT_NO_THROW(RVec rvcol0 = rm.col(0));
	ASSERT_NO_THROW(rm.col(0) = rm.col(2));

	// Getting 	
	RVec rvcol0(rm.col(0));
	RVec rvcol1(rm.col(1));
	RVec rvcol2(rm.col(2));
	ASSERT_EQ(rm.at(0,0),rvcol0.at(0));	ASSERT_EQ(rm.at(0,1),rvcol1.at(0)); ASSERT_EQ(rm.at(0,2),rvcol2.at(0));
	ASSERT_EQ(rm.at(1,0),rvcol0.at(1)); ASSERT_EQ(rm.at(1,1),rvcol1.at(1)); ASSERT_EQ(rm.at(1,2),rvcol2.at(1));

	// Setting
	rm.col(0) = rv9;
	rm.col(1) = rv9;
	rm.col(2) = rv9;
	ASSERT_EQ(rm.at(0,0),rv9.at(0));	ASSERT_EQ(rm.at(0,1),rv9.at(0)); ASSERT_EQ(rm.at(0,2),rv9.at(0));
	ASSERT_EQ(rm.at(1,0),rv9.at(1)); ASSERT_EQ(rm.at(1,1),rv9.at(1)); ASSERT_EQ(rm.at(1,2),rv9.at(1));	

	///// complex
	CMat cm({{{1.0,1.0},{2.0,2.0},{3.0,3.0}},{{4.0,4.0},{5.0,5.0},{6.0,6.0}}});
	CVec cv9(2,{9.0,9.0}); // fill vector of length 2 with 9+9i

	ASSERT_NO_THROW(CVec cvcol0(cm.col(0)));
	ASSERT_NO_THROW(cm.col(0) = cm.col(2));
	
	// Getting
	CVec cvcol0(cm.col(0));
	CVec cvcol1(cm.col(1));
	CVec cvcol2(cm.col(2));
	ASSERT_NEAR_C(cm.at(0,0),cvcol0.at(0),1e-16);	ASSERT_NEAR_C(cm.at(0,1),cvcol1.at(0),1e-16);	ASSERT_NEAR_C(cm.at(0,2),cvcol2.at(0),1e-16);		
	ASSERT_NEAR_C(cm.at(1,0),cvcol0.at(1),1e-16);	ASSERT_NEAR_C(cm.at(1,1),cvcol1.at(1),1e-16);	ASSERT_NEAR_C(cm.at(1,2),cvcol2.at(1),1e-16);		

	// Setting 
	cm.col(0) = cv9;
	cm.col(1) = cv9;
	cm.col(2) = cv9;
	ASSERT_NEAR_C(cm.at(0,0),cv9.at(0),1e-16); ASSERT_NEAR_C(cm.at(0,1),cv9.at(0),1e-16); ASSERT_NEAR_C(cm.at(0,2),cv9.at(0),1e-16);
	ASSERT_NEAR_C(cm.at(1,0),cv9.at(1),1e-16); ASSERT_NEAR_C(cm.at(1,1),cv9.at(1),1e-16); ASSERT_NEAR_C(cm.at(1,2),cv9.at(1),1e-16);	
}

TEST_F(SubviewCol_Fixture,dataaccess){
	ASSERT_EQ(rm33.at(0,0),svc3.at(0));
	ASSERT_EQ(rm33.at(1,0),svc3.at(1));
	ASSERT_EQ(rm33.at(2,0),svc3.at(2));

	// check real and imag
	auto count_val = 1u;
	for(auto i = 0u;i<svc3.size();i++){
		ASSERT_EQ(count_val,svc3.re().at(i));
		ASSERT_EQ(0,svc3.im().at(i));

		ASSERT_EQ(count_val,csvc31.re().at(i));
		ASSERT_EQ(count_val+1,csvc31.im().at(i));
		count_val++;
	}
	
	
}

TEST_F(SubviewCol_Fixture,memberMathOperators){
	RMat rm33ones(3,3,1);
	RMat rm33original(rm33);

	CMat cm33ones(3,3,{1,1});
	CMat cm33original(cm33);

	rm33.col(0) +=rm33ones.col(0);
	ASSERT_EQ(rm33original.at(0)+1,rm33.at(0));
	ASSERT_EQ(rm33original.at(1)+1,rm33.at(1));
	ASSERT_EQ(rm33original.at(2)+1,rm33.at(2));

	rm33.col(0) -=rm33ones.col(0);
	ASSERT_EQ(rm33original.at(0),rm33.at(0));
	ASSERT_EQ(rm33original.at(1),rm33.at(1));
	ASSERT_EQ(rm33original.at(2),rm33.at(2));

	rm33.col(0) /= 2.0;
	ASSERT_EQ(rm33original.col(0).at(0)/2.0,rm33.col(0).at(0));
	ASSERT_EQ(rm33original.col(0).at(1)/2.0,rm33.col(0).at(1));
	ASSERT_EQ(rm33original.col(0).at(2)/2.0,rm33.col(0).at(2));

	rm33.col(0) *= 2.0;
	ASSERT_EQ(rm33original.col(0).at(0),rm33.col(0).at(0));
	ASSERT_EQ(rm33original.col(0).at(1),rm33.col(0).at(1));
	ASSERT_EQ(rm33original.col(0).at(2),rm33.col(0).at(2));

	complex c(2,2);
	cm33.col(0) /= c;
	ASSERT_NEAR_C(cm33original.col(0).at(0)/c,cm33.col(0).at(0),1e-10);
	ASSERT_NEAR_C(cm33original.col(0).at(1)/c,cm33.col(0).at(1),1e-10);
	ASSERT_NEAR_C(cm33original.col(0).at(2)/c,cm33.col(0).at(2),1e-10);

	cm33.col(0) *= c;
	ASSERT_NEAR_C(cm33original.col(0).at(0),cm33.col(0).at(0),1e-10);
	ASSERT_NEAR_C(cm33original.col(0).at(1),cm33.col(0).at(1),1e-10);
	ASSERT_NEAR_C(cm33original.col(0).at(2),cm33.col(0).at(2),1e-10);

	
	ASSERT_NEAR_C(-(cm33original.col(0).at(0)),CVec(-cm33original.col(0)).at(0),1e-10);
	ASSERT_NEAR_C(-(cm33original.col(0).at(1)),CVec(-cm33original.col(0)).at(1),1e-10);
	ASSERT_NEAR_C(-(cm33original.col(0).at(2)),CVec(-cm33original.col(0)).at(2),1e-10);

	CVec cvOnes(3,complex(1,1));
	cm33 = cm33original;
	cm33.col(0) += cvOnes;
	
	ASSERT_NEAR_C(complex(1, 1) + cm33original.at(0, 0), cm33.at(0, 0), 1e-10);
	ASSERT_NEAR_C(complex(1, 1) + cm33original.at(1, 0), cm33.at(1, 0), 1e-10);
	ASSERT_NEAR_C(complex(1, 1) + cm33original.at(2, 0), cm33.at(2, 0), 1e-10);

	cm33 = cm33original;
	cm33.col(0) -= cvOnes;
	
	ASSERT_NEAR_C(cm33original.at(0, 0) - complex(1, 1), cm33.at(0, 0), 1e-10);
	ASSERT_NEAR_C(cm33original.at(1, 0) - complex(1, 1), cm33.at(1, 0), 1e-10);
	ASSERT_NEAR_C(cm33original.at(2, 0) - complex(1, 1), cm33.at(2, 0), 1e-10);

	//  scalar addition
	rm33 = rm33original;
	rm33.col(0) += 1.0;

	ASSERT_EQ(rm33original.at(0,0)+1.0,rm33.at(0,0));
	ASSERT_EQ(rm33original.at(1,0)+1.0,rm33.at(1,0));
	ASSERT_EQ(rm33original.at(2,0)+1.0,rm33.at(2,0));

	cm33 = cm33original;
	cm33.col(0) += 1.0;
	ASSERT_EQ(cm33original.at(0,0)+1.0,cm33.at(0,0));
	ASSERT_EQ(cm33original.at(1,0)+1.0,cm33.at(1,0));
	ASSERT_EQ(cm33original.at(2,0)+1.0,cm33.at(2,0));

	cm33 = cm33original;
	cm33.col(0) += complex(1,1);
	ASSERT_EQ(cm33original.at(0,0)+complex(1.0,1.0),cm33.at(0,0));
	ASSERT_EQ(cm33original.at(1,0)+complex(1.0,1.0),cm33.at(1,0));
	ASSERT_EQ(cm33original.at(2,0)+complex(1.0,1.0),cm33.at(2,0));

	//  scalar subtraction
	rm33 = rm33original;
	rm33.col(0) -= 1.0;

	ASSERT_EQ(rm33original.at(0,0)-1.0,rm33.at(0,0));
	ASSERT_EQ(rm33original.at(1,0)-1.0,rm33.at(1,0));
	ASSERT_EQ(rm33original.at(2,0)-1.0,rm33.at(2,0));

	cm33 = cm33original;
	cm33.col(0) -= 1.0;
	ASSERT_EQ(cm33original.at(0,0)-1.0,cm33.at(0,0));
	ASSERT_EQ(cm33original.at(1,0)-1.0,cm33.at(1,0));
	ASSERT_EQ(cm33original.at(2,0)-1.0,cm33.at(2,0));

	cm33 = cm33original;
	cm33.col(0) += -complex(1,1);
	ASSERT_EQ(cm33original.at(0,0)-complex(1.0,1.0),cm33.at(0,0));
	ASSERT_EQ(cm33original.at(1,0)-complex(1.0,1.0),cm33.at(1,0));
	ASSERT_EQ(cm33original.at(2,0)-complex(1.0,1.0),cm33.at(2,0));
}



TEST_F(SubviewCol_Fixture,misc){
	ASSERT_EQ(rm33.n_rows(),svc3.size());

	RVec rv3({1,2,3});
	
	ASSERT_EQ(rv3.norm(),svc3.norm());
	ASSERT_EQ(rm33.col(0).norm(),svc3.norm());
	
	for(auto i =0u;i<svc3.size();i++){
		ASSERT_EQ(rv3.absSquare().at(i),svc3.absSquare().at(i));
		ASSERT_EQ(rm33.col(0).absSquare().at(i),svc3.absSquare().at(i));
	}
}

TEST_F(SubviewCol_Fixture, assignScalar)
{
	static_assert(std::is_assignable<real&, real>::value, "");
	// assign real to real
	rm33.col(0) = 0.0;
	ASSERT_EQ(0.0, rm33.at(0,0));
	ASSERT_EQ(0.0, rm33.at(1,0));
	ASSERT_EQ(0.0, rm33.at(2,0));

	// assign int to real
	rm33.col(1) = 1;
	ASSERT_EQ(1.0, rm33.at(0,1));
	ASSERT_EQ(1.0, rm33.at(1,1));
	ASSERT_EQ(1.0, rm33.at(2,1));

	// assign unsigned int to real
	rm33.col(2) = 2u;
	ASSERT_EQ(2.0, rm33.at(0,2));
	ASSERT_EQ(2.0, rm33.at(1,2));
	ASSERT_EQ(2.0, rm33.at(2,2));

	// assign real to complex
	cm33.col(0) = 0.0;
	ASSERT_EQ(0.0, cm33.at(0,0));
	ASSERT_EQ(0.0, cm33.at(1,0));
	ASSERT_EQ(0.0, cm33.at(2,0));

	// assign int to complex
	cm33.col(1) = 1;
	ASSERT_EQ(1.0, cm33.at(0,1));
	ASSERT_EQ(1.0, cm33.at(1,1));
	ASSERT_EQ(1.0, cm33.at(2,1));

	// assign unsigned int to complex
	cm33.col(2) = 2u;
	ASSERT_EQ(2.0, cm33.at(0,2));
	ASSERT_EQ(2.0, cm33.at(1,2));
	ASSERT_EQ(2.0, cm33.at(2,2));

	using namespace std::complex_literals;
	// assign unsigned int to complex
	complex val = 1.0 + 1i;
	cm33.col(0) = val;
	ASSERT_EQ(val, cm33.at(0,0));
	ASSERT_EQ(val, cm33.at(1,0));
	ASSERT_EQ(val, cm33.at(2,0));


}

// These are disabled for now
// TEST_F(SubviewCol_Fixture,operations){
	
// 	SubviewCol<complex> csvc32(cm33,1);


// 	ASSERT_EQ(14,dot(svc3,svc3));
// 	ASSERT_NEAR_C(std::complex<double>(14,20),dot(svc3,csvc31),1e-10);
// 	ASSERT_NEAR_C(std::complex<double>(14,20),dot(csvc31,svc3),1e-10);
// 	ASSERT_NEAR_C(std::complex<double>(-24,85),dot(csvc31,csvc32),1e-10);

// 	ASSERT_NEAR_C(std::complex<double>(14,-20), cdot(csvc31,svc3),1e-10);
// 	ASSERT_NEAR_C(std::complex<double>(88,-9), cdot(csvc31,csvc32),1e-10);
// 	ASSERT_NEAR_C(std::complex<double>(88,9), cdot(csvc32,csvc31),1e-10);
// 	ASSERT_NEAR_C(std::complex<double>(43,0),cdot(csvc31,csvc31),1e-10);
// 	ASSERT_NEAR(csvc31.norm(),sqrt(cdot(csvc31,csvc31).real()),1e-10);

// }
