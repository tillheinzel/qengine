﻿/* COPYRIGHT
 *
 * file="T_Vector.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "additionalAsserts.h"

using namespace qengine;
using namespace internal;
using namespace std::complex_literals;

class LinearAlgebra_Fixture : public ::testing::Test {};

class Vector_Fixture : public LinearAlgebra_Fixture {};

TEST_F(Vector_Fixture, constructors)
{

	std::vector<double> rvec = { 1.0,2.0,3.0 };
	std::vector<complex> cvec = { {1.0, 0.0},{2.0,1.0},{3.0,2.0} };

	ASSERT_NO_THROW(Vector<real>(3));
	ASSERT_NO_THROW(Vector<complex>(3));

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<real>(3, 2.0).at(i), 2.0, 1e-16);

		ASSERT_NEAR(Vector<complex>(3, 2.0).at(i).real(), 2.0, 1e-16);
		ASSERT_NEAR(Vector<complex>(3, 2.0).at(i).imag(), 0.0, 1e-16);

		ASSERT_NEAR(Vector<complex>(3, { 2.0 , 1.0 }).at(i).real(), 2.0, 1e-16);
		ASSERT_NEAR(Vector<complex>(3, { 2.0 , 1.0 }).at(i).imag(), 1.0, 1e-16);
	}

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<real>(3, rvec.data()).at(i), i + 1, 1e-16);
	}

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<complex>(3, cvec.data()).at(i).real(), i + 1, 1e-16);
		ASSERT_NEAR(Vector<complex>(3, cvec.data()).at(i).imag(), i, 1e-16);
	}


	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<real>({ 1.0,2.0,3.0 }).at(i), i + 1, 1e-16);
	}

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<complex>({ { 1.0, 0.0 }, { 2.0,1.0 }, { 3.0,2.0 } }).at(i).real(), i + 1, 1e-16);
		ASSERT_NEAR(Vector<complex>({ { 1.0, 0.0 }, { 2.0,1.0 }, { 3.0,2.0 } }).at(i).imag(), i, 1e-16);
	}

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<real>(rvec).at(i), i + 1, 1e-16);
	}

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(Vector<complex>(cvec).at(i).real(), i + 1, 1e-16);
		ASSERT_NEAR(Vector<complex>(cvec).at(i).imag(), i, 1e-16);
	}
}

TEST_F(Vector_Fixture, assignment)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	ASSERT_NO_THROW(auto rvec2 = rvec);
	ASSERT_NO_THROW(auto cvec2 = cvec);

	auto rvec2 = rvec;
	auto cvec2 = cvec;

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(rvec.at(i), rvec2.at(i), 1e-16);

		ASSERT_NEAR_C(cvec.at(i), cvec2.at(i), 1e-16);
	}

	ASSERT_NO_THROW(rvec = std::move(rvec2));
	ASSERT_NO_THROW(cvec = std::move(cvec2));

	ASSERT_NO_THROW(auto rvec3 = std::move(rvec));
	ASSERT_NO_THROW(auto cvec3 = std::move(cvec));


}

TEST_F(Vector_Fixture, data_access)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };
	
	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(rvec.at(i), i + 1, 1e-16);

		ASSERT_NEAR_C(cvec.at(i), i + 1.0 + double(i)*1i, 1e-16);
	}

	real* rdata = rvec._data();
	complex* cdata = cvec._data();

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR(rdata[i], i + 1, 1e-16);

		ASSERT_NEAR_C(cdata[i], i + 1.0 + double(i)*1i, 1e-16);
	}

	for(auto i=0u;i<3;++i){
		ASSERT_NEAR(rvec.at(i),rvec.re().at(i),1e-16);
		ASSERT_NEAR(0,rvec.im().at(i),1e-16);

		ASSERT_NEAR(i+1,cvec.re().at(i),1e-16);
		ASSERT_NEAR(i,cvec.im().at(i),1e-16);
	}
}

TEST_F(Vector_Fixture, equality)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto rvecAsC = CVec(rvec);
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };



	// self-comparison
	ASSERT_TRUE(rvec==rvec);
	ASSERT_TRUE(cvec==cvec);
	ASSERT_TRUE(rvecAsC == rvecAsC);

	ASSERT_FALSE(rvec != rvec);
	ASSERT_FALSE(cvec != cvec);
	ASSERT_FALSE(rvecAsC != rvecAsC);


	// real and complex can be compared, too!
	ASSERT_TRUE(rvec == rvecAsC);
	ASSERT_TRUE(rvecAsC == rvec);

	ASSERT_FALSE(rvec != rvecAsC);
	ASSERT_FALSE(rvecAsC != rvec);


	// different vectors
	auto obviouslyDifferentRVec = RVec{ 2.0,3.0,4.0 };
	ASSERT_TRUE(rvec != obviouslyDifferentRVec);
	ASSERT_TRUE(rvec != cvec);
	ASSERT_TRUE(rvecAsC != cvec);

	ASSERT_FALSE(rvec == obviouslyDifferentRVec);
	ASSERT_FALSE(rvec == cvec);
	ASSERT_FALSE(rvecAsC == cvec);
}

TEST_F(Vector_Fixture, absSquare)
{
	const auto rvec = RVec{ 1.0,2.0,3.0 };
	const auto rvecAsC = CVec(rvec);
	const auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	auto ref = RVec{1.0, 4.0, 9.0};
	ASSERT_NEAR_V(ref, rvec.absSquare(), 1e-16);
	ASSERT_NEAR_V(ref, rvecAsC.absSquare(), 1e-16);

	ref = {1.0, 5.0, 13.0};
	ASSERT_NEAR_V(ref, cvec.absSquare(), 1e-16);

}


TEST_F(Vector_Fixture, addition)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	ASSERT_NEAR_V(rvec + rvec, RVec({2.0,4.0,6.0}), 1e-16);

	ASSERT_NEAR_V(cvec + cvec, CVec({ {2.0, 0.0},
														 {4.0, 2.0},
														 {6.0, 4.0} }), 1e-16);

	ASSERT_NEAR_V(cvec + rvec, CVec({ {2.0, 0.0},
														 {4.0, 1.0},
														 {6.0, 2.0} }), 1e-16);

	ASSERT_NEAR_V(rvec + cvec, CVec({ {2.0, 0.0},
														 {4.0, 1.0},
														 {6.0, 2.0} }), 1e-16);


}

TEST_F(Vector_Fixture, addition_with_scalar)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	ASSERT_NEAR_V(rvec + 1, RVec({2.0,3.0,4.0}), 1e-16);
	ASSERT_NEAR_V(rvec + 1.0, RVec({2.0,3.0,4.0}), 1e-16);

	ASSERT_NEAR_V(cvec + 1i, CVec({ {1.0, 1.0},
														 {2.0, 2.0},
														 {3.0, 3.0} }), 1e-16);

	ASSERT_NEAR_V(cvec + 1, CVec({ {2.0, 0.0},
														 {3.0, 1.0},
														 {4.0, 2.0} }), 1e-16);	

	ASSERT_NEAR_V(cvec + 1.0, CVec({ {2.0, 0.0},
														 {3.0, 1.0},
														 {4.0, 2.0} }), 1e-16);	

	ASSERT_NEAR_V(rvec + 1i, CVec({ {1.0, 1.0},
														 {2.0, 1.0},
														 {3.0, 1.0} }), 1e-16);
}

TEST_F(Vector_Fixture, subtraction)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR((rvec - rvec).at(i), 0.0*rvec.at(i), 1e-16);

		ASSERT_NEAR((cvec - cvec).at(i).real(), 0.0 * cvec.at(i).real(), 1e-16);
		ASSERT_NEAR((cvec - cvec).at(i).imag(), 0.0 * cvec.at(i).imag(), 1e-16);


		ASSERT_NEAR((cvec - rvec).at(i).real(), 0.0 * rvec.at(i), 1e-16);
		ASSERT_NEAR((cvec - rvec).at(i).imag(), cvec.at(i).imag(), 1e-16);

		ASSERT_NEAR((rvec - cvec).at(i).real(), 0.0 * rvec.at(i), 1e-16);
		ASSERT_NEAR((rvec - cvec).at(i).imag(), -cvec.at(i).imag(), 1e-16);
	}
}

TEST_F(Vector_Fixture, multiplication)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR((2*rvec).at(i), 2*rvec.at(i), 1e-16);
		ASSERT_NEAR((2.0*rvec).at(i), 2.0*rvec.at(i), 1e-16);
		
		ASSERT_NEAR((2.0*cvec).at(i).real(), 2.0*cvec.at(i).real(), 1e-16);
		ASSERT_NEAR((2.0*cvec).at(i).imag(), 2.0*cvec.at(i).imag(), 1e-16);

		ASSERT_NEAR((1i*rvec).at(i).real(), 0.0, 1e-16);
		ASSERT_NEAR((1i*rvec).at(i).imag(), rvec.at(i), 1e-16);

		ASSERT_NEAR((1i*cvec).at(i).real(), -cvec.at(i).imag(), 1e-16);
		ASSERT_NEAR((1i*cvec).at(i).imag(), cvec.at(i).real(), 1e-16);

	}
}

TEST_F(Vector_Fixture, division)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR((rvec/2.0).at(i), 0.5*rvec.at(i), 1e-16);
		ASSERT_NEAR((rvec/2).at(i), 0.5*rvec.at(i), 1e-16);

		ASSERT_NEAR((cvec/2.0).at(i).real(), 0.5*cvec.at(i).real(), 1e-16);
		ASSERT_NEAR((cvec/2.0).at(i).imag(), 0.5*cvec.at(i).imag(), 1e-16);

		ASSERT_NEAR((cvec/2).at(i).real(), 0.5*cvec.at(i).real(), 1e-16);
		ASSERT_NEAR((cvec/2).at(i).imag(), 0.5*cvec.at(i).imag(), 1e-16);

		ASSERT_NEAR((rvec/1i).at(i).real(), 0.0, 1e-16);
		ASSERT_NEAR((rvec/1i).at(i).imag(), -rvec.at(i), 1e-16);

		ASSERT_NEAR((cvec/1i).at(i).real(), cvec.at(i).imag(), 1e-16);
		ASSERT_NEAR((cvec/1i).at(i).imag(), -cvec.at(i).real(), 1e-16);
	}
}


TEST_F(Vector_Fixture, point_wise_division)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.1 },{ 2.0,1.0 },{ 3.0,2.0 } };
	
	for (auto i = 0u; i < 3; ++i)
	{
		ASSERT_NEAR((rvec / rvec).at(i), 1.0, 1e-16);

		ASSERT_NEAR((cvec / cvec).at(i).real(), 1.0, 1e-16);
		ASSERT_NEAR((cvec / cvec).at(i).imag(), 0.0, 1e-16);

		ASSERT_NEAR((1.0 / rvec).at(i), 1.0/rvec.at(i), 1e-16);

		ASSERT_NEAR((1 / rvec).at(i), 1 / rvec.at(i), 1e-16);
	}
}

// todo: test for % (pointwise multiplication)

//todo: test for kron

TEST_F(Vector_Fixture, norm)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	ASSERT_NEAR(rvec.norm(), std::sqrt(14), 1e-16);
	ASSERT_NEAR(cvec.norm(), std::sqrt(19), 1e-16);
}

TEST_F(Vector_Fixture, dot)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	EXPECT_NEAR(dot(rvec,rvec),14, 1e-16);

	EXPECT_NEAR_C(dot(cvec, cvec), 9.0 + 16.0*1i, 1e-16);

	EXPECT_NEAR_C(dot(rvec, cvec), 14.0 + 8.0*1i, 1e-16);

	EXPECT_NEAR_C(dot(cvec, rvec), 14.0 + 8.0*1i, 1e-16);
}

TEST_F(Vector_Fixture, cdot)
{
	auto rvec = RVec{ 1.0,2.0,3.0 };
	auto cvec = CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } };

	ASSERT_NEAR_C(cdot(cvec, rvec), 14.0-8.0*1i, 1e-16);
	ASSERT_NEAR_C(cdot(cvec, cvec), 19.0+0.0*1i, 1e-16);
}

// todo: test for norm_dot


TEST_F(Vector_Fixture, step)
{
	auto x = qengine::linspace(-1, 1, 128);
	auto stepPos = 0.0;

	auto y = step(x, stepPos);

	for(auto i = 0u; i< x.size(); ++i)
	{
		ASSERT_TRUE(y.at(i) == (x.at(i) < stepPos ? 0 : 1));
	}
}
TEST_F(Vector_Fixture, box)
{
	auto x = qengine::linspace(-1, 1, 128);
	auto left = -0.5;
	auto right = 0.5;

	auto y = box(x, left, right);

	for(auto i = 0u; i< x.size(); ++i)
	{
		ASSERT_TRUE(y.at(i) == (left < x.at(i) && x.at(i) < right  ? 1 : 0));
	}
}

TEST_F(Vector_Fixture, kron)
{
	const auto n_rows = 2;

	const auto Ar = Vector<real>(arma::randn(n_rows));
	const auto Br = Vector<real>(arma::randn(n_rows));

	const auto Ac = Vector<complex>(arma::cx_mat(arma::randn(n_rows), arma::randn(n_rows)));
	const auto Bc = Vector<complex>(arma::cx_mat(arma::randn(n_rows), arma::randn(n_rows)));

	const auto Crr = Vector<real>(arma::vec(arma::kron(arma::vec(Ar._vec()), arma::vec(Br._vec()))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = Vector<complex>((arma::kron(arma::vec(Ar._vec()), arma::cx_vec(Bc._vec()))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = Vector<complex>((arma::kron(arma::cx_vec(Ac._vec()), arma::vec(Br._vec()))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = Vector<complex>((arma::kron(arma::cx_vec(Ac._vec()), arma::cx_vec(Bc._vec()))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}

