﻿/* COPYRIGHT
 *
 * file="T_Matrix.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "additionalAsserts.h"

using namespace qengine;
using namespace internal;
class Matrix_Fixture : public ::testing::Test {
	public:
	Matrix_Fixture():one(1.0,1.0),zero(0.0,0.0){};

	complex one;
	complex zero;
};

TEST_F(Matrix_Fixture,constructors){
    std::vector<real>  rvec = {1,2,3,4,5,6};
    std::vector<complex> cvec = {{1.0,1.0},{2.0,2.0},{3.0,3.0},{4.0,4.0},{5.0,5.0},{6.0,6.0}};
	
	Matrix<real> mr32(3, 2); // real 3x2 matrix
	Matrix<real> mr443(4, 4, 3); // real 4x4 matrix filled with 3's
	Matrix<complex> mc46(4, 6); // complex 4x6 matrix
	Matrix<complex> mc3321(3, 3, { 2.0,1.0 }); // complex 3x3 matrix filled with 2+1i

	ASSERT_NO_THROW(Matrix<real>(3,3));
	ASSERT_NO_THROW(Matrix<complex>(3,3));


	ASSERT_NO_THROW(Matrix<real>(3,2));
	ASSERT_NO_THROW(Matrix<complex>(1,2));

	for(auto i=0u; i<mr443.n_rows();++i){
		for(auto j=0u; j<mr443.n_cols();++j){
			ASSERT_EQ(3,mr443.at(i,j));
		}
	}

	for(auto i=0u; i<mc3321.n_rows();++i){
		for(auto j=0u; j<mc3321.n_cols();++j){
			ASSERT_EQ(2.0,mc3321.at(i,j).real());
			ASSERT_EQ(1.0,mc3321.at(i,j).imag());
		}
	}


    Matrix<real> mr23FromDataVec(2,3,rvec.data());
    ASSERT_EQ(1,mr23FromDataVec.at(0,0));
    ASSERT_EQ(2,mr23FromDataVec.at(1,0));
    ASSERT_EQ(3,mr23FromDataVec.at(0,1));
    ASSERT_EQ(4,mr23FromDataVec.at(1,1));
    ASSERT_EQ(5,mr23FromDataVec.at(0,2));
    ASSERT_EQ(6,mr23FromDataVec.at(1,2));

    Matrix<complex> mc32FromDataVec(3,2,cvec.data());
    ASSERT_EQ(1,mc32FromDataVec.at(0,0).real()); ASSERT_EQ(1,mc32FromDataVec.at(0,0).imag());
    ASSERT_EQ(2,mc32FromDataVec.at(1,0).real()); ASSERT_EQ(2,mc32FromDataVec.at(1,0).imag());
    ASSERT_EQ(3,mc32FromDataVec.at(2,0).real()); ASSERT_EQ(3,mc32FromDataVec.at(2,0).imag());
    ASSERT_EQ(4,mc32FromDataVec.at(0,1).real()); ASSERT_EQ(4,mc32FromDataVec.at(0,1).imag());
    ASSERT_EQ(5,mc32FromDataVec.at(1,1).real()); ASSERT_EQ(5,mc32FromDataVec.at(1,1).imag());
    ASSERT_EQ(6,mc32FromDataVec.at(2,1).real()); ASSERT_EQ(6,mc32FromDataVec.at(2,1).imag());

	RVec rVec = RVec{1,2,3};
	Matrix<real> mrFromVec(rVec);
	ASSERT_EQ(3u,mrFromVec.n_rows());
	ASSERT_EQ(1u,mrFromVec.n_cols());
	ASSERT_EQ(1,mrFromVec.at(0,0));
	ASSERT_EQ(2,mrFromVec.at(1,0));
	ASSERT_EQ(3,mrFromVec.at(2,0));

	Matrix<real> mrFromVec2(RVec({1,2,3}));
	ASSERT_EQ(3u,mrFromVec2.n_rows());
	ASSERT_EQ(1u,mrFromVec2.n_cols());
	ASSERT_EQ(1,mrFromVec2.at(0,0));
	ASSERT_EQ(2,mrFromVec2.at(1,0));
	ASSERT_EQ(3,mrFromVec2.at(2,0));

    // todo: make macro asserts for matrices
	std::function<arma::mat()> makeArmaMatR = []()->arma::mat{
		arma::mat a(3,3,arma::fill::ones);
		return a;
	};

	for(auto i=0u;i<Matrix<real>(makeArmaMatR()).n_elem();++i){
		ASSERT_EQ(1,Matrix<real>(makeArmaMatR()).at(i));
	}

    std::function<arma::cx_mat()> makeArmaMatC = []()->arma::cx_mat{
        arma::cx_mat a(3,3,arma::fill::ones);
        arma::cx_double c(2.0,1.0);
        return c*a;
    };

    for(auto i=0u;i<Matrix<complex>(makeArmaMatC()).n_elem();++i){
        ASSERT_EQ(2.0,Matrix<complex>(makeArmaMatC()).at(i).real());
        ASSERT_EQ(1.0,Matrix<complex>(makeArmaMatC()).at(i).imag());
    }


    Matrix<real> mrFromArmaMatrix(mr443._mat());
	for(auto i=0u;i<mrFromArmaMatrix.n_elem();++i){
		ASSERT_EQ(3,mrFromArmaMatrix.at(i));
	}

	Matrix<complex> mcFromArmaMatrix(mc3321._mat());
	for(auto i=0u;i<mcFromArmaMatrix.n_elem();++i){
		ASSERT_EQ(2.0,mcFromArmaMatrix.at(i).real());
		ASSERT_EQ(1.0,mcFromArmaMatrix.at(i).imag());
	}


	//test std::vector<number> ctor
    RMat mrFromStdVec({{2,2},{2,2}});
	ASSERT_EQ(2,mrFromStdVec.at(0,0)); ASSERT_EQ(2,mrFromStdVec.at(0,1));
	ASSERT_EQ(2,mrFromStdVec.at(1,0)); ASSERT_EQ(2,mrFromStdVec.at(1,1));

	CMat mcFromStdVec({{{2.0,1.0},{2.0,1.0}},{{2.0,1.0},{2.0,1.0}}});
	ASSERT_EQ(2,mcFromStdVec.at(0,0).real()); ASSERT_EQ(1,mcFromStdVec.at(0,0).imag()); ASSERT_EQ(2,mcFromStdVec.at(0,1).real()); ASSERT_EQ(1,mcFromStdVec.at(0,1).imag());
	ASSERT_EQ(2,mcFromStdVec.at(1,0).real()); ASSERT_EQ(1,mcFromStdVec.at(1,0).imag()); ASSERT_EQ(2,mcFromStdVec.at(1,1).real()); ASSERT_EQ(1,mcFromStdVec.at(1,1).imag());


    ASSERT_NO_THROW(RMat r(mr32));
    ASSERT_NO_THROW(CMat c(mc46));

    ASSERT_NO_THROW(RMat(Matrix<real>(2,2,1)));
    ASSERT_NO_THROW(CMat(Matrix<complex>(2,2,{2.0,1.0})));


    for(auto i=0u;i<mr443.n_elem();++i){
        ASSERT_EQ(3,mr443.at(i));
    }

    for(auto i=0u;i<mc3321.n_elem();++i){
        ASSERT_EQ(2.0,mc3321.at(i).real());
        ASSERT_EQ(1.0,mc3321.at(i).imag());
    }
}

TEST_F(Matrix_Fixture,accessCols){
	////// real
	RMat rm({{1,2,3},{4,5,6}});
	RVec rv9(2,9); // fill vector of length 2 with 9's
	
	ASSERT_NO_THROW(RVec rvcol0 = rm.col(0));
	ASSERT_NO_THROW(rm.col(0) = rm.col(2));

	// Getting 	
	RVec rvcol0(rm.col(0));
	RVec rvcol1(rm.col(1));
	RVec rvcol2(rm.col(2));
	ASSERT_EQ(rm.at(0,0),rvcol0.at(0));	ASSERT_EQ(rm.at(0,1),rvcol1.at(0)); ASSERT_EQ(rm.at(0,2),rvcol2.at(0));
	ASSERT_EQ(rm.at(1,0),rvcol0.at(1)); ASSERT_EQ(rm.at(1,1),rvcol1.at(1)); ASSERT_EQ(rm.at(1,2),rvcol2.at(1));

	// Setting
	rm.col(0) = rv9;
	rm.col(1) = rv9;
	rm.col(2) = rv9;
	ASSERT_EQ(rm.at(0,0),rv9.at(0));	ASSERT_EQ(rm.at(0,1),rv9.at(0)); ASSERT_EQ(rm.at(0,2),rv9.at(0));
	ASSERT_EQ(rm.at(1,0),rv9.at(1)); ASSERT_EQ(rm.at(1,1),rv9.at(1)); ASSERT_EQ(rm.at(1,2),rv9.at(1));	

	///// complex
	CMat cm({{{1.0,1.0},{2.0,2.0},{3.0,3.0}},{{4.0,4.0},{5.0,5.0},{6.0,6.0}}});
	CVec cv9(2,{9.0,9.0}); // fill vector of length 2 with 9+9i

	ASSERT_NO_THROW(CVec cvcol0(cm.col(0)));
	ASSERT_NO_THROW(cm.col(0) = cm.col(2));
	
	// Getting
	CVec cvcol0(cm.col(0));
	CVec cvcol1(cm.col(1));
	CVec cvcol2(cm.col(2));
	ASSERT_NEAR_C(cm.at(0,0),cvcol0.at(0),1e-16);	ASSERT_NEAR_C(cm.at(0,1),cvcol1.at(0),1e-16);	ASSERT_NEAR_C(cm.at(0,2),cvcol2.at(0),1e-16);		
	ASSERT_NEAR_C(cm.at(1,0),cvcol0.at(1),1e-16);	ASSERT_NEAR_C(cm.at(1,1),cvcol1.at(1),1e-16);	ASSERT_NEAR_C(cm.at(1,2),cvcol2.at(1),1e-16);		

	// Setting 
	cm.col(0) = cv9;
	cm.col(1) = cv9;
	cm.col(2) = cv9;
	ASSERT_NEAR_C(cm.at(0,0),cv9.at(0),1e-16); ASSERT_NEAR_C(cm.at(0,1),cv9.at(0),1e-16); ASSERT_NEAR_C(cm.at(0,2),cv9.at(0),1e-16);
	ASSERT_NEAR_C(cm.at(1,0),cv9.at(1),1e-16); ASSERT_NEAR_C(cm.at(1,1),cv9.at(1),1e-16); ASSERT_NEAR_C(cm.at(1,2),cv9.at(1),1e-16);	
}

TEST_F(Matrix_Fixture,assignment){
    RMat r({{1,1},{1,1}});
    CMat c({{{2.0,1.0},{2.0,1.0}},{{2.0,1.0},{2.0,1.0}}});
    //RMat r2({{2,2,2},{2,2,2}});

    ASSERT_NO_THROW(RMat r2 = r);
    ASSERT_NO_THROW(CMat c2 = c);

    RMat r2 = r;
    CMat c2 = c;

    for(auto i=0u;i<r2.n_elem();++i){
        ASSERT_EQ(r.at(i),r2.at(i));
    }
    for(auto i=0u;i<c2.n_elem();++i){
        ASSERT_NEAR_C(c.at(i),c2.at(i),1e-16);
    }

    ASSERT_NO_THROW(r = std::move(r2));
    ASSERT_NO_THROW(c = std::move(c2));

    auto r3 = std::move(r2);
    auto c3 = std::move(c2);

}

TEST_F(Matrix_Fixture,misc){
	
	Matrix<real> mr32(3,2); // real 3x2 matrix
	Matrix<real> mr443(4,4,3); // real 4x4 matrix filled with 3's
	Matrix<complex> mc46(4,6); // complex 4x6 matrix
	Matrix<complex> mc3321(3,3,{2.0,1.0}); // complex 3x3 matrix filled with 2+1i
	
	ASSERT_EQ(3u,mr32.n_rows());
	ASSERT_EQ(2u,mr32.n_cols());
	ASSERT_EQ(3u*2u,mr32.n_elem());

	ASSERT_EQ(4u,mc46.n_rows());
	ASSERT_EQ(6u,mc46.n_cols());
	ASSERT_EQ(4u*6u,mc46.n_elem());
}

TEST_F(Matrix_Fixture,realAndimag){
	real rdata[9] = {1,2,3,4,5,6,7,8,9};
	RMat rm3(3,3,rdata);

	auto count_val = 1u;
	for(auto j=0u;j<rm3.n_cols();j++){
		for(auto i=0u;i<rm3.n_rows();i++){
			ASSERT_EQ(count_val,rm3.re().at(i,j));
			ASSERT_EQ(0.0,rm3.im().at(i,j));
			count_val++;
		}
	}

	complex cdata[9] = {{1,2},{2,3},{3,4},{4,5},{5,6},{6,7},{7,8},{8,9},{9,10}};
	CMat cm3(3,3,cdata);

	count_val = 1;
	for(auto j=0u;j<cm3.n_cols();j++){
		for(auto i=0u;i<cm3.n_rows();i++){
			ASSERT_EQ(count_val,cm3.re().at(i,j));
			ASSERT_EQ(count_val+1,cm3.im().at(i,j));
			count_val++;
		}
	}
}

TEST_F(Matrix_Fixture, equality)
{
	auto rmat = RMat(RVec{ 1.0,2.0,3.0 });
	auto rmatAsC = CMat(rmat);
	auto cmat = CMat(CVec{ { 1.0, 0.0 },{ 2.0,1.0 },{ 3.0,2.0 } });

	// self-comparison
	ASSERT_TRUE(rmat == rmat);
	ASSERT_TRUE(cmat == cmat);
	ASSERT_TRUE(rmatAsC == rmatAsC);

	ASSERT_FALSE(rmat != rmat);
	ASSERT_FALSE(cmat != cmat);
	ASSERT_FALSE(rmatAsC != rmatAsC);


	// real and complex can be compared, too!
	ASSERT_TRUE(rmat == rmatAsC);
	ASSERT_TRUE(rmatAsC == rmat);

	ASSERT_FALSE(rmat != rmatAsC);
	ASSERT_FALSE(rmatAsC != rmat);


	// value-different matrices
	auto obviouslyDifferentRMat = RMat(RVec{ 2.0,3.0,4.0 });
	ASSERT_TRUE(rmat != obviouslyDifferentRMat);
	ASSERT_TRUE(rmat != cmat);
	ASSERT_TRUE(rmatAsC != cmat);

	ASSERT_FALSE(rmat == obviouslyDifferentRMat);
	ASSERT_FALSE(rmat == cmat);
	ASSERT_FALSE(rmatAsC == cmat);

	// size-different matrices
	auto sizeDifferentRMat = RMat(2, 2, 0.0);
	auto sizeDifferentCMat = CMat(2, 2, 0.0);
	ASSERT_TRUE(rmat != sizeDifferentRMat);
	ASSERT_TRUE(cmat != sizeDifferentCMat);

	ASSERT_FALSE(rmat == sizeDifferentRMat);
	ASSERT_FALSE(cmat == sizeDifferentCMat);

}


TEST_F(Matrix_Fixture,mathoperators){
	Matrix<real> mr22(2, 2, 1); // real 2x2 matrix of ones
	auto mr22original = mr22;
	
	Matrix<complex> mc22(2,2,complex(1,1)); // complex 2x2 matrix of {1,1} entries
	auto mc22original = mc22;

	// addition
	mr22+=mr22;
	ASSERT_EQ(2,mr22.at(0,0));
	ASSERT_EQ(2,mr22.at(0,1));
	ASSERT_EQ(2,mr22.at(1,0));
	ASSERT_EQ(2,mr22.at(1,1));
	mr22=mr22original;


    mc22+=mr22;
    ASSERT_EQ(one+1.0,mc22.at(0,0));
    ASSERT_EQ(one+1.0,mc22.at(1,0));
    ASSERT_EQ(one+1.0,mc22.at(0,1));
    ASSERT_EQ(one+1.0,mc22.at(1,1));
    mc22 = mc22original;


	mc22+=mc22;	
	ASSERT_EQ(2.0*one,mc22.at(0,0));
	ASSERT_EQ(2.0*one,mc22.at(1,0));
	ASSERT_EQ(2.0*one,mc22.at(0,1));
	ASSERT_EQ(2.0*one,mc22.at(1,1));
	mc22 = mc22original;

	// subtraction
	mr22-=mr22;
	ASSERT_EQ(0,mr22.at(0,0));
	ASSERT_EQ(0,mr22.at(0,1));
	ASSERT_EQ(0,mr22.at(1,0));
	ASSERT_EQ(0,mr22.at(1,1));
    mr22 = mr22original;

    mc22-=mr22;
    ASSERT_EQ(one-1.0,mc22.at(0,0));
    ASSERT_EQ(one-1.0,mc22.at(1,0));
    ASSERT_EQ(one-1.0,mc22.at(0,1));
    ASSERT_EQ(one-1.0,mc22.at(1,1));
    mc22 = mc22original;

	mc22-=mc22;	
	ASSERT_EQ(zero,mc22.at(0,0));
	ASSERT_EQ(zero,mc22.at(1,0));
	ASSERT_EQ(zero,mc22.at(0,1));
	ASSERT_EQ(zero,mc22.at(1,1));
	mc22 = mc22original;

	// scalar multiplication
	mr22 *=2.0; 
	ASSERT_EQ(2,mr22.at(0,0));
	ASSERT_EQ(2,mr22.at(0,1));
	ASSERT_EQ(2,mr22.at(1,0));
	ASSERT_EQ(2,mr22.at(1,1));
	mr22 = mr22original;

	mc22 *=2.0;
	ASSERT_EQ(2.0*one,mc22.at(0,0));
	ASSERT_EQ(2.0*one,mc22.at(1,0));
	ASSERT_EQ(2.0*one,mc22.at(0,1));
	ASSERT_EQ(2.0*one,mc22.at(1,1));
	mc22 = mc22original;

	mc22 *=one;
	ASSERT_EQ(complex(0,2),mc22.at(0,0));
	ASSERT_EQ(complex(0,2),mc22.at(1,0));
	ASSERT_EQ(complex(0,2),mc22.at(0,1));
	ASSERT_EQ(complex(0,2),mc22.at(1,1));
	mc22 = mc22original;

	// scalar division
	mr22 /=2.0; 
	ASSERT_EQ(0.5,mr22.at(0,0));
	ASSERT_EQ(0.5,mr22.at(0,1));
	ASSERT_EQ(0.5,mr22.at(1,0));
	ASSERT_EQ(0.5,mr22.at(1,1));
	mr22 = mr22original;

	mc22 /=2.0;
	ASSERT_EQ(0.5*one,mc22.at(0,0));
	ASSERT_EQ(0.5*one,mc22.at(1,0));
	ASSERT_EQ(0.5*one,mc22.at(0,1));
	ASSERT_EQ(0.5*one,mc22.at(1,1));
	mc22 = mc22original;

	mc22 /=one;
	ASSERT_EQ(complex(1,0),mc22.at(0,0));
	ASSERT_EQ(complex(1,0),mc22.at(1,0));
	ASSERT_EQ(complex(1,0),mc22.at(0,1));
	ASSERT_EQ(complex(1,0),mc22.at(1,1));
	mc22 = mc22original;


	// negation
	ASSERT_EQ(-1,-mr22.at(0,0));
	ASSERT_EQ(-1,-mr22.at(0,1));
	ASSERT_EQ(-1,-mr22.at(1,0));
	ASSERT_EQ(-1,-mr22.at(1,1));

	ASSERT_EQ(-1.0*one,-mc22.at(0,0));
	ASSERT_EQ(-1.0*one,-mc22.at(0,1));
	ASSERT_EQ(-1.0*one,-mc22.at(1,0));
	ASSERT_EQ(-1.0*one,-mc22.at(1,1));
}

TEST_F(Matrix_Fixture,freeMatrixMatrixOperations){
	RMat mr22(2,2,1);
	CMat mc22(2,2,one);

	////// addition
	// real+real
	ASSERT_EQ(2,(mr22+mr22).at(0,0));ASSERT_EQ(2,(mr22+mr22).at(0,1));
	ASSERT_EQ(2,(mr22+mr22).at(1,0));ASSERT_EQ(2,(mr22+mr22).at(1,1));

	// complex+real
	ASSERT_EQ(complex(2,1),(mc22+mr22).at(0,0)); ASSERT_EQ(complex(2,1),(mc22+mr22).at(0,1));
	ASSERT_EQ(complex(2,1),(mc22+mr22).at(1,0)); ASSERT_EQ(complex(2,1),(mc22+mr22).at(1,1));

	// real+complex
	ASSERT_EQ(complex(2,1),(mr22+mc22).at(0,0)); ASSERT_EQ(complex(2,1),(mr22+mc22).at(0,1));
	ASSERT_EQ(complex(2,1),(mr22+mc22).at(1,0)); ASSERT_EQ(complex(2,1),(mr22+mc22).at(1,1));

	// comple+complex	
	ASSERT_EQ(complex(2,2),(mc22+mc22).at(0,0)); ASSERT_EQ(complex(2,2),(mc22+mc22).at(0,1));
	ASSERT_EQ(complex(2,2),(mc22+mc22).at(1,0)); ASSERT_EQ(complex(2,2),(mc22+mc22).at(1,1));

	////// subtraction
	// real-real
	ASSERT_EQ(0,(mr22-mr22).at(0,0));ASSERT_EQ(0,(mr22-mr22).at(0,1));
	ASSERT_EQ(0,(mr22-mr22).at(1,0));ASSERT_EQ(0,(mr22-mr22).at(1,1));

	// complex-real
	ASSERT_EQ(complex(0,1),(mc22-mr22).at(0,0)); ASSERT_EQ(complex(0,1),(mc22-mr22).at(0,1));
	ASSERT_EQ(complex(0,1),(mc22-mr22).at(1,0)); ASSERT_EQ(complex(0,1),(mc22-mr22).at(1,1));
	
	// real-complex
	ASSERT_EQ(complex(0,-1),(mr22-mc22).at(0,0)); ASSERT_EQ(complex(0,-1),(mr22-mc22).at(0,1));
	ASSERT_EQ(complex(0,-1),(mr22-mc22).at(1,0)); ASSERT_EQ(complex(0,-1),(mr22-mc22).at(1,1));
	
	// complex+complex	
	ASSERT_EQ(zero,(mc22-mc22).at(0,0)); ASSERT_EQ(zero,(mc22-mc22).at(0,1));
	ASSERT_EQ(zero,(mc22-mc22).at(1,0)); ASSERT_EQ(zero,(mc22-mc22).at(1,1));


	///// multiplication
	// real*real
	ASSERT_EQ(2.0,(mr22*mr22).at(0,0)); ASSERT_EQ(2.0,(mr22*mr22).at(0,1));
	ASSERT_EQ(2.0,(mr22*mr22).at(1,0)); ASSERT_EQ(2.0,(mr22*mr22).at(1,1));

	// real*complex
	ASSERT_EQ(complex(2,2),(mr22*mc22).at(0,0)); ASSERT_EQ(complex(2,2),(mr22*mc22).at(0,1));
	ASSERT_EQ(complex(2,2),(mr22*mc22).at(1,0)); ASSERT_EQ(complex(2,2),(mr22*mc22).at(1,1));

	// complex*real
	ASSERT_EQ(complex(2,2),(mc22*mr22).at(0,0)); ASSERT_EQ(complex(2,2),(mc22*mr22).at(0,1));
	ASSERT_EQ(complex(2,2),(mc22*mr22).at(1,0)); ASSERT_EQ(complex(2,2),(mc22*mr22).at(1,1));

	// complex*complex
	ASSERT_EQ(complex(0,4),(mc22*mc22).at(0,0)); ASSERT_EQ(complex(0,4),(mc22*mc22).at(0,1));
	ASSERT_EQ(complex(0,4),(mc22*mc22).at(1,0)); ASSERT_EQ(complex(0,4),(mc22*mc22).at(1,1));

}

TEST_F(Matrix_Fixture,freeScalarMatrixOperations){
	RMat mr22(2,2,1);
	CMat mc22(2,2,one);

	// ADDITION
	// real matrix + real scalar
	ASSERT_EQ(2,(mr22+1.0).at(0,0)); ASSERT_EQ(2,(mr22+1.0).at(0,1)); 
	ASSERT_EQ(2,(mr22+1.0).at(1,0)); ASSERT_EQ(2,(mr22+1.0).at(1,1)); 
	// real scalar + real matrix
	ASSERT_EQ(2,(1.0+mr22).at(0,0)); ASSERT_EQ(2,(1.0+mr22).at(0,1)); 
	ASSERT_EQ(2,(1.0+mr22).at(1,0)); ASSERT_EQ(2,(1.0+mr22).at(1,1)); 

	// real matrix + complex scalar
	ASSERT_EQ(complex(2,1),(mr22+one).at(0,0));ASSERT_EQ(complex(2,1),(mr22+one).at(0,1));
	ASSERT_EQ(complex(2,1),(mr22+one).at(1,0));ASSERT_EQ(complex(2,1),(mr22+one).at(1,1));

	// complex scalar + real matrix
	ASSERT_EQ(complex(2,1),(one+mr22).at(0,0));ASSERT_EQ(complex(2,1),(one+mr22).at(0,1));
	ASSERT_EQ(complex(2,1),(one+mr22).at(1,0));ASSERT_EQ(complex(2,1),(one+mr22).at(1,1));

	// complex matrix + real scalar
	ASSERT_EQ(complex(2,1),(mc22+1.0).at(0,0));ASSERT_EQ(complex(2,1),(mc22+1.0).at(0,1));
	ASSERT_EQ(complex(2,1),(mc22+1.0).at(1,0));ASSERT_EQ(complex(2,1),(mc22+1.0).at(1,1));

	// real scalar + complex matrix
	ASSERT_EQ(complex(2,1),(1.0+mc22).at(0,0));ASSERT_EQ(complex(2,1),(1.0+mc22).at(0,1));
	ASSERT_EQ(complex(2,1),(1.0+mc22).at(1,0));ASSERT_EQ(complex(2,1),(1.0+mc22).at(1,1));

	// complex matrix + complex scalar
	ASSERT_EQ(complex(2,2),(mc22+one).at(0,0));ASSERT_EQ(complex(2,2),(mc22+one).at(0,1));
	ASSERT_EQ(complex(2,2),(mc22+one).at(1,0));ASSERT_EQ(complex(2,2),(mc22+one).at(1,1));

	// complex scalar + complex matrix
	ASSERT_EQ(complex(2,2),(one+mc22).at(0,0));ASSERT_EQ(complex(2,2),(one+mc22).at(0,1));
	ASSERT_EQ(complex(2,2),(one+mc22).at(1,0));ASSERT_EQ(complex(2,2),(one+mc22).at(1,1));


	/// SUBTRACTION
	// real matrix - real scalar
	ASSERT_EQ(0,(mr22-1.0).at(0,0)); ASSERT_EQ(0,(mr22-1.0).at(0,1)); 
	ASSERT_EQ(0,(mr22-1.0).at(1,0)); ASSERT_EQ(0,(mr22-1.0).at(1,1)); 

	// real matrix - complex scalar
	ASSERT_EQ(complex(0,-1),(mr22-one).at(0,0));ASSERT_EQ(complex(0,-1),(mr22-one).at(0,1));
	ASSERT_EQ(complex(0,-1),(mr22-one).at(1,0));ASSERT_EQ(complex(0,-1),(mr22-one).at(1,1));
	
	// complex matrix - real scalar
	ASSERT_EQ(complex(0,1),(mc22-1.0).at(0,0)); ASSERT_EQ(complex(0,1),(mc22-1.0).at(0,1)); 
	ASSERT_EQ(complex(0,1),(mc22-1.0).at(1,0)); ASSERT_EQ(complex(0,1),(mc22-1.0).at(1,1)); 

	// complex matrix - complex scalar
	ASSERT_EQ(complex(0,0),(mc22-one).at(0,0)); ASSERT_EQ(complex(0,0),(mc22-one).at(0,1)); 
	ASSERT_EQ(complex(0,0),(mc22-one).at(1,0)); ASSERT_EQ(complex(0,0),(mc22-one).at(1,1)); 

	/// ELEMENTWISE MULTIPLICATION
	mr22 = RMat(2,2,2);
	mc22 = CMat(2,2,complex(2.0,2.0));

	// real%real
	ASSERT_EQ(4,(mr22%mr22).at(0,0));ASSERT_EQ(4,(mr22%mr22).at(0,1));
	ASSERT_EQ(4,(mr22%mr22).at(1,0));ASSERT_EQ(4,(mr22%mr22).at(1,1));

	// real%complex
	ASSERT_EQ(complex(4,4),(mr22%mc22).at(0,0));ASSERT_EQ(complex(4,4),(mr22%mc22).at(0,1));
	ASSERT_EQ(complex(4,4),(mr22%mc22).at(1,0));ASSERT_EQ(complex(4,4),(mr22%mc22).at(1,1));

	// complex%real
	ASSERT_EQ(complex(4,4),(mc22%mr22).at(0,0));ASSERT_EQ(complex(4,4),(mc22%mr22).at(0,1));
	ASSERT_EQ(complex(4,4),(mc22%mr22).at(1,0));ASSERT_EQ(complex(4,4),(mc22%mr22).at(1,1));

	// complex%real
	ASSERT_EQ(complex(0,8),(mc22%mc22).at(0,0));ASSERT_EQ(complex(0,8),(mc22%mc22).at(0,1));
	ASSERT_EQ(complex(0,8),(mc22%mc22).at(1,0));ASSERT_EQ(complex(0,8),(mc22%mc22).at(1,1));


	/// SCALAR MULTIPLICATION
	mr22 = RMat(2,2,1);
	mc22 = CMat(2,2,complex(1.0,1.0));
	
	// real scalar * real mat
	ASSERT_EQ(2,(2.0*mr22).at(0,0));ASSERT_EQ(2,(2.0*mr22).at(0,1));
	ASSERT_EQ(2,(2.0*mr22).at(1,0));ASSERT_EQ(2,(2.0*mr22).at(1,1));

	// real scalar * complex mat
	ASSERT_EQ(complex(2,2),(2.0*mc22).at(0,0));ASSERT_EQ(complex(2,2),(2.0*mc22).at(0,1));
	ASSERT_EQ(complex(2,2),(2.0*mc22).at(1,0));ASSERT_EQ(complex(2,2),(2.0*mc22).at(1,1));

	// complex scalar * real mat
	ASSERT_EQ(complex(2,2),(complex(2,2)*mr22).at(0,0));ASSERT_EQ(complex(2,2),(complex(2,2)*mr22).at(0,1));
	ASSERT_EQ(complex(2,2),(complex(2,2)*mr22).at(1,0));ASSERT_EQ(complex(2,2),(complex(2,2)*mr22).at(1,1));

	// complex scalar * complex mat
	ASSERT_EQ(complex(0,4),(complex(2,2)*mc22).at(0,0));ASSERT_EQ(complex(0,4),(complex(2,2)*mc22).at(0,1));
	ASSERT_EQ(complex(0,4),(complex(2,2)*mc22).at(1,0));ASSERT_EQ(complex(0,4),(complex(2,2)*mc22).at(1,1));

	mr22 = RMat(2,2,1);
	mc22 = CMat(2,2,complex(1.0,1.0));
	
	// real mat * real scalar
	ASSERT_EQ(2,(mr22*2.0).at(0,0));ASSERT_EQ(2,(mr22*2.0).at(0,1));
	ASSERT_EQ(2,(mr22*2.0).at(1,0));ASSERT_EQ(2,(mr22*2.0).at(1,1));

	// complex mat * real scalar 
	ASSERT_EQ(complex(2,2),(mc22*2.0).at(0,0));ASSERT_EQ(complex(2,2),(mc22*2.0).at(0,1));
	ASSERT_EQ(complex(2,2),(mc22*2.0).at(1,0));ASSERT_EQ(complex(2,2),(mc22*2.0).at(1,1));

	// real mat * complex scalar
	ASSERT_EQ(complex(2,2),(mr22*complex(2,2)).at(0,0));ASSERT_EQ(complex(2,2),(mr22*complex(2,2)).at(0,1));
	ASSERT_EQ(complex(2,2),(mr22*complex(2,2)).at(1,0));ASSERT_EQ(complex(2,2),(mr22*complex(2,2)).at(1,1));

	// complex scalar * complex mat
	ASSERT_EQ(complex(0,4),(mc22*complex(2,2)).at(0,0));ASSERT_EQ(complex(0,4),(mc22*complex(2,2)).at(0,1));
	ASSERT_EQ(complex(0,4),(mc22*complex(2,2)).at(1,0));ASSERT_EQ(complex(0,4),(mc22*complex(2,2)).at(1,1));

	/// SCALAR DIVISION
	// real mat / real scalar
	ASSERT_EQ(0.5,(mr22/2.0).at(0,0));ASSERT_EQ(0.5,(mr22/2.0).at(0,1));
	ASSERT_EQ(0.5,(mr22/2.0).at(1,0));ASSERT_EQ(0.5,(mr22/2.0).at(1,1));

	// real mat / complex scalar
	ASSERT_EQ(complex(0.25,-0.25),(mr22/complex(2,2)).at(0,0));ASSERT_EQ(complex(0.25,-0.25),(mr22/complex(2,2)).at(0,1));
	ASSERT_EQ(complex(0.25,-0.25),(mr22/complex(2,2)).at(1,0));ASSERT_EQ(complex(0.25,-0.25),(mr22/complex(2,2)).at(1,1));

	// complx mat / real scalar
	ASSERT_EQ(complex(0.5,0.5),(mc22/2.0).at(0,0));ASSERT_EQ(complex(0.5,0.5),(mc22/2.0).at(0,1));
	ASSERT_EQ(complex(0.5,0.5),(mc22/2.0).at(1,0));ASSERT_EQ(complex(0.5,0.5),(mc22/2.0).at(1,1));

	// complex mat/complex scalar
	ASSERT_EQ(complex(0.5,0),(mc22/complex(2,2)).at(0,0));ASSERT_EQ(complex(0.5,0),(mc22/complex(2,2)).at(0,1));
	ASSERT_EQ(complex(0.5,0),(mc22/complex(2,2)).at(1,0));ASSERT_EQ(complex(0.5,0),(mc22/complex(2,2)).at(1,1));
}

TEST_F(Matrix_Fixture, kron)
{
	const auto n_rows = 2;
	const auto n_cols = 3;

	const auto Ar = Matrix<real>(arma::randn(n_rows, n_cols));
	const auto Br = Matrix<real>(arma::randn(n_rows, n_cols));

	const auto Ac = Matrix<complex>(arma::cx_mat(arma::randn(n_rows, n_cols), arma::randn(n_rows, n_cols)));
	const auto Bc = Matrix<complex>(arma::cx_mat(arma::randn(n_rows, n_cols), arma::randn(n_rows, n_cols)));

	const auto Crr = Matrix<real>(arma::mat(arma::kron(arma::mat(Ar._mat()), arma::mat(Br._mat()))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = Matrix<complex>((arma::kron(arma::mat(Ar._mat()), arma::cx_mat(Bc._mat()))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = Matrix<complex>((arma::kron(arma::cx_mat(Ac._mat()), arma::mat(Br._mat()))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = Matrix<complex>((arma::kron(arma::cx_mat(Ac._mat()), arma::cx_mat(Bc._mat()))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}

