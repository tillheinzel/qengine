﻿/* COPYRIGHT
 *
 * file="T_SqHermitianBandMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"
#include "src/Utility/LinearAlgebra/SqDenseMat.h"

#include "additionalAsserts.h"
#include "HarmonicAnalyticResults.h"

#include "SqMat_Fixture.h"

using namespace qengine;
using namespace internal;

class SqHermitianBandMat_Fixture : public SqMat_Fixture
{
	SqHermitianBandMat<real> makeHBandMat(unsigned dimension) const
	{
		auto a = 30.0 / 12.0;
		auto b = -16.0 / 12.0;
		auto c = 1.0 / 12.0;

		auto scale = 0.5 / (dx*dx);

		auto kinetic = arma::mat(3, dimension, arma::fill::zeros);
		kinetic.row(0) += a*scale;
		kinetic.row(1) += b*scale;
		kinetic.row(2) += c*scale;

		arma::vec potential = 0.5*omega*omega*(xArray._vec() - x0) % (xArray._vec() - x0);
		auto Hmat = kinetic;
		Hmat.row(0) += potential.t();
		return SqHermitianBandMat<real>(Matrix<real>(Hmat), SqHermitianBandMat<real>::ROW_LAYOUT);
	}
public:
	// todo: (maybe) make this into macro instead and use template
	std::function<void(const Matrix<real>&, const Matrix<real>&)> ASSERT_EQ_MAT = [&](const Matrix<real>& from, const Matrix<real>& to) 
	{
		ASSERT_EQ(from.n_cols(), to.n_cols());
		ASSERT_EQ(from.n_rows(), to.n_rows());

		for (auto j = 0u; j < from.n_cols(); ++j) 
		{
			for (auto i = 0u; i < from.n_rows(); ++i) 
			{
				ASSERT_EQ(from._mat().at(i, j), to._mat().at(i, j));
			}
		}
	};
	
	SqHermitianBandMat<real> H{ makeHBandMat(defaultDim) };
};

TEST_F(SqHermitianBandMat_Fixture, constructors)
{
	count_t dim2 = 6;
	count_t bandwidth = 2;

	RMat m(dim2, bandwidth + 1);
	m.col(0) = RVec(dim2, 1);
	m.col(1) = RVec(dim2, 2);
	m.col(2) = RVec(dim2, 3);
	m.at(m.n_rows() - 1, 1) = 0;
	m.at(m.n_rows() - 1, 2) = 0;
	m.at(m.n_rows() - 2, 2) = 0;

	ASSERT_NO_THROW(SqHermitianBandMat<real> sqhbm(10, 3));
	ASSERT_NO_THROW(SqHermitianBandMat<real> sqhbm(m));

	SqHermitianBandMat<real> sqhbm1(10, 3);
	SqHermitianBandMat<real> sqhbm2(m);

	ASSERT_EQ_MAT(m, Matrix<real>(sqhbm2.bands()._mat().st()));

	SqHermitianBandMat<real> mToMove(m);
	SqHermitianBandMat<real> mToMove2(m);
	const auto makeRMat = [m]()->RMat {return m; };


	SqHermitianBandMat<real> sqhbm31(std::move(mToMove)); // test 1, move existing matrix

	ASSERT_EQ_MAT(mToMove2.bands(), sqhbm31.bands());

	SqHermitianBandMat<real> sqhbm32(makeRMat()); // test 2, move temporary returned matrix
	const RMat rmat(makeRMat());

	ASSERT_EQ_MAT(rmat, RMat(sqhbm32.bands()._mat().st()));


	// copy constructor
	SqHermitianBandMat<real> sqhbm41(sqhbm1);
	std::function<SqHermitianBandMat<real>()> makeSqHermBandMat = []()->SqHermitianBandMat<real> {return SqHermitianBandMat<real>(6, 2); };
	SqHermitianBandMat<real> sqhbm42(makeSqHermBandMat());


}

TEST_F(SqHermitianBandMat_Fixture, assignment) {
	RMat m(6, 2, 3);
	m.at(m.n_rows() - 1, 1) = 0;

	SqHermitianBandMat<real> sqhbm1(m);

	SqHermitianBandMat<real> sqhbm2 = sqhbm1;
	ASSERT_EQ_MAT(sqhbm1.bands(), sqhbm2.bands());

	sqhbm2 = std::move(sqhbm1);
	ASSERT_EQ_MAT(m, RMat(sqhbm2.bands()._mat().st()));
}

TEST_F(SqHermitianBandMat_Fixture, misc) {
	SqHermitianBandMat<real> sqhbm(10, 3);

	EXPECT_EQ(10u, sqhbm.dim());
	EXPECT_EQ(3u, sqhbm.bandwidth());

}

TEST_F(SqHermitianBandMat_Fixture, multiplyInPlace) {
	int size = 100;
	double value = 2.5;

	CVec v(size, { 1,1 });
	SqHermitianBandMat<real> sqhbm1(RMat(size, size, value));

	// std::cout << v << std::endl;
	// std::cout << sqhbm1.bands() <<std::endl;
	sqhbm1.multiplyInPlace(v);
	std::complex<double> entries(size*value, size*value);


	for (auto i = 0u; i < v.size(); i++) 
	{
		ASSERT_NEAR_C(entries, v.at(i), 1e-10);
	}

	// std::cout << v << std::endl;

}

TEST_F(SqHermitianBandMat_Fixture, toDenseMat) {
	auto size = 10;
	CVec v(size, { 1,1 });

	auto value = 2.5;
	auto M = SqHermitianBandMat<real>(RMat(size, 3, value));
	auto mv = M*v;


	auto M_dense = static_cast<SqDenseMat<real>>(M);
	auto m_dv = M_dense*v;

	ASSERT_NEAR_V(mv, m_dv, 1e-16);
}

TEST_F(SqHermitianBandMat_Fixture, exp) 
{
	auto size = 10;
	CVec v(size, { 1,1 });

	auto value = 2.5;
	auto M = SqHermitianBandMat<real>(RMat(size, 3, value));
	auto mv = exp(M)*v;


	auto M_dense = static_cast<SqDenseMat<real>>(M);
	auto m_dv = exp(M_dense)*v;

	ASSERT_NEAR_V(mv, m_dv, 1e-16);
}

TEST_F(SqHermitianBandMat_Fixture, eigHerm)
{
	count_t largestEigenstate = 2;

	auto spectrum = getSpectrum_herm(H, largestEigenstate);

	auto vals = spectrum.eigenvalues();
	auto vecs = spectrum.eigenvectors();

	EXPECT_EQ(largestEigenstate + 1, vals.size());
	EXPECT_EQ(largestEigenstate + 1, vecs.n_cols());

	arma::vec expectedvals = omega*(0.5 + arma::linspace(0, static_cast<double>(largestEigenstate), largestEigenstate + 1));

	for (auto i = 0u; i <= largestEigenstate; ++i)
	{
		EXPECT_NEAR(expectedvals.at(i), vals.at(i), 5e-6) << "x val" << i;

		const auto state = CVec(vecs.col(i));
		EXPECT_NEAR(vals.at(i), std::real(dot(state, H*state)), 1e-7) << "x vec" << i;
	}
}

TEST_F(SqHermitianBandMat_Fixture, kron)
{
	const auto dim = 20;
	const auto bandwidth = 3;

	const auto A = SqHermitianBandMat<real>(Matrix<real>(arma::randn(dim, bandwidth)));
	const auto B = SqHermitianBandMat<real>(Matrix<real>(arma::randn(dim, bandwidth)));

	const auto C = kron(SqDenseMat<real>(A), SqDenseMat<real>(B));

	EXPECT_EQ(C, SqDenseMat<real>(kron(A,B)));
}
