﻿/* COPYRIGHT
 *
 * file="T_SqMatCasts.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "additionalAsserts.h"

#include "SqMat_Fixture.h"


using namespace qengine::internal;

class SqMatCasts_Fixture: public SqMat_Fixture
{
	
};

TEST_F(SqMatCasts_Fixture, diagToSparse)
{
	const auto diagMat = SqDiagMat<real>(xArray);

	const auto sparseMat = static_cast<SqSparseMat<real>>(diagMat);

	for(auto i = 0ull; i<defaultDim; ++i)
	{
		for (auto j = 0ull; j<defaultDim; ++j)
		{
			if(i==j)
			{
				ASSERT_EQ(diagMat.diag().at(i), sparseMat.mat().at(i, j)) << "diagpos: " << i;
			}
			else
			{
				ASSERT_EQ(0, sparseMat.mat().at(i, j));
			}
		}
	}
}
