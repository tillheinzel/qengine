﻿/* COPYRIGHT
 *
 * file="T_SumProblem.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <qengine/OptimalControl.h>

using namespace qengine;


class DummyPhysicsProblem
{
public:
	DummyPhysicsProblem(const count_t controlsize, const count_t paramCount, const real dt):controlsize_(controlsize), paramCount_(paramCount), dt_(dt){}

	void update(const Control&){}
	void _update(count_t, const RVec&) {}

	real cost() const { return 0.0; }
	real _cost(count_t) const { return 0.0; }

	Control gradient() const { return Control::zeros(controlsize_,paramCount_, dt_); }
	RVec _gradient(count_t) const { return RVec(paramCount_, 0.0); }

	real fidelity() const { return 1.2; } //boom! magic fidelity

	Control _params() const { return Control{}; }
	Control control() const { return Control{}; }

private:
	count_t controlsize_;
	count_t paramCount_;
	real dt_;
};



class SumProblem_Fixture : public ::testing::Test
{
public:
	count_t controllength = 21;
	real dt = 0.1;

	Control makeLinearControl(count_t paramCount, real multiplier) const
	{

		auto x0_mat = arma::mat(paramCount, controllength);
		for (auto i = 0u; i < paramCount; ++i) 
		{
			x0_mat.row(i) = arma::linspace(i, multiplier*(i+1), controllength).t();
		}
		return Control(RMat(x0_mat), dt);

	}

	Control makeParabolicControl(count_t paramCount = 1) const
	{

		auto x0_mat = arma::mat(paramCount, controllength);
		for (auto i = 0u; i < paramCount; ++i)
		{
			x0_mat.row(i) = arma::pow(arma::linspace(i,  i + 1, controllength).t(),2);
		}
		return Control(RMat(x0_mat), dt);

	}


};

TEST_F(SumProblem_Fixture, cost)
{
	auto control = makeLinearControl(1, 2.0);

	auto problem= DummyPhysicsProblem(controllength,1, dt) + Regularization(control, 1.0);

	/// expected regularization-cost for linear control = multiplier^2/(N-1)
	EXPECT_NEAR(problem._right().cost(), problem.cost(), 1e-16);

	control = makeLinearControl(1, 3.0);
	problem.update(control);

	EXPECT_NEAR(problem._right().cost(), problem.cost(), 1e-16);
}

TEST_F(SumProblem_Fixture, gradient)
{
	auto control = makeLinearControl(1, 2.0);

	auto problem = DummyPhysicsProblem(controllength,1, dt) + Regularization(control, 1.0);

	auto gradient = problem.gradient();
	for(auto i = 0u; i<control.size();++i)
	{
		ASSERT_NEAR(0.0, gradient.at(i).at(0), 1e-7);
	}

	control = makeParabolicControl(1);
	problem.update(control);
	auto regGradient = problem._right().gradient();
	gradient = problem.gradient();

	for (auto i = 1u; i<control.size()-1; ++i)
	{
		ASSERT_NEAR(regGradient.at(i).at(0), gradient.at(i).at(0), 1e-7);
	}
}

TEST_F(SumProblem_Fixture, indexedCost)
{
	auto control = makeLinearControl(1, 2.0);

	auto problem = DummyPhysicsProblem(controllength, 1, dt) + Regularization(control, 1.0);

	for (auto i = 0u; i < control.size(); ++i) 
	{
		ASSERT_NEAR(problem._right()._cost(0), problem._cost(0), 1e-16);
	}
}

TEST_F(SumProblem_Fixture, indexedGradient)
{
	auto control = makeParabolicControl(1);

	auto problem = DummyPhysicsProblem(controllength, 1, dt) + Regularization(control, 1.0);

	EXPECT_NEAR(0.0, problem._gradient(0).at(0), 1e-16);
	EXPECT_NEAR(0.0, problem._gradient(controllength-1).at(0), 1e-16);

	for (auto i = 1u; i < control.size()-1; ++i)
	{
		ASSERT_NEAR(problem._right()._gradient(i).at(0), problem._gradient(i).at(0), 1e-15);
	}
}


TEST_F(SumProblem_Fixture, doubleSum)
{
	controllength = 10001; // required for accurate calculation wrt. boundary

	RVec lowerleft{ -1.0 };
	RVec upperright{ 1.0 };


	auto control = makeLinearControl(1, 1.0);
	auto problem = DummyPhysicsProblem(controllength, 1, dt) + 1.0*Regularization(control)+1.0*Boundaries(control, lowerleft, upperright);

	/// pure regularization-cost: control is within bounds
	EXPECT_NEAR(problem._left()._right().cost(), problem.cost(), 1e-16);

	/// control leaves bounds: added bound-cost.
	control = makeLinearControl(1, 2.0);
	problem.update(control);
	EXPECT_NEAR(problem._left()._right().cost() + problem._right().cost(), problem.cost(), 6e-5);
}
