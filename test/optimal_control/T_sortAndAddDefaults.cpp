﻿/* COPYRIGHT
 *
 * file="T_sortAndAddDefaults.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/Specifics/Algorithms/Policies/PolicyWrapper.h"
#include "src/Specifics/Algorithms/Policies/Stoppers.h"
#include "src/Specifics/Algorithms/Policies/Collectors.h"
#include "src/Specifics/Algorithms/Policies/StepsizeFinders.h"
#include "src/Specifics/Algorithms/Policies/BfgsRestarters.h"


#include "src/Specifics/Algorithms/sortAndAddDefaults.h"

using namespace qengine;

class SortAndAddDefaults_Fixture: public ::testing::Test{};

TEST_F(SortAndAddDefaults_Fixture, orderMapAccess)
{
	auto orderMap = internal::makeTypeMap(internal::TypeList<internal::policy::Stopper, internal::policy::Collector>{}, std::make_tuple(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); })));

	ASSERT_EQ(std::string("defaultStopper"), orderMap.template get<internal::policy::Stopper>().f());
	ASSERT_EQ(std::string("defaultCollector"), orderMap.template get<internal::policy::Collector>().f());
}

TEST_F(SortAndAddDefaults_Fixture, wrappersToTypeMap)
{
	auto map = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	ASSERT_EQ(std::string("defaultStopper"), map.template get<internal::policy::Stopper>()());
	ASSERT_EQ(std::string("defaultCollector"), map.template get<internal::policy::Collector>()());
}

TEST_F(SortAndAddDefaults_Fixture, areInputIDsValid)
{
	auto orderMap = internal::makeTypeMap(internal::TypeList<internal::policy::Stopper, internal::policy::Collector>{}, std::make_tuple(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); })));

	auto map = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	static_assert(internal::areInputIDsValid(internal::getKeys(internal::Type<decltype(orderMap)>()), internal::getKeys(internal::Type<decltype(map)>())), "");
}

//TEST_F(SortAndAddDefaults_Fixture, extractOrDefaultPolicy)
//{
//	auto orderMap = internal::makeTypeMap(internal::TypeList<internal::policy::Stopper, internal::policy::Collector>{}, std::make_tuple(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); })));
//
//	auto inputs = std::make_tuple(makeCollector([]() {return std::string("NotDefaultCollector"); }));
//
//	auto res1 = internal::extractOrDefaultPolicy<internal::policy::Stopper>(orderMap, inputs, internal::Index<1>());
//	
//	auto stopper = std::get<0>(res1);
//
//	ASSERT_EQ(std::string("defaultStopper"), stopper());
//
//	auto res2 = internal::extractOrDefaultPolicy<internal::policy::Collector>(orderMap, inputs, internal::Index<1>());
//
//	auto collector = std::get<0>(res2);
//
//	ASSERT_EQ(std::string("NotDefaultCollector"), collector());
//}

TEST_F(SortAndAddDefaults_Fixture, sortAndAddDefaults_impl)
{
	auto orderMap = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	auto inputs = internal::toTypeMap(makeCollector([]() {return std::string("NotDefaultCollector"); }));

	auto sorted = internal::sortAndAddDefaults_impl<internal::policy::Stopper, internal::policy::Collector>(orderMap, inputs);
	
	ASSERT_EQ(std::string("defaultStopper"), std::get<0>(sorted)());
	ASSERT_EQ(std::string("NotDefaultCollector"), std::get<1>(sorted)());
}

TEST_F(SortAndAddDefaults_Fixture, sortAndAddDefaults)
{
	auto orderMap = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	auto inputs = internal::toTypeMap(makeCollector([]() {return std::string("NotDefaultCollector"); }));

	auto sorted = internal::sortAndAddDefaults(orderMap, inputs);
	
	ASSERT_EQ(std::string("defaultStopper"), std::get<0>(sorted)());
	ASSERT_EQ(std::string("NotDefaultCollector"), std::get<1>(sorted)());
}
