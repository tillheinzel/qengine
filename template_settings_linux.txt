# Some settings and paths to required and optional dependencies. 
# If paths are not absolute paths (or do not exist), the build-system will throw a warning.

set(CREATE_TESTS true)
set(CREATE_EXAMPLE_PROJECTS true) # set to true if you want to build the example projects. 
set(ENABLE_MAT_IO false)
set(DISABLE_DEBUG_CHECKS false)

################## MKL ##################
# This is the default installation directory for mkl. Set them if you have installed mkl to a different directory
# set(MKL_INCLUDE_DIR "/opt/intel/compilers_and_libraries/linux/mkl/include") 
# set(MKL_LIB_DIR "/opt/intel/compilers_and_libraries/linux/mkl/lib/intel64_lin")  

################## MATIO ##################
# only needed if ENABLE_MAT_IO is true
# set(MATIO_INCLUDE_DIR "...")
# set(MATIO_LIB_DIR "...")

################## ARMADILLO & JSON ##################
# set these if you do not want to use the submodules. Note that we can't guarantee it will work with versions other than these,

# set(ARMADILLO_INCLUDE_DIR ...)
# set(JSON_INCLUDE_DIR ...)